@include("commons.header")

@if (Session::has("user"))

    @if (Session::get("user")->groupId == 1)
        @include("commons.menu")
    @endif

    @if (Session::get("user")->groupId == 2)
        @include("commons.menu")
    @endif

    @if (Session::get("user")->groupId == 3)
        @include("commons.partner-menu")
    @endif

    @include("commons.topbar")

@endif
@yield("content")

@include("commons.footer")

@include("commons.popup")


{{--@include("errors.errors-popup")--}}

{{--@include("debug.debug-popup")--}}

