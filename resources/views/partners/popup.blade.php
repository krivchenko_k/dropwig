<div class="popup-content login-page-container col-xs-12">
    <div class="row">
        <form class="edit-partner-form registration-form form-horizontal login-form">
            <div class="col-xs-6">
                <div class="form-group">
                    <label class="" for="partner-email">E-mail</label>
                    <div class="">
                        <input class="partner-email form-control" type="email" name="email" id="partner-email"
                               placeholder="email"
                               value="{{ $partner->profile->email or ""}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="" for="partner-login">Логин</label>
                    <div class="">
                        <input class="partner-login form-control" id="partner-login" type="text" name="login"
                               placeholder="Логин" minlength="5" required value="{{ $partner->login or ""}}"
                               @if (isset($partner)) readonly @endif>
                    </div>
                </div>
                <div class="form-group">
                    @if (!isset($partner))
                        <label class="partner-password-label" for="partner-password">Пароль</label>
                        <input type="password" name="password" class="partner-password form-control" id="partner-password" placeholder="Пароль"
                               value="{{ $partner->password or "" }}">
                    @else
                        <label class="partner-password-label" for="partner-password" style="display: none">Пароль</label>
                        <input type="password" name="password" class="partner-password form-control" id="partner-password" placeholder="Новый пароль" style="display: none">
                        <center><span class="change-password-btn">Изменить пароль</span></center>
                    @endif
                </div>

                {{--@if (!isset($partner))--}}
                    {{--<div class="form-group">--}}
                        {{--<label class="">Пароль</label>--}}
                        {{--<div class="">--}}
                            {{--<input class="partner-password form-control" type="text" name="password"--}}
                                   {{--id="partner-reg-date"--}}
                                   {{--readonly--}}
                                   {{--placeholder="Фамилия" minlength="2" required--}}
                                   {{--value="{{ date("d.m.Y (H:i)", strtotime($partner->profile->created_at))}}">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--@endif--}}
                <div class="form-group">
                    <label class="" for="partner-nickname">Ник</label>
                        <input class="partner-nickname form-control" type="text" name="nickname" id="partner-nickname"
                               placeholder="Никнейм" value="{{ $partner->profile->nickname or ""}}">
                </div>
                <label for="partner-nickname">Доп.контакты</label>
                <div class="form-group">
                        <input class="partner-contacts form-control" type="text" name="contacts" id="partner-contacts"
                               value="{{ $partner->profile->contacts or ""}}">
                </div>
                @if (isset($partner))
                    <div class="form-group">
                        <label class="" for="partner-reg-date">Дата регистрации</label>
                        <div class="">
                            <input class="partner-reg-date form-control" type="text" name="regDate"
                                   id="partner-reg-date"
                                   readonly
                                   placeholder="Фамилия" minlength="2" required
                                   value="{{ date("d.m.Y (H:i)", strtotime($partner->profile->created_at))}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="" for="partner-last-activity">Последняя активность</label>
                        <div class="">
                            <input class="partner-last-activity form-control" type="text" name="lastActivity"
                                   id="partner-last-activity" readonly
                                   placeholder="Фамилия" minlength="2" required
                                   value="{{ date("d.m.Y (H:i)", strtotime($partner->profile->updated_at))}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>IP (Город) при регистрации</label>
                        <div>
                            <input class="partner-last-activity form-control" type="text" name="lastActivity"
                                   id="partner-last-activity" readonly
                                   value="{{ $partner->profile->ip }} ({{ $partner->profile->cityByIp}})">
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    <label class="" for="partner-name">Имя</label>
                    <div class="">
                        <input class="partner-name form-control" type="text" name="firstName" id="partner-name"
                               placeholder="Имя"
                               minlength="2" required value="{{ $partner->profile->firstName or ""}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="" for="partner-surname">Фамилия</label>
                    <div class="">
                        <input class="partner-surname form-control" type="text" name="lastName" id="partner-surname"
                               placeholder="Фамилия" minlength="2" required
                               value="{{ $partner->profile->lastName or ""}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="" for="partner-phone">Телефон</label>
                    <div class="">
                        <input class="partner-phone form-control" type="text" name="phone" id="partner-phone"
                               placeholder="Телефон"
                               value="{{ $partner->profile->phone or ""}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="" for="partner-card">Номер карты</label>
                    <div class="">
                        <input class="partner-card form-control" type="text" name="card" id="partner-card"
                               placeholder="Номер кредитной карты" value="{{ $partner->profile->card or ""}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="" for="partner-card-owner">Владелец карты</label>
                    <div class="">

                        <input class="partner-card-owner form-control" type="text" name="cardOwner"
                               id="partner-card-owner"
                               placeholder="Имя на кого оформлена карта"
                               value="{{ $partner->profile->cardOwner or "" }}" @if (isset($partner)) {{ ($partner->profile->isCardOwnerSelf == 1) ? 'readonly' : '' }} @endif>

                    </div>
                </div>


                <div class="form-group">
                    <div class="">
                        <label for="partner-card-owner-self">
                            <input class="cbr partner-card-owner-self" style="width: 13px" name="isCardOwnerSelf"
                                   id="partner-card-owner-self"
                                   type="checkbox" @if (isset($partner)) {{ ($partner->profile->isCardOwnerSelf == 1) ? 'checked' : '' }} @endif>
                            Партнёр является владельцем карты</label>
                    </div>
                </div>

            </div>
            @if(isset($statuses))
                <div class="col-xs-6 status-container">
                    <label for="partner-status">Статус</label>
                    <select class="partner-status form-control" name="statusId" id="partner-status">
                        @if (isset($partner))
                            <option value="{{ $partner->statusId }}">{{ $partner->status->name }}</option>
                            <option disabled>-----------------</option>
                        @endif
                        @foreach($statuses as $status)
                            <option value="{{ $status->id }}">{{ $status->name }}</option>
                        @endforeach
                    </select>
                </div>
            @endif

        </form>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <button data-partner-id="{{ $partner->id or ""}}"
                    class="btn btn-secondary btn-sm btn-icon icon-left flex-item-center btn-save">
                Сохранить
            </button>
        </div>
        <div class="col-xs-6">
            <button href="#" class=" btn btn-gray btn-sm btn-icon icon-left flex-item-center btn-close-popup">
                Отмена
            </button>
        </div>
    </div>
</div>
