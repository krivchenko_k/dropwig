@extends("app")

@section("content")

    {{ Html::script("js/partners/partners.js") }}
    {{ Html::style("css/partners/partners.css") }}

    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Партнёры</h1>
        </div>

        <div class="breadcrumb-env">
            <button class="btn btn-secondary btn-icon add-partner-btn">
                <i class="fa-plus"></i>
                <span>Добавить</span>
            </button>

            <button class="btn btn-danger btn-icon common-delete-btn delete-partners-btn">
                <i class="fa fa-trash"></i>
                <span>Удалить</span>
                (<span class="deleted-row-count"></span>)
            </button>
        </div>
    </div>

    <div class="partners-page-container">
        <table class="table table-model-2 table-hover partners-table">
            <thead>
            <tr>
                <th class="no-sorting cbx-th">
                    <input type="checkbox" class="cbr no-sort select-all-cbx select-all-partners-cbx">
                </th>
                <th class="id-th">
                    ID
                </th>
                <th class="btn-th no-sorting">
                </th>
                <th>
                    ФИО
                </th>
                <th>
                    Ник
                </th>
                <th class="num-th">
                    Заказов
                </th>
                <th class="num-th">
                    На балансе
                </th>
                <th class="num-th">
                    Выплачено
                </th>
                <th>
                    Зарегистрирован
                </th>
                <th>
                    Был активен
                </th>
            </tr>
            </thead>
            <tbody class="middle-align">
            @include("partners.tableBody")
            </tbody>
        </table>
    </div>
@stop


