@if(isset($partners))
    @foreach($partners as $partner)
        <tr data-partner-id="{{ $partner->id }}">
            <td class="cbx-td">
                <input type="checkbox" class="cbr cbx">
            </td>
            <td class="id-td">
                {{ $partner->id }}
            </td>
            <td class="btn-td">
                <button class="btn btn-edit btn-secondary btn-xs">
                    <i class="fa-pencil"></i>
                </button>
            </td>
            <td class="dblclickable">
                {{ $partner->profile->firstName." ".$partner->profile->lastName }}
            </td>
            <td class="dblclickable">
                {{ $partner->profile->nickname }}
            </td>
            <td class="num-td">
                {{ $partner->order->count() }}
            </td>
            <td class="num-td">
                {{ ceil($partner->balance->amount) }}
            </td>
            <td class="num-td">
                {{ $partner->payment->sum("price") }}
            </td>
            <td>
                {{ date("d.m.Y", strtotime($partner->profile->created_at)) }}
            </td>
            <td>
                {{ date("d.m.Y", strtotime($partner->profile->lastActivity)) }}
            </td>
        </tr>
    @endforeach
@endif