<div class="popup-content">
    <div class="error-block"></div>
    <div>
        <select class="form-control sel-decline-reason">
            @if(isset($rejectionReasons))
                @foreach($rejectionReasons as $rejectionReason)
                    <option value="{{ $rejectionReason->id }}">{{ $rejectionReason->text }}</option>
                @endforeach
            @endif
            <option value="-1">Другая причина...</option>
        </select>
        <textarea class="decline-reason" rows="5" placeholder="Введите причину..."></textarea>
    </div>
    <div class="buttons-container">
        <button href="#" data-order-id="{{ $orderId }}" class="btn btn-secondary btn-sm btn-icon icon-left flex-item-center btn-decline">Отказать</button>
        <button href="#"  class=" btn btn-gray btn-sm btn-icon icon-left flex-item-center btn-close">Отмена</button>
    </div>
</div>

{{ Html::script('/js/moderationRequests/decline-popup.js') }}
