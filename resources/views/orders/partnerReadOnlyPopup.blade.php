<div class="row">
    <div class="col-xs-3">
        <div>
            <label class="control-label">Фамилия</label>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Фамилия покупателя" id="client-last-name"
                       value="{{ $order->client->lastName or '' }}" readonly>
            </div>
        </div>
        <div>
            <label class="control-label">Имя</label>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Имя покупателя" id="client-first-name"
                       value="{{ $order->client->firstName or '' }}" readonly>
            </div>
        </div>
        <div>
            <label class="control-label">Отчество <small>(не обязательно)</small></label>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Отчество покупателя" id="client-middle-name"
                       value="{{ $order->client->middleName or '' }}" readonly>
            </div>
        </div>

        <div>
            <label class="control-label">Телефон</label>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Телефон покупателя" id="order-phone"
                       value="{{ $order->phone or '' }}" readonly>
            </div>
        </div>

        <div>
            <label class="control-label">Статус заказа</label>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Статус заказа" id="order-phone"
                       value="{{ $order->status->name or ""}}" readonly>
            </div>
        </div>

        <div>
            <label class="control-label">Статус оплаты</label>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Статус заказа" id="order-phone"
                       value="{{ $order->paymentType->name or ""}}" readonly>
            </div>
        </div>
        <div>
            <label class="control-label">Способ доставки</label>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Статус заказа" id="order-phone"
                       value="{{ $order->deliveryParams->deliveryService->name or ""}}" readonly>
            </div>
        </div>
    </div>
    <div class="col-xs-9 pl-0">
        <label class="control-label">&nbsp;</label>
        <div class="goods-table-holder">
            <table class="table table-model-2 table-hover order-goods-table stat-data-1 no-footer">
                <thead>
                <tr role="row">
                    <th>№</th>
                    <th><i class="fa fa-question-circle"></i></th>
                    <th class="goods-name-th">
                        Товар
                    </th>
                    <th>
                        По дропу
                    </th>
                    <th>
                        Наценка
                    </th>
                    <th>
                        Стоимость
                    </th>
                    <th>
                        Кол-во
                    </th>
                    <th>
                        Итого
                    </th>
                    <th>
                        Прибыль
                    </th>
                </tr>
                </thead>
                <tbody class="middle-align">
                @if(isset($order->goodsInOrder))
                    <?php $counter = 1; ?>
                    @foreach($order->goodsInOrder as $goodsInOrder)
                        @if (isset($goodsInOrder->goods))

                            <tr class="goods-table-row @if ($counter%2 == 0) even @endif"
                                data-row-number="{{ $goodsInOrder->id }}"
                                data-category-id="{{ $goodsInOrder->goods->categoryId }}"
                                data-goods-id="{{ $goodsInOrder->goods->id }}">
                                <td class="goods-number">{{ $counter++ }}</td>
                                <td class="btn-td">
                                    <button class="btn btn-edit goods-description"
                                            data-content="{{ $goodsInOrder->goods->params->largeDescription }}">
                                        <i class="fa fa-question-circle"></i>
                                    </button>
                                </td>
                                <td class="goods-name">
                                    {{ $goodsInOrder->goods->name }}
                                </td>
                                <td class="goods-drop-price">
                                    {{ number_format($goodsInOrder->goodsPrice, 2) }}
                                </td>
                                <td>
                                    <input type="number" class="table-input goods-markup form-control" maxlength="6"
                                           min="0"
                                           value="{{ $goodsInOrder->markup }}" readonly>
                                </td>
                                <td class="goods-price">
                                    {{ number_format($goodsInOrder->goodsPrice + $goodsInOrder->markup, 2) }}
                                </td>
                                <td>
                                    <input type="number" class="table-input goods-quantity form-control" maxlength="6"
                                           min="0"
                                           value="{{ $goodsInOrder->quantity }}" readonly>
                                </td>
                                <td class="total-goods-price">
                                    {{ number_format(($goodsInOrder->goodsPrice + $goodsInOrder->markup) * $goodsInOrder->quantity, 2) }}
                                </td>
                                <td class="total-goods-profit">
                                    {{ $goodsInOrder->markup * $goodsInOrder->quantity }}
                                </td>
                            </tr>
                        @endif
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <div class="row goods-table-footer">
            <div class="col-1">
            </div>
            <div class="col-2">
                <label for="order-total-price">Всего:</label>
                <span id="order-total-price" readonly>1</span>
            </div>
            <div class="col-2">
                <label for="order-total-profit">Прибыль:</label>
                <span id="order-total-profit">1</span>
            </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-xs-3">
        <div id="order-city-holder">
            <label class="control-label">Город</label>
            <div class="form-group">
                @if($order->deliveryParams->deliveryServiceId != 2)
                    <input type="text" class="form-control" id="order-city"
                           value="{{ $order->deliveryParams->commonCity->name or '' }}" readonly>
                @else
                    <input type="text" class="form-control" id="order-city"
                           value="{{ $order->deliveryParams->NPCity->name or '' }}" readonly>
                @endif
            </div>
        </div>

        <div id="order-office-holder">
            <label class="control-label">Адрес доставки</label>
            @if($order->deliveryParams->deliveryServiceId != 2)
                <input type="text" class="form-control" id="order-city"
                       value="{{ $order->deliveryParams->commonAddress->name or '' }}" readonly>
            @else
                <input type="text" class="form-control" id="order-city"
                       value="{{ $order->deliveryParams->NPWarehouse->name or '' }}" readonly>
            @endif
        </div>

        @if (isset($order->invoice))

            <div id="invoice-holder">
                <label class="control-label">ТТН</label>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Код накладной" id="order-invoice"
                           value="{{ $order->invoice or '' }}" readonly>
                </div>
            </div>
        @endif
    </div>
    <div class="col-xs-3 pl-0">
        <div>
            <label class="control-label">
                Комментарий
            </label>
            <div class="form-group">
                    <textarea class="form-control order-comment"
                              data-stylesheet-url="assets/js/wysihtml5/lib/css/wysiwyg-color.css"
                              placeholder="Введите Ваш комментарий..."
                              id="order-comment" readonly
                              style="height: 140px">@if (isset($order->details->comment)){{ trim($order->details->comment) }}@endif</textarea>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="button-container flex-container">
        <div class="col-xs-3">
            <button class="btn btn-gray btn-close-popup">
                <span>Закрыть</span>
            </button>
        </div>
    </div>
</div>

{{ Html::script("/js/orders/read-only-popup.js") }}