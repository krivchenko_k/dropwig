@if(isset($recommendedGoods))
    <? $c = 1; ?>
    @foreach($recommendedGoods as $goods)
        <tr @if($c % 2 == 0) class="even" @endif>
            <td class="btn-td">
                <button class="btn btn-edit btn-icon add-recommended-goods-btn" data-goods-id="{{ $goods->recomendedGoods->id }}">
                    <i class="fa-plus"></i>
                    <span></span>
                </button>
            </td>
            <td class="btn-td">
                <button class="btn btn-edit goods-description"
                        data-content="{{ $goods->recomendedGoods->params->largeDescription }}">
                    <i class="fa fa-question-circle"></i>
                </button>
            </td>
            <td class="goods-name">{{ str_limit($goods->recomendedGoods->name, 50, "...") }}</td>
            <td class="goods-price num-td">{{ number_format($goods->recomendedGoods->price, 2) }}</td>
        </tr>
        <? $c++ ?>
    @endforeach
@endif
