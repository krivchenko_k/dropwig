<div class="row">
    <div class="col-xs-3">
        <div>
            <label class="control-label">Фамилия</label>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Фамилия покупателя" id="client-last-name"
                       value="{{ $order->client->lastName or '' }}">
            </div>
        </div>
        <div>
            <label class="control-label">Имя</label>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Имя покупателя" id="client-first-name"
                       value="{{ $order->client->firstName or '' }}">
            </div>
        </div>
        <div>
            <label class="control-label">Отчество <small>(не обязательно)</small></label>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Отчество покупателя (не обязательно)"
                       id="client-middle-name"
                       value="{{ $order->client->middleName or '' }}">
            </div>
        </div>

        <div>
            <label class="control-label">Телефон</label>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Телефон покупателя" id="order-phone"
                       value="{{ $order->phone or '' }}">
            </div>
        </div>
        <div>
            <label class="control-label">Статус оплаты</label>
            <div class="form-group">
                <select class="form-control" id="order-payment-type">
                    @if(isset($paymentTypes))
                        @foreach($paymentTypes as $paymentType)
                            <option value="{{ $paymentType->id }}"
                                    @if (isset($order)) @if($paymentType->id == $order->paymentTypeId) selected @endif @endif>
                                {{ $paymentType->name }}
                            </option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div>
            <label class="control-label">Способ доставки</label>
            <div class="form-group">
                <select class="form-control" id="order-delivery-service">
                    @if(isset($deliveryServices))
                        @foreach($deliveryServices as $deliveryService)
                            <option value="{{ $deliveryService->id }}"
                                    @if (isset($order)) @if($deliveryService->id == $order->deliveryParams->deliveryServiceId) selected @endif @endif>
                                {{ $deliveryService->name }}
                            </option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div id="order-city-holder"
             @if (isset($order))
             @if($order->deliveryParams->deliveryServiceId == 2) style="display: none"
             @endif
             @else
             style="display: none"
                @endif>
            <label class="control-label">Город</label>
            <div class="form-group">
                <input type="text" class="form-control" id="order-city"
                       value="{{ $order->deliveryParams->commonCity->name or '' }}">
            </div>
        </div>
        <div id="order-np-city-holder"
             @if (isset($order))
             @if($order->deliveryParams->deliveryServiceId != 2) style="display: none" @endif
                @endif>
            <label class="control-label">Город</label>
            <div class="form-group">
                <select class="form-control" id="order-np-city">
                    <option value="">Выберите город</option>
                    @if(isset($cities))
                        @foreach($cities as $city)
                            <option value="{{ $city->id }}"
                                    @if (isset($order)) @if ($city->id == $order->deliveryParams->NPCityId) selected @endif @endif>
                                {{ $city->name }}
                            </option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>

    </div>
    <div class="col-xs-9 pl-0">
        <label class="control-label">&nbsp;</label>
        <div class="goods-table-holder">
            <table class="table table-model-2 table-hover order-goods-table stat-data-1 no-footer">
                <thead>
                <tr role="row">
                    <th>№</th>
                    <th><i class="fa fa-question-circle"></i></th>
                    <th class="goods-name-th">
                        Товар
                    </th>
                    <th>
                        По дропу
                    </th>
                    <th>
                        Наценка
                    </th>
                    <th>
                        Стоимость
                    </th>
                    <th>
                        Кол-во
                    </th>
                    <th>
                        Итого
                    </th>
                    <th>
                        Прибыль
                    </th>
                    <th class="btn-th">
                        <i class="fa-trash"></i>
                    </th>
                </tr>
                </thead>
                <tbody class="middle-align">
                @if (isset($order))
                    @if(isset($order->goodsInOrder))
                        <?php $counter = 1; ?>
                        @foreach($order->goodsInOrder as $goodsInOrder)
                            @if (isset($goodsInOrder->goods))

                                <tr class="goods-table-row @if ($counter%2 == 0) even @endif"
                                    data-row-number="{{ $goodsInOrder->id }}"
                                    data-category-id="{{ $goodsInOrder->goods->categoryId }}"
                                    data-goods-id="{{ $goodsInOrder->goods->id }}">
                                    <td class="goods-number">{{ $counter++ }}</td>
                                    <td class="btn-td">
                                        <button class="btn btn-edit goods-description"
                                                data-content="{{ $goodsInOrder->goods->params->largeDescription }}">
                                            <i class="fa fa-question-circle"></i>
                                        </button>
                                    </td>
                                    <td class="goods-name">
                                        {{ $goodsInOrder->goods->name }}
                                    </td>
                                    <td class="goods-drop-price">
                                        {{ number_format($goodsInOrder->goodsPrice, 2) }}
                                    </td>
                                    <td>
                                        <input type="number" class="table-input goods-markup form-control" maxlength="6"
                                               min="0"
                                               value="{{ $goodsInOrder->markup }}">
                                    </td>
                                    <td class="goods-price">
                                        {{ number_format($goodsInOrder->goodsPrice + $goodsInOrder->markup, 2) }}
                                    </td>
                                    <td>
                                        <input type="number" class="table-input goods-quantity form-control"
                                               maxlength="6"
                                               min="0"
                                               value="{{ $goodsInOrder->quantity }}">
                                    </td>
                                    <td class="total-goods-price">
                                        {{ number_format(($goodsInOrder->goodsPrice + $goodsInOrder->markup) * $goodsInOrder->quantity, 2) }}
                                    </td>
                                    <td class="total-goods-profit">
                                        {{ $goodsInOrder->markup * $goodsInOrder->quantity }}
                                    </td>
                                    <td class="btn-td">
                                        <button class="btn btn-edit btn-order-item-delete">
                                            <i class="fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    @endif
                @endif
                </tbody>
            </table>
        </div>
        <div class="row goods-table-footer">
            <div class="col-1">
                <div class="col-1-1">
                    <select id="goods-list">
                        @if(isset($goodsList))
                            <option value="">Выберите товар для добавления в заказ</option>
                            @foreach($goodsList as $goods)
                                <option value="{{ $goods->id }}" data-price="{{ $goods->price }}"
                                        data-description="{{ $goods->params["largeDescription"]}}">{{ $goods->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-1-2">
                    <button class="btn btn-secondary btn-icon add-goods-btn">
                        <i class="fa-plus"></i>
                        <span>Добавить товар</span>
                    </button>
                </div>
            </div>

            <div class="col-2">
                <label for="order-total-price">Всего:</label>
                <span id="order-total-price" readonly></span>
            </div>
            <div class="col-2">
                <label for="order-total-profit">Прибыль:</label>
                <span id="order-total-profit"></span>
            </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-xs-3" style="z-index:5;">
        <div id="order-office-holder"
             @if (isset($order))
             style="display: none"
             @if($order->deliveryParams->deliveryServiceId == 2) style="display: none"
             @endif
             @else
             style="display: none"
                @endif>
            <label class="control-label">Адрес доставки</label>
            <div class="form-group">
                <input type="text" class="form-control" id="order-office"
                       value="{{ $order->deliveryParams->commonAddress->name or '' }}">
            </div>
        </div>

        <div id="order-np-office-holder"
             @if (isset($order)) @if($order->deliveryParams->deliveryServiceId != 2) style="display: none" @endif @endif>
            <label class="control-label">Отделение</label>
            <div class="form-group">
                <select class="form-control" id="order-np-office">
                    @if(isset($offices))
                        <option value="">Выберите отделение</option>
                        @foreach($offices as $office)
                            <option value="{{ $office->id }}"
                                    @if (isset($order)) @if($office->id == $order->deliveryParams->NPWarehouseId) selected @endif @endif>
                                {{ $office->name }}
                            </option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>

        <div>
            <label class="control-label">
                Комментарий
            </label>
            <div class="form-group">
                    <textarea class="form-control order-comment"
                              data-stylesheet-url="assets/js/wysihtml5/lib/css/wysiwyg-color.css"
                              placeholder="Введите Ваш комментарий..."
                              id="order-comment">{{ $order->details->comment or '' }}</textarea>
            </div>
        </div>
    </div>


</div>

@if(isset($order))
    @if ($order->statusId == 4)
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-danger">
                    @if (!empty($order->rejections->last()))
                        Причина отказа:
                        @if (empty($order->rejections->last()->rejectionReasonId))
                            {{ $order->rejections->last()->text }}
                        @else
                            {{ $order->rejections->last()->rejectionReason->text }}
                        @endif
                    @else
                        Причина отказа: Не указано
                    @endif
                </div>
            </div>
        </div>
    @endif
@endif


<div class="row">
    <div class="button-container flex-container">
        <div class="col-xs-3 pl-0">
            <button class="btn btn-secondary btn-icon btn-save-order" data-order-id="{{ $order->id or ""}}">
                <span>Сохранить</span>
            </button>
        </div>
        <div class="col-xs-3">
            <button class="btn btn-gray btn-icon btn-close-popup">
                <span>Отмена</span>
            </button>
        </div>
        @if (\Illuminate\Support\Facades\Session::get("user")->groupId == 3 /*&& !empty($order)*/)
            <div class="col-xs-3 pr-0">
                <button class="btn btn-success btn-icon btn-moderate-order" data-order-id="{{ $order->id or ""}}">
                    <span>Передать на отправку</span>
                </button>
            </div>
        @endif
    </div>
</div>

{{ Html::script("/js/orders/edit-popup.js") }}