@extends("app")
@section("content")
    {{ Html::style("/css/orders/orders.css") }}
    {{ Html::script("/js/orders/orders.js") }}
    {{ Html::script("lib/js/datepicker/bootstrap-datepicker.js") }}

    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Заказы</h1>
        </div>
        <div class="breadcrumb-env">
            <button class="btn btn-secondary btn-icon btn-add-order">
                <i class="fa-plus"></i>
                <span>Добавить</span>
            </button>

            <button class="btn btn-danger btn-icon common-delete-btn btn-delete-orders">
                <i class="fa fa-trash"></i>
                <span>Удалить</span>
                (<span class="deleted-row-count"></span>)
            </button>
        </div>
    </div>

    <div class="filters-holder">
        <div>
            {{--<label for="payment-type-filter">Тип оплаты:</label>--}}
            <select class="form-control" id="payment-type-filter">
                <option value="0">Выберите тип оплаты...</option>
                @if(isset($paymentTypes))
                    @foreach($paymentTypes as $paymentType)
                        <option value="{{ $paymentType->id }}">{{ $paymentType->name }}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div>

            {{--<label for="delivery-service-filter">Способ доставки:</label>--}}
            <select class="form-control" id="delivery-service-filter">
                <option value="0">Выберите способ доставки...</option>
                @if(isset($deliveryServices))
                    @foreach($deliveryServices as $deliveryService)
                        <option value="{{ $deliveryService->id }}">{{ $deliveryService->name }}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div>
            {{--<label for="partner-filter">Партнер:</label>--}}
            <select class="form-control" id="partner-filter">
                <option value="0">Выберите партнера...</option>
                @if(isset($partners))
                    @foreach($partners as $partner)
                        <option value="{{ $partner->id }}">{{ $partner->profile->lastName . ' ' . $partner->profile->firstName }}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="date-filte-holder">
        <div>
            С <input type="text"
                     class="form-control datepicker-startDate" id="datepicker-1">
        </div>
        <div>
            По <input type="text"class="form-control datepicker-endDate" id="datepicker-2">
        </div>
        <div>
            <button href="#" class="btn btn-secondary btn-sm accept-filters-btn">Применить фильтры</button>
            <button href="#" class="btn btn-secondary btn-sm clear-filters-btn">Очистить фильтры</button>
        </div>
    </div>

    <div class="orders-page-container">
        <div class="row">
            <div class="col-md-12">
                <div class="tabs-container no-dt">
                    @if(isset($orderStatuses))
                        @foreach($orderStatuses as $orderStatus)
                            <div class="tab @if($orderStatus->id == 1) {{ "active" }} @endif"
                                 data-order-status-id="{{$orderStatus->id}}">
                                <div class="order-status-color"
                                     @if (!empty($orderStatus->color)) style="background-color: {{ $orderStatus->color }}" @endif></div>
                                <div class="order-status-title">
                                    {{ $orderStatus->name }}
                                </div>
                            </div>
                        @endforeach
                            <div class="tab" data-order-status-id="0">
                                <div class="order-status-color"></div>
                                <div class="order-status-title">
                                    Все
                                </div>
                            </div>
                    @endif
                </div>

                <div class="orders-container">
                    <table class="table table-model-2 table-hover orders-table">
                        <thead>
                        <tr role="row">
                            <th class="no-sorting cbx-th">
                                <input type="checkbox" class="cbr no-sort select-all-cbx">
                            </th>
                            <th class="id-th">
                                ID
                            </th>
                            <th class="btn-th no-sorting">
                                <i class="fa-pencil"></i>
                            </th>
                            <th>Покупатель</th>
                            <th class="phone-th">
                                Телефон
                            </th>
                            <th class="comment-th">
                                Комментарий
                            </th>
                            <th class="goods-th">
                                Товар
                            </th>
                            <th class="sum-th">
                                Сумма заказа
                            </th>
                            <th class="status-th">
                                Статус оплаты
                            </th>
                            <th class="crerated-at-th">
                                Добавлен
                            </th>
                            <th class="updated-at-th">
                                Изменен
                            </th>
                            <th class="delivery-th">
                                Сбособ доставки
                            </th>
                            <th class="adress-th">
                                Адрес
                            </th>
                            <th class="invoice-th num-th">
                                ТТН
                            </th>
                            <th class="back-invoice-th num-th">
                                ТТН Обр.
                            </th>
                            <th>
                                Партнёр
                            </th>
                            <th>
                                Статус заказа
                            </th>
                            <th>
                                IP
                            </th>
                            <th>
                                Оформил
                            </th>
                            <th>
                                Сдано
                            </th>
                        </tr>
                        </thead>
                        <tbody class="middle-align">
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@stop