@if(isset($orders))
    @foreach($orders as $order)
        <tr data-order-id="{{ $order->id }}" {{--@if (!empty($order->status->color)) style="background-color: {{ $order->status->color }}" @endif--}}>
            <td class="cbx-td"
                @if (!empty($order->status->color)) style="background-color: {{ $order->status->color }}" @endif>
                @if ($order->statusId == 1 || $order->statusId ==4)
                    <input type="checkbox" class="cbr cbx order-status-cbx">
                @endif
            </td>
            <td class="id-td">
                {{ $order->id }}
            </td>
            <td class="btn-td">
                <a class="btn btn-secondary btn-edit" data-order-id="{{ $order->id }}">
                    <i class="fa-pencil"></i>
                </a>
            </td>
            <td>{{ $order->client->lastName}} {{$order->client->firstName }}</td>
            <td class="phone-td">{{ $order->phone }}</td>
            <td>
                @if(isset($order->details->comment))
                    <div class="comment-container">
                        <div class="dt-comment" data-popover-content="#comment-{{ $order->id }}">
                            ?
                        </div>
                        <div>
                            <small>
                                {{ str_limit($order->details->comment, 10, "...") }}
                            </small>
                        </div>
                        <div class="popover-source" id="comment-{{ $order->id }}">
                            {{ $order->details->comment }}
                        </div>
                    </div>
                @endif
            </td>
            <td class="goods-td">
                @if (!empty($order->goodsQuantity))
                    <div class="dt-goods-quantity" data-popover-content="#goods-{{ $order->id }}">
                        {{ $order->goodsQuantity }}
                    </div>
                    <div class="popover-source" id="goods-{{ $order->id }}">
                        @if(isset($order->goodsInOrder))
                            <table class="dt-goods-table">
                                <thead>
                                <tr>
                                    <th class="num-th">№</th>
                                    <th>Товар</th>
                                    <th class="num-th">Кол-во</th>
                                    <th class="num-th">Дроп цена</th>
                                    <th class="num-th">Наценка</th>
                                    <th class="num-th">Сумма</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($order->goodsInOrder))
                                    <?php $counter = 1 ?>
                                    @foreach($order->goodsInOrder as $goodsInOrder)
                                        @if (isset($goodsInOrder->goods))
                                            <tr @if ($counter % 2 == 0) class="even" @endif>
                                                <td class="num-td">{{ $counter++ }}</td>
                                                <td>{{ $goodsInOrder->goods->name }}</td>
                                                <td class="num-td">{{ $goodsInOrder->quantity }}</td>
                                                <td class="num-td">{{ round($goodsInOrder->goodsPrice, 2) }}</td>
                                                <td class="num-td">{{ $goodsInOrder->markup }}</td>
                                                <td class="num-td">{{ ($goodsInOrder->goodsPrice + $goodsInOrder->markup) * $goodsInOrder->quantity }}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        @else Информация о товарах отсутствует
                        @endif
                    </div>
                @endif
            </td>
            <td class="sum-td">
                {{ number_format($order->price, 2) }} грн.
            </td>
            <td>{{ $order->paymentType->name }}</td>
            <td class="crerated-at-td">
                <small>
                    {{ date("d.m.Y", strtotime($order->created_at)) }}
                    <small>{{ date("(H:i)", strtotime($order->created_at)) }}</small>
                </small>
            </td>
            <td class="updated-at-td">
                <small>
                    {{ date("d.m.Y", strtotime($order->updated_at)) }}
                    <small>{{ date("(H:i)", strtotime($order->updated_at)) }}</small>
                </small>
            </td>
            <td>
                <img class="delivery-icon"
                     src="/images/icons/delivery/{{$order->deliveryParams->deliveryService->id}}.png" alt="">
                {{ $order->deliveryParams->deliveryService->name or "" }}
            </td>
            <td>
                <small>
                    @if ($order->deliveryParams->deliveryService->id == 2)
                        {{ $order->deliveryParams->NPCity["name"].", ".$order->deliveryParams->NPWarehouse["name"]}}
                    @else
                        {{ $order->deliveryParams->commonCity["name"].", ".$order->deliveryParams->commonAddress["name"]}}
                    @endif
                </small>
            </td>
            <td class="num-td">
                {{ $order->invoice }}
            </td>
            <td class="delivered-at-td-at-td">
                @if (isset($order->delivered_at))
                    <small>
                        {{ date("d.m.Y", strtotime($order->delivered_at)) }}
                        <small>{{ date("(H:i)", strtotime($order->delivered_at)) }}</small>
                    </small>
                @endif
            </td>
        </tr>
    @endforeach
@endif

