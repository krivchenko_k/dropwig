<div class="row">
    <div class="col-xs-9 stat-title">
        Заказов за сегодня
    </div>
    <div class="col-xs-3 num-td orders-per-day-count">
        {{ $ordersToday or "Н/Д"}}
    </div>
</div>
<div class="row">
    <div class="col-xs-9 stat-title">
        Заказов за все время
    </div>
    <div class="col-xs-3 num-td orders-total-count">
        {{ $ordersTotal or "Н/Д"}}
    </div>
</div>
<div class="row">
    <div class="col-xs-7 stat-title">
        Баланс
    </div>
    <div class="col-xs-5 num-td balance">
        {{ number_format($balance, 2) }} грн.
    </div>
</div>
<div class="row">
    <div class="col-xs-7 stat-title">
        Выплачено за всё время
    </div>
    <div class="col-xs-5 num-td balance">
        {{ number_format($totalPayout, 2) }} грн.
    </div>
</div>
