@if (!empty($newsList))

    @foreach($newsList as $news)
        <div class="row">
            <div class="date">
                {{ date("d.m.Y", strtotime($news->created_at)) }}
                <span class="title">
                    {{ $news->title }}
                </span>
            </div>
            <div class="text">
                {!! $news->text !!}
            </div>
        </div>
    @endforeach

@endif