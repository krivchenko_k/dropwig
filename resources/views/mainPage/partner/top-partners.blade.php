@if (isset($partners))
    <?php $i = 1 ?>
    @foreach($partners as $partner)
        <div class="row  @if ($i % 2 == 0) even @endif">
            <div class="col-xs-12">
                @if ($i <= 3) <p class="bg-warning">{{ $partner->nickname }}</p> @endif
                @if ($i > 3) <p class="bg-info">{{ $partner->nickname }}</p> @endif
            </div>
            {{--<div class="col-xs-5 num-td">--}}
                {{--{{ $partner->markup }}--}}
            {{--</div>--}}
        </div>
        <? $i++ ?>
    @endforeach
@endif