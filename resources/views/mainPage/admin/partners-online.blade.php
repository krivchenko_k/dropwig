@if (isset($partnersOnline))
    <?php $i = 1
    ?>
    @foreach($partnersOnline as $partnerOnline)
        <div class="row  @if ($i % 2 == 0) odd @endif">
            <div class="col-xs-8">
                {{ mb_strimwidth($partnerOnline->lastName ." ".$partnerOnline->firstName, 0, 100, "...") }}
            </div>
            <div class="col-xs-4 number">
                {{ $partnerOnline->lastActivity }}
            </div>
        </div>
        <? $i++ ?>
    @endforeach
@else
    <div class="row">
        <div class="col-xs-12">
            <p class="bg-info">Партнёров нет онлайн</p>
        </div>
    </div>
@endif