<div class="row">
    <div class="col-xs-9 stat-title">
        Заказов за сегодня
    </div>
    <div class="col-xs-3 number orders-per-day-count">
        {{ $stat["ordersPerDay"] }}
    </div>
</div>
<div class="row">
    <div class="col-xs-9 stat-title">
        Заказов за неделю
    </div>
    <div class="col-xs-3 number orders-per-week-count">
        {{ $stat["ordersPerWeek"] }}
    </div>
</div>
<div class="row">
    <div class="col-xs-9 stat-title">
        Заказов за месяц
    </div>
    <div class="col-xs-3 number orders-per-month-count">
        {{ $stat["ordersPerMonth"] }}
    </div>
</div>
<div class="row">
    <div class="col-xs-9 stat-title">
        В среднем заказов за день
    </div>
    <div class="col-xs-3 number orders-avg-per-day-count">
        {{ $stat["ordersPerDayAvg"] }}
    </div>
</div>
