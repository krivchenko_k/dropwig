@if (isset($goodsList))
    <?php $i = 1 ?>
    @foreach($goodsList as $goods)
        <div class="row  @if ($i % 2 == 0) odd @endif">

            <div class="col-xs-9">
                {{ mb_strimwidth($goods->name, 0, 40, "...") }}
            </div>
            <div class="col-xs-3 number">
                {{ $goods->totalQuantity }}
            </div>
        </div>
        <? $i++ ?>
    @endforeach
@endif