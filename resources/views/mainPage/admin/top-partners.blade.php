@if (isset($partners))
    <?php $i = 1 ?>
    @foreach($partners as $partner)
        <div class="row  @if ($i % 2 == 0) odd @endif">
            <div class="col-xs-9">
                {{ mb_strimwidth($partner->lastName ." ".$partner->firstName, 0, 100, "...") }}
            </div>
            <div class="col-xs-3 number">
                {{ $partner->ordersCount }}
            </div>
        </div>
        <? $i++ ?>
    @endforeach
@endif