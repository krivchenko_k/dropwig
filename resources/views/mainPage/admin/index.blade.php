@extends("app")

@section("content")
    {{ Html::style("/css/mainPage/admMainPage.css") }}
    {{ Html::script("/js/mainPage/admMainPage.js") }}
    <div class="adm-main-page-container">
        <div class="row">
            <div class="col-xs-12 statistic-container">
                <div class="col-xs-6 partners-stat">
                    <div class="loader"></div>
                    <div class="fade-in-effect content">
                    </div>
                </div>
                <div class="col-xs-6 goods-stat">
                    <div class="loader"></div>
                    <div class="fade-in-effect content">
                    </div>
                </div>
            </div>

            <div class="col-xs-12 rating-container">
                <div class="col-xs-6 top-partners-container">
                    <div class="col-xs-12 title">
                        Топ-10 партнёров за неделю
                    </div>
                    <div class="col-xs-12 border">
                        <div class="col-xs-12 table-head">
                            <div class="col-xs-9">
                                Партнёр
                            </div>
                            <div class="col-xs-3 number-title">
                                Заказов
                            </div>
                        </div>
                        <div class="col-xs-12 content">
                            <div class="loader"></div>
                            <div class="fade-in-effect stat-holder">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 top-goods-container">
                    <div class="col-xs-12 title">
                        Топ-10 товаров за неделю
                    </div>
                    <div class="col-xs-12 border">
                        <div class="col-xs-12 table-head">
                            <div class="col-xs-9">
                                Товар
                            </div>
                            <div class="col-xs-3 number-title">
                                Продано
                            </div>
                        </div>
                        <div class="col-xs-12 content">
                            <div class="loader"></div>
                            <div class="fade-in-effect stat-holder">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 rating-container">
                <div class="col-xs-6 partners-online-container">
                    <div class="col-xs-12 title">
                        Партнёры онлайн
                    </div>
                    <div class="col-xs-12 border">
                        <div class="col-xs-12 table-head">
                            <div class="col-xs-6">
                                Партнёр
                            </div>
                            <div class="col-xs-6">
                                Последняя активность
                            </div>
                        </div>
                        <div class="col-xs-12 content">
                            <div class="loader"></div>
                            <div class="fade-in-effect stat-holder">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop