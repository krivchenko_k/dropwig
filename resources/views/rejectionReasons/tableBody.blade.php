@foreach($rejectionReasons as $rejectionReason)
    <tr data-rejection-reason-id="{{ $rejectionReason->id }}">
        <td class="cbx-td">
            <input type="checkbox" class="cbr cbx">
        </td>
        <td class="id-td">
            {{ $rejectionReason->id }}
        </td>
        <td class="btn-td">
            <button class="btn btn-edit btn-secondary btn-xs">
                <i class="fa-pencil"></i>
            </button>
        </td>
        <td class="dblclickable">
            {{ $rejectionReason->rejectionType->name or ""}}
        </td>
        <td class="dblclickable">
            {{ $rejectionReason->text }}
        </td>
    </tr>
@endforeach
