<div class="popup-content">
    <div>
        <div>
            <select class="form-control rejection-type-id-sel">
                @if (!empty($rejectionReason->typeId) && isset($rejectionReason->rejectionType))
                    <option value="{{$rejectionReason->typeId}}">{{ $rejectionReason->rejectionType->name }}</option>
                @else
                    <option value="">Выберите тип отказа</option>
                @endif
                <option value="" disabled>-----------------</option>
                @foreach($rejectionTypes as $rejectionType)
                    <option value="{{ $rejectionType->id }}">{{ $rejectionType->name }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <textarea cols="30" rows="10" class="form-control rejection-reason-text-inp"
                      placeholder="Текст причины отказа">{{ $rejectionReason->text or "" }}</textarea>
        </div>
        <div class="button-container">
            <button class="btn btn-secondary btn-icon btn-save"
                    data-rejection-reason-id="{{ $rejectionReason->id or ''}}">
                <span>Сохранить</span>
            </button>
            <button class="btn btn-gray btn-icon btn-close-popup">
                <span>Отмена</span>
            </button>
        </div>
    </div>
</div>