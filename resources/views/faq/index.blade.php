@extends("app")
@section("content")

    {{ Html::style("/css/faq/faq.css")  }}
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Вопросы и ответы</h1>
        </div>
    </div>

    <div class="faq-page-container">
        @foreach ($FAQList as $FAQelem)
            <div class="panel-group" id="accordion">
                <div class="faq-panel-heading faq-heading">
                    <h4 class="faq-panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $FAQelem->id }}">
                            {{ $FAQelem->question }}
                        </a>
                    </h4>
                </div>
                <div id="collapse-{{ $FAQelem->id }}" class="panel-collapse collapse">
                    <div class="faq-panel-body">
                        {!! $FAQelem->answer !!}
                    </div>
                </div>
            </div>
        @endforeach
    </div>

@stop