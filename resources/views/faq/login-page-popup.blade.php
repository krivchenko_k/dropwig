<div class="popup-content">
    @foreach ($FAQList as $FAQelem)

        <div class="panel-group" id="accordion">
                <div class="panel-heading faq-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $FAQelem->id }}">
                            {{ $FAQelem->question }}
                        </a>
                    </h4>
                </div>
                <div id="collapse-{{ $FAQelem->id }}" class="panel-collapse collapse">
                    <div class="panel-body">
                        {!! $FAQelem->answer !!}
                    </div>
                </div>
        </div>
    @endforeach
</div>
