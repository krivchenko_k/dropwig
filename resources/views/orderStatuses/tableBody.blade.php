@foreach($orderStatuses as $orderStatus)
    <tr data-order-status-id="{{ $orderStatus->id }}">
        <td class="cbx-td">
            <input type="checkbox" class="cbr cbx order-status-cbx">
        </td>
        <td class="id-td">
            {{ $orderStatus->id }}
        </td>
        <td class="btn-td">
            <button class="btn btn-edit btn-secondary btn-xs">
                <i class="fa-pencil"></i>
            </button>
        </td>
        <td class="dblclickable">
            {{ $orderStatus->name }}
        </td>
        <td class="dblclickable" style="background-color: {{ $orderStatus->color }}">
            {{ $orderStatus->color }}
        </td>
    </tr>
@endforeach