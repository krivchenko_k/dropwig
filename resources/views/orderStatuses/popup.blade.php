<div class="popup-content">
    <div>
        <div>
            <input type="text" class="form-control order-status-name-inp" placeholder="Имя группы" value="{{ $orderStatus->name or "" }}">
        </div>
        <div>
            <input type="color" class="form-control order-status-color-inp" value="{{ $orderStatus->color or "" }}">
        </div>
        <div class="button-container">
            <button class="btn btn-secondary btn-icon btn-save" data-order-status-id="{{ $orderStatus->id or ''}}">
                <span>Сохранить</span>
            </button>
            <button class="btn btn-gray btn-icon btn-close-popup">
                <span>Отмена</span>
            </button>
        </div>
    </div>
</div>

