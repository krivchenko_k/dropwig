@extends("app")

@section("content")

    {{ Html::script("js/orderStatuses/orderStatuses.js") }}
    {{ Html::style("css/orderStatuses/orderStatuses.css") }}

    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Статусы заказов</h1>
        </div>

        <div class="breadcrumb-env">
            <button class="btn btn-secondary btn-icon add-order-status-btn">
                <i class="fa-plus"></i>
                <span>Добавить</span>
            </button>

            <button class="btn btn-danger btn-icon common-delete-btn delete-order-statuses-btn">
                <i class="fa fa-trash"></i>
                <span>Удалить</span>
                (<span class="deleted-row-count"></span>)
            </button>
        </div>
    </div>

    <div class="order-statuses-page-container">
        <table class="table table-model-2 table-hover order-statuses-table">
            <thead>
            <tr>
                <th class="no-sorting cbx-th">
                    <input type="checkbox" class="cbr no-sort select-all-cbx select-all-order-statuses-cbx">
                </th>
                <th class="id-th">
                    ID
                </th>
                <th class="btn-th no-sorting">
                </th>
                <th>Название статуса</th>
                <th>
                    Цвет
                </th>
            </tr>
            </thead>
            <tbody class="middle-align">
                @include("orderStatuses.tableBody")
            </tbody>
        </table>
    </div>
@stop
