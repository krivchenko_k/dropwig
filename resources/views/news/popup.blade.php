
<div class="popup-content">
    <div>
        <div>
            <input type="text" class="form-control news-title-inp" placeholder="Заголовок новости" value="{{ $news->title or "" }}">
        </div>
        <div>
            <textarea name="news-text" class="news-text-inp" placeholder="Текст новости" value="{!! $news->text or '' !!}">{!! $news->text or '' !!}</textarea>
        </div>
        <div class="button-container">
            <button class="btn btn-secondary btn-icon btn-save-news" data-news-id="{{ $news->id or ''}}">
                <span>Сохранить</span>
            </button>
            <button class="btn btn-gray btn-icon btn-close-popup">
                <span>Отмена</span>
            </button>
        </div>
    </div>
</div>