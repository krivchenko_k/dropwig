@foreach ($news as $news)
    <tr data-news-id="{{ $news->id }}">
        <td class="cbx-td">
            <input type="checkbox" class="cbr cbx">
        </td>
        <td class="id-td">
            {{ $news->id }}
        </td>
        <td class="btn-td">
            <button class="btn btn-edit btn-secondary btn-xs">
                <i class="fa-pencil"></i>
            </button>
        </td>
        <td class="dblclickable">
            {{ $news->title }}
        </td>
        <td class="dblclickable news-text-td">
            <div>
                {{ str_limit(html_entity_decode($news->text), 200) }}
            </div>
        </td>
        <td class="dblclickable">
            {{ date("H:m d.m.Y", strtotime($news->created_at)) }}
        </td>
    </tr>
@endforeach