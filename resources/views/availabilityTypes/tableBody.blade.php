@foreach($availabilityTypes as $availabilityType)
    <tr data-availability-type-id="{{ $availabilityType->id }}">
        <td class="cbx-td">
            <input type="checkbox" class="cbr cbx">
        </td>
        <td class="id-td">
            {{ $availabilityType->id }}
        </td>
        <td class="btn-td">
            <button class="btn btn-edit btn-secondary btn-xs">
                <i class="fa-pencil"></i>
            </button>
        </td>
        <td class="dblclickable">
            {{ $availabilityType->name }}
        </td>
    </tr>
@endforeach