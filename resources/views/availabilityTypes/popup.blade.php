<div class="popup-content">
    <div>
        <div>
            <input type="text" class="form-control availability-type-name-inp" placeholder="Название типа наличия товара" value="{{ $availabilityType->name or "" }}">
        </div>
        <div class="button-container">
            <button class="btn btn-secondary btn-icon btn-save" data-availability-type-id="{{ $availabilityType->id or ''}}">
                <span>Сохранить</span>
            </button>
            <button class="btn btn-gray btn-icon btn-close-popup">
                <span>Отмена</span>
            </button>
        </div>
    </div>
</div>