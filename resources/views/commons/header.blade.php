<!DOCTYPE html>
<html lang="en">
<head>

    <meta class="app-token" name="_token" content="{!! csrf_token() !!}"/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Xenon Boostrap Admin Panel"/>
    <meta name="author" content=""/>

    <title>Дропшиппинг система</title>

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
{{ Html::style("/lib/css/fonts/linecons/css/linecons.css") }}
{{ Html::style("/lib/css/fonts/fontawesome/css/font-awesome.min.css") }}
{{ Html::style("/lib/css/bootstrap.css") }}
{{--{{ Html::style("/lib/css/xenon-core.css") }}--}}
{{--{{ Html::style("/lib/css/xenon-forms.css") }}--}}
{{--{{ Html::style("/lib/css/xenon-components.css") }}--}}
{{--{{ Html::style("/lib/css/xenon-skins.css") }}--}}
{{ Html::style("/lib/css/xenon.css") }}
{{ Html::style("/lib/css/fonts/elusive/css/elusive.css") }}
{{ Html::style("/lib/css/custom.css") }}
{{ Html::style("/lib/css/magnific-popup.css") }}
{{ Html::style("/lib/js/select2/select2.css") }}
{{ Html::style("/lib/js/select2/select2-bootstrap.css") }}

{{ HTML::script("/lib/js/jbone.min.js") }}
{{ HTML::script('/lib/js/jquery-3.1.0.min.js') }}
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    {{ HTML::script("https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js") }}
    {{ HTML::script("https://oss.maxcdn.com/respond/1.4.2/respond.min.js") }}
    <![endif]-->

{{ Html::style('/css/main.css') }}
{{ Html::style('/css/google-icons.css') }}



<!-- Bottom Scripts -->
{{ HTML::script("/lib/js/bootstrap.min.js") }}
{{ HTML::script("/lib/js/select2/select2.min.js") }}
{{ HTML::script("/lib/js/TweenMax.min.js") }}
{{ HTML::script("/lib/js/resizeable.js") }}
{{ HTML::script("/lib/js/joinable.js") }}
{{ HTML::script("/lib/js/xenon-api.js") }}
{{ HTML::script("/lib/js/xenon-toggles.js") }}
{{ Html::script("/lib/js/colorpicker/bootstrap-colorpicker.min.js") }}

{{ Html::script("/lib/js/moment.min.js") }}
{{ Html::script("/lib/js/jquery.dataTables.min.js") }}
{{ Html::script("/lib/js/datetime-moment.js") }}

{{ Html::script("/lib/js/jquery.validate.min.js") }}
{{ Html::script("/lib/js/jquery.mask.min.js") }}
{{ Html::script("/lib/js/additional-methods.min.js") }}
{{ Html::script("/lib/js/jquery.magnific-popup.js") }}
{{ Html::script("/lib/js/toastr/toastr.min.js") }}

{{ HTML::script('/js/main.js') }}

<!-- JavaScripts initializations and stuff -->
    {{ HTML::script("/lib/js/xenon-custom.js") }}

    <meta name="mailru-domain" content="ZaUys8hIDWGizuDi" />

</head>
<body class="page-body">
