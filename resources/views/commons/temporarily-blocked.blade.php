@extends("app")

@section("content")
    {{ Html::style("/css/commons/404.css") }}

    <div class="page-404-container">
        <div class="page-error centered">

            <div class="error-symbol">
                <i class="fa-warning"></i>
            </div>

            <h2>
                В связи с отсутствием активности, Ваш аккаунт временно заблокирован.<br>Для активации аккаунта просим Вас связаться с модератором.
                Контакты для связи Вы можете найти на странице <a style="color: #467fdd; text-decoration: underline" href="#" onclick="$('.contacts-btn').click()">контактов</a>.
                <small>Данная страница пока не доступна</small>
            </h2>


        </div>
    </div>
@stop