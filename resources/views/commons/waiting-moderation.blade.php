@extends("app")

@section("content")
    {{ Html::style("/css/commons/404.css") }}

    <div class="page-404-container">
        <div class="page-error centered">

            <div class="error-symbol">
                <i class="fa-warning"></i>
            </div>

            <h2>
                Ваш аккаунт ожидает модерации
                <small>Данная страница пока не доступна</small>
            </h2>


        </div>

        {{--<div class="page-error-search centered">--}}
            {{--<a class="go-back" onclick="window.history.back()">--}}
                {{--<i class="fa-angle-left"></i>--}}
                {{--Назад--}}
            {{--</a>--}}
        {{--</div>--}}
    </div>
@stop