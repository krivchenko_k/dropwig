
{{ Html::script("/lib/js/jquery.validate.min.js") }}
{{ Html::script("/lib/js/additional-methods.min.js") }}
{{ Html::script("/lib/js/jquery.mask.min.js") }}

{{ Html::style("/lib/js/datatables/dataTables.bootstrap.css") }}


{{ Html::script("/lib/js/datatables/dataTables.bootstrap.js") }}
{{ Html::script("/lib/js/datatables/yadcf/jquery.dataTables.yadcf.js") }}
{{ Html::script("/lib/js/datatables/tabletools/dataTables.tableTools.min.js") }}
        

</body>
</html>