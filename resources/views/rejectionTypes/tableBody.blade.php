@foreach($rejectionTypes as $rejectionType)
    <tr data-rejection-type-id="{{ $rejectionType->id }}">
        <td class="cbx-td">
            <input type="checkbox" class="cbr cbx">
        </td>
        <td class="id-td">
            {{ $rejectionType->id }}
        </td>
        <td class="btn-td">
            <button class="btn btn-edit btn-secondary btn-xs">
                <i class="fa-pencil"></i>
            </button>
        </td>
        <td class="rejection-type-name-td dblclickable">
            {{ $rejectionType->name }}
        </td>
    </tr>
@endforeach