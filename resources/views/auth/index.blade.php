@include("commons.header")

{{ Html::script("/lib/js/jquery.validate.min.js") }}
{{ Html::style('/css/auth/auth.css') }}
{{ Html::script("/js/auth/auth.js") }}
{{--{{ Html::script("/js/auth/faq-popup.js") }}--}}
{{--{{ Html::script("/js/auth/contacts-popup.js") }}--}}

<div class="login-page-container fade-in-effect">
    <div class="login-form-container">
        <div class="alert-container">
            @if(Session::has("error"))
                <div class="alert alert-danger">
                    {{Session::get('error')}}
                </div>
            @endif
            @if(Session::has("auth-warn"))
                <div class="alert alert-warning">
                    {{Session::get('auth-warn')}}
                </div>
            @endif
            @if(Session::has("reg-confirm-warn"))
                <div class="alert alert-warning">
                    {{Session::get('reg-confirm-warn')}}
                </div>
            @endif
                @if(Session::has("reg-confirm"))
                    <div class="alert alert-info">
                        {{Session::get('reg-confirm')}}
                    </div>
                @endif
        </div>

        <form method="post" role="form" id="login" action="login" class="login-form">
            {!! csrf_field() !!}
            <label class="control-label login-label" for="login">Логин</label>
            <div class="form-group">
                <input type="text" class="form-control" name="login" id="login" value="@if(Session::has("login")){{ Session::get('login') }}@endif"/>
            </div>
            <label class="control-label password-label" for="password">Пароль</label>
            <div class="form-group">
                <input type="password" class="form-control" name="password" id="password"
                       autocomplete="off"/>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-secondary btn-login btn-block">
                    <i class="fa-sign-in"></i>
                    Войти
                </button>
            </div>

            <div class="form-group">
                <a href="/registration" class="btn btn-white btn-block">
                    <i class="fa-user"></i>
                    <i class="fa-plus"></i>
                    Зарегистрироваться
                </a>
            </div>

        </form>

    </div>

    <div class="buttons-container">
        <div class="show-faq-btn">
            Ответы на вопросы
        </div>
        <div class="show-contacts-btn">
            Наши контакты
        </div>
    </div>
<!--    <a href="http://vk.com"><img src="/images/vk.png" alt="Группа нашей компании Вконтакте"></a>-->
    <div class="policy-container">
        <div class="show-agreement">
            Пользовательское соглашение
        </div>
        <div class="show-policy">
            Политика конфиденциальности
        </div>
    </div>
</div>


<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter39922815 = new Ya.Metrika({
                    id:39922815,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js ";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/39922815 " style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

@include("commons.popup")

@include("commons.footer")



