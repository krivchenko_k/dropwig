@foreach($deliveryServices as $deliveryService)
    <tr data-delivery-service-id="{{ $deliveryService->id }}">
        <td class="cbx-td">
            <input type="checkbox" class="cbr cbx">
        </td>
        <td class="id-td">
            {{ $deliveryService->id }}
        </td>
        <td class="btn-td">
            <button class="btn btn-edit btn-secondary btn-xs">
                <i class="fa-pencil"></i>
            </button>
        </td>
        <td class="delivery-service-name-td dblclickable">
            {{ $deliveryService->name }}
        </td>
    </tr>
@endforeach