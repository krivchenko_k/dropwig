<div class="popup-content">
    <div>
        <div>
            <input type="text" class="form-control payment-type-name-inp" placeholder="Название типа оплаты" value="{{ $paymentType->name or "" }}">
        </div>
        <div class="button-container">
            <button class="btn btn-secondary btn-icon btn-save" data-payment-type-id="{{ $paymentType->id or ''}}">
                <span>Сохранить</span>
            </button>
            <button class="btn btn-gray btn-icon btn-close-popup">
                <span>Отмена</span>
            </button>
        </div>
    </div>
</div>