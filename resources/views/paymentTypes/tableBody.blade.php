@foreach($paymentTypes as $paymentType)
    <tr data-payment-type-id="{{ $paymentType->id }}">
        <td class="cbx-td">
            <input type="checkbox" class="cbr cbx">
        </td>
        <td class="id-td">
            {{ $paymentType->id }}
        </td>
        <td class="btn-td">
            <button class="btn btn-edit btn-secondary btn-xs">
                <i class="fa-pencil"></i>
            </button>
        </td>
        <td class="dblclickable">
            {{ $paymentType->name }}
        </td>
    </tr>
@endforeach