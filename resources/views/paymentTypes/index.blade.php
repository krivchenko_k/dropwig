@extends("app")

@section("content")

    {{ Html::script("js/paymentTypes/paymentTypes.js") }}
    {{ Html::style("css/paymentTypes/paymentTypes.css") }}

    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Типы оплаты</h1>
        </div>

        <div class="breadcrumb-env">
            <button class="btn btn-secondary btn-icon add-payment-type-btn">
                <i class="fa-plus"></i>
                <span>Добавить</span>
            </button>

            <button class="btn btn-danger btn-icon common-delete-btn delete-payment-types-btn">
                <i class="fa fa-trash"></i>
                <span>Удалить</span>
                (<span class="deleted-row-count"></span>)
            </button>
        </div>
    </div>

    <div class="payment-types-page-container">
        <table class="table table-model-2 table-hover payment-types-table">
            <thead>
            <tr>
                <th class="no-sorting cbx-th">
                    <input type="checkbox" class="cbr no-sort select-all-cbx">
                </th>
                <th class="id-th">
                    ID
                </th>
                <th class="btn-th no-sorting">
                </th>
                <th>Название типа</th>
            </tr>
            </thead>
            <tbody class="middle-align">
            @include("paymentTypes.tableBody")
            </tbody>
        </table>
    </div>
@stop

