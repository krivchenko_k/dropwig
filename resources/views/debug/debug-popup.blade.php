<div class="debug-popup">
    <pre>
        </pre>
</div>
<button class="show-debug-btn btn btn-primary">
    DEBUG INFO
</button>

<script>
    $(document).ready(function () {
        $(".show-debug-btn").click(function () {
            if ($(".debug-popup").css("display") == "none")
                $(".debug-popup").show();
            else
                $(".debug-popup").hide();
        })
    })
</script>