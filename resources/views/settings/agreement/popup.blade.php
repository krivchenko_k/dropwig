<div class="popup-content">
        <div>
                <textarea name="agreement-text" class="agreement-text-inp" placeholder="Введите текст пользовательского соглашения" value="{!! $agreement->text or '' !!}">
                    {!! $agreement->text or '' !!}
                </textarea>
        </div>
        <div class="button-container">
            <button class="btn btn-secondary btn-icon btn-save-agreement">
                <span>Сохранить</span>
            </button>
            <button class="btn btn-gray btn-icon btn-close-popup">
                <span>Отмена</span>
            </button>
        </div>
</div>