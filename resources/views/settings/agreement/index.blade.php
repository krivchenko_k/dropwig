@extends("app")

@section("content")

    {{ Html::script("/js/settings/agreement/agreement.js") }}
    {{ Html::style("/css/settings/agreement/agreement.css") }}
    {{ Html::script("/lib/js/ckeditor/ckeditor.js") }}

    <div class="page-title">

        <div class="title-env">
            <h1 class="title">Пользовательское соглашение</h1>
        </div>

        <div class="breadcrumb-env">
            <button class="btn btn-secondary btn-icon edit-agreement-btn">
                <i class="fa-edit"></i>
                <span>Редактировать</span>
            </button>

        </div>

    </div>

    <div class="agreement-page-container">

            {!! $agreement->text or 'Текст пользовательского соглашения отсутствует' !!}

    </div>
@stop
