
<div class="popup-content">
    <div>
        <div>
            <input type="text" class="form-control faq-question-inp" placeholder="Вопрос" value="{{ $FAQ->question or "" }}">
        </div>
        <div>
            Ответ
            <textarea name="answer" class="form-control faq-answer-inp" placeholder="Ответ" value="{!! $FAQ->answer or '' !!}">{!! $FAQ->answer or '' !!}</textarea>
        </div>
        <div class="button-container">
            <button class="btn btn-secondary btn-icon btn-save" data-faq-id="{{ $FAQ->id or ''}}">
                <span>Сохранить</span>
            </button>
            <button class="btn btn-gray btn-icon btn-close-popup">
                <span>Отмена</span>
            </button>
        </div>
    </div>
</div>
