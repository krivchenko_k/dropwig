@foreach($FAQList as $FAQElem)
    <tr data-faq-id="{{ $FAQElem->id }}">
        <td class="cbx-td">
            <input type="checkbox" class="cbr cbx order-status-cbx">
        </td>
        <td class="id-td">
            {{ $FAQElem->id }}
        </td>
        <td class="btn-td">
            <button class="btn btn-edit btn-secondary btn-xs">
                <i class="fa-pencil"></i>
            </button>
        </td>
        <td class="dblclickable">
            {{ $FAQElem->question }}
        </td>
        <td class="dblclickable answer-td">
            <div>
                {{ str_limit(html_entity_decode($FAQElem->answer), 200) }}
            </div>
        </td>
    </tr>
@endforeach