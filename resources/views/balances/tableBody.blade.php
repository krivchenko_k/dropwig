@if(isset($partners))
    @foreach($partners as $partner)
        <tr>
            <td>{{ $partner->balance->id }}</td>
            <td>{{ $partner->profile->lastName }} {{ $partner->profile->firstName }}</td>
            <td>{{ $partner->profile->card }}</td>
            <td>{{ number_format($partner->balance->amount, 2) }}</td>
        </tr>
    @endforeach
@endif