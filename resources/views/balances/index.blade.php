@extends("app")

@section("content")

    <div class="access-page-container">
                                                                                            <div id="test"></div>
        <h3>Балансы партнеров</h3>
        <h4>Всего на балансе партнеров: <b>{{ round($totalBalancesSum, 2) }}</b> грн</h4>
        <h4>Максимальная сумма на балансе: <b>{{ round($maxBalance, 2) }}</b> грн</h4>
        <table class="table table-model-2 table-hover balances-table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Партнёр</th>
                <th>Номер карты</th>
                <th>На балансе</th>
            </tr>
            </thead>
            <tbody class="middle-align">
            @include("balances.tableBody")
            </tbody>
        </table>
    </div>
    {{ Html::script("/js/balances/balances.js") }}

@stop


