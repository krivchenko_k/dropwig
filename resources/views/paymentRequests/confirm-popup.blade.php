@if(isset($request))
    <div class="popup-content">
        <div class="flex-container">
            <div class="droppable-area image-preview">
                <input type="file" name="file" class="upload-image-btn" accept=".jpg, .png, .jpeg, .gif, .bmp"/>
                <div class="img-preview">
                    <img src="" alt="">
                </div>
            </div>
        </div>
        <div class="button-container ">
            <button class="btn btn-secondary btn-icon btn-confirm" data-request-id="{{ $request->id }}">
                <span>Выплатить</span>
            </button>
            <button class="btn btn-gray btn-icon btn-close-popup">
                <span>Отмена</span>
            </button>
        </div>
    </div>
@endif