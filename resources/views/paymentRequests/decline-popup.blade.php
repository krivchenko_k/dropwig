@if(isset($requestId))
    <div class="popup-content">
        <div>
            <div class="form-group">
                <select class="form-control sel-decline-reason">
                    @if(isset($rejectionReasons))
                        @foreach($rejectionReasons as $declineReason)
                            <option value="{{ $declineReason->id }}">{{ $declineReason->text }}</option>
                        @endforeach
                    @endif
                    <option value="-1">Другая причина...</option>
                </select>
            </div>
            <div>
                @if(isset($rejectionReasons))
                    <textarea class="decline-reason" placeholder="Введите причину..." disabled></textarea>
                @else
                    <textarea class="decline-reason" placeholder="Введите причину..."></textarea>
                @endif
            </div>
        </div>
        <div class="button-container">
            <button data-request-id="{{ $requestId }}"
                    class="btn btn-danger btn-decline">Отказать
            </button>
            <button href="#"
                    class="btn btn-gray btn-close">Отмена
            </button>
        </div>
    </div>
    {{ Html::script('/js/paymentRequests/decline-popup.js') }}
@endif
