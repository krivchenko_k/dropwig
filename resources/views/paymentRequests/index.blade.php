@extends("app")

@section("content")
    {{ Html::style("/css/paymentRequests/paymentRequests.css") }}

    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Заявки к выплате</h1>
        </div>
    </div>

    <div class="date-filte-holder">
        <div>
            С <input type="text"
                     class="form-control datepicker-startDate" id="datepicker-1">
        </div>
        <div>
            По <input type="text"class="form-control datepicker-endDate" id="datepicker-2">
        </div>
        <div>
            {{--<button href="#" class="btn btn-secondary btn-sm accept-filters-btn">Применить фильтры</button>--}}
            <button href="#" class="btn btn-secondary btn-sm clear-filters-btn">Очистить фильтры</button>
        </div>
    </div>

    <div class="access-page-container">
        <table class="table table-model-2 table-hover payment-requests-table">
            <thead>
            <tr>
                <th>id</th>
                <th>Партнер</th>
                <th>Просит к выплате</th>
                <th>Средств на балансе</th>
                <th>Дата заявки</th>
                <th>Действие</th>


            </tr>
            </thead>

            <tbody class="middle-align">

            </tbody>
        </table>


    </div>

    {{ Html::script("/lib/js/datepicker/bootstrap-datepicker.js") }}
    {{ Html::script("/js/paymentRequests/paymentRequests.js") }}

@stop