@if(isset($requests))
    @foreach($requests as $request)
        <tr
            @if (!$request->isActive && !empty($request->imageLink))
                style="background-color: #9dd4a0;"
            @elseif (!$request->isActive && empty($request->imageLink))
                style="background-color: #ec8a8a;"
            @endif
        >
            <td>{{ $request->id }}</td>
            <td> {{ $request->partnerProfile->lastName.' '.$request->partnerProfile->firstName }}</td>
            <td> {{ $request->price }}</td>
            <td>{{ round($request->partnerProfile->balance->amount, 2) }}</td>
            <td>
                {{ date("d.m.Y", strtotime($request->created_at)) }}
                <small>({{ date("H:i:s", strtotime($request->created_at)) }})</small></td>
            <td>
                @if($request->isActive)
                <div style="width: 150px;">
                    <button class="btn btn-secondary confirm-payment" data-request-id="{{ $request->id }}">Выплатить</button>
                    <button class="btn btn-secondary decline-payment" data-request-id="{{ $request->id }}">Отказать</button>
                </div>
                @endif
            </td>
        </tr>
    @endforeach
@endif