@extends("app")

@section("content")
    {{ Html::style("/css/orders/orders.css") }}
    {{ Html::style("/css/paymentRequests/sweetalert.css") }}
    {{ Html::style("/css/paymentRequests/paymentRequests.css") }}
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Выплаты</h1>
        </div>
        <div class="breadcrumb-env">
            <div>Сумма к выплате: <span id="payment-summary">0</span> грн</div>
        </div>
        <div class="breadcrumb-env">
            <button class="btn btn-secondary btn-icon btn-make-request">
                <i class="fa-plus"></i>
                <span>Запросить выплату</span>
                (<span class="selected-rows-count"></span>)
            </button>
        </div>
    </div>

    <div class="orders-page-container">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-model-2 table-hover payment-requests-table">
                    <thead>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            Клиент
                        </th>
                        <th class="num-th">
                            Товары
                        </th>
                        <th class="num-th">
                            Сумма заказа
                        </th>
                        <th class="num-th">
                            Прибыль
                        </th>
                        <th>
                            Дата
                        </th>
                    </tr>
                    </thead>
                    <tbody class="middle-align">
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    {{ Html::script("/lib/js/datepicker/bootstrap-datepicker.js") }}
    {{ Html::script("/js/paymentRequests/sweetalert.min.js") }}
    {{ Html::script("/js/paymentRequests/partnerPaymentRequests.js") }}

@stop