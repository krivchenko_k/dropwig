@if(isset($orders))
    @foreach($orders as $order)
        <tr data-order-id="{{ $order->id }}">
            <td>
                <input type="checkbox" class="cbr cbx">&nbsp{{ $order->id }}
            </td>

            <td>
                {{ $order->client->lastName }} {{ $order->client->firstName }} {{  $order->clientMiddleName }}
            </td>
            <td>
                <div class="dt-goods-quantity" data-popover-content="#popover{{ $order->id }}">
                    {{ $order->goodsQuantity }}
                </div>
                <div class="popover-source" id="popover{{ $order->id }}">
                    @if(isset($order->goodsInOrder))
                        <table class="dt-goods-table">
                            <thead>
                            <tr>
                                <th class="num-th">№</th>
                                <th>Товар</th>
                                <th class="num-th">Кол-во</th>
                                <th class="num-th">Дроп цена</th>
                                <th class="num-th">Наценка</th>
                                <th class="num-th">Сумма</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($order->goodsInOrder))
                                <?php $counter = 1 ?>
                                @foreach($order->goodsInOrder as $goodsInOrder)
                                    <tr @if ($counter % 2 == 0) class="even" @endif>
                                        <td class="num-td">{{ $counter++ }}</td>
                                        <td>{{ $goodsInOrder->goods->name }}</td>
                                        <td class="num-td">{{ $goodsInOrder->quantity }}</td>
                                        <td class="num-td">{{ round($goodsInOrder->goodsPrice, 2) }}</td>
                                        <td class="num-td">{{ $goodsInOrder->markup }}</td>
                                        <td class="num-td">{{ ($goodsInOrder->goodsPrice + $goodsInOrder->markup) * $goodsInOrder->quantity }}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    @else Информация о товарах отсутствует
                    @endif
                </div>
            </td>
            <td class="num-td">{{ $order->price }}</td>
            <td class="num-td order-markup">{{ $order->markup }}</td>
            <td class="num-td">{{ $order->created_at }}</td>
        </tr>
    @endforeach
@endif

