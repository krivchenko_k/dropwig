<div class="errors-popup">
    <pre class="errors-text"></pre>
</div>
<button class="show-errors-btn btn btn-primary">
    ERRORS
</button>

<script>
    $(document).ready(function () {
        $(".show-errors-btn").click(function () {
            if ($(".errors-popup").css("display") == "none")
                $(".errors-popup").show();
            else
                $(".errors-popup").hide();
        })
    })
</script>