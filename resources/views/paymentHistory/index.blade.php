@extends("app")

@section("content")

    <div class="access-page-container">
        <h3>История выплат</h3>
        <table class="table table-model-2 table-hover payment-history-table">
            <thead>
            <tr>
                <th>№</th>
                <th>Операция</th>
                <th>Сумма</th>
                <th>Причина</th>
                <th>Дата</th>
            </tr>
            </thead>
            <tbody class="middle-align">
            @if(isset($paymentHistory))
                @foreach($paymentHistory as $paymentHistoryRecord)
                    @if ($paymentHistoryRecord->operation->id == 1 || $paymentHistoryRecord->operation->id == 2)
                        @if (isset($paymentHistoryRecord->order) )
                            <tr>
                                <td>{{ $paymentHistoryRecord->id }}</td>
                                <td>{{ $paymentHistoryRecord->operation->name }}</td>
                                <td>{{ $paymentHistoryRecord->price }}</td>
                                <td>
                                    @if($paymentHistoryRecord->operation->id == 1)
                                        Клиент
                                        {{
                                            $paymentHistoryRecord->order->client->lastName . ' '
                                            . $paymentHistoryRecord->order->client->firstName . ' '
                                            . $paymentHistoryRecord->order->client->middleName . ' '
                                            . '(№' . $paymentHistoryRecord->order->id . ')'
                                        }} успешно забрал посылку
                                    @elseif($paymentHistoryRecord->operation->id == 2)
                                        Клиент
                                        {{
                                            $paymentHistoryRecord->order->client->lastName . ' '
                                            . $paymentHistoryRecord->order->client->firstName . ' '
                                            . $paymentHistoryRecord->order->client->middleName . ' '
                                            . '(№' . $paymentHistoryRecord->order->id . ')'
                                        }} не забрал посылку
                                    @endif
                                </td>
                                <td>{{ date("d.m.Y (H:i)", strtotime($paymentHistoryRecord->created_at)) }}</td>
                            </tr>
                        @endif
                    @else
                        <tr>
                            <td>{{ $paymentHistoryRecord->id }}</td>
                            <td>{{ $paymentHistoryRecord->operation->name }}</td>
                            <td>{{ $paymentHistoryRecord->price }}</td>
                            <td>
                                @if($paymentHistoryRecord->operation->id == 3)
                                    Создан запрос на выплату средств №{{ $paymentHistoryRecord->paymentRequestId }}.
                                    ID пользователя: {{ $paymentHistoryRecord->partnerId }}
                                @elseif($paymentHistoryRecord->operation->id == 4)
                                    Запрос на выплату средств №{{ $paymentHistoryRecord->paymentRequestId }} успешно
                                    выполнен. ID пользователя: {{ $paymentHistoryRecord->partnerId }}
                                    @if(isset($paymentHistoryRecord->paymentRequest->imageLink))
                                        <br>
                                        <a target="_blank" href="{{ $paymentHistoryRecord->paymentRequest->imageLink }}">(Скрин выплаты)</a>
                                    @endif
                                @elseif($paymentHistoryRecord->operation->id == 5)
                                    Запрос на выплату средств №{{ $paymentHistoryRecord->paymentRequestId }} отклонен (Причина:
                                    {{ !empty($paymentHistoryRecord->paymentRequest->rejections->rejectionReasonId) ?
                                            $paymentHistoryRecord->paymentRequest->rejections->rejectionReason->text :
                                            (!empty($paymentHistoryRecord->paymentRequest->rejections->text)) ?
                                                $paymentHistoryRecord->paymentRequest->rejections->text :
                                                "" }}) ID пользователя: {{ $paymentHistoryRecord->partnerId }}
                                @elseif($paymentHistoryRecord->operation->id == 6)
                                    Оплата за счет баланса. ID пользователя: {{ $paymentHistoryRecord->partnerId }}
                                @endif
                            </td>
                            <td>{{ date("d.m.Y (H:i)", strtotime($paymentHistoryRecord->created_at)) }}</td>
                        </tr>
                    @endif
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
    {{ Html::script("/js/paymentHistory/paymentHistory.js") }}
@stop


