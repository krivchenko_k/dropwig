<div class="popup-content">
    <div>
        <div class="form-group">
            <input type="text" class="form-control employee-login-inp" placeholder="Логин"
                   value="{{ $employee->login or "" }}" @if (isset($employee)) readonly @endif>
        </div>
        <div class="form-group password-form-group">
            @if (!isset($employee))
                <input type="password" class="form-control employee-password-inp" placeholder="Пароль"
                       value="{{ $employee->password or "" }}">
            @else
                <input type="password" class="form-control employee-password-inp" placeholder="Новый пароль" style="display: none">
                <span class="change-password-btn">Изменить пароль</span>
            @endif
        </div>
        <div class="form-group">
            <input type="text" class="form-control employee-last-name-inp" placeholder="Фамилия"
                   value="{{ $employee->profile->lastName or "" }}">

        </div>
        <div class="form-group">
            <input type="text" class="form-control employee-first-name-inp" placeholder="Имя"
                   value="{{ $employee->profile->firstName or "" }}">
        </div>
        <div class="form-group">
            <input type="text" class="form-control employee-phone-inp" placeholder="Телефон"
                   value="{{ $employee->profile->phone or "" }}">
        </div>
        <div class="form-group">
            <select class="employee-group-id form-control">
                @foreach($userGroups as $userGroup)
                   {{--не выводим группу Партнеры--}}
                    @if ($userGroup->id != 3)
                    <option value="{{ $userGroup->id }}"
                            @if (isset($employee))
                            @if ($employee->groupId == $userGroup->id) selected @endif
                            @endif>
                        {{ $userGroup->name }}
                    </option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="button-container">
            <button class="btn btn-secondary btn-icon btn-save" data-employee-id="{{ $employee->id or ''}}">
                <span>Сохранить</span>
            </button>
            <button class="btn btn-gray btn-icon btn-close-popup">
                <span>Отмена</span>
            </button>
        </div>
    </div>
</div>
