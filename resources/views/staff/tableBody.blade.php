@foreach ($staff as $employee)
    <tr data-employee-id="{{ $employee->id }}">
        <td class="cbx-td">
            <input type="checkbox" class="cbr cbx">
        </td>
        <td class="id-td">
            {{ $employee->id }}
        </td>
        <td class="btn-td">
            <button class="btn btn-edit btn-secondary btn-xs">
                <i class="fa-pencil"></i>
            </button>
        </td>
        <td>
            {{ $employee->profile->lastName }} {{ $employee->profile->firstName }}
        </td>
        <td>
            {{ $employee->login }}
        </td>
        <td>
            {{ $employee->group->name }}
        </td>
    </tr>
@endforeach