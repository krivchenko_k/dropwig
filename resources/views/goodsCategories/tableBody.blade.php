@foreach($goodsCategories as $goodsCategory)
    <tr data-goods-category-id="{{ $goodsCategory->id }}">
        <td class="cbx-td">
            <input type="checkbox" class="cbr cbx">
        </td>
        <td class="id-td">
            {{ $goodsCategory->id }}
        </td>
        <td class="btn-td">
            <button class="btn btn-edit btn-secondary btn-xs">
                <i class="fa-pencil"></i>
            </button>
        </td>
        <td class="dblclickable">
            {{ $goodsCategory->name }}
        </td>
    </tr>
@endforeach