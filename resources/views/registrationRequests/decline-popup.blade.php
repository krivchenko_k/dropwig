<div class="popup-content">
    <div class="error-block"></div>
    <div>
        <select class="form-control sel-decline-reason">
            @if(isset($declineReasons))
                @foreach($declineReasons as $declineReason)
                    <option value="{{ $declineReason->id }}">{{ $declineReason->text }}</option>
                @endforeach
            @endif
            <option value="-1">Другая причина...</option>
        </select>
        <div class="form-group">
            <textarea class="form-control decline-reason" rows="5" placeholder="Введите причину..."></textarea>
        </div>
    </div>
    <div class="buttons-container">
        <button href="#" data-partner-id="{{ $partnerId}}" class="btn btn-secondary btn-sm btn-icon icon-left flex-item-center btn-decline-reg">Отказать</button>
        <button href="#"  class=" btn btn-gray btn-sm btn-icon icon-left flex-item-center btn-close">Отмена</button>
    </div>
</div>
