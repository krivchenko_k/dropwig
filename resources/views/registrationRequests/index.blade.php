@extends("app")

@section("content")
    {{ Html::style('/css/registrationRequests/registrationRequests.css') }}
    {{ Html::script("/js/registrationRequests/registrationRequests.js") }}

    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Заявки к регистрации</h1>
        </div>
    </div>

    <div class="reg-requests-container">
        <table class="table table-model-2 table-hover reg-requests-table">
            <thead>
            <tr>
                <th>
                    №
                </th>
                <th>
                </th>
                <th>
                    ФИО
                </th>
                <th>
                    Телефон
                </th>
                <th>
                    Зарегистрирован
                </th>
                <th>
                    IP (Город)
                </th>
            </tr>
            </thead>
            <tbody class="middle-align">
                @include("registrationRequests.tableBody")
            </tbody>
        </table>
    </div>
@stop
