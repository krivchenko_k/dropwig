@if(isset($partners))
    @foreach($partners as $partner)
        <tr data-partner-id="{{ $partner->id }}">
            <td class="id-td">
                {{ $partner->id }}
            </td>
            <td class="btn-td">
                <button class="btn btn-edit btn-secondary btn-xs">
                    <i class="fa-info-circle"></i>
                </button>
            </td>
            <td class="dblclickable">
                {{ $partner->profile->firstName }} {{ $partner->profile->lastName }}
            </td>
            <td class="dblclickable">
                {{ $partner->profile->phone }}
            </td>
            <td class="dblclickable">
                {{ date("d.m.Y (H:m)", strtotime($partner->profile->created_at)) }}
            </td>
            <td>
                {{ $partner->profile->ip }} ({{ $partner->profile->cityByIp }})
            </td>
        </tr>
    @endforeach
@endif