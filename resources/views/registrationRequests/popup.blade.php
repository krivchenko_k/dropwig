<div class="popup-content login-page-container col-xs-12">
    <div class="row">
        <form class="registration-form form-horizontal reg-edit-form login-form">
            <div class=" col-sm-6 col-xs-12">
                <label for="partner-email">E-mail</label>
                <div class="form-group">
                    <input class="partner-email form-control" type="email" name="email" id="partner-email"
                           value="{{ $partner->profile->email or ""}}">
                </div>
                <label for="partner-login">Логин</label>
                <div class="form-group">
                    <div>
                        <input class="partner-login form-control" id="partner-login" type="text" name="login"
                               value="{{ $partner->login or ""}}"
                               @if (isset($partner)) readonly @endif>
                    </div>
                </div>
                <label for="partner-nickname">Ник</label>
                <div class="form-group">
                    <div>
                        <input class="partner-nickname form-control" type="text" name="nickname" id="partner-nickname"
                               value="{{ $partner->profile->nickname or ""}}">
                    </div>
                </div>
                <label for="partner-nickname">Доп.контакты</label>
                <div class="form-group">
                    <div>
                        <input class="partner-contacts form-control" type="text" name="contacts" id="partner-contacts"
                               value="{{ $partner->profile->contacts or ""}}">
                    </div>
                </div>
                @if (isset($partner))
                    <label for="partner-reg-date">Дата регистрации</label>
                    <div class="form-group">
                        <div>
                            <input class="partner-reg-date form-control" type="text" name="regDate"
                                   id="partner-reg-date"
                                   readonly
                                   value="{{ date("d.m.Y (H:i)", strtotime($partner->profile->created_at))}}">
                        </div>
                    </div>
                    <label>Последняя активность</label>
                    <div class="form-group">
                        <div>
                            <input class="partner-last-activity form-control" type="text" name="lastActivity"
                                   id="partner-last-activity" readonly
                                   value="{{ date("d.m.Y (H:i)", strtotime($partner->profile->updated_at))}}">
                        </div>
                    </div>
                @endif
                <label>IP (Город) при регистрации</label>
                <div class="form-group">
                    <div>
                        <input class="partner-last-activity form-control" type="text" name="lastActivity"
                               id="partner-last-activity" readonly
                               value="{{ $partner->profile->ip }} ({{ $partner->profile->cityByIp}})">
                    </div>
                </div>
            </div>
            <div class=" col-sm-6 col-xs-12">
                <label for="partner-name">Имя</label>
                <div class="form-group">
                    <div>
                        <input class="partner-name form-control" type="text" name="firstName" id="partner-name"
                               value="{{ $partner->profile->firstName or ""}}">
                    </div>
                </div>
                <label for="partner-surname">Фамилия</label>
                <div class="form-group">
                    <div>
                        <input class="partner-surname form-control" type="text" name="lastName" id="partner-surname"
                               value="{{ $partner->profile->lastName or ""}}">
                    </div>
                </div>
                @if(isset($statuses))
                    <label for="partner-status">Статус</label>
                    <div class="form-group">
                        <div>
                            <select class="partner-status form-control" name="statusId" id="partner-status">
                                @if (isset($partner))
                                    <option value="{{ $partner->statusId }}">{{ $partner->status->name }}</option>
                                    <option disabled>-----------------</option>
                                @endif
                                @foreach($statuses as $status)
                                    <option value="{{ $status->id }}">{{ $status->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif


                <label for="partner-phone">Телефон</label>
                <div class="form-group ">
                    <div>
                        <input class="partner-phone form-control" type="text" name="phone" id="partner-phone"
                               value="{{ $partner->profile->phone or ""}}">
                    </div>
                </div>


                <label for="partner-card">Номер карты</label>
                <div class="form-group">
                    <div>
                        <input class="partner-card form-control" type="text" name="card" id="partner-card"
                               value="{{ $partner->profile->card or ""}}">
                    </div>
                </div>
                <label for="partner-card-owner">Владелец карты</label>
                <div class="form-group">
                    <div>
                        <input class="partner-card-owner form-control" type="text" name="cardOwner"
                               id="partner-card-owner"
                               value="{{ $partner->profile->cardOwner or "" }}" @if (isset($partner)) {{ ($partner->profile->isCardOwnerSelf == 1) ? 'readonly' : '' }} @endif>

                    </div>
                </div>
                <div class="form-group">
                    <div>
                        <label for="partner-card-owner-self">
                            <input class="cbr partner-card-owner-self" style="width: 13px" name="isCardOwnerSelf"
                                   id="partner-card-owner-self"
                                   type="checkbox" @if (isset($partner)) {{ ($partner->profile->isCardOwnerSelf == 1) ? 'checked' : '' }} @endif>
                            Партнёр является владельцем карты</label>
                    </div>
                </div>
            </div>
        </form>
    </div>
    {{--<div class="buttons-container">--}}
    {{--<button href="#" data-partner-id="{{ $partner->id }}"--}}
    {{--class="btn btn-secondary btn-sm flex-item-center btn-confirm">Подтвердить регистрацию--}}
    {{--</button>--}}
    {{--<button href="#" data-partner-id="{{ $partner->id }}"--}}
    {{--class=" btn btn-gray btn-sm flex-item-center btn-decline">Отказать--}}
    {{--</button>--}}
    {{--</div>--}}
    <div class="row">
        <div class="col-xs-4">
            <button data-partner-id="{{ $partner->id }}"
                    class="btn btn-secondary btn-sm flex-item-center btn-confirm">Подтвердить
            </button>
        </div>
        <div class="col-xs-4">
            <button data-partner-id="{{ $partner->id }}"
                    class=" btn btn-danger btn-sm flex-item-center btn-decline">Отказать
            </button>
        </div>
        <div class="col-xs-4">
            <button class="btn btn-gray btn-sm flex-item-center btn-close-popup">Отмена
            </button>
        </div>
    </div>
</div>
