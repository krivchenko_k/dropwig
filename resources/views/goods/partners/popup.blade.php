<div class="popup-content">
    <div class="row">
        <div class="col-xs-6">
            <div class="row row-aic">
                <div class="col-xs-4 title-right">
                    <label for="goods-name">Название</label>
                </div>
                <div class="col-xs-8">
                    <input class="goods-name form-control" id="goods-name" type="text" name="goods-name"
                           placeholder="Название товара" required maxlength="300" value="{{ $goods->name or "" }}" readonly>
                </div>
            </div>
            <div class="row row-aic">
                <div class="col-xs-4 title-right">
                    <label for="goods-category">Категория</label>
                </div>
                <div class="col-xs-8">
                    <input class="goods-name form-control" id="goods-name" type="text" name="goods-name" required maxlength="300" value="{{ $goods->category->name }}" readonly>
                </div>
            </div>

            <div class="row row-aic">
                <div class="col-xs-4 title-right">
                    <label for="goods-category">Цена</label>
                </div>
                <div class="col-xs-8">
                    <input class="goods-price form-control" id="goods-price" type="number" name="goods-price"
                           placeholder="Цена" required maxlength="5" value="{{ $goods->price or "" }}" readonly>
                </div>
            </div>

            <div class="row row-aic">
                <div class="col-xs-4 title-right">
                    <label for="goods-weight">Вес</label>
                </div>
                <div class="col-xs-8">
                    <input class="goods-weight form-control" id="goods-weight" type="number" name="goods-weight"
                           placeholder="Вес" required maxlength="6" value="{{ $goods->params->weight or "" }}" readonly>
                </div>
            </div>

            <div class="row row-aic">
                <div class="col-xs-4 title-right">
                    Габариты
                </div>
                <div class="col-xs-8">
                    <div class="col-xs-4 no-padding no-margin row row-aic">
                        <div class="col-xs-2 no-padding">
                            <label for="goods-width">Д</label>
                        </div>
                        <div class="col-xs-10 no-padding">
                            <input class="goods-length form-control" id="goods-length" type="number" name="goods-length"
                                   placeholder="Длина" required maxlength="3"
                                   value="{{ $goods->params->length or "" }}" readonly>
                        </div>
                    </div>
                    <div class="col-xs-4 no-padding no-margin row row-aic">
                        <div class="col-xs-2 no-padding">
                            <label for="goods-width">Ш</label>
                        </div>
                        <div class="col-xs-10 no-padding">
                            <input class="goods-width form-control" id="goods-width" type="number" name="goods-width"
                                   placeholder="Ширина" required maxlength="3"
                                   value="{{ $goods->params->width or "" }}" readonly>
                        </div>
                    </div>
                    <div class="col-xs-4 no-padding no-margin row row-aic">
                        <div class="col-xs-2 no-padding">
                            <label for="goods-height">В</label>
                        </div>
                        <div class="col-xs-10 no-padding">
                            <input class="goods-height form-control" id="goods-height" type="number" name="goods-height"
                                   placeholder="Высота" required maxlength="3"
                                   value="{{ $goods->params->height or "" }}" readonly>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row row-aic">
                <div class="col-xs-4 title-right">
                    <label for="goods-volume">Количество</label>
                </div>
                <div class="col-xs-8">
                    <input class="goods-volume form-control" id="goods-volume" type="number" name="goods-volume"
                           placeholder="Количество" required maxlength="6" value="{{ $goods->params->volume or "" }}" readonly>
                </div>
            </div>
        </div>
        <div class="col-xs-6 pl-0">
            <div class="droppable-area image-preview">
                @if (!empty($goods->params->imageLink))
                    <img src="{{ $goods->params->imageLink }}" alt="">
                @else
                    <img src="/images/goods/no_image.jpg" alt="">
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="row row-aic">
                <div class="col-xs-4 title-right">
                    <label for="goods-measure-id">Ед. измерения</label>
                </div>
                <div class="col-xs-8">
                    <input class="form-control" value="{{ $goods->params->measure->name or ""}}" readonly>
                </div>
            </div>
            <div class="row row-aic">
                <div class="col-xs-4 title-right">
                    <label for="goods-availability">В наличии</label>
                </div>
                <div class="col-xs-8">
                    <input class="goods-availability form-control" id="goods-availability" type="number"
                           name="goods-availability"
                           placeholder="В наличии" required maxlength="5"
                           value="{{ $goods->availability->availability or "" }}" readonly>
                </div>
            </div>
            <div class="row row-aic">
                <div class="col-xs-4 title-right">
                    <label for="goods-availability-type">Отправка</label>
                </div>
                <div class="col-xs-8">
                    <input class="goods-availability form-control" value="{{ $goods->availability->availabilityType->name or ""}}" readonly>
                </div>
            </div>
<!--            <div class="row row-aic">-->
<!--                <div class="col-xs-4 title-right">-->
<!--                    <label for="goods-link">Ссылка</label>-->
<!--                </div>-->
<!--                <div class="col-xs-8">-->
<!--                    <input class="goods-link form-control" id="goods-link" type="text" name="goods-link"-->
<!--                           placeholder="Ссылка" maxlength="500" value="{{ $goods->params->link or "" }}" readonly>-->
<!--                </div>-->
<!--            </div>-->
        </div>
        <div class="col-xs-6 pl-0">
            <label for="goods-short-desc">Полное описаниe</label>
            <textarea class="goods-full-desc form-control" id="goods-full-desc" readonly>{{ $goods->params->largeDescription or "" }}</textarea>
        </div>
    </div>
    <div class="flex-container">
        <div class="button-container flex-container">
            <button class="btn btn-gray btn-icon btn-close-popup">
                <span>Закрыть</span>
            </button>
        </div>
    </div>
</div>