@extends("app")
@section("content")
    {{ Html::style("/css/goods/goods.css") }}
    {{ Html::style("/css/goods/partners/goods.css") }}
    {{ Html::script("/js/goods/partners/goods.js") }}

    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Товары</h1>
        </div>
    </div>

    <div class="goods-page-container">
        <table class="table table-model-2 table-hover goods-table">
            <thead>
            <tr>
                <th>
                    ID
                </th>
                <th class="goods-img-th no-sorting">
                    Фото
                </th>
                <th>
                    Наименование
                </th>
                <th>
                    Цена
                </th>
                <th>
                    Вес
                </th>
                <th>
                    Объём
                </th>
                <th class="num-th">
                    В наличии
                </th>
                <th class="no-sorting">
                    <i class="fa-eye"></i>
                </th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
@stop