@foreach($goodsList as $goods)
    <tr data-goods-id="{{ $goods->id }}">
        <td class="id-td">
            {{ $goods->id }}
        </td>
        <td class="goods-img-td dblclickable">
            @if (!empty($goods->params["imageLink"]))
                <img src="{{ $goods->params["imageLink"] }}" alt="">
            @else
                <img src="/images/goods/no_image.jpg" alt="No image">
            @endif
        </td>
        <td class="dblclickable">
            {{ $goods->name }}
        </td>
        <td class="dblclickable">
            {{ number_format($goods->price, 2) }}
        </td>
        <td class="dblclickable">
            {{ $goods->params["weight"]}}
        </td>
        <td class="dblclickable">
            {{ $goods->params["volume"] }} {{ $goods->params["measure"]["name"] }}
        </td>
        <td class="num-td">
            @if($goods->availability->availability > 0)
                <div class="availability">
                    <i class="fa-plus"></i>
                    <span>Есть</span>
                </div>
            @else
                <div class="no-availability">
                    <i class="fa-minus"></i>
                    <span>Нет</span>
                </div>
            @endif
        </td>
        <td class="btn-td num-th">
            <button class="btn btn-view btn-secondary btn-xs">
                Подробнее
            </button>
        </td>
    </tr>
@endforeach