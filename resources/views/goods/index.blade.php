@extends("app")
@section("content")
    {{ Html::style("/css/goods/goods.css") }}
    {{ Html::script("/js/goods/goods.js") }}

    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Товары</h1>
        </div>

        <div class="breadcrumb-env">
            <button class="btn btn-secondary btn-icon add-goods-btn">
                <i class="fa-plus"></i>
                <span>Добавить</span>
            </button>

            <button class="btn btn-danger btn-icon common-delete-btn delete-goods-btn">
                <i class="fa fa-trash"></i>
                <span>Удалить</span>
                (<span class="deleted-row-count"></span>)
            </button>
        </div>
    </div>

    <div class="goods-page-container">
        <table class="table table-model-2 table-hover goods-table">
            <thead>
            <tr>
                <th class="no-sorting cbx-th">
                    <input type="checkbox" class="cbr no-sort select-all-cbx select-all-order-statuses-cbx">
                </th>
                <th>
                    ID
                </th>
                <th class="no-sorting">
                </th>
                <th class="goods-img-th no-sorting">
                    Фото
                </th>
                <th>
                    Наименование
                </th>
                <th>
                    Цена
                </th>
                <th>
                    Вес
                </th>
                <th>
                    К-во
                </th>
                <th class="no-sorting">
                    Активен
                </th>
                <th>
                    В наличии
                </th>
            </tr>
            </thead>
            <tbody>
            {{--@include("goods.tableBody")--}}

            </tbody>
        </table>
    </div>
@stop