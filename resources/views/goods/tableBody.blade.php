@foreach($goodsList as $goods)
    <tr data-goods-id="{{ $goods->id }}">
        <td class="cbx-td">
            <input type="checkbox" class="cbr cbx order-status-cbx">
        </td>
        <td class="id-td">
            {{ $goods->id }}
        </td>
        <td class="btn-td">
            <button class="btn btn-edit btn-secondary btn-xs">
                <i class="fa-pencil"></i>
            </button>
        </td>
        <td class="goods-img-td dblclickable">
            @if (!empty($goods->params["imageLink"]))
                <img src="{{ $goods->params["imageLink"] }}" alt="">
            @else
                <img src="/images/goods/no_image.jpg" alt="No image">
            @endif
        </td>
        <td class="dblclickable">
            {{ $goods->name }}
        </td>
        <td class="dblclickable">
            {{ number_format($goods->price, 2) }}
        </td>
        <td class="dblclickable">
            {{ $goods->params["weight"]}}
        </td>
        <td class="dblclickable">
            {{ $goods->params["volume"] }} {{ $goods->params["measure"]["name"] }}
        </td>
        <td>
            <input type="checkbox" class="change-goods-activity-cbx iswitch iswitch-secondary"
                   @if($goods->isActive) checked @endif>

        </td>
        <td>
            {{ number_format($goods->availability->availability,2) }}
        </td>
    </tr>
@endforeach