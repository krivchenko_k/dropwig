<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\GoodsCategoriesModel::class, function (Faker $faker, array $attributes) {
    $defaultAttributes = [
        'name' => $faker->text(100),
    ];

    return array_replace_recursive($defaultAttributes, $attributes);
});