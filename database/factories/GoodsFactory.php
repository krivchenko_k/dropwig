<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\GoodsModel::class, function(Faker $faker, array $attributes) {
    $name = $faker->sentence();

    $defaultAttributes = [
        'name'            => $name,
        'categoryId'      => random_int(1, 10),
        'price'           => random_int(100, 999999),
        'isActive'        => (bool)random_int(0, 1),
    ];

    return array_replace_recursive($defaultAttributes, $attributes);
});