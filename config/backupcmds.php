<?php
return [
	/**
	 * Directtory to place backup files
	 * Must be without trailing slashes
	 */
	'backupDir'	=> base_path() . '/backup/to_load/',
	/**
	 * array of database-aliases from config/database.php
	 * (key values of connections array)
	 */
	'databases' => [
		'mysql',
	],
	/**
	 * Exclude to backup
	 * WARNING! DO NOT REMOVE BACKUPS DIRECTORY, TO AVOID EXCESS ARCHIVE SIZE
	 */
	'exclude'   => [
		base_path().'/app/Classes',
		base_path().'/app/Console',
		base_path().'/app/Exceptions',
		base_path().'/app/Providers',
		base_path().'/backup',
		base_path().'/bootstrap',
		base_path().'/config',
		base_path().'/database',
		base_path().'/public/less',
		base_path().'/public/lib',
		base_path().'/resources/assets',
		base_path().'/resources/lang',
		base_path().'/src',
		base_path().'/tests',
		base_path().'/vendor',
		base_path().'/.git',
		base_path().'/.sql',
		base_path().'/.md',
		base_path().'/.yml',
		base_path().'/.xml',
	],
];
