<?php $__env->startSection("content"); ?>

    <?php echo e(Html::script("/js/faq/faq.js")); ?>

    <?php echo e(Html::style("/css/faq/faq.css")); ?>

    <?php echo e(Html::script("/lib/js/ckeditor/ckeditor.js")); ?>


    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Вопросы и ответы</h1>
        </div>

        <div class="breadcrumb-env">
            <button class="btn btn-secondary btn-icon add-faq-btn">
                <i class="fa-plus"></i>
                <span>Добавить</span>
            </button>

            <button class="btn btn-danger btn-icon common-delete-btn delete-faq-btn">
                <i class="fa fa-trash"></i>
                <span>Удалить</span>
                (<span class="deleted-row-count"></span>)
            </button>
        </div>
    </div>

    <div class="faq-page-container">
        <table class="table table-model-2 table-hover faq-table">
            <thead>
            <tr>
                <th class="no-sorting cbx-th">
                    <input type="checkbox" class="cbr no-sort select-all-cbx select-all-order-statuses-cbx">
                </th>
                <th class="id-th">
                    ID
                </th>
                <th class="btn-th no-sorting">
                </th>
                <th>Вопрос</th>
                <th>Ответ</th>
            </tr>
            </thead>
            <tbody class="middle-align">
            <?php echo $__env->make("settings.faq.tableBody", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </tbody>
        </table>

    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>