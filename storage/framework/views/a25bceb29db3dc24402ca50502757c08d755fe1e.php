<?php if(!empty($newsList)): ?>

    <?php $__currentLoopData = $newsList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $news): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <div class="row">
            <div class="date">
                <?php echo e(date("d.m.Y", strtotime($news->created_at))); ?>

                <span class="title">
                    <?php echo e($news->title); ?>

                </span>
            </div>
            <div class="text">
                <?php echo $news->text; ?>

            </div>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

<?php endif; ?>