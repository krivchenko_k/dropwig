<?php $__currentLoopData = $rejectionTypes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rejectionType): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
    <tr data-rejection-type-id="<?php echo e($rejectionType->id); ?>">
        <td class="cbx-td">
            <input type="checkbox" class="cbr cbx">
        </td>
        <td class="id-td">
            <?php echo e($rejectionType->id); ?>

        </td>
        <td class="btn-td">
            <button class="btn btn-edit btn-secondary btn-xs">
                <i class="fa-pencil"></i>
            </button>
        </td>
        <td class="rejection-type-name-td dblclickable">
            <?php echo e($rejectionType->name); ?>

        </td>
    </tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>