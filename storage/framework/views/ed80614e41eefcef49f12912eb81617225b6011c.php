<?php if(isset($requests)): ?>
    <?php $__currentLoopData = $requests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $request): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <tr
            <?php if(!$request->isActive && !empty($request->imageLink)): ?>
                style="background-color: #9dd4a0;"
            <?php elseif(!$request->isActive && empty($request->imageLink)): ?>
                style="background-color: #ec8a8a;"
            <?php endif; ?>
        >
            <td><?php echo e($request->id); ?></td>
            <td> <?php echo e($request->partnerProfile->lastName.' '.$request->partnerProfile->firstName); ?></td>
            <td> <?php echo e($request->price); ?></td>
            <td><?php echo e(round($request->partnerProfile->balance->amount, 2)); ?></td>
            <td>
                <?php echo e(date("d.m.Y", strtotime($request->created_at))); ?>

                <small>(<?php echo e(date("H:i:s", strtotime($request->created_at))); ?>)</small></td>
            <td>
                <?php if($request->isActive): ?>
                <div style="width: 150px;">
                    <button class="btn btn-secondary confirm-payment" data-request-id="<?php echo e($request->id); ?>">Выплатить</button>
                    <button class="btn btn-secondary decline-payment" data-request-id="<?php echo e($request->id); ?>">Отказать</button>
                </div>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
<?php endif; ?>