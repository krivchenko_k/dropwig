<?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $news): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
    <tr data-news-id="<?php echo e($news->id); ?>">
        <td class="cbx-td">
            <input type="checkbox" class="cbr cbx">
        </td>
        <td class="id-td">
            <?php echo e($news->id); ?>

        </td>
        <td class="btn-td">
            <button class="btn btn-edit btn-secondary btn-xs">
                <i class="fa-pencil"></i>
            </button>
        </td>
        <td class="dblclickable">
            <?php echo e($news->title); ?>

        </td>
        <td class="dblclickable news-text-td">
            <div>
                <?php echo e(str_limit(html_entity_decode($news->text), 200)); ?>

            </div>
        </td>
        <td class="dblclickable">
            <?php echo e(date("H:m d.m.Y", strtotime($news->created_at))); ?>

        </td>
    </tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>