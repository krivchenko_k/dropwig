<?php if(isset($partners)): ?>
    <?php $i = 1 ?>
    <?php $__currentLoopData = $partners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $partner): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <div class="row  <?php if($i % 2 == 0): ?> odd <?php endif; ?>">
            <div class="col-xs-9">
                <?php echo e(mb_strimwidth($partner->lastName ." ".$partner->firstName, 0, 100, "...")); ?>

            </div>
            <div class="col-xs-3 number">
                <?php echo e($partner->ordersCount); ?>

            </div>
        </div>
        <? $i++ ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
<?php endif; ?>