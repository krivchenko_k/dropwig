<?php $__currentLoopData = $deliveryServices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $deliveryService): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
    <tr data-delivery-service-id="<?php echo e($deliveryService->id); ?>">
        <td class="cbx-td">
            <input type="checkbox" class="cbr cbx">
        </td>
        <td class="id-td">
            <?php echo e($deliveryService->id); ?>

        </td>
        <td class="btn-td">
            <button class="btn btn-edit btn-secondary btn-xs">
                <i class="fa-pencil"></i>
            </button>
        </td>
        <td class="delivery-service-name-td dblclickable">
            <?php echo e($deliveryService->name); ?>

        </td>
    </tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>