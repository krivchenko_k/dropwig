<?php $__env->startSection("content"); ?>
    <?php echo e(Html::style("/css/moderationRequests/moderationRequests.css")); ?>

    <?php echo e(Html::script("/js/moderationRequests/moderationRequests.js")); ?>


    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Заявки на модерацию</h1>
        </div>
        <div class="breadcrumb-env">
            <button class="btn btn-danger btn-icon common-delete-btn btn-delete-orders">
                <i class="fa fa-trash"></i>
                <span>Удалить</span>
                (<span class="deleted-row-count"></span>)
            </button>
        </div>
    </div>

    <div class="orders-page-container">
        <div class="row">
            <div class="col-md-12">
                <div class="orders-container">
                    <table class="table table-model-2 table-hover orders-table">
                        <thead>
                        <tr role="row">
                            <th class="no-sorting cbx-th">
                                <input type="checkbox" class="cbr no-sort select-all-cbx">
                            </th>
                            <th class="id-th">
                                ID
                            </th>
                            <th class="btn-th no-sorting">
                                <i class="fa-pencil"></i>
                            </th>
                            <th>Покупатель</th>
                            <th class="phone-th">
                                Телефон
                            </th>
                            <th class="comment-th">
                                Комментарий
                            </th>
                            <th class="goods-th">
                                Товар
                            </th>
                            <th class="sum-th">
                                Сумма заказа
                            </th>
                            <th class="status-th">
                                Статус оплаты
                            </th>
                            <th class="crerated-at-th">
                                Добавлен
                            </th>
                            <th class="updated-at-th">
                                Изменен
                            </th>
                            <th class="delivery-th">
                                Сбособ доставки
                            </th>
                            <th class="adress-th">
                                Адрес
                            </th>
                            <th class="invoice-th num-th">
                                ТТН
                            </th>
                            <th class="back-invoice-th num-th">
                                ТТН Обр.
                            </th>
                            <th>
                                Партнёр
                            </th>
                            <th>
                                Статус заказа
                            </th>
                            <th>
                                IP
                            </th>
                            <th>
                                Оформил
                            </th>
                            <th>
                                Сдано
                            </th>
                        </tr>
                        </thead>
                        <tbody class="middle-align">
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>