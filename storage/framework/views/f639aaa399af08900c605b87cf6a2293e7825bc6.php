<?php if(isset($goodsList)): ?>
    <?php $i = 1 ?>
    <?php $__currentLoopData = $goodsList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $goods): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <div class="row  <?php if($i % 2 == 0): ?> odd <?php endif; ?>">

            <div class="col-xs-9">
                <?php echo e(mb_strimwidth($goods->name, 0, 40, "...")); ?>

            </div>
            <div class="col-xs-3 number">
                <?php echo e($goods->totalQuantity); ?>

            </div>
        </div>
        <? $i++ ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
<?php endif; ?>