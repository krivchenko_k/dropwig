<div class="popup-content">
    <div>
        <div class="form-group">
            <input type="text" class="form-control employee-login-inp" placeholder="Логин"
                   value="<?php echo e(isset($employee->login) ? $employee->login : ""); ?>" <?php if(isset($employee)): ?> readonly <?php endif; ?>>
        </div>
        <div class="form-group password-form-group">
            <?php if(!isset($employee)): ?>
                <input type="password" class="form-control employee-password-inp" placeholder="Пароль"
                       value="<?php echo e(isset($employee->password) ? $employee->password : ""); ?>">
            <?php else: ?>
                <input type="password" class="form-control employee-password-inp" placeholder="Новый пароль" style="display: none">
                <span class="change-password-btn">Изменить пароль</span>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <input type="text" class="form-control employee-last-name-inp" placeholder="Фамилия"
                   value="<?php echo e(isset($employee->profile->lastName) ? $employee->profile->lastName : ""); ?>">

        </div>
        <div class="form-group">
            <input type="text" class="form-control employee-first-name-inp" placeholder="Имя"
                   value="<?php echo e(isset($employee->profile->firstName) ? $employee->profile->firstName : ""); ?>">
        </div>
        <div class="form-group">
            <input type="text" class="form-control employee-phone-inp" placeholder="Телефон"
                   value="<?php echo e(isset($employee->profile->phone) ? $employee->profile->phone : ""); ?>">
        </div>
        <div class="form-group">
            <select class="employee-group-id form-control">
                <?php $__currentLoopData = $userGroups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $userGroup): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                   
                    <?php if($userGroup->id != 3): ?>
                    <option value="<?php echo e($userGroup->id); ?>"
                            <?php if(isset($employee)): ?>
                            <?php if($employee->groupId == $userGroup->id): ?> selected <?php endif; ?>
                            <?php endif; ?>>
                        <?php echo e($userGroup->name); ?>

                    </option>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </select>
        </div>
        <div class="button-container">
            <button class="btn btn-secondary btn-icon btn-save" data-employee-id="<?php echo e(isset($employee->id) ? $employee->id : ''); ?>">
                <span>Сохранить</span>
            </button>
            <button class="btn btn-gray btn-icon btn-close-popup">
                <span>Отмена</span>
            </button>
        </div>
    </div>
</div>
