<div class="popup-content">
    <div>
        <div>
            <input type="text" class="form-control delivery-service-name-inp" placeholder="Название службы доставки" value="<?php echo e(isset($deliveryService->name) ? $deliveryService->name : ""); ?>">
        </div>
        <div class="button-container">
            <button class="btn btn-secondary btn-icon btn-save" data-delivery-service-id="<?php echo e(isset($deliveryService->id) ? $deliveryService->id : ''); ?>">
                <span>Сохранить</span>
            </button>
            <button class="btn btn-gray btn-icon btn-close-popup">
                <span>Отмена</span>
            </button>
        </div>
    </div>
</div>

<?php echo e(Html::script("js/deliveryServices/deliveryServicesPopup.js")); ?>

