<div class="row">
    <div class="col-xs-9 stat-title">
        Заказов за сегодня
    </div>
    <div class="col-xs-3 num-td orders-per-day-count">
        <?php echo e(isset($ordersToday) ? $ordersToday : "Н/Д"); ?>

    </div>
</div>
<div class="row">
    <div class="col-xs-9 stat-title">
        Заказов за все время
    </div>
    <div class="col-xs-3 num-td orders-total-count">
        <?php echo e(isset($ordersTotal) ? $ordersTotal : "Н/Д"); ?>

    </div>
</div>
<div class="row">
    <div class="col-xs-7 stat-title">
        Баланс
    </div>
    <div class="col-xs-5 num-td balance">
        <?php echo e(number_format($balance, 2)); ?> грн.
    </div>
</div>
<div class="row">
    <div class="col-xs-7 stat-title">
        Выплачено за всё время
    </div>
    <div class="col-xs-5 num-td balance">
        <?php echo e(number_format($totalPayout, 2)); ?> грн.
    </div>
</div>
