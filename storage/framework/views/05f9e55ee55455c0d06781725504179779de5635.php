<?php $__env->startSection("content"); ?>

    <div class="access-page-container">
                                                                                            <div id="test"></div>
        <h3>Балансы партнеров</h3>
        <h4>Всего на балансе партнеров: <b><?php echo e(round($totalBalancesSum, 2)); ?></b> грн</h4>
        <h4>Максимальная сумма на балансе: <b><?php echo e(round($maxBalance, 2)); ?></b> грн</h4>
        <table class="table table-model-2 table-hover balances-table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Партнёр</th>
                <th>Номер карты</th>
                <th>На балансе</th>
            </tr>
            </thead>
            <tbody class="middle-align">
            <?php echo $__env->make("balances.tableBody", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </tbody>
        </table>
    </div>
    <?php echo e(Html::script("/js/balances/balances.js")); ?>


<?php $__env->stopSection(); ?>



<?php echo $__env->make("app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>