<div class="popup-content">
    <?php $__currentLoopData = $FAQList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $FAQelem): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>

        <div class="panel-group" id="accordion">
                <div class="panel-heading faq-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo e($FAQelem->id); ?>">
                            <?php echo e($FAQelem->question); ?>

                        </a>
                    </h4>
                </div>
                <div id="collapse-<?php echo e($FAQelem->id); ?>" class="panel-collapse collapse">
                    <div class="panel-body">
                        <?php echo $FAQelem->answer; ?>

                    </div>
                </div>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
</div>
