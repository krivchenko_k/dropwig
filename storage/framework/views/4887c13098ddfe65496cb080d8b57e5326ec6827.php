<?php $__env->startSection("content"); ?>
    <?php echo e(Html::style("/css/orders/partnerOrders.css")); ?>

    <?php echo e(Html::script("/js/orders/partnerOrders.js")); ?>

    <?php echo e(Html::script("lib/js/datepicker/bootstrap-datepicker.js")); ?>


    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Заказы</h1>
        </div>
        <div class="breadcrumb-env">
            <button class="btn btn-secondary btn-icon btn-add-order">
                <i class="fa-plus"></i>
                <span>Добавить</span>
            </button>

            <button class="btn btn-danger btn-icon common-delete-btn btn-delete-orders">
                <i class="fa fa-trash"></i>
                <span>Удалить</span>
                (<span class="deleted-row-count"></span>)
            </button>
        </div>
    </div>

    <div class="filters-holder">

    </div>
    <div class="date-filte-holder">
        <div>
            
            <select class="form-control" id="payment-type-filter">
                <option value="0">Выберите тип оплаты...</option>
                <?php if(isset($paymentTypes)): ?>
                    <?php $__currentLoopData = $paymentTypes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $paymentType): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <option value="<?php echo e($paymentType->id); ?>"><?php echo e($paymentType->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                <?php endif; ?>
            </select>
        </div>
        <div>

            
            <select class="form-control" id="delivery-service-filter">
                <option value="0">Выберите способ доставки...</option>
                <?php if(isset($deliveryServices)): ?>
                    <?php $__currentLoopData = $deliveryServices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $deliveryService): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <option value="<?php echo e($deliveryService->id); ?>"><?php echo e($deliveryService->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                <?php endif; ?>
            </select>
        </div>

        <div>
            С <input type="text"
                     class="form-control datepicker-startDate" id="datepicker-1">
        </div>
        <div>
            По <input type="text"class="form-control datepicker-endDate" id="datepicker-2">
        </div>
        <div>
            <button href="#" class="btn btn-secondary btn-sm accept-filters-btn">Применить фильтры</button>
            <button href="#" class="btn btn-secondary btn-sm clear-filters-btn">Очистить фильтры</button>
        </div>
    </div>

    <div class="orders-page-container">
        <div class="row">
            <div class="col-md-12">
                <div class="tabs-container no-dt">
                    <?php if(isset($orderStatuses)): ?>
                        <?php $__currentLoopData = $orderStatuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $orderStatus): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <div class="tab <?php if($orderStatus->id == 1): ?> <?php echo e("active"); ?> <?php endif; ?>"
                                 data-order-status-id="<?php echo e($orderStatus->id); ?>">
                                <div class="order-status-color"
                                     <?php if(!empty($orderStatus->color)): ?> style="background-color: <?php echo e($orderStatus->color); ?>" <?php endif; ?>></div>
                                <div class="order-status-title">
                                    <?php echo e($orderStatus->name); ?>

                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        <div class="tab"
                             data-order-status-id="0">
                            <div class="order-status-color"></div>
                            <div class="order-status-title">
                                Все
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="orders-container">
                    <table class="table table-model-2 table-hover orders-table">
                        <thead>
                        <tr role="row">
                            <th class="no-sorting cbx-th">
                                <input type="checkbox" class="cbr no-sort select-all-cbx">
                            </th>
                            <th class="id-th">
                                ID
                            </th>
                            <th class="btn-th no-sorting">
                                <i class="fa-pencil"></i>
                            </th>
                            <th style="width: 150px !important;">
                                Покупатель
                            </th>
                            <th class="phone-th">
                                Телефон
                            </th>
                            <th class="comment-th">
                                Комментарий
                            </th>
                            <th class="goods-th">
                                Товар
                            </th>
                            <th class="sum-th">
                                Сумма заказа
                            </th>
                            <th class="status-th">
                                Статус оплаты
                            </th>
                            <th class="crerated-at-th">
                                Добавлен
                            </th>
                            <th class="updated-at-th">
                                Изменен
                            </th>
                            <th class="delivery-th">
                                Сбособ доставки
                            </th>
                            <th class="adress-th">
                                Адрес
                            </th>
                            <th class="invoice-th num-th">
                                ТТН
                            </th>
                            <th>
                                Дата отправки
                            </th>
                        </tr>
                        </thead>
                        <tbody class="middle-align">
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>