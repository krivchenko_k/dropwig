<div class="popup-content login-page-container col-xs-12">
    <div class="row">
        <form class="registration-form form-horizontal reg-edit-form login-form">
            <div class=" col-sm-6 col-xs-12">
                <label for="partner-email">E-mail</label>
                <div class="form-group">
                    <input class="partner-email form-control" type="email" name="email" id="partner-email"
                           value="<?php echo e(isset($partner->profile->email) ? $partner->profile->email : ""); ?>">
                </div>
                <label for="partner-login">Логин</label>
                <div class="form-group">
                    <div>
                        <input class="partner-login form-control" id="partner-login" type="text" name="login"
                               value="<?php echo e(isset($partner->login) ? $partner->login : ""); ?>"
                               <?php if(isset($partner)): ?> readonly <?php endif; ?>>
                    </div>
                </div>
                <label for="partner-nickname">Ник</label>
                <div class="form-group">
                    <div>
                        <input class="partner-nickname form-control" type="text" name="nickname" id="partner-nickname"
                               value="<?php echo e(isset($partner->profile->nickname) ? $partner->profile->nickname : ""); ?>">
                    </div>
                </div>
                <label for="partner-nickname">Доп.контакты</label>
                <div class="form-group">
                    <div>
                        <input class="partner-contacts form-control" type="text" name="contacts" id="partner-contacts"
                               value="<?php echo e(isset($partner->profile->contacts) ? $partner->profile->contacts : ""); ?>">
                    </div>
                </div>
                <?php if(isset($partner)): ?>
                    <label for="partner-reg-date">Дата регистрации</label>
                    <div class="form-group">
                        <div>
                            <input class="partner-reg-date form-control" type="text" name="regDate"
                                   id="partner-reg-date"
                                   readonly
                                   value="<?php echo e(date("d.m.Y (H:i)", strtotime($partner->profile->created_at))); ?>">
                        </div>
                    </div>
                    <label>Последняя активность</label>
                    <div class="form-group">
                        <div>
                            <input class="partner-last-activity form-control" type="text" name="lastActivity"
                                   id="partner-last-activity" readonly
                                   value="<?php echo e(date("d.m.Y (H:i)", strtotime($partner->profile->updated_at))); ?>">
                        </div>
                    </div>
                <?php endif; ?>
                <label>IP (Город) при регистрации</label>
                <div class="form-group">
                    <div>
                        <input class="partner-last-activity form-control" type="text" name="lastActivity"
                               id="partner-last-activity" readonly
                               value="<?php echo e($partner->profile->ip); ?> (<?php echo e($partner->profile->cityByIp); ?>)">
                    </div>
                </div>
            </div>
            <div class=" col-sm-6 col-xs-12">
                <label for="partner-name">Имя</label>
                <div class="form-group">
                    <div>
                        <input class="partner-name form-control" type="text" name="firstName" id="partner-name"
                               value="<?php echo e(isset($partner->profile->firstName) ? $partner->profile->firstName : ""); ?>">
                    </div>
                </div>
                <label for="partner-surname">Фамилия</label>
                <div class="form-group">
                    <div>
                        <input class="partner-surname form-control" type="text" name="lastName" id="partner-surname"
                               value="<?php echo e(isset($partner->profile->lastName) ? $partner->profile->lastName : ""); ?>">
                    </div>
                </div>
                <?php if(isset($statuses)): ?>
                    <label for="partner-status">Статус</label>
                    <div class="form-group">
                        <div>
                            <select class="partner-status form-control" name="statusId" id="partner-status">
                                <?php if(isset($partner)): ?>
                                    <option value="<?php echo e($partner->statusId); ?>"><?php echo e($partner->status->name); ?></option>
                                    <option disabled>-----------------</option>
                                <?php endif; ?>
                                <?php $__currentLoopData = $statuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $status): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <option value="<?php echo e($status->id); ?>"><?php echo e($status->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </select>
                        </div>
                    </div>
                <?php endif; ?>


                <label for="partner-phone">Телефон</label>
                <div class="form-group ">
                    <div>
                        <input class="partner-phone form-control" type="text" name="phone" id="partner-phone"
                               value="<?php echo e(isset($partner->profile->phone) ? $partner->profile->phone : ""); ?>">
                    </div>
                </div>


                <label for="partner-card">Номер карты</label>
                <div class="form-group">
                    <div>
                        <input class="partner-card form-control" type="text" name="card" id="partner-card"
                               value="<?php echo e(isset($partner->profile->card) ? $partner->profile->card : ""); ?>">
                    </div>
                </div>
                <label for="partner-card-owner">Владелец карты</label>
                <div class="form-group">
                    <div>
                        <input class="partner-card-owner form-control" type="text" name="cardOwner"
                               id="partner-card-owner"
                               value="<?php echo e(isset($partner->profile->cardOwner) ? $partner->profile->cardOwner : ""); ?>" <?php if(isset($partner)): ?> <?php echo e(($partner->profile->isCardOwnerSelf == 1) ? 'readonly' : ''); ?> <?php endif; ?>>

                    </div>
                </div>
                <div class="form-group">
                    <div>
                        <label for="partner-card-owner-self">
                            <input class="cbr partner-card-owner-self" style="width: 13px" name="isCardOwnerSelf"
                                   id="partner-card-owner-self"
                                   type="checkbox" <?php if(isset($partner)): ?> <?php echo e(($partner->profile->isCardOwnerSelf == 1) ? 'checked' : ''); ?> <?php endif; ?>>
                            Партнёр является владельцем карты</label>
                    </div>
                </div>
            </div>
        </form>
    </div>
    
    
    
    
    
    
    
    
    <div class="row">
        <div class="col-xs-4">
            <button data-partner-id="<?php echo e($partner->id); ?>"
                    class="btn btn-secondary btn-sm flex-item-center btn-confirm">Подтвердить
            </button>
        </div>
        <div class="col-xs-4">
            <button data-partner-id="<?php echo e($partner->id); ?>"
                    class=" btn btn-danger btn-sm flex-item-center btn-decline">Отказать
            </button>
        </div>
        <div class="col-xs-4">
            <button class="btn btn-gray btn-sm flex-item-center btn-close-popup">Отмена
            </button>
        </div>
    </div>
</div>
