<?php $__env->startSection("content"); ?>

    <?php echo e(Html::style("/css/faq/faq.css")); ?>

    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Вопросы и ответы</h1>
        </div>
    </div>

    <div class="faq-page-container">
        <?php $__currentLoopData = $FAQList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $FAQelem): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <div class="panel-group" id="accordion">
                <div class="faq-panel-heading faq-heading">
                    <h4 class="faq-panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo e($FAQelem->id); ?>">
                            <?php echo e($FAQelem->question); ?>

                        </a>
                    </h4>
                </div>
                <div id="collapse-<?php echo e($FAQelem->id); ?>" class="panel-collapse collapse">
                    <div class="faq-panel-body">
                        <?php echo $FAQelem->answer; ?>

                    </div>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make("app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>