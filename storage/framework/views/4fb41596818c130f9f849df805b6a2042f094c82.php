<?php echo $__env->make("commons.header", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo e(Html::script("/lib/js/jquery.validate.min.js")); ?>

<?php echo e(Html::script("/lib/js/additional-methods.min.js")); ?>

<?php echo e(Html::script("/lib/js/jquery.mask.min.js")); ?>


<?php echo e(Html::style('/css/registration/registration.css')); ?>

<?php echo e(Html::script("/js/registration/registration.js")); ?>


<div class="reg-page-container fade-in-effect">
<!--    <div class="site-name">-->
<!--        <?php echo e($_SERVER["SERVER_NAME"]); ?>-->
<!--    </div>-->

    <div class="reg-form-container">
        <form class="reg-form login-form" action="/registration" method="POST">
            <?php echo csrf_field(); ?>


            <div class="row">
                <div class="col-sm-6 col-xs-10">
                    <label class="control-label">E-mail</label>
                    <div class="form-group">
                        <input class="partner-email form-control" type="email" name="email" placeholder="E-mail">
                    </div>
                    <label class="control-label">Логин</label>
                    <div class="form-group">
                        <input class="partner-login form-control" type="text" name="login" required placeholder="Логин">
                    </div>
                    <label class="control-label">Пароль</label>
                    <div class="form-group">
                        <input class="partner-password form-control" type="password" name="password" placeholder="Пароль">
                    </div>
                    <label class="control-label">Повторите пароль</label>
                    <div class="form-group">
                        <input class="partner-password-confirm form-control" type="password" name="passwordConfirm" placeholder="Повторите пароль">
                        <div class="error-block"></div>
                    </div>
                    <label class="control-label">Ник</label>
                    <div class="form-group">
                        <input class="partner-nickname form-control" type="text" name="nickname" placeholder="Ник">
                    </div>
                    <label class="control-label">Доп.контакты</label>
                    <div class="form-group">
                        <input class="partner-contacts form-control" type="text" name="contacts" placeholder="Skype/ICQ/Доп.номер и пр">
                    </div>
                </div>

                <div class="col-sm-6 col-xs-10">
                    <label class="control-label">Имя</label>
                    <div class="form-group">
                        <input class="partner-name form-control" type="text" name="firstName" required placeholder="Имя">
                    </div>
                    <label class="control-label">Фамилия</label>
                    <div class="form-group">
                        <input class="partner-surname form-control" type="text" name="lastName" placeholder="Фамилия">
                    </div>
                    <label class="control-label">Телефон</label>
                    <div class="form-group">
                        <input class="partner-phone form-control" type="text" name="phone" placeholder="Телефон">
                    </div>
                    <label class="control-label">Карта ПриватБанка для выплат <small>(не обязательно)</small></label>
                    <div class="form-group">
                        <input class="partner-card form-control" type="text" name="card" placeholder="Номер кредитной карты">
                    </div>
                    <label class="control-label">Владелец карты <small>(не обязательно)</small></label>
                    <div class="form-group">
                        <input class="partner-card-owner form-control" type="text" name="cardOwner" placeholder="Владелец карты">
                    </div>
                    <div>
                        <label class="card-owner-label" disabled>
                            <input class="cbr partner-card-owner-self" name="isCardOwnerSelf" type="checkbox">Карта оформлена на меня
                        </label>
                    </div>

                    <div>
                        
                            <input class="cbr agree-with-policy" name="agree-with-policy" type="checkbox"> Я принимаю <span class="show-policy">политику конфиденциальности</span>
                        
                    </div>
                </div>

                <div class="col-xs-12 reg-btn-container">
                    <div class="col-xs-10 form-group">
                        <input class="btn btn-secondary btn-block reg-submit-button"
                               type="button" value="Зарегистрироваться">
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="buttons-container">
        <div class="back-btn">
            <i class="fa-arrow-circle-left"></i>
            Назад
        </div>
    </div>
</div>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter39922815 = new Ya.Metrika({
                    id:39922815,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js ";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/39922815 " style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<?php echo $__env->make("commons.popup", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->make("commons.footer", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>