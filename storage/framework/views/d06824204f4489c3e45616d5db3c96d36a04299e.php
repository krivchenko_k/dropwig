<?php if(isset($orders)): ?>
    <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <tr data-order-id="<?php echo e($order->id); ?>">
            <td>
                <input type="checkbox" class="cbr cbx">&nbsp<?php echo e($order->id); ?>

            </td>

            <td>
                <?php echo e($order->client->lastName); ?> <?php echo e($order->client->firstName); ?> <?php echo e($order->clientMiddleName); ?>

            </td>
            <td>
                <div class="dt-goods-quantity" data-popover-content="#popover<?php echo e($order->id); ?>">
                    <?php echo e($order->goodsQuantity); ?>

                </div>
                <div class="popover-source" id="popover<?php echo e($order->id); ?>">
                    <?php if(isset($order->goodsInOrder)): ?>
                        <table class="dt-goods-table">
                            <thead>
                            <tr>
                                <th class="num-th">№</th>
                                <th>Товар</th>
                                <th class="num-th">Кол-во</th>
                                <th class="num-th">Дроп цена</th>
                                <th class="num-th">Наценка</th>
                                <th class="num-th">Сумма</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(isset($order->goodsInOrder)): ?>
                                <?php $counter = 1 ?>
                                <?php $__currentLoopData = $order->goodsInOrder; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $goodsInOrder): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <tr <?php if($counter % 2 == 0): ?> class="even" <?php endif; ?>>
                                        <td class="num-td"><?php echo e($counter++); ?></td>
                                        <td><?php echo e($goodsInOrder->goods->name); ?></td>
                                        <td class="num-td"><?php echo e($goodsInOrder->quantity); ?></td>
                                        <td class="num-td"><?php echo e(round($goodsInOrder->goodsPrice, 2)); ?></td>
                                        <td class="num-td"><?php echo e($goodsInOrder->markup); ?></td>
                                        <td class="num-td"><?php echo e(($goodsInOrder->goodsPrice + $goodsInOrder->markup) * $goodsInOrder->quantity); ?></td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    <?php else: ?> Информация о товарах отсутствует
                    <?php endif; ?>
                </div>
            </td>
            <td class="num-td"><?php echo e($order->price); ?></td>
            <td class="num-td order-markup"><?php echo e($order->markup); ?></td>
            <td class="num-td"><?php echo e($order->created_at); ?></td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
<?php endif; ?>

