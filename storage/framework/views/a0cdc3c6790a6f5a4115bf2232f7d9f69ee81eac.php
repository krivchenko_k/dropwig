<div class="popup-frame login-page popup-fade-in-effect">
    <div class="modal2">
        <div class="modal_container common-popup">
            <div class="popup-header">
                <div class="panel-options">
                    <a href="#" data-toggle="remove" class="close-popup-btn">
                        &times;
                    </a>
                </div>
                <div class="panel panel-color panel-success">
                    <div class="panel-heading ">
                        <div class="panel-title-container">
                            <h3 class="panel-title">
                            </h3>
                        </div>
                    </div>
                    <div class="panel-body">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
