<?php $__currentLoopData = $FAQList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $FAQElem): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
    <tr data-faq-id="<?php echo e($FAQElem->id); ?>">
        <td class="cbx-td">
            <input type="checkbox" class="cbr cbx order-status-cbx">
        </td>
        <td class="id-td">
            <?php echo e($FAQElem->id); ?>

        </td>
        <td class="btn-td">
            <button class="btn btn-edit btn-secondary btn-xs">
                <i class="fa-pencil"></i>
            </button>
        </td>
        <td class="dblclickable">
            <?php echo e($FAQElem->question); ?>

        </td>
        <td class="dblclickable answer-td">
            <div>
                <?php echo e(str_limit(html_entity_decode($FAQElem->answer), 200)); ?>

            </div>
        </td>
    </tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>