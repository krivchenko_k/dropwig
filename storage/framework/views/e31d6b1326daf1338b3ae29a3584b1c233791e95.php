
<div class="popup-content">
    <div>
        <div>
            <input type="text" class="form-control news-title-inp" placeholder="Заголовок новости" value="<?php echo e(isset($news->title) ? $news->title : ""); ?>">
        </div>
        <div>
            <textarea name="news-text" class="news-text-inp" placeholder="Текст новости" value="<?php echo isset($news->text) ? $news->text : ''; ?>"><?php echo isset($news->text) ? $news->text : ''; ?></textarea>
        </div>
        <div class="button-container">
            <button class="btn btn-secondary btn-icon btn-save-news" data-news-id="<?php echo e(isset($news->id) ? $news->id : ''); ?>">
                <span>Сохранить</span>
            </button>
            <button class="btn btn-gray btn-icon btn-close-popup">
                <span>Отмена</span>
            </button>
        </div>
    </div>
</div>