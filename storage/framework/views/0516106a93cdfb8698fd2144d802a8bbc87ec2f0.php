<?php $__env->startSection("content"); ?>

    <?php echo e(Html::style("/css/mainPage/partner/mainPage.css")); ?>

    <?php echo e(Html::script("/js/mainPage/partner/mainPage.js")); ?>


    <div class="partner-main-page-container fade-in-effect">
        <div>
            <div class="col-xs-4 flex-container-column no-padding">
                <div class="info-conrainer">
                    <div class="hello">
                        <div>
                            Здравствуйте, <?php echo e(\Illuminate\Support\Facades\Session::get("user")->profile->firstName); ?>!
                        </div>
                        <div style="font-size: 18px; color: #000;">
                            Ваш ID: <?php echo e(\Illuminate\Support\Facades\Session::get("user")->id); ?>

                        </div>
                    </div>
                    <div>
                        <div class="goods-stat">
                            <div class="loader"></div>
                            <div class="fade-in-effect content">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="rating-container top-partners-container">
                    <div class="title">ТОП-5 партнёров за неделю</div>
                    <div class="col-xs-12">
<!--                        <div class="col-xs-5 num-th pl-0">-->
<!--                            Заработано-->
<!--                        </div>-->
                    </div>
                    <div class="col-xs-12 content">
                        <div class="loader"></div>
                        <div class="fade-in-effect stat-holder">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 news-container">
                <div class="row main-title">
                    Новости
                </div>
                <?php echo $__env->make("mainPage.partner.news", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>