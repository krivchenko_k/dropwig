<?php $__env->startSection("content"); ?>

    <div class="access-page-container">
        <h3>История выплат</h3>
        <table class="table table-model-2 table-hover payment-history-table">
            <thead>
            <tr>
                <th>№</th>
                <th>Операция</th>
                <th>Сумма</th>
                <th>Причина</th>
                <th>Дата</th>
            </tr>
            </thead>
            <tbody class="middle-align">
            <?php if(isset($paymentHistory)): ?>
                <?php $__currentLoopData = $paymentHistory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $paymentHistoryRecord): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <?php if($paymentHistoryRecord->operation->id == 1 || $paymentHistoryRecord->operation->id == 2): ?>
                        <?php if(isset($paymentHistoryRecord->order) ): ?>
                            <tr>
                                <td><?php echo e($paymentHistoryRecord->id); ?></td>
                                <td><?php echo e($paymentHistoryRecord->operation->name); ?></td>
                                <td><?php echo e($paymentHistoryRecord->price); ?></td>
                                <td>
                                    <?php if($paymentHistoryRecord->operation->id == 1): ?>
                                        Клиент
                                        <?php echo e($paymentHistoryRecord->order->client->lastName . ' '
                                            . $paymentHistoryRecord->order->client->firstName . ' '
                                            . $paymentHistoryRecord->order->client->middleName . ' '
                                            . '(№' . $paymentHistoryRecord->order->id . ')'); ?> успешно забрал посылку
                                    <?php elseif($paymentHistoryRecord->operation->id == 2): ?>
                                        Клиент
                                        <?php echo e($paymentHistoryRecord->order->client->lastName . ' '
                                            . $paymentHistoryRecord->order->client->firstName . ' '
                                            . $paymentHistoryRecord->order->client->middleName . ' '
                                            . '(№' . $paymentHistoryRecord->order->id . ')'); ?> не забрал посылку
                                    <?php endif; ?>
                                </td>
                                <td><?php echo e(date("d.m.Y (H:i)", strtotime($paymentHistoryRecord->created_at))); ?></td>
                            </tr>
                        <?php endif; ?>
                    <?php else: ?>
                        <tr>
                            <td><?php echo e($paymentHistoryRecord->id); ?></td>
                            <td><?php echo e($paymentHistoryRecord->operation->name); ?></td>
                            <td><?php echo e($paymentHistoryRecord->price); ?></td>
                            <td>
                                <?php if($paymentHistoryRecord->operation->id == 3): ?>
                                    Создан запрос на выплату средств №<?php echo e($paymentHistoryRecord->paymentRequestId); ?>.
                                    ID пользователя: <?php echo e($paymentHistoryRecord->partnerId); ?>

                                <?php elseif($paymentHistoryRecord->operation->id == 4): ?>
                                    Запрос на выплату средств №<?php echo e($paymentHistoryRecord->paymentRequestId); ?> успешно
                                    выполнен. ID пользователя: <?php echo e($paymentHistoryRecord->partnerId); ?>

                                    <?php if(isset($paymentHistoryRecord->paymentRequest->imageLink)): ?>
                                        <br>
                                        <a target="_blank" href="<?php echo e($paymentHistoryRecord->paymentRequest->imageLink); ?>">(Скрин выплаты)</a>
                                    <?php endif; ?>
                                <?php elseif($paymentHistoryRecord->operation->id == 5): ?>
                                    Запрос на выплату средств №<?php echo e($paymentHistoryRecord->paymentRequestId); ?> отклонен (Причина:
                                    <?php echo e(!empty($paymentHistoryRecord->paymentRequest->rejections->rejectionReasonId) ?
                                            $paymentHistoryRecord->paymentRequest->rejections->rejectionReason->text :
                                            (!empty($paymentHistoryRecord->paymentRequest->rejections->text)) ?
                                                $paymentHistoryRecord->paymentRequest->rejections->text :
                                                ""); ?>) ID пользователя: <?php echo e($paymentHistoryRecord->partnerId); ?>

                                <?php elseif($paymentHistoryRecord->operation->id == 6): ?>
                                    Оплата за счет баланса. ID пользователя: <?php echo e($paymentHistoryRecord->partnerId); ?>

                                <?php endif; ?>
                            </td>
                            <td><?php echo e(date("d.m.Y (H:i)", strtotime($paymentHistoryRecord->created_at))); ?></td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
    <?php echo e(Html::script("/js/paymentHistory/paymentHistory.js")); ?>

<?php $__env->stopSection(); ?>



<?php echo $__env->make("app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>