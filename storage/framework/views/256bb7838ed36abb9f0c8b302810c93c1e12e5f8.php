<?php $__currentLoopData = $availabilityTypes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $availabilityType): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
    <tr data-availability-type-id="<?php echo e($availabilityType->id); ?>">
        <td class="cbx-td">
            <input type="checkbox" class="cbr cbx">
        </td>
        <td class="id-td">
            <?php echo e($availabilityType->id); ?>

        </td>
        <td class="btn-td">
            <button class="btn btn-edit btn-secondary btn-xs">
                <i class="fa-pencil"></i>
            </button>
        </td>
        <td class="dblclickable">
            <?php echo e($availabilityType->name); ?>

        </td>
    </tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>