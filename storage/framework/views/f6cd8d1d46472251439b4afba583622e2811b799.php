<?php if(isset($partnersOnline)): ?>
    <?php $i = 1
    ?>
    <?php $__currentLoopData = $partnersOnline; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $partnerOnline): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <div class="row  <?php if($i % 2 == 0): ?> odd <?php endif; ?>">
            <div class="col-xs-8">
                <?php echo e(mb_strimwidth($partnerOnline->lastName ." ".$partnerOnline->firstName, 0, 100, "...")); ?>

            </div>
            <div class="col-xs-4 number">
                <?php echo e($partnerOnline->lastActivity); ?>

            </div>
        </div>
        <? $i++ ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
<?php else: ?>
    <div class="row">
        <div class="col-xs-12">
            <p class="bg-info">Партнёров нет онлайн</p>
        </div>
    </div>
<?php endif; ?>