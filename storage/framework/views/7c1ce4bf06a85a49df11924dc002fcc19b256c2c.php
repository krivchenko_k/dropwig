<?php $__currentLoopData = $goodsList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $goods): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
    <tr data-goods-id="<?php echo e($goods->id); ?>">
        <td class="id-td">
            <?php echo e($goods->id); ?>

        </td>
        <td class="goods-img-td dblclickable">
            <?php if(!empty($goods->params["imageLink"])): ?>
                <img src="<?php echo e($goods->params["imageLink"]); ?>" alt="">
            <?php else: ?>
                <img src="/images/goods/no_image.jpg" alt="No image">
            <?php endif; ?>
        </td>
        <td class="dblclickable">
            <?php echo e($goods->name); ?>

        </td>
        <td class="dblclickable">
            <?php echo e(number_format($goods->price, 2)); ?>

        </td>
        <td class="dblclickable">
            <?php echo e($goods->params["weight"]); ?>

        </td>
        <td class="dblclickable">
            <?php echo e($goods->params["volume"]); ?> <?php echo e($goods->params["measure"]["name"]); ?>

        </td>
        <td class="num-td">
            <?php if($goods->availability->availability > 0): ?>
                <div class="availability">
                    <i class="fa-plus"></i>
                    <span>Есть</span>
                </div>
            <?php else: ?>
                <div class="no-availability">
                    <i class="fa-minus"></i>
                    <span>Нет</span>
                </div>
            <?php endif; ?>
        </td>
        <td class="btn-td num-th">
            <button class="btn btn-view btn-secondary btn-xs">
                Подробнее
            </button>
        </td>
    </tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>