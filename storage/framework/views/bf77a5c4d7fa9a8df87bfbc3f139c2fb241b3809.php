<div class="main-content fade-in-effect">

    <?php if(\Illuminate\Support\Facades\Session::get("user")->groupId == 1 || \Illuminate\Support\Facades\Session::get("user")->groupId == 2): ?>
        <?php echo e(Html::script("/js/commons/admin/topbar.js")); ?>

    <?php elseif(\Illuminate\Support\Facades\Session::get("user")->groupId == 3): ?>
        <?php echo e(Html::script("/js/commons/partner/topbar.js")); ?>

        <?php echo e(Html::style("/css/commons/topbar.css")); ?>

    <?php endif; ?>

    <nav class="navbar user-info-navbar" role="navigation">
        <!-- Left links for user info navbar -->
        <ul class="user-info-menu left-links list-inline list-unstyled">
            <li class="hidden-xs hover-line toogle-menu">
                <a href="#" data-toggle="sidebar">
                    <i class="fa-bars"></i>
                </a>
            </li>

            <?php if(\Illuminate\Support\Facades\Session::get("user")->groupId != 3): ?>
                <li class="hover-line">
                    <a href="/reg-requests" class="menu-link">
                        <div class="text">
                            Заявки к регистрации
                        </div>
                    </a>
                </li>
                <li class="hover-line">
                    <a href="/moderation-requests" class="menu-link">
                        <div class="text">
                            Заказы к модерации
                        </div>
                    </a>
                </li>
                <li class="hover-line">
                    <a href="/payment-requests" class="menu-link">
                        <div class="text">
                            Заявки к выплате
                        </div>
                    </a>
                </li>
            <?php endif; ?>
        </ul>

        <!-- Right links for user info navbar -->
        <ul class="user-info-menu right-links list-inline list-unstyled">
            <?php if(\Illuminate\Support\Facades\Session::get("user")->groupId == 3): ?>
                <li class="hover-line">
                    <a STYLE="height: 76px; font-weight: bold; color: green;" id="balance">
                        <div class="loader"></div>
                        <div style="display: none">
                            Баланс:
                            <span id="amount"></span>
                            грн.
                        </div>
                    </a>
                </li>
            <?php endif; ?>
            <li class="dropdown user-profile hover-line">
                <a href="#" data-toggle="dropdown">
                    <span style="width: 200px; text-align: center;">
                        <?php echo e(Session::get("user")->profile->firstName); ?> <?php echo e(Session::get("user")->profile->lastName); ?>

                        (id:<?php echo e(Session::get("user")->id); ?>)
                        <i class="fa-angle-down"></i>
                    </span>
                </a>
                <ul class="dropdown-menu user-profile-menu list-unstyled">
                    <?php if(\Illuminate\Support\Facades\Session::get("user")->groupId == 3): ?>
                        <li class="profile-btn" style="cursor: pointer">
                            <a>
                                <i class="el-user"></i>
                                Профиль
                            </a>
                        </li>
                    <?php endif; ?>
                    <li>
                        <a href="/faq">
                            <i class="fa-info"></i>
                            FAQ
                        </a>
                    </li>
                </ul>
            </li>
            <li class="hover-line">
                <a href="/logout">
                    <i class="fa-sign-out"></i>
                    Выход
                </a>
            </li>

        </ul>
    </nav>

