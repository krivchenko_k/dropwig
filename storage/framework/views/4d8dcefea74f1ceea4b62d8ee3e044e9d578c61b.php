<?php if(isset($partners)): ?>
    <?php $__currentLoopData = $partners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $partner): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <tr data-partner-id="<?php echo e($partner->id); ?>">
            <td class="id-td">
                <?php echo e($partner->id); ?>

            </td>
            <td class="btn-td">
                <button class="btn btn-edit btn-secondary btn-xs">
                    <i class="fa-info-circle"></i>
                </button>
            </td>
            <td class="dblclickable">
                <?php echo e($partner->profile->firstName); ?> <?php echo e($partner->profile->lastName); ?>

            </td>
            <td class="dblclickable">
                <?php echo e($partner->profile->phone); ?>

            </td>
            <td class="dblclickable">
                <?php echo e(date("d.m.Y (H:m)", strtotime($partner->profile->created_at))); ?>

            </td>
            <td>
                <?php echo e($partner->profile->ip); ?> (<?php echo e($partner->profile->cityByIp); ?>)
            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
<?php endif; ?>