<?php if(isset($orders)): ?>
    <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <tr data-order-id="<?php echo e($order->id); ?>" >
            <td class="cbx-td"
                <?php if(!empty($order->status->color)): ?> style="background-color: <?php echo e($order->status->color); ?>" <?php endif; ?>>
                <input type="checkbox" class="cbr cbx order-status-cbx">
            </td>
            <td class="id-td">
                <?php echo e($order->id); ?>

            </td>
            <td class="btn-td">
                <a class="btn btn-secondary btn-edit" data-order-id="<?php echo e($order->id); ?>">
                    <i class="fa-pencil"></i>
                </a>
            </td>
            <td><?php echo e($order->client->lastName); ?> <?php echo e($order->client->firstName); ?></td>
            <td class="phone-td"><?php echo e($order->phone); ?></td>
            <td>
                <div class="comment-container">
                    <div class="dt-comment" data-popover-content="#comment-<?php echo e($order->id); ?>">
                        ?
                    </div>
                    <div>
                        <small>
                            <?php echo e(str_limit($order->details->comment, 10, "...")); ?>

                        </small>
                    </div>
                    <div class="popover-source" id="comment-<?php echo e($order->id); ?>">
                        <?php echo e($order->details->comment); ?>

                    </div>
                </div>
            </td>
            <td class="goods-td">
                <div class="dt-goods-quantity" data-popover-content="#goods-<?php echo e($order->id); ?>">
                    <?php echo e($order->goodsQuantity); ?>

                </div>
                <div class="popover-source" id="goods-<?php echo e($order->id); ?>">
                    <?php if(isset($order->goodsInOrder)): ?>
                        <table class="dt-goods-table">
                            <thead>
                            <tr>
                                <th class="num-th">№</th>
                                <th>Товар</th>
                                <th class="num-th">Кол-во</th>
                                <th class="num-th">Дроп цена</th>
                                <th class="num-th">Наценка</th>
                                <th class="num-th">Сумма</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(isset($order->goodsInOrder)): ?>
                                <?php $counter = 1 ?>
                                <?php $__currentLoopData = $order->goodsInOrder; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $goodsInOrder): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <tr <?php if($counter % 2 == 0): ?> class="even" <?php endif; ?>>
                                        <td class="num-td"><?php echo e($counter++); ?></td>
                                        <td><?php echo e($goodsInOrder->goods->name); ?></td>
                                        <td class="num-td"><?php echo e($goodsInOrder->quantity); ?></td>
                                        <td class="num-td"><?php echo e(round($goodsInOrder->goodsPrice, 2)); ?></td>
                                        <td class="num-td"><?php echo e($goodsInOrder->markup); ?></td>
                                        <td class="num-td"><?php echo e(($goodsInOrder->goodsPrice + $goodsInOrder->markup) * $goodsInOrder->quantity); ?></td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    <?php else: ?> Информация о товарах отсутствует
                    <?php endif; ?>
                </div>
            </td>
            <td class="sum-td">
                <?php echo e(number_format($order->price, 2)); ?> грн.
            </td>
            <td><?php echo e($order->paymentType->name); ?></td>
            <td class="crerated-at-td">
                <small>
                    <?php echo e(date("d.m.Y", strtotime($order->created_at))); ?>

                    <small><?php echo e(date("(H:m)", strtotime($order->created_at))); ?></small>
                </small>
            </td>
            <td class="updated-at-td">
                <small>
                    <?php echo e(date("d.m.Y", strtotime($order->updated_at))); ?>

                    <small><?php echo e(date("(H:m)", strtotime($order->updated_at))); ?></small>
                </small>
            </td>
            <td>
                <img class="delivery-icon"
                     src="/images/icons/delivery/<?php echo e($order->deliveryParams->deliveryService->id); ?>.png" alt="">
                <?php echo e(isset($order->deliveryParams->deliveryService->name) ? $order->deliveryParams->deliveryService->name : ""); ?>

            </td>
            <td>
                <small>
                    <?php if($order->deliveryParams->deliveryService->id == 2): ?>
                        <?php echo e($order->deliveryParams->NPCity["name"].", ".$order->deliveryParams->NPWarehouse["name"]); ?>

                    <?php else: ?>
                        <?php echo e($order->deliveryParams->commonCity["name"].", ".$order->deliveryParams->commonAddress["name"]); ?>

                    <?php endif; ?>
                </small>
            </td>
            <td class="num-td">
                <?php echo e($order->invoice); ?>

            </td>
            <td class="num-td">
                <?php echo e($order->backwardInvoice); ?>

            </td>
            <td>
                <?php echo e($order->partner->profile->lastName); ?>

                <?php echo e($order->partner->profile->firstName); ?>

            </td>
            <td>
                <?php echo e($order->status->name); ?>

            </td>
            <td>
                <?php echo e(isset($order->details->ip) ? $order->details->ip : ""); ?>

            </td>
            <td>
                <?php if(isset($order->staff)): ?>
                    <?php echo e($order->staff->profile->lastName); ?>

                    <?php echo e($order->staff->profile->firstName); ?>

                <?php endif; ?>
            </td>
            <td>
                <?php if(isset($order->delivered_at)): ?>
                    <?php echo e(date("d.m.Y", strtotime($order->delivered_at))); ?>

                    <small><?php echo e(date("(H:m)", strtotime($order->delivered_at))); ?></small>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
<?php endif; ?>

