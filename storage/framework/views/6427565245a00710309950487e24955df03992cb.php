<?php $__currentLoopData = $rejectionReasons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rejectionReason): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
    <tr data-rejection-reason-id="<?php echo e($rejectionReason->id); ?>">
        <td class="cbx-td">
            <input type="checkbox" class="cbr cbx">
        </td>
        <td class="id-td">
            <?php echo e($rejectionReason->id); ?>

        </td>
        <td class="btn-td">
            <button class="btn btn-edit btn-secondary btn-xs">
                <i class="fa-pencil"></i>
            </button>
        </td>
        <td class="dblclickable">
            <?php echo e(isset($rejectionReason->rejectionType->name) ? $rejectionReason->rejectionType->name : ""); ?>

        </td>
        <td class="dblclickable">
            <?php echo e($rejectionReason->text); ?>

        </td>
    </tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
