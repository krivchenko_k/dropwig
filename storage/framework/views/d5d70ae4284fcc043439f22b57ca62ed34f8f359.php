<div class="page-container">
    <div class="sidebar-menu toggle-others">
        <div class="sidebar-menu-inner">
            <header class="logo-env">
                <!-- This will toggle the mobile menu and will be visible only on mobile devices -->
                <div class="mobile-menu-toggle visible-xs">

                    <a data-toggle="mobile-menu">
                        <i class="fa-bars"></i>
                    </a>
                </div>
            </header>


            <ul id="main-menu" class="main-menu">
                <li>
                    <a href="/">
                        <span class="title go-main-page-span">Главная</span>
                    </a>
                </li>

                <li>
                    <a href="/orders">
                        <i class="fa-shopping-cart"></i>
                        <span class="title">Перечень заказов</span>
                    </a>
                </li>


                <li>
                    <a href="/catalog">
                        <i class="fa-list-alt"></i>
                        <span class="title">Каталог</span>
                    </a>
                </li>

                <li>
                    <a href="/statistics">
                        <i class="fa-line-chart"></i>
                        <span class="title">Статистика</span>
                    </a>
                </li>

                <li>
                    <a>
                        <i class="linecons-money"></i>
                        <span class="title">Выплаты</span>
                    </a>
                    <ul>
                        <li>
                            <a href="/payment-requests">
                                <span class="title">К выплате</span>
                            </a>
                        </li>
                        <li>
                            <a href="/payment-history">
                                <span class="title">История выплат</span>
                            </a>
                        </li>
                    </ul>
                </li>


                <li class="profile-btn">
                    <a>
                        <i class="linecons-cog"></i>
                        <span class="title">Редактировать данные</span>
                    </a>
                </li>

                
                    
                        
                        
                    
                


                <li>
                    <a href="/faq">
                        <i class="fa-question"></i>
                        <span class="title">Как это работает?</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>

<?php echo e(Html::script("/js/commons/menu.js")); ?>

<?php echo e(Html::style("/css/commons/menu.css")); ?>