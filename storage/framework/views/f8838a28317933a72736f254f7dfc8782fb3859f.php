<?php $__currentLoopData = $goodsList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $goods): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
    <tr data-goods-id="<?php echo e($goods->id); ?>">
        <td class="cbx-td">
            <input type="checkbox" class="cbr cbx order-status-cbx">
        </td>
        <td class="id-td">
            <?php echo e($goods->id); ?>

        </td>
        <td class="btn-td">
            <button class="btn btn-edit btn-secondary btn-xs">
                <i class="fa-pencil"></i>
            </button>
        </td>
        <td class="goods-img-td dblclickable">
            <?php if(!empty($goods->params["imageLink"])): ?>
                <img src="<?php echo e($goods->params["imageLink"]); ?>" alt="">
            <?php else: ?>
                <img src="/images/goods/no_image.jpg" alt="No image">
            <?php endif; ?>
        </td>
        <td class="dblclickable">
            <?php echo e($goods->name); ?>

        </td>
        <td class="dblclickable">
            <?php echo e(number_format($goods->price, 2)); ?>

        </td>
        <td class="dblclickable">
            <?php echo e($goods->params["weight"]); ?>

        </td>
        <td class="dblclickable">
            <?php echo e($goods->params["volume"]); ?> <?php echo e($goods->params["measure"]["name"]); ?>

        </td>
        <td>
            <input type="checkbox" class="change-goods-activity-cbx iswitch iswitch-secondary"
                   <?php if($goods->isActive): ?> checked <?php endif; ?>>

        </td>
        <td>
            <?php echo e(number_format($goods->availability->availability,2)); ?>

        </td>
    </tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>