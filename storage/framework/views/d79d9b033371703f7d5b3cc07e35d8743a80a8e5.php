<div class="row">
    <div class="col-xs-9 stat-title">
        Сейчас на сайте
    </div>
    <div class="col-xs-3 number online-partners-count">
        <?php echo e($stat["online"]); ?>

    </div>
</div>
<div class="row">
    <div class="col-xs-9 stat-title">
        Новых за неделю
    </div>
    <div class="col-xs-3 number new-partners-count">
        <?php echo e($stat["newPerWeek"]); ?>

    </div>
</div>
<div class="row">
    <div class="col-xs-9 stat-title">
        Активных за неделю
    </div>
    <div class="col-xs-3 number active-partners-count">
        <?php echo e($stat["activePerWeek"]); ?>

    </div>
</div>
<div class="row">
    <div class="col-xs-9 stat-title">
        Создали заказ за неделю
    </div>
    <div class="col-xs-3 number with-orders-partners-count">
        <?php echo e($stat["hasOrdersPerWeek"]); ?>

    </div>
</div>
<div class="row">
    <div class="col-xs-9 stat-title">
        Всего зарегистрировано
    </div>
    <div class="col-xs-3 number summury-partners-count">
        <?php echo e($stat["partnersCount"]); ?>

    </div>
</div>