<div class="row">
    <div class="col-xs-3">
        <div>
            <label class="control-label">Партнер</label>
            <div class="form-group">
                <?php if(isset($order)): ?>
                    <input type="text" class="form-control" placeholder="ФИО партнера" id="partner-name"
                           value="<?php echo e($order->partnerProfile->lastName . ' ' . $order->partnerProfile->firstName); ?>"
                           readonly>
                <?php else: ?>
                    <select id="order-creator">
                        <?php if(isset($partners)): ?>
                            <?php $__currentLoopData = $partners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $partner): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <option value="<?php echo e($partner->id); ?>"><?php echo e($partner->profile->lastName . ' ' . $partner->profile->firstName); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        <?php endif; ?>
                    </select>
                <?php endif; ?>
            </div>
        </div>
        <div>
            <label class="control-label">Фамилия</label>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Фамилия покупателя" id="client-last-name"
                       value="<?php echo e(isset($order->client->lastName) ? $order->client->lastName : ''); ?>">
            </div>
        </div>
        <div>
            <label class="control-label">Имя</label>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Имя покупателя" id="client-first-name"
                       value="<?php echo e(isset($order->client->firstName) ? $order->client->firstName : ''); ?>">
            </div>
        </div>
        <div>
            <label class="control-label">Отчество <small>(не обязательно)</small></label>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Отчество покупателя" id="client-middle-name"
                       value="<?php echo e(isset($order->client->middleName) ? $order->client->middleName : ''); ?>">
            </div>
        </div>

        <div>
            <label class="control-label">Телефон</label>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Телефон покупателя" id="order-phone"
                       value="<?php echo e(isset($order->phone) ? $order->phone : ''); ?>">
            </div>
        </div>

        <div>
            <label class="control-label">Статус заказа</label>
            <div class="form-group">
                <select class="form-control" id="order-status">
                    <?php if(isset($orderStatuses)): ?>
                        <?php $__currentLoopData = $orderStatuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $status): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <option value="<?php echo e($status->id); ?>"
                                    <?php if(isset($order)): ?> <?php if($status->id == $order->statusId): ?> selected <?php endif; ?> <?php endif; ?>>
                                <?php echo e($status->name); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    <?php endif; ?>
                </select>
            </div>
        </div>

        <div>
            <label class="control-label">Статус оплаты</label>
            <div class="form-group">
                <select class="form-control" id="order-payment-type">
                    <?php if(isset($paymentTypes)): ?>
                        <?php $__currentLoopData = $paymentTypes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $paymentType): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <option value="<?php echo e($paymentType->id); ?>"
                                    <?php if(isset($order)): ?> <?php if($paymentType->id == $order->paymentTypeId): ?> selected <?php endif; ?> <?php endif; ?>>
                                <?php echo e($paymentType->name); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    <?php endif; ?>
                </select>
            </div>
        </div>
    </div>
    <div class="col-xs-9 pl-0">
        <label class="control-label">&nbsp;</label>
        <div class="goods-table-holder">
            <table class="table table-model-2 table-hover order-goods-table stat-data-1 no-footer">
                <thead>
                <tr role="row">
                    <th>№</th>
                    <th><i class="fa fa-question-circle"></i></th>
                    <th class="goods-name-th">
                        Товар
                    </th>
                    <th>
                        По дропу
                    </th>
                    <th>
                        Наценка
                    </th>
                    <th>
                        Стоимость
                    </th>
                    <th>
                        Кол-во
                    </th>
                    <th>
                        Итого
                    </th>
                    <th>
                        Прибыль
                    </th>
                    <th class="btn-th">
                        <i class="fa-trash"></i>
                    </th>
                </tr>
                </thead>
                <tbody class="middle-align">
                <?php if(isset($order)): ?>
                    <?php if(isset($order->goodsInOrder)): ?>
                        <?php $counter = 1; ?>
                        <?php $__currentLoopData = $order->goodsInOrder; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $goodsInOrder): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <?php if(isset($goodsInOrder->goods)): ?>
                                <tr class="goods-table-row <?php if($counter%2 == 0): ?> even <?php endif; ?>"
                                    data-row-number="<?php echo e($goodsInOrder->id); ?>"
                                    data-category-id="<?php echo e($goodsInOrder->goods->categoryId); ?>"
                                    data-goods-id="<?php echo e($goodsInOrder->goods->id); ?>">
                                    <td class="goods-number"><?php echo e($counter++); ?></td>
                                    <td class="btn-td">
                                        <button class="btn btn-edit goods-description"
                                                data-content="<?php echo e($goodsInOrder->goods->params->largeDescription); ?>">
                                            <i class="fa fa-question-circle"></i>
                                        </button>
                                    </td>
                                    <td class="goods-name">
                                        <?php echo e($goodsInOrder->goods->name); ?>

                                    </td>
                                    <td class="goods-drop-price">
                                        <?php echo e(number_format($goodsInOrder->goodsPrice, 2)); ?>

                                    </td>
                                    <td>
                                        <input type="number" class="table-input goods-markup form-control" maxlength="6"
                                               min="0"
                                               value="<?php echo e($goodsInOrder->markup); ?>">
                                    </td>
                                    <td class="goods-price">
                                        <?php echo e(number_format($goodsInOrder->goodsPrice + $goodsInOrder->markup, 2)); ?>

                                    </td>
                                    <td>
                                        <input type="number" class="table-input goods-quantity form-control"
                                               maxlength="6"
                                               min="0"
                                               value="<?php echo e($goodsInOrder->quantity); ?>">
                                    </td>
                                    <td class="total-goods-price">
                                        <?php echo e(number_format(($goodsInOrder->goodsPrice + $goodsInOrder->markup) * $goodsInOrder->quantity, 2)); ?>

                                    </td>
                                    <td class="total-goods-profit">
                                        <?php echo e($goodsInOrder->markup * $goodsInOrder->quantity); ?>

                                    </td>
                                    <td class="btn-td">
                                        <button class="btn btn-edit btn-order-item-delete">
                                            <i class="fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    <?php endif; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <div class="row goods-table-footer">
            <div class="col-1">
                <div class="col-1-1">
                    <select id="goods-list">
                        <?php if(isset($goodsList)): ?>
                            <option value="">Выберите товар для добавления в заказ</option>
                            <?php $__currentLoopData = $goodsList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $goods): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <option value="<?php echo e($goods->id); ?>" data-price="<?php echo e($goods->price); ?>"
                                        data-description="<?php echo e($goods->params["largeDescription"]); ?>"><?php echo e($goods->name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        <?php endif; ?>
                    </select>
                </div>
                <div class="col-1-2">
                    <button class="btn btn-secondary btn-icon add-goods-btn">
                        <i class="fa-plus"></i>
                        <span>Добавить товар</span>
                    </button>
                </div>
            </div>

            <div class="col-2">
                <label for="order-total-price">Всего:</label>
                <span id="order-total-price" readonly></span>
            </div>
            <div class="col-2">
                <label for="order-total-profit">Прибыль:</label>
                <span id="order-total-profit"></span>
            </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-xs-3">
        <div>
            <label class="control-label">Способ доставки</label>
            <div class="form-group">
                <select class="form-control" id="order-delivery-service">
                    <?php if(isset($deliveryServices)): ?>
                        <?php $__currentLoopData = $deliveryServices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $deliveryService): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <option value="<?php echo e($deliveryService->id); ?>"
                                    <?php if(isset($order)): ?> <?php if(isset($order->deliveryParams->deliveryServiceId)): ?> <?php if($deliveryService->id == $order->deliveryParams->deliveryServiceId): ?> selected <?php endif; ?> <?php endif; ?> <?php endif; ?>>
                                <?php echo e($deliveryService->name); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    <?php endif; ?>
                </select>
            </div>
        </div>
        <div id="order-city-holder" <?php if(!isset($order)): ?> style="display: none"
             <?php elseif(isset($order->deliveryParams->deliveryServiceId)): ?>
             <?php if($order->deliveryParams->deliveryServiceId == 2): ?>
             style="display: none"

             <?php endif; ?>
             <?php else: ?>
             style="display: none"
                <?php endif; ?>>
            <label class="control-label">Город</label>
            <div class="form-group">
                <input type="text" class="form-control" id="order-city"
                       value="<?php echo e(isset($order->deliveryParams->commonCity->name) ? $order->deliveryParams->commonCity->name : ''); ?>">
            </div>
        </div>

        <div id="order-office-holder"
             <?php if(!isset($order)): ?> style="display: none"
             <?php elseif(isset($order->deliveryParams->deliveryServiceId)): ?>
             <?php if($order->deliveryParams->deliveryServiceId == 2): ?>
             style="display: none"

             <?php endif; ?>
             <?php else: ?>
             style="display: none"
                <?php endif; ?>>
            <label class="control-label">Адрес доставки</label>
            <div class="form-group">
                <input type="text" class="form-control" id="order-office"
                       value="<?php echo e(isset($order->deliveryParams->commonAddress->name) ? $order->deliveryParams->commonAddress->name : ''); ?>">
            </div>
        </div>

        <div id="order-np-city-holder"
             <?php if(isset($order)): ?>
             <?php if(isset($order->deliveryParams->deliveryServiceId)): ?>
             <?php if($order->deliveryParams->deliveryServiceId != 2): ?>
             style="display: none"
                <?php endif; ?>
                <?php endif; ?>
                <?php endif; ?>>
            <label class="control-label">Город</label>
            <div class="form-group">
                <select class="form-control" id="order-np-city">
                    <option value="">Выберите город</option>
                    <?php if(isset($cities)): ?>
                        <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <option value="<?php echo e($city->id); ?>"
                                    <?php if(isset($order)): ?> <?php if(isset($order->deliveryParams->deliveryServiceId)): ?> <?php if($city->id == $order->deliveryParams->NPCityId): ?> selected <?php endif; ?> <?php endif; ?> <?php endif; ?>>
                                <?php echo e($city->name); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    <?php endif; ?>
                </select>
            </div>
        </div>

        <div id="order-np-office-holder"
             <?php if(isset($order)): ?>
             <?php if(isset($order->deliveryParams->deliveryServiceId)): ?>
             <?php if($order->deliveryParams->deliveryServiceId != 2): ?>
             style="display: none"
                <?php endif; ?>
                <?php endif; ?>
                <?php endif; ?>>
            <label class="control-label">Отделение</label>
            <div class="form-group">
                <select class="form-control" id="order-np-office">
                    <?php if(isset($offices) && isset($order)): ?>
                        <option value="">Выберите отделение</option>

                        <?php $__currentLoopData = $offices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $office): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <option value="<?php echo e($office->id); ?>"
                                    <?php if(isset($order)): ?> <?php if(isset($order->deliveryParams->deliveryServiceId)): ?> <?php if($office->id == $order->deliveryParams->NPWarehouseId): ?> selected <?php endif; ?> <?php endif; ?> <?php endif; ?>>
                                <?php echo e($office->name); ?>

                            </option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    <?php endif; ?>
                </select>
            </div>
        </div>


        <div>
            <label class="control-label">ТТН</label>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Код накладной" id="order-invoice"
                       value="<?php echo e(isset($order->invoice) ? $order->invoice : ''); ?>">
            </div>
        </div>
    </div>

    <div class="col-xs-9 pl-0">
        <div class="col-xs-4 pl-0 order-comment-holder">
            <div>
                <label class="control-label">
                    Комментарий
                </label>
                <div class="form-group">
                    <textarea class="form-control order-comment"
                              data-stylesheet-url="assets/js/wysihtml5/lib/css/wysiwyg-color.css"
                              placeholder="Введите Ваш комментарий..."
                              id="order-comment"><?php echo e(isset($order->details->comment) ? $order->details->comment : ''); ?></textarea>
                </div>
            </div>
        </div>

    </div>
</div>

<?php if(isset($order)): ?>
    <?php if($order->statusId == 4): ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="alert alert-danger">
                    <?php if(!empty($order->rejections->last())): ?>
                        Причина отказа:
                        <?php if(empty($order->rejections->last()->rejectionReasonId)): ?>
                            <?php echo e($order->rejections->last()->text); ?>

                        <?php else: ?>
                            <?php echo e($order->rejections->last()->rejectionReason->text); ?>

                        <?php endif; ?>
                    <?php else: ?>
                        Причина отказа: Не указано
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>

<div class="row">
    <div class="button-container flex-container">
        <div class="col-xs-3 pl-0">
            <button class="btn btn-secondary btn-icon btn-save-order" data-order-id="<?php echo e(isset($order->id) ? $order->id : ""); ?>">
                <span>Сохранить</span>
            </button>
        </div>
        <div class="col-xs-3">
            <button class="btn btn-gray btn-icon btn-close-popup">
                <span>Отмена</span>
            </button>
        </div>
    </div>
</div>

<?php echo e(Html::script("/js/orders/edit-popup.js")); ?>