<?php $__env->startSection("content"); ?>
    <?php echo e(Html::style('/css/registrationRequests/registrationRequests.css')); ?>

    <?php echo e(Html::script("/js/registrationRequests/registrationRequests.js")); ?>


    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Заявки к регистрации</h1>
        </div>
    </div>

    <div class="reg-requests-container">
        <table class="table table-model-2 table-hover reg-requests-table">
            <thead>
            <tr>
                <th>
                    №
                </th>
                <th>
                </th>
                <th>
                    ФИО
                </th>
                <th>
                    Телефон
                </th>
                <th>
                    Зарегистрирован
                </th>
                <th>
                    IP (Город)
                </th>
            </tr>
            </thead>
            <tbody class="middle-align">
                <?php echo $__env->make("registrationRequests.tableBody", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </tbody>
        </table>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>