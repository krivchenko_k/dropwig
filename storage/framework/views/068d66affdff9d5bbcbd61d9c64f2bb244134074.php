<?php $__env->startSection("content"); ?>
    <?php echo e(Html::style("/css/commons/404.css")); ?>


    <div class="page-404-container">
        <div class="page-error centered">

            <div class="error-symbol">
                <i class="fa-warning"></i>
            </div>

            <h2>
                Ваш аккаунт ожидает модерации
                <small>Данная страница пока не доступна</small>
            </h2>


        </div>

        
            
                
                
            
        
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>