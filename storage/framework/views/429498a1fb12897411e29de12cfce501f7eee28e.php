<!DOCTYPE html>
<html lang="en">
<head>

    <meta class="app-token" name="_token" content="<?php echo csrf_token(); ?>"/>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Xenon Boostrap Admin Panel"/>
    <meta name="author" content=""/>

    <title>Дропшиппинг система</title>

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
<?php echo e(Html::style("/lib/css/fonts/linecons/css/linecons.css")); ?>

<?php echo e(Html::style("/lib/css/fonts/fontawesome/css/font-awesome.min.css")); ?>

<?php echo e(Html::style("/lib/css/bootstrap.css")); ?>





<?php echo e(Html::style("/lib/css/xenon.css")); ?>

<?php echo e(Html::style("/lib/css/fonts/elusive/css/elusive.css")); ?>

<?php echo e(Html::style("/lib/css/custom.css")); ?>

<?php echo e(Html::style("/lib/css/magnific-popup.css")); ?>

<?php echo e(Html::style("/lib/js/select2/select2.css")); ?>

<?php echo e(Html::style("/lib/js/select2/select2-bootstrap.css")); ?>


<?php echo e(HTML::script("/lib/js/jbone.min.js")); ?>

<?php echo e(HTML::script('/lib/js/jquery-3.1.0.min.js')); ?>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <?php echo e(HTML::script("https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js")); ?>

    <?php echo e(HTML::script("https://oss.maxcdn.com/respond/1.4.2/respond.min.js")); ?>

    <![endif]-->

<?php echo e(Html::style('/css/main.css')); ?>

<?php echo e(Html::style('/css/google-icons.css')); ?>




<!-- Bottom Scripts -->
<?php echo e(HTML::script("/lib/js/bootstrap.min.js")); ?>

<?php echo e(HTML::script("/lib/js/select2/select2.min.js")); ?>

<?php echo e(HTML::script("/lib/js/TweenMax.min.js")); ?>

<?php echo e(HTML::script("/lib/js/resizeable.js")); ?>

<?php echo e(HTML::script("/lib/js/joinable.js")); ?>

<?php echo e(HTML::script("/lib/js/xenon-api.js")); ?>

<?php echo e(HTML::script("/lib/js/xenon-toggles.js")); ?>

<?php echo e(Html::script("/lib/js/colorpicker/bootstrap-colorpicker.min.js")); ?>


<?php echo e(Html::script("/lib/js/moment.min.js")); ?>

<?php echo e(Html::script("/lib/js/jquery.dataTables.min.js")); ?>

<?php echo e(Html::script("/lib/js/datetime-moment.js")); ?>


<?php echo e(Html::script("/lib/js/jquery.validate.min.js")); ?>

<?php echo e(Html::script("/lib/js/jquery.mask.min.js")); ?>

<?php echo e(Html::script("/lib/js/additional-methods.min.js")); ?>

<?php echo e(Html::script("/lib/js/jquery.magnific-popup.js")); ?>

<?php echo e(Html::script("/lib/js/toastr/toastr.min.js")); ?>


<?php echo e(HTML::script('/js/main.js')); ?>


<!-- JavaScripts initializations and stuff -->
    <?php echo e(HTML::script("/lib/js/xenon-custom.js")); ?>


    <meta name="mailru-domain" content="ZaUys8hIDWGizuDi" />

</head>
<body class="page-body">
