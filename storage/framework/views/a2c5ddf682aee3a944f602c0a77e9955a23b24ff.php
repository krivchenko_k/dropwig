<div class="popup-content">
    <div>
        <div>
            <input type="text" class="form-control rejection-type-name-inp" placeholder="Название типа отказа" value="<?php echo e(isset($rejectionType->name) ? $rejectionType->name : ""); ?>">
        </div>
        <div class="button-container">
            <button class="btn btn-secondary btn-icon btn-save" data-rejection-type-id="<?php echo e(isset($rejectionType->id) ? $rejectionType->id : ''); ?>">
                <span>Сохранить</span>
            </button>
            <button class="btn btn-gray btn-icon btn-close-popup">
                <span>Отмена</span>
            </button>
        </div>
    </div>
</div>