<?php echo $__env->make("commons.header", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php if(Session::has("user")): ?>

    <?php if(Session::get("user")->groupId == 1): ?>
        <?php echo $__env->make("commons.menu", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    <?php if(Session::get("user")->groupId == 2): ?>
        <?php echo $__env->make("commons.menu", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    <?php if(Session::get("user")->groupId == 3): ?>
        <?php echo $__env->make("commons.partner-menu", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    <?php echo $__env->make("commons.topbar", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php endif; ?>
<?php echo $__env->yieldContent("content"); ?>

<?php echo $__env->make("commons.footer", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->make("commons.popup", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>






