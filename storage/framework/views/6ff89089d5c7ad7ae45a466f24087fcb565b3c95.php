<?php if(isset($partners)): ?>
    <?php $__currentLoopData = $partners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $partner): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <tr>
            <td><?php echo e($partner->balance->id); ?></td>
            <td><?php echo e($partner->profile->lastName); ?> <?php echo e($partner->profile->firstName); ?></td>
            <td><?php echo e($partner->profile->card); ?></td>
            <td><?php echo e(number_format($partner->balance->amount, 2)); ?></td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
<?php endif; ?>