<div class="popup-content">
    <div>
        <div>
            <input type="text" class="form-control order-status-name-inp" placeholder="Имя группы" value="<?php echo e(isset($orderStatus->name) ? $orderStatus->name : ""); ?>">
        </div>
        <div>
            <input type="color" class="form-control order-status-color-inp" value="<?php echo e(isset($orderStatus->color) ? $orderStatus->color : ""); ?>">
        </div>
        <div class="button-container">
            <button class="btn btn-secondary btn-icon btn-save" data-order-status-id="<?php echo e(isset($orderStatus->id) ? $orderStatus->id : ''); ?>">
                <span>Сохранить</span>
            </button>
            <button class="btn btn-gray btn-icon btn-close-popup">
                <span>Отмена</span>
            </button>
        </div>
    </div>
</div>

