<div class="page-container">
    <div class="sidebar-menu toggle-others">
        <div class="sidebar-menu-inner">
            <header class="logo-env">
                <!-- This will toggle the mobile menu and will be visible only on mobile devices -->
                <div class="mobile-menu-toggle visible-xs">

                    <a href="#" data-toggle="mobile-menu">
                        <i class="fa-bars"></i>
                    </a>
                </div>
            </header>

            <?php echo e(Html::script("/js/commons/menu.js")); ?>


            <ul id="main-menu" class="main-menu">
                <li>
                    <a href="/">
                        <span class="title go-main-page-span">Главная</span>
                    </a>
                </li>

                <li>
                    <a href="#">
                        <i class="el-group"></i>
                        <span class="title">Пользователи</span>
                    </a>
                    <ul>
                        <li>
                            <a href="/reg-requests">
                                <span class="title">К регистрации</span>
                            </a>
                        </li>
                        <li>
                            <a href="/partners">
                                <span class="title">Партнёры</span>
                            </a>
                        </li>
                        <li>
                            <a href="/staff">
                                <span class="title">Сотрудники</span>
                            </a>
                        </li>
                    </ul>
                </li>



                <li>
                    <a href="#">
                        <i class="fa-shopping-cart"></i>
                        <span class="title">Заказы</span>
                    </a>
                    <ul>
                        <li>
                            <a href="/moderation-requests">
                                <span class="title">К модерации</span>
                            </a>
                        </li>
                        <li>
                            <a href="/orders">
                                <span class="title">Перечень заказов</span>
                            </a>
                        </li>
                        <li>
                            <a href="/delivery-services">
                                <span class="title">Службы доставки</span>
                            </a>
                        </li>
                        <li>
                            <a href="/order-statuses">
                                <span class="title">Статусы заказов</span>
                            </a>
                        </li>
                    </ul>
                </li>



                <li>
                    <a href="#">
                        <i class="linecons-money"></i>
                        <span class="title">Выплаты</span>
                    </a>
                    <ul>
                        <li>
                            <a href="/payment-requests">
                                <span class="title">К выплате</span>
                            </a>
                        </li>
                        <li>
                            <a href="/balances">
                                <span class="title">Баланс партнёров</span>
                            </a>
                        </li>
                        <li>
                            <a href="/payment-history">
                                <span class="title">История операций</span>
                            </a>
                        </li>
                    </ul>
                </li>



                <li>
                    <a href="#">
                        <i class="fa-list-alt"></i>
                        <span class="title">Каталог</span>
                    </a>
                    <ul>
                        <li>
                            <a href="/goods">
                                <span class="title">Товары</span>
                            </a>
                        </li>
                        <li>
                            <a href="/goods-categories">
                                <span class="title">Категории товаров</span>
                            </a>
                        </li>
                    </ul>
                </li>



                <li>
                    <a href="#">
                        <i class="fa-newspaper-o"></i>
                        <span class="title">Контент</span>
                    </a>
                    <ul>
                        <li>
                            <a href="/news">
                                <span class="title">Новости</span>
                            </a>
                        </li>
                        <li>
                            <a href="/settings/faq">
                                <span class="title">Вопросы и ответы</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="/invoice-printing">
                        <i class="fa-truck"></i>
                        <span class="title">Отправка</span>
                    </a>
                </li>


                <li>
                    <a href="#">
                        <i class="linecons-cog"></i>
                        <span class="title">Настройки</span>
                    </a>
                    <ul>

                        <li>
                            <a href="/settings/rejection-types">
                                <span class="title">Типы отказов</span>
                            </a>
                        </li>
                        <li>
                            <a href="/settings/rejection-reasons">
                                <span class="title">Причины отказов</span>
                            </a>
                        </li>
                        <li>
                            <a href="/settings/payment-types">
                                <span class="title">Типы оплаты</span>
                            </a>
                        </li>
                        <li>
                            <a href="/settings/availability-types">
                                <span class="title">Типы наличия товара</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="/faq">
                        <i class="fa-question"></i>
                        <span class="title">Вопросы и ответы</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>