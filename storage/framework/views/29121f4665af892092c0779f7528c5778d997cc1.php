<div class="popup-content login-page-container col-xs-12">
    <div class="row">
        <form class="edit-partner-form registration-form form-horizontal login-form">
            <div class="col-xs-6">
                <div class="form-group">
                    <label class="" for="partner-email">E-mail</label>
                    <div class="">
                        <input class="partner-email form-control" type="email" name="email" id="partner-email"
                               placeholder="email"
                               value="<?php echo e(isset($partner->profile->email) ? $partner->profile->email : ""); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="" for="partner-login">Логин</label>
                    <div class="">
                        <input class="partner-login form-control" id="partner-login" type="text" name="login"
                               placeholder="Логин" minlength="5" required value="<?php echo e(isset($partner->login) ? $partner->login : ""); ?>"
                               <?php if(isset($partner)): ?> readonly <?php endif; ?>>
                    </div>
                </div>
                <div class="form-group">
                    <?php if(!isset($partner)): ?>
                        <label class="partner-password-label" for="partner-password">Пароль</label>
                        <input type="password" name="password" class="partner-password form-control" id="partner-password" placeholder="Пароль"
                               value="<?php echo e(isset($partner->password) ? $partner->password : ""); ?>">
                    <?php else: ?>
                        <label class="partner-password-label" for="partner-password" style="display: none">Пароль</label>
                        <input type="password" name="password" class="partner-password form-control" id="partner-password" placeholder="Новый пароль" style="display: none">
                        <center><span class="change-password-btn">Изменить пароль</span></center>
                    <?php endif; ?>
                </div>

                
                    
                        
                        
                            
                                   
                                   
                                   
                                   
                        
                    
                
                <div class="form-group">
                    <label class="" for="partner-nickname">Ник</label>
                        <input class="partner-nickname form-control" type="text" name="nickname" id="partner-nickname"
                               placeholder="Никнейм" value="<?php echo e(isset($partner->profile->nickname) ? $partner->profile->nickname : ""); ?>">
                </div>
                <label for="partner-nickname">Доп.контакты</label>
                <div class="form-group">
                        <input class="partner-contacts form-control" type="text" name="contacts" id="partner-contacts"
                               value="<?php echo e(isset($partner->profile->contacts) ? $partner->profile->contacts : ""); ?>">
                </div>
                <?php if(isset($partner)): ?>
                    <div class="form-group">
                        <label class="" for="partner-reg-date">Дата регистрации</label>
                        <div class="">
                            <input class="partner-reg-date form-control" type="text" name="regDate"
                                   id="partner-reg-date"
                                   readonly
                                   placeholder="Фамилия" minlength="2" required
                                   value="<?php echo e(date("d.m.Y (H:i)", strtotime($partner->profile->created_at))); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="" for="partner-last-activity">Последняя активность</label>
                        <div class="">
                            <input class="partner-last-activity form-control" type="text" name="lastActivity"
                                   id="partner-last-activity" readonly
                                   placeholder="Фамилия" minlength="2" required
                                   value="<?php echo e(date("d.m.Y (H:i)", strtotime($partner->profile->updated_at))); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>IP (Город) при регистрации</label>
                        <div>
                            <input class="partner-last-activity form-control" type="text" name="lastActivity"
                                   id="partner-last-activity" readonly
                                   value="<?php echo e($partner->profile->ip); ?> (<?php echo e($partner->profile->cityByIp); ?>)">
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    <label class="" for="partner-name">Имя</label>
                    <div class="">
                        <input class="partner-name form-control" type="text" name="firstName" id="partner-name"
                               placeholder="Имя"
                               minlength="2" required value="<?php echo e(isset($partner->profile->firstName) ? $partner->profile->firstName : ""); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="" for="partner-surname">Фамилия</label>
                    <div class="">
                        <input class="partner-surname form-control" type="text" name="lastName" id="partner-surname"
                               placeholder="Фамилия" minlength="2" required
                               value="<?php echo e(isset($partner->profile->lastName) ? $partner->profile->lastName : ""); ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="" for="partner-phone">Телефон</label>
                    <div class="">
                        <input class="partner-phone form-control" type="text" name="phone" id="partner-phone"
                               placeholder="Телефон"
                               value="<?php echo e(isset($partner->profile->phone) ? $partner->profile->phone : ""); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="" for="partner-card">Номер карты</label>
                    <div class="">
                        <input class="partner-card form-control" type="text" name="card" id="partner-card"
                               placeholder="Номер кредитной карты" value="<?php echo e(isset($partner->profile->card) ? $partner->profile->card : ""); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="" for="partner-card-owner">Владелец карты</label>
                    <div class="">

                        <input class="partner-card-owner form-control" type="text" name="cardOwner"
                               id="partner-card-owner"
                               placeholder="Имя на кого оформлена карта"
                               value="<?php echo e(isset($partner->profile->cardOwner) ? $partner->profile->cardOwner : ""); ?>" <?php if(isset($partner)): ?> <?php echo e(($partner->profile->isCardOwnerSelf == 1) ? 'readonly' : ''); ?> <?php endif; ?>>

                    </div>
                </div>


                <div class="form-group">
                    <div class="">
                        <label for="partner-card-owner-self">
                            <input class="cbr partner-card-owner-self" style="width: 13px" name="isCardOwnerSelf"
                                   id="partner-card-owner-self"
                                   type="checkbox" <?php if(isset($partner)): ?> <?php echo e(($partner->profile->isCardOwnerSelf == 1) ? 'checked' : ''); ?> <?php endif; ?>>
                            Партнёр является владельцем карты</label>
                    </div>
                </div>

            </div>
            <?php if(isset($statuses)): ?>
                <div class="col-xs-6 status-container">
                    <label for="partner-status">Статус</label>
                    <select class="partner-status form-control" name="statusId" id="partner-status">
                        <?php if(isset($partner)): ?>
                            <option value="<?php echo e($partner->statusId); ?>"><?php echo e($partner->status->name); ?></option>
                            <option disabled>-----------------</option>
                        <?php endif; ?>
                        <?php $__currentLoopData = $statuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $status): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <option value="<?php echo e($status->id); ?>"><?php echo e($status->name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    </select>
                </div>
            <?php endif; ?>

        </form>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <button data-partner-id="<?php echo e(isset($partner->id) ? $partner->id : ""); ?>"
                    class="btn btn-secondary btn-sm btn-icon icon-left flex-item-center btn-save">
                Сохранить
            </button>
        </div>
        <div class="col-xs-6">
            <button href="#" class=" btn btn-gray btn-sm btn-icon icon-left flex-item-center btn-close-popup">
                Отмена
            </button>
        </div>
    </div>
</div>
