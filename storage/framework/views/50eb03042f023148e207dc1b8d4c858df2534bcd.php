<?php $__currentLoopData = $goodsCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $goodsCategory): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
    <tr data-goods-category-id="<?php echo e($goodsCategory->id); ?>">
        <td class="cbx-td">
            <input type="checkbox" class="cbr cbx">
        </td>
        <td class="id-td">
            <?php echo e($goodsCategory->id); ?>

        </td>
        <td class="btn-td">
            <button class="btn btn-edit btn-secondary btn-xs">
                <i class="fa-pencil"></i>
            </button>
        </td>
        <td class="dblclickable">
            <?php echo e($goodsCategory->name); ?>

        </td>
    </tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>