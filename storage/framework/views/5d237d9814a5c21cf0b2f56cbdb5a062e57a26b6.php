<?php $__env->startSection("content"); ?>

    <?php echo e(Html::script("js/rejectionTypes/rejectionTypes.js")); ?>

    <?php echo e(Html::style("css/rejectionTypes/rejectionTypes.css")); ?>


    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Типы отказов</h1>
        </div>

        <div class="breadcrumb-env">
            <button class="btn btn-secondary btn-icon add-rejection-type-btn">
                <i class="fa-plus"></i>
                <span>Добавить</span>
            </button>

            <button class="btn btn-danger btn-icon common-delete-btn delete-rejection-types-btn">
                <i class="fa fa-trash"></i>
                <span>Удалить</span>
                (<span class="deleted-row-count"></span>)
            </button>
        </div>
    </div>

    <div class="rejection-types-page-container">
        <table class="table table-model-2 table-hover rejection-types-table">
            <thead>
            <tr>
                <th class="no-sorting cbx-th">
                    <input type="checkbox" class="cbr no-sort select-all-cbx">
                </th>
                <th class="id-th">
                    ID
                </th>
                <th class="btn-th no-sorting">
                </th>
                <th>Название типа</th>
            </tr>
            </thead>
            <tbody class="middle-align">
            <?php echo $__env->make("rejectionTypes.tableBody", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </tbody>
        </table>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>