<div class="popup-content">
    <div>
        <div>
            <input type="text" class="form-control goods-сategory-name-inp" placeholder="Название категории" value="<?php echo e(isset($goodsCategory->name) ? $goodsCategory->name : ""); ?>">
        </div>
        <div class="button-container">
            <button class="btn btn-secondary btn-icon btn-save" data-goods-category-id="<?php echo e(isset($goodsCategory->id) ? $goodsCategory->id : ''); ?>">
                <span>Сохранить</span>
            </button>
            <button class="btn btn-gray btn-icon btn-close-popup">
                <span>Отмена</span>
            </button>
        </div>
    </div>
</div>