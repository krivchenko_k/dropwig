<?php $__env->startSection("content"); ?>

    <?php echo e(Html::script("/js/staff/staff.js")); ?>

    <?php echo e(Html::style("/css/staff/staff.css")); ?>


    <div class="page-title">

        <div class="title-env">
            <h1 class="title">Сотрудники</h1>
        </div>

        <div class="breadcrumb-env">
            <button class="btn btn-secondary btn-icon add-employee-btn">
                <i class="fa-plus"></i>
                <span>Добавить</span>
            </button>

            <button class="btn btn-danger btn-icon common-delete-btn delete-staff-btn">
                <i class="fa fa-trash"></i>
                <span>Удалить</span>
                (<span class="deleted-row-count"></span>)
            </button>
        </div>

    </div>
    
    <div class="staff-page-container">
        <table class="table table-model-2 table-hover staff-table">
            <thead>
                <tr>
                    <th class="no-sorting cbx-th">
                        <input type="checkbox" class="cbr no-sort select-all-cbx">
                    </th>
                    <th class="id-th">
                        ID
                    </th>
                    <th class="btn-th no-sorting">
                    </th>
                    <th>
                        ФИО
                    </th>
                    <th>
                        Логин
                    </th>
                    <th>
                        Группа
                    </th>
                </tr>
            </thead>
                 <tbody class="middle-align">
                     <?php echo $__env->make("staff.tableBody", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                 </tbody>
        </table>
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make("app", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>