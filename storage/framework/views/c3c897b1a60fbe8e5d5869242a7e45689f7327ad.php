<?php $__currentLoopData = $orderStatuses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $orderStatus): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
    <tr data-order-status-id="<?php echo e($orderStatus->id); ?>">
        <td class="cbx-td">
            <input type="checkbox" class="cbr cbx order-status-cbx">
        </td>
        <td class="id-td">
            <?php echo e($orderStatus->id); ?>

        </td>
        <td class="btn-td">
            <button class="btn btn-edit btn-secondary btn-xs">
                <i class="fa-pencil"></i>
            </button>
        </td>
        <td class="dblclickable">
            <?php echo e($orderStatus->name); ?>

        </td>
        <td class="dblclickable" style="background-color: <?php echo e($orderStatus->color); ?>">
            <?php echo e($orderStatus->color); ?>

        </td>
    </tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>