<?php if(isset($partners)): ?>
    <?php $__currentLoopData = $partners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $partner): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
        <tr data-partner-id="<?php echo e($partner->id); ?>">
            <td class="cbx-td">
                <input type="checkbox" class="cbr cbx">
            </td>
            <td class="id-td">
                <?php echo e($partner->id); ?>

            </td>
            <td class="btn-td">
                <button class="btn btn-edit btn-secondary btn-xs">
                    <i class="fa-pencil"></i>
                </button>
            </td>
            <td class="dblclickable">
                <?php echo e($partner->profile->firstName." ".$partner->profile->lastName); ?>

            </td>
            <td class="dblclickable">
                <?php echo e($partner->profile->nickname); ?>

            </td>
            <td class="num-td">
                <?php echo e($partner->order->count()); ?>

            </td>
            <td class="num-td">
                <?php echo e(ceil($partner->balance->amount)); ?>

            </td>
            <td class="num-td">
                <?php echo e($partner->payment->sum("price")); ?>

            </td>
            <td>
                <?php echo e(date("d.m.Y", strtotime($partner->profile->created_at))); ?>

            </td>
            <td>
                <?php echo e(date("d.m.Y", strtotime($partner->profile->lastActivity))); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
<?php endif; ?>