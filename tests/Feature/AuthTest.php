<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Session;
use App\Models\StaffModel;
use Tests\TestCase;

class AuthTest extends TestCase
{
    public function testLoginTo() {
        $user = StaffModel::query()->find(8);
        Session::put("user", $user);

        $response = $this->action('GET', 'MainPageController@index');

        if($response->status() !== 200) {
            return self::fail();
        }
    }
}
