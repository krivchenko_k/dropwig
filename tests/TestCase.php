<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    protected $baseUrl = 'http://dropwig.in.ua';

    use CreatesApplication;

    /**
     * Check data structure of successful response
     */
    protected function checkSuccessfulResponseStructure()
    {
        $this->seeJsonStructure([
            'success',
            'code',
            'data',
        ]);
    }
}