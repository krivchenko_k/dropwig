<?php

use Tests\TestCase;

class DeleteRejectionReasonsTest extends TestCase
{
    public function testRemovePaymentTypeData()
    {
        $user = \App\Models\StaffModel::query()->find(8);
        $rejectionReason = \App\Models\RejectionReasonsModel::query()->first();

        $this->withSession(['user' => $user])
            ->call('DELETE', '/settings/rejection-reasons', [
                'rejectionReasonsId' => $rejectionReason->id,
            ]);

        $this->assertSessionHas('user');
    }
}
