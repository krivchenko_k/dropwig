<?php

use Tests\TestCase;

class AddRejectionReasonsTest extends TestCase
{
    public function testAddRejectionReasonsData()
    {
        $user = \App\Models\StaffModel::query()->find(8);
        $rejectionType = \App\Models\RejectionTypesModel::query()->first();

        $this->withSession(['user' => $user])
            ->call('POST', '/settings/rejection-reasons', [
                'typeId' => $rejectionType->id,
                'text' => str_random(100)
            ]);

        $this->checkSuccessfulResponseStructure();
    }

    protected function checkSuccessfulResponseStructure()
    {
        parent::checkSuccessfulResponseStructure();
        $this->seeJsonStructure([
            'data' => [
                'id',
            ],
        ]);
    }
}
