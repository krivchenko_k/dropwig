<?php

use Tests\TestCase;

class GetRejectionReasonsTest extends TestCase
{
    public function testGetRejectionReasonsData()
    {
        $user = \App\Models\StaffModel::query()->find(8);

        $this->withSession(['user' => $user])
            ->call('GET', '/settings/rejection-reasons');

        $this->assertResponseOk();
        $this->assertSessionHas('user');
    }
}
