<?php

use Tests\TestCase;

class DeleteFAQTest extends TestCase
{
    public function testRemoveFAQData()
    {
        $user = \App\Models\StaffModel::query()->find(8);
        $faq = \App\Models\FAQModel::query()->first();

        $this->withSession(['user' => $user])
            ->call('DELETE', '/settings/faq', [
                'FAQId' => $faq->id,
            ]);

        $this->checkSuccessfulResponseStructure();
    }

    protected function checkSuccessfulResponseStructure()
    {
        parent::checkSuccessfulResponseStructure();
        $this->seeJsonStructure([
            'data' => [],
        ]);
    }
}
