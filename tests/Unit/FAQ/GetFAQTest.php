<?php

use Tests\TestCase;

class GetFAQTest extends TestCase
{
    public function testGetFAQUserPageData()
    {
        $user = \App\Models\StaffModel::query()->find(8);

        $this->withSession(['user' => $user])
            ->call('GET', '/faq');

        $this->assertResponseOk();
        $this->assertSessionHas('user');
    }

    public function testGetFAQStaffPageData()
    {
        $user = \App\Models\StaffModel::query()->find(8);

        $this->withSession(['user' => $user])
            ->call('GET', '/settings/faq');

        $this->assertResponseOk();
        $this->assertSessionHas('user');
    }
}