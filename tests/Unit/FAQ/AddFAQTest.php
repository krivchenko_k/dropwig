<?php

use Tests\TestCase;

class AddFAQTest extends TestCase
{
    public function testAddFAQData()
    {
        $user = \App\Models\StaffModel::query()->find(8);

        $this->withSession(['user' => $user])
            ->call('POST', '/settings/faq', [
                'question' => str_random(50),
                'answer' => str_random(250)
            ]);

        $this->checkSuccessfulResponseStructure();
    }

    protected function checkSuccessfulResponseStructure()
    {
        parent::checkSuccessfulResponseStructure();
        $this->seeJsonStructure([
            'data' => [
                'id',
            ],
        ]);
    }
}
