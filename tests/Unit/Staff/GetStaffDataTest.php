<?php

use Tests\TestCase;

class GetStaffDataTest extends TestCase
{
    public function testGetStaffData()
    {
        $user = \App\Models\StaffModel::query()->find(8);

        $this->withSession(['user' => $user])
            ->call('GET', '/staff');

        $this->assertResponseOk();
        $this->assertSessionHas('user');
    }
}
