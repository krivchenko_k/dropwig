<?php

use Tests\TestCase;

class GetRegRequestDataTest extends TestCase
{
    public function testGetRegRequestData()
    {
        $user = \App\Models\StaffModel::query()->find(8);

        $this->withSession(['user' => $user])
            ->call('GET', '/reg-requests');

        $this->assertResponseOk();
        $this->assertSessionHas('user');
    }
}
