<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class OrdersTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetOrdersPage()
    {
        $user = \App\Models\StaffModel::query()->find(8);

        $this->withSession(['user' => $user])
            ->visit('/orders')
            ->see('<h1 class="title">Заказы</h1>');

    }
}
