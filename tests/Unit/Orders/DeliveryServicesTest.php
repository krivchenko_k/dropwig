<?php

use Tests\TestCase;

class DeliveryServicesTest extends TestCase
{
    public function testGetDeliveryServicesData()
    {
        $user = \App\Models\StaffModel::query()->find(8);

        $this->withSession(['user' => $user])
            ->call('GET', '/delivery-services');

        $this->assertResponseOk();
        $this->assertSessionHas('user');
    }
}
