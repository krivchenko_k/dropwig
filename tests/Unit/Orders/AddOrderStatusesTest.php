<?php

use Tests\TestCase;

class AddOrderStatusesTest extends TestCase
{
    public function testAddOrderStatus()
    {
        $user = \App\Models\StaffModel::query()->find(8);

        $this->withSession(['user' => $user])
            ->call('POST', '/order-statuses', [
                'name' => str_random(15),
                'color' => '#' . random_int(100000, 999999)
            ]);

        $this->checkSuccessfulResponseStructure();
    }

    protected function checkSuccessfulResponseStructure()
    {
        parent::checkSuccessfulResponseStructure();
        $this->seeJsonStructure([
            'data' => [
                'id',
            ],
        ]);
    }
}
