<?php

use Tests\TestCase;

class AddDeliveryServiceTest extends TestCase
{
    public function testAddDeliveryService()
    {
        $user = \App\Models\StaffModel::query()->find(8);

        $this->withSession(['user' => $user])
            ->call('POST', '/delivery-services', [
                'name' => str_random(50)
            ]);

        $this->checkSuccessfulResponseStructure();
    }

    protected function checkSuccessfulResponseStructure()
    {
        parent::checkSuccessfulResponseStructure();
        $this->seeJsonStructure([
            'data' => [
                'id',
            ],
        ]);
    }
}
