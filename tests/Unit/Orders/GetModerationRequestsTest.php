<?php

use Tests\TestCase;

class GetModerationRequestsTest extends TestCase
{
    public function testGetModerationRequestsData()
    {
        $user = \App\Models\StaffModel::query()->find(8);

        $this->withSession(['user' => $user])
            ->call('GET', '/moderation-requests');

        $this->assertResponseOk();
        $this->assertSessionHas('user');
    }
}
