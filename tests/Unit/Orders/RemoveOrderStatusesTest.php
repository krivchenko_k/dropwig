<?php

use Tests\TestCase;

class RemoveOrderStatusesTest extends TestCase
{
    public function testRemoveOrderStatus()
    {
        $user = \App\Models\StaffModel::query()->find(8);
        $orderStatus = \App\Models\OrderStatusesModel::query()->first();

        $this->withSession(['user' => $user])
            ->call('DELETE', '/order-statuses', [
                'orderStatusesId' => $orderStatus->id,
            ]);

        $this->checkSuccessfulResponseStructure();
    }

    protected function checkSuccessfulResponseStructure()
    {
        parent::checkSuccessfulResponseStructure();
        $this->seeJsonStructure([
            'data' => [],
        ]);
    }
}
