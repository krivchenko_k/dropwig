<?php

use Tests\TestCase;

class OrderStatusesTest extends TestCase
{
    public function testGetModerationRequestsData()
    {
        $user = \App\Models\StaffModel::query()->find(8);

        $this->withSession(['user' => $user])
            ->call('GET', '/order-statuses');

        $this->assertResponseOk();
        $this->assertSessionHas('user');
    }
}
