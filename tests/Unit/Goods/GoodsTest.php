<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory;
use Tests\TestCase;
use Illuminate\Support\Facades\Session;

class GoodsTest extends TestCase
{
    use WithoutMiddleware;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetOneIsActiveGood()
    {
        $mockedGoodsCategoryCount = 10;

        $mockedCategoryGoodsData = [];
        $faker = Factory::create();

        for($i = 1; $i <= $mockedGoodsCategoryCount; $i++    ) {
            $mockedCategoryGoodsData[] = factory(\App\Models\GoodsCategoriesModel::class)->create();
        }

        if(empty($mockedCategoryGoodsData)) {
            self::fail();
        }

        $mockedCategoryGoodsData = array_reverse($mockedCategoryGoodsData);


        $mockedGoodsData = factory(\App\Models\GoodsModel::class)->create([
            'categoryId' => collect($mockedCategoryGoodsData)->random()->id
        ]);

        if(empty($mockedCategoryGoodsData)) {
            self::fail();
        }

        $user = \App\Models\StaffModel::query()->find(8);

//        $this->withSession(['user' => $user])
//            ->visit('/goods')
//            ->see($mockedGoodsData->name);

    }
}
