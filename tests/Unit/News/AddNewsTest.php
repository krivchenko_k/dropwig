<?php

use Tests\TestCase;

class AddNewsTest extends TestCase
{
    public function testAddNewsAndDeleteHisAfter()
    {
        $user = \App\Models\StaffModel::query()->find(8);

        $this->withSession(['user' => $user])
            ->call('POST', '/news', [
                'title' => 'Test News',
                'text' => str_random(2000)
            ]);

        $this->checkSuccessfulResponseStructure();
    }

    protected function checkSuccessfulResponseStructure()
    {
        parent::checkSuccessfulResponseStructure();
        $this->seeJsonStructure([
            'data' => [
                'id',
            ],
        ]);
    }
}
