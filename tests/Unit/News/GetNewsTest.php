<?php

use Tests\TestCase;

class GetNewsTest extends TestCase
{
    public function testGetNewsData()
    {
        $user = \App\Models\StaffModel::query()->find(8);

        $this->withSession(['user' => $user])
            ->call('GET', '/news/list');

//        dd($response->getContent());
//        \App\Models\NewsModel::query()->delete();

        $this->checkSuccessfulResponseStructure();
        $newsInResponse = array_first(json_decode($this->response->original)->data->news);

        $this->assertInternalType('int', $newsInResponse->id);
        $this->assertInternalType('string', $newsInResponse->title);
        $this->assertInternalType('string', $newsInResponse->text);
    }

    protected function checkSuccessfulResponseStructure()
    {
        parent::checkSuccessfulResponseStructure();
        $this->seeJsonStructure([
            'data' => [
                'news' => [
                    '*' => [
                        'id',
                        'title',
                        'text'
                    ],
                ],
            ],
        ]);
    }
}
