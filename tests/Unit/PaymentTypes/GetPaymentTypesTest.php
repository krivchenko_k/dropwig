<?php

use Tests\TestCase;

class GetPaymentTypesTest extends TestCase
{
    public function testGetPaymentTypesData()
    {
        $user = \App\Models\StaffModel::query()->find(8);

        $this->withSession(['user' => $user])
            ->call('GET', '/settings/payment-types');

        $this->assertResponseOk();
        $this->assertSessionHas('user');
    }
}
