<?php

use Tests\TestCase;

class AddPaymentTypesTest extends TestCase
{
    public function testAddPaymentTypesData()
    {
        $user = \App\Models\StaffModel::query()->find(8);

        $this->withSession(['user' => $user])
            ->call('POST', '/settings/payment-types', [
                'name' => str_random(50),
            ]);

        $this->checkSuccessfulResponseStructure();
    }

    protected function checkSuccessfulResponseStructure()
    {
        parent::checkSuccessfulResponseStructure();
        $this->seeJsonStructure([
            'data' => [
                'id',
            ],
        ]);
    }
}
