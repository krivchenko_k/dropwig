<?php

use Tests\TestCase;

class DeletePaymentTypesTest extends TestCase
{
    public function testRemovePaymentTypeData()
    {
        $user = \App\Models\StaffModel::query()->find(8);
        $paymentType = \App\Models\PaymentTypesModel::query()->first();

        $this->withSession(['user' => $user])
            ->call('DELETE', '/settings/payment-types', [
                'paymentTypesId' => $paymentType->id,
            ]);

        $this->assertSessionHas('user');
    }
}
