<?php

use Tests\TestCase;

class InvoicePrintingTest extends TestCase
{
    public function testGetInvoicePrintingForBlockedUser()
    {
        $user = \App\Models\StaffModel::query()->find(9);

        $response = $this->withSession(['user' => $user])
            ->call('GET', '/invoice-printing');

        $this->assertResponseOk();
        $this->assertSessionHas('user');
    }

    public function testGetInvoicePrintingPage()
    {
        $user = \App\Models\StaffModel::query()->find(8);

        $this->withSession(['user' => $user])
            ->visit('/invoice-printing')
            ->see('<h1 class="title">Печать накладных</h1>');
    }
}
