<?php

use Tests\TestCase;

class GetRejectionTypesTest extends TestCase
{
    public function testGetRejectionReasonsData()
    {
        $user = \App\Models\StaffModel::query()->find(8);

        $this->withSession(['user' => $user])
            ->call('GET', '/settings/rejection-types');

        $this->assertResponseOk();
        $this->assertSessionHas('user');
    }
}
