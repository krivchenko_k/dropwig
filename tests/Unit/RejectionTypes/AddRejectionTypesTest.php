<?php

use Tests\TestCase;

class AddRejectionTypesTest extends TestCase
{
    public function testAddRejectionReasonsData()
    {
        $user = \App\Models\StaffModel::query()->find(8);

        $this->withSession(['user' => $user])
            ->call('POST', '/settings/rejection-types', [
                'name' => str_random(60)
            ]);

        $this->checkSuccessfulResponseStructure();
    }

    protected function checkSuccessfulResponseStructure()
    {
        parent::checkSuccessfulResponseStructure();
        $this->seeJsonStructure([
            'data' => [
                'id',
            ],
        ]);
    }
}
