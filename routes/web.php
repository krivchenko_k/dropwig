<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get("login", "AuthController@index");
Route::post("login", "AuthController@authenticate");
Route::get("logout", "AuthController@logout");
Route::get("login/faq", "AuthController@getFAQPopup");
Route::get("login/contacts", "AuthController@getContactsPopup");
Route::get("login/agreement", "AuthController@getAgreementPopup");
Route::get("login/policy", "AuthController@getPolicyPopup");

Route::get("registration", "RegistrationController@index");
Route::post("registration", "RegistrationController@register");


Route::group(["middleware" => ["checkSessionMiddleware", "checkAccess", "updateLastActivity"]], function () {
    Route::get("/", "MainPageController@index");


    Route::get("reg-requests", "RegistrationRequestsController@index");
    Route::get("reg-requests/tableBody", "RegistrationRequestsController@getTableBody");
    Route::get("reg-requests/confirmation/{partnerId}", "RegistrationRequestsController@getPopup");
    Route::get("reg-requests/decline/{partnerId}", "RegistrationRequestsController@getDeclinePopup");
    Route::put("reg-requests/confirm", "RegistrationRequestsController@confirmRegistration");
    Route::put("reg-requests/decline", "RegistrationRequestsController@declineRegistration");


    Route::get("partners", "PartnersController@index");
    Route::get("partners/tableBody", "PartnersController@getTableBody");
    Route::get("partners/{userId}", "PartnersController@getPopup");
    Route::post("partners", "PartnersController@addPartner");
    Route::put("partners", "PartnersController@editPartner");
    Route::delete("partners", "PartnersController@removePartners");


    Route::get("goods", "GoodsController@index");
    Route::get("goods/update", "GoodsController@getTableBody");
    Route::get("goods/{id}", "GoodsController@getPopup");
    Route::post("goods", "GoodsController@addGoods");
    Route::post("goods/update", "GoodsController@editGoods");
    Route::delete("goods", "GoodsController@deleteGoods");
    Route::put("goods/{id}/changeActivity", "GoodsController@changeGoodsActivity");



    Route::get("order-statuses", "OrderStatusesController@index");
    Route::get("order-statuses/update", "OrderStatusesController@getOrderStatusesTable");
    Route::get("order-statuses/{id}", "OrderStatusesController@getPopup");
    Route::post("order-statuses", "OrderStatusesController@addOrderStatus");
    Route::put("order-statuses", "OrderStatusesController@editOrderStatus");
    Route::delete("order-statuses", "OrderStatusesController@deleteOrderStatuses");


    Route::get("delivery-services", "DeliveryServicesController@index");
    Route::get("delivery-services/update", "DeliveryServicesController@getDeliveryServicesTable");
    Route::get("delivery-services/{id}", "DeliveryServicesController@getPopup");
    Route::post("delivery-services", "DeliveryServicesController@addDeliveryService");
    Route::put("delivery-services", "DeliveryServicesController@editDeliveryService");
    Route::delete("delivery-services", "DeliveryServicesController@deleteDeliveryServices");


    Route::get("goods-measures", "GoodsMeasuresController@index");
    Route::get("goods-measures/update", "GoodsMeasuresController@getGoodsMeasuresTable");
    Route::get("goods-measures/{id}", "GoodsMeasuresController@getPopup");
    Route::post("goods-measures", "GoodsMeasuresController@addGoodsMeasure");
    Route::put("goods-measures", "GoodsMeasuresController@editGoodsMeasure");
    Route::delete("goods-measures", "GoodsMeasuresController@deleteGoodsMeasures");


    Route::get("settings/rejection-types", "RejectionTypesController@index");
    Route::get("settings/rejection-types/update", "RejectionTypesController@getRejectionTypesTable");
    Route::get("settings/rejection-types/{id}", "RejectionTypesController@getPopup");
    Route::post("settings/rejection-types", "RejectionTypesController@addRejectionType");
    Route::put("settings/rejection-types", "RejectionTypesController@editRejectionType");
    Route::delete("settings/rejection-types", "RejectionTypesController@deleteRejectionTypes");


    Route::get("settings/rejection-reasons", "RejectionReasonsController@index");
    Route::get("settings/rejection-reasons/update", "RejectionReasonsController@getRejectionReasonsTable");
    Route::get("settings/rejection-reasons/{id}", "RejectionReasonsController@getPopup");
    Route::post("settings/rejection-reasons", "RejectionReasonsController@addRejectionReason");
    Route::put("settings/rejection-reasons", "RejectionReasonsController@editRejectionReason");
    Route::delete("settings/rejection-reasons", "RejectionReasonsController@deleteRejectionReasons");


    Route::get("settings/payment-types", "PaymentTypeController@index");
    Route::get("settings/payment-types/update", "PaymentTypeController@getPaymentTypesTable");
    Route::get("settings/payment-types/{id}", "PaymentTypeController@getPopup");
    Route::post("settings/payment-types", "PaymentTypeController@addPaymentType");
    Route::put("settings/payment-types", "PaymentTypeController@editPaymentType");
    Route::delete("settings/payment-types", "PaymentTypeController@deletePaymentTypes");


    Route::get("settings/availability-types", "AvailabilityTypesController@index");
    Route::get("settings/availability-types/update", "AvailabilityTypesController@getAvailabilityTypesTable");
    Route::get("settings/availability-types/{id}", "AvailabilityTypesController@getPopup");
    Route::post("settings/availability-types", "AvailabilityTypesController@addAvailabilityType");
    Route::put("settings/availability-types", "AvailabilityTypesController@editAvailabilityType");
    Route::delete("settings/availability-types", "AvailabilityTypesController@deleteAvailabilityTypes");


    Route::get("goods-categories", "GoodsCategoriesController@index");
    Route::get("goods-categories/update", "GoodsCategoriesController@getGoodsCategoriesTable");
    Route::get("goods-categories/{id}", "GoodsCategoriesController@getPopup");
    Route::post("goods-categories", "GoodsCategoriesController@addGoodsCategory");
    Route::put("goods-categories", "GoodsCategoriesController@editGoodsCategory");
    Route::delete("goods-categories", "GoodsCategoriesController@deleteGoodsCategories");


    Route::get("faq", "FAQController@index");
    Route::get("settings/faq", "FAQController@settingsIndex");
    Route::get("settings/faq/update", "FAQController@getFAQTable");
    Route::get("settings/faq/{id}", "FAQController@getPopup");
    Route::post("settings/faq", "FAQController@addFAQ");
    Route::put("settings/faq", "FAQController@editFAQ");
    Route::delete("settings/faq", "FAQController@deleteFAQList");


    Route::get("settings/agreement", "AgreementController@index");
    Route::get("settings/agreement/edit", "AgreementController@getPopup");
    Route::post("settings/agreement/save", "AgreementController@editAgreement");


    Route::get("settings/privacy-policy", "PrivacyController@index");
    Route::get("settings/privacy-policy/edit", "PrivacyController@getPopup");
    Route::post("settings/privacy-policy/save", "PrivacyController@editPrivacy");


    Route::get("news", "NewsController@index");
    Route::get("news/list", "NewsController@getNewsList");
    Route::get("news/update", "NewsController@getNewsTable");
    Route::get("news/{id}", "NewsController@getPopup");
    Route::post("news", "NewsController@addNews");
    Route::put("news/edit", "NewsController@editNews");
    Route::delete("news", "NewsController@deleteNews");


    Route::get("partners-stat", "MainPageController@getPartnersStat");
    Route::get("orders-stat", "MainPageController@getOrdersStat");
    Route::get("personal-stat", "MainPageController@getPersonalStat");
    Route::get("top-partners", "MainPageController@getTopPartners");
    Route::get("partners-online", "MainPageController@getPartnersOnline");
    Route::get("top-goods", "MainPageController@getTopGoods");


    Route::get("recomended-goods", "RecomendedGoodsController@index");
    Route::get("recomended-goods/popupTableBody", "RecomendedGoodsController@getPopupTableBody");
    Route::get("recomended-goods/{goodsId}", "RecomendedGoodsController@getPopup");
    Route::post("recomended-goods", "RecomendedGoodsController@addRecomendedGoods");
    Route::delete("recomended-goods", "RecomendedGoodsController@deleteRecomendedGoods");


    Route::get('balances', 'PartnerBalancesController@index');

    Route::get('payment-requests', 'PaymentRequestsController@index');
    Route::get('payment-requests/tableBody', 'PaymentRequestsController@getTableBody');
    Route::get('payment-requests/confirm/{requestId}', 'PaymentRequestsController@getConfirmPopup');
    Route::get('payment-requests/decline/{requestId}', 'PaymentRequestsController@getDeclinePopup');
    Route::put('payment-requests', 'PaymentRequestsController@makePaymentRequest');
    Route::post('payment-requests/confirm', 'PaymentRequestsController@confirmRequest');
    Route::post('payment-requests/decline', 'PaymentRequestsController@declineRequest');

    Route::get('payment-history', 'PaymentHistoryController@index');


    Route::get('np/getWarehousesForCity/{cityId}', 'NPWarehousesController@getNPAddressesByCityId');
    Route::get('commonCities/getCities', 'CommonCitiesController@getCities');


    Route::get('orders', 'OrdersController@index');
    Route::get('orders/get', 'OrdersController@getOrders');
    Route::get('orders/tableBody', 'OrdersController@getTableBody');
    Route::get('orders/recommended-goods', 'OrdersController@getRecommendedGoods');
    Route::get('orders/{orderId}', 'OrdersController@getOrderById');
    Route::put('orders/{orderId}', 'OrdersController@sendOrderToModeration');
    Route::post('orders', 'OrdersController@addOrder');
    Route::put('orders', 'OrdersController@updateOrder');
    Route::delete('orders', 'OrdersController@removeOrders');


    Route::get("trashed-orders", "TrashedOrdersController@index");
    Route::get('trashed-orders/tableBody', 'TrashedOrdersController@getTableBody');
    Route::get("trashed-orders/{id}", "TrashedOrdersController@getPopup");
    Route::put("trashed-orders", "TrashedOrdersController@recoverOrders");
    Route::delete("trashed-orders", "TrashedOrdersController@eraseOrders");

    Route::get('partner-statistics', 'PartnerStatistics@index');
    Route::get('partner-statistics/tableBody', 'PartnerStatisticsController@getByDate');

    Route::get('moderation-requests', 'ModerationRequestsController@index');
    Route::get('moderation-requests/tableBody', 'ModerationRequestsController@getTableBody');
    Route::get('moderation-requests/declinePopup', 'ModerationRequestsController@getDeclinePopup');
    Route::get('moderation-requests/{orderId}', 'ModerationRequestsController@getOrderById');
    Route::put('moderation-requests/accept', 'ModerationRequestsController@acceptOrder');
    Route::put('moderation-requests/decline', 'ModerationRequestsController@declineOrder');
    Route::put('moderation-requests', 'ModerationRequestsController@updateOrder');
    Route::delete('moderation-requests', 'ModerationRequestsController@remove');

    Route::get('rt-notifications', 'SSEController@getAdminNotifications');
    Route::get('rt-balance', 'SSEController@getPartnerBalance');

    Route::get('payment-requests', 'PaymentRequestsController@index');
    Route::get('payment-requests/tableBody', 'PaymentRequestsController@getTableBody');
    Route::get('payment-requests/confirm/{requestId}', 'PaymentRequestsController@getConfirmPopup');
    Route::get('payment-requests/decline/{requestId}', 'PaymentRequestsController@getDeclinePopup');
    Route::put('payment-requests', 'PaymentRequestsController@makePaymentRequest');
    Route::post('payment-requests/confirm', 'PaymentRequestsController@confirmRequest');
    Route::post('payment-requests/decline', 'PaymentRequestsController@declineRequest');

    Route::get('payment-history', 'PaymentHistoryController@index');

    Route::get('goods-statistics', 'GoodsStatisticsController@index');
    Route::get('goods-statistics/tableBody', 'GoodsStatisticsController@getByDate');

    Route::get("staff", "StaffController@index");
    Route::get('staff/tableBody', 'StaffController@getTableBody');
    Route::get("staff/{id}", "StaffController@getPopup");
    Route::post("staff", "StaffController@addEmployee");
    Route::put("staff/edit", "StaffController@editEmployee");
    Route::delete("staff", "StaffController@deleteStaff");


    Route::get('invoice-printing', 'InvoicePrintingController@index');
    Route::get('invoice-printing/tableBody', 'InvoicePrintingController@getTableBody');
    Route::get('invoice-printing/export', 'InvoicePrintingController@ExcelExport');
    Route::get('invoice-printing/{orderId}', 'OrdersController@getOrderById');
    Route::put('invoice-printing', 'OrdersController@updateOrder');
    Route::post('invoice-printing', 'InvoicePrintingController@printInvoices');
    Route::delete('invoice-printing', 'OrdersController@removeOrders');

    Route::get('logs', 'LogsController@index');
    Route::get('logs/tableBody', 'LogsController@getTableBody');

    //Роуты для партнёров
    Route::get("contacts", "ContactsController@getLoginPagePopup");
    Route::get("catalog", "GoodsController@index");
    Route::get("catalog/update", "GoodsController@getTableBody");
    Route::get("catalog/{id}", "GoodsController@getPopup");
    Route::get('statistics', 'PartnerStatisticsController@index');

    Route::get("np/cities", "NPCitiesController@getCities");

    Route::get("profile", "PartnersController@getProfilePopup");
    Route::put("profile", "PartnersController@editPartner");

});


Route::get("np/uw", function () {
   \App\Http\Controllers\NPWarehousesController::updateWarehouses();
   return;
});

Route::get("np/uc", function () {
   \App\Http\Controllers\NPCitiesController::updateCities();
   return;
});

    Route::get("np/uos", function () {
        \App\Http\Controllers\NPController::updateNPOrderStatuses();
   return;
});