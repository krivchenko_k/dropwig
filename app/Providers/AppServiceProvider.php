<?php

namespace App\Providers;

use App\Models\DeliveryServicesModel;
use App\Models\OrdersModel;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    public function boot()
    {
        OrdersModel::deleting(function($order){
                $order->deliveryParams->delete();
                $order->details->delete();
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
