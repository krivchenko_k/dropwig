<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class CheckSessionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has("user"))
            return $next($request);
        else
            return redirect('login')->with("auth-warn", "Вы не авторизованы. Пожалуйста, войдите в систему");
    }
}
