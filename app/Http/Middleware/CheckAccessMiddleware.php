<?php

namespace App\Http\Middleware;

use App\Http\Controllers\AccessController;
use App\Http\Controllers\PartnersController;
use App\Http\Controllers\ResponseController;
use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;

class CheckAccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //убрать после теста
//        return $next($request);

        $response = $next($request);

//        $user = Session::get("user");
//        if ($user->statusId == 4) {
//            Session::forget("user");
//            return redirect('login')->with('error', 'Ваш аккаунт временно заблокирован')->with("login", $user->login);
//        } elseif ($user->statusId == 5) {
//            Session::forget("user");
//            return redirect('login')->with('error', 'Ваш аккаунт заблокирован')->with("login", $user->login);
//        }

        $path = $request->path();

        if (AccessController::checkAccess($request)) {
            $user = Session::get("user");

            if ($user->groupId == 3) {
                $user = PartnersController::getPartners($user->id);
            }

            if (empty($user)){
                Session::forget("user");
                return redirect('login');
            }

            if ($user->groupId == 3 && $user->statusId == 2) {
                if ($path == "faq" || $path == "contacts" || $path == "profile") {
                    return $response;
                } else {
                    $viewParams = array();

                    if ($request->ajax()) {
                        $errors[] = "Ваш аккаунт ожидает модерации. Данная функция пока не доступна";
                        return response()->json(ResponseController::jsonResponseFail($errors), 200);
                    } else {
                        return new Response(view("commons.waiting-moderation", $viewParams));
                    }
                }
            }
            if ($user->groupId == 3 && $user->statusId == 5) {
                if ($path == "faq" || $path == "contacts" || $path == "profile") {
                    return $response;
                } else {
                    $viewParams = array();

                    if ($request->ajax()) {
                        $errors[] = "Ваш аккаунт ожидает модерации. Данная функция пока не доступна";
                        return response()->json(ResponseController::jsonResponseFail($errors), 200);
                    } else {
                        return new Response(view("commons.temporarily-blocked", $viewParams));
                    }
                }
            }
            return $response;
        } else {
            $viewParams = array();

            if ($request->ajax()) {
                $errors[] = "Доступ запрещён!";
                return response()->json(ResponseController::jsonResponseFail($errors), 200);
            } else {
                return new Response(view("commons.access-denied", $viewParams));
            }
        }
    }
}
