<?php

namespace App\Http\Middleware;

use App\Http\Controllers\PartnerProfilesController;
use Closure;
use Illuminate\Support\Facades\Session;

class UpdateLastActivityMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has("user")){
            $user = Session::get("user");
            if ($user->groupId == 3){
                PartnerProfilesController::updateLastActivity($user->id);
            }
        }
        return $next($request);
    }
}
