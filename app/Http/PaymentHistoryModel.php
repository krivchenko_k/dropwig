<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentHistoryModel extends Model
{
    protected $table = "PaymentHistory";
    public $timestamps = false;

    public static $publicTableName = "PaymentHistory";

    public function order() {
        return self::hasOne('App\Models\OrdersModel', 'id', 'orderId');
    }

    public function operation() {
        return self::hasOne('App\Models\PaymentOperationsModel', 'id', 'paymentOperationId');
    }

    public function rejectionReason() {
        return self::hasOne('App\Models\RejectionReasonsModel', 'id', 'rejectionReasonId');
    }

    public function paymentRequest(){
        return self::belongsTo('App\Models\PaymentRequestsModel', "paymentRequestId");
    }


    public static function get($idArray = "", $params = "") {
        if (!empty($idArray)) {
            if (is_array($idArray)) {
                if (!empty($params))
                    return self::whereIn("id", $idArray)->where($params);
                else
                    return self::whereIn("id", $idArray)->get();
            } else
                if (!empty($params))
                    return self::where("id", $idArray)->first();
                else
                    return self::where("id", $idArray)->first();
        } else {
            if (!empty($params))
                return self::where($params)->get();
            else
                return self::all();
        }
    }

    public static function add($data) {
        $paymentHistoryRecord = new PaymentHistoryModel();
        $paymentHistoryRecord->partnerId = $data->partnerId;
        $paymentHistoryRecord->paymentOperationId = $data->paymentOperationId;
        $paymentHistoryRecord->orderId = isset($data->orderId) ? $data->orderId : null;
        $paymentHistoryRecord->paymentRequestId = isset($data->paymentRequestId) ? $data->paymentRequestId : null;
        $paymentHistoryRecord->price = $data->price;
        $paymentHistoryRecord->rejectionReasonId = isset($data->rejectionReasonId) ? $data->rejectionReasonId : null;
        $paymentHistoryRecord->save();
        return $paymentHistoryRecord->id;
    }

    public static function getPayoutSumByPartnerId($patrnerId){
        return self::where("partnerId", $patrnerId)->where("paymentOperationId", 4)->sum();
    }
    
}
