<?php

namespace App\Http\Controllers;

use App\Models\PartnerProfilesModel;
use App\Models\PartnersModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class PartnersController extends Controller
{

    private static function validateField($field)
    {
        $result = trim($field);
        $result = stripslashes($result);
        $result = htmlspecialchars($result);
        return $result;
    }

    public function index()
    {
        $viewParams = array();
        $viewParams['partners'] = PartnersModel::getWithPayout();
        return view('partners.index', $viewParams);
    }

    public static function getTableBody()
    {
        $viewParams = array();
        $viewParams['partners'] = PartnersModel::get();
        return view("partners.tableBody", $viewParams);
    }

    public static function getPopup($id)
    {
        $viewsParams = array();
        $viewsParams["statuses"] = UserStatusesController::getUserStatuses();
        if ($id != 0)
            $viewsParams["partner"] = PartnersModel::get($id);

        return view("partners.popup", $viewsParams);
    }

    public static function getProfilePopup()
    {
        $viewsParams = array();
        $user = Session::get("user");
        $viewsParams["partner"] = PartnersModel::get($user->id);

        return view("partners.partner.profilePopup", $viewsParams);
    }

    public static function addPartner(Request $request)
    {
        try {
            $data = (object)$request->all();

            $errors = array();

            if (!isset($data->login))
                $errors[] = "Не заполнено поле Логин";

            if (!isset($data->password))
                $errors[] = "Не заполнено поле Пароль";

            if (!isset($data->firstName))
                $errors[] = "Не заполнено поле Имя";

            if (!isset($data->lastName))
                $errors[] = "Не заполнено поле Фамилия";

            if (!isset($data->phone))
                $errors[] = "Не заполнено поле Телефон";

            if (!isset($data->email))
                $errors[] = "Не заполнено поле E-mail";

//            if (!isset($data->card))
//                $errors[] = "Не заполнено поле Номер карты";
//
//            if (!isset($data->cardOwner))
//                $errors[] = "Не заполнено поле Владелец карты";

            if (empty($data->card))
                $data->card = "";

            if (empty($data->cardOwner))
                $data->cardOwner = "";

            if (!isset($data->isCardOwnerSelf)) {
                $data->isCardOwnerSelf = 0;
            } else {
                $data->isCardOwnerSelf = 1;
            }

            if (!isset($data->nickname))
                $errors[] = "Не заполнено поле Ник";

//            if (!isset($data->statusId))
//                $errors[] = "Не выбран статус";

            if (PartnerProfilesController::isPartnerEmailExitst($request->email))
                $errors[] = "Пользователь с таким почтовым ящиком уже существует";
            if (PartnerProfilesController::isPartnerPhoneExitst($request->phone))
                $errors[] = "Пользователь с таким телефоном уже существует";
            if (PartnerProfilesController::isPartnerNicknameExitst($request->nickname))
                $errors[] = "Пользователь с таким ником уже существует";
            if (!empty($request->card)) {
                if (PartnerProfilesController::isPartnerCardExitst($request->card))
                    $errors[] = "Пользователь с таким номером карты уже существует";
            }
            if (PartnersModel::isLoginExist($request->login))
                $errors[] = "Пользователь с таким логином уже существует";


            if (count($errors) > 0) {
                return ResponseController::jsonResponseFail($errors);
            } else {
                $data->login = self::validateField($data->login);
                $data->password = self::validateField($data->password);
                $data->firstName = self::validateField($data->firstName);
                $data->lastName = self::validateField($data->lastName);
                $data->phone = self::validateField($data->phone);
                $data->email = self::validateField($data->email);
                $data->contacts = self::validateField($data->contacts);
                $data->card = self::validateField($data->card);
                $data->cardOwner = self::validateField($data->cardOwner);
                $data->isCardOwnerSelf = self::validateField($data->isCardOwnerSelf);
                $data->nickname = self::validateField($data->nickname);
                if (isset($data->statusId)) {
                $data->statusId = self::validateField($data->statusId);
                }

                $partnerId = PartnersModel::add($data);


                return ResponseController::jsonResponseDone();
            }


        } catch (Exception $ex) {
            return $ex;
        }
    }

    public static function editPartner(Request $request)
    {
        try {
            $partnerId = '';
            $password = '';
            $firstName = '';
            $lastName = '';
            $phone = '';
            $email = '';
            $contacts = '';
            $card = '';
            $cardOwner = '';
            $isCardOwnerSelf = '';
            $nickname = '';
            $statusId = '';

            $errors = array();

            if (isset($request->partnerId))
                $partnerId = $request->partnerId;
            else
                $errors[] = "Не заполнено поле ";

            if ($request->password == null)
                $password = false;
            else
                $password = $request->password;
            
            if (isset($request->firstName))
                $firstName = $request->firstName;
            else
                $errors[] = "Не заполнено поле ";

            if (isset($request->lastName))
                $lastName = $request->lastName;
            else
                $errors[] = "Не заполнено поле ";

            if (isset($request->phone))
                $phone = $request->phone;
            else
                $errors[] = "Не заполнено поле ";

            if (isset($request->email))
                $email = $request->email;
            else
                $errors[] = "Не заполнено поле ";

            if (isset($request->contacts))
                $contacts = $request->contacts;

            if (isset($request->card))
                $card = $request->card;
            else
                $card = "";
//                $errors[] = "Не заполнено поле";

            if (isset($request->cardOwner))
                $cardOwner = $request->cardOwner;
            else
                $cardOwner = "";
//                $errors[] = "Не заполнено поле ";

            if (isset($request->isCardOwnerSelf)) {
                $isCardOwnerSelf = $request->isCardOwnerSelf;
                $isCardOwnerSelf = ($isCardOwnerSelf == "true") ? 1 : 0;
            } else {
                $isCardOwnerSelf = 0;
            }

            if (isset($request->nickname))
                $nickname = $request->nickname;
            else
                $errors[] = "Не заполнено поле ";

            if (isset($request->statusId)) {
                $statusId = $request->statusId;
            }

//            if (isset($request->statusId))
//                $statusId = $request->statusId;
//            else
//                $errors[] = "Не заполнено поле ";

            $prevPartnerData = PartnersModel::get($partnerId);

            $tmpPartnerId = PartnerProfilesController::isPartnerEmailExitst($email);
            if ($tmpPartnerId && $tmpPartnerId != $partnerId)
                $errors[] = "Пользователь с таким почтовым ящиком уже существует";
            $tmpPartnerId = PartnerProfilesController::isPartnerPhoneExitst($phone);
            if ($tmpPartnerId && $tmpPartnerId != $partnerId)
                $errors[] = "Пользователь с таким телефоном уже существует";
            $tmpPartnerId = PartnerProfilesController::isPartnerNicknameExitst($nickname);
            if ($tmpPartnerId && $tmpPartnerId != $partnerId)
                $errors[] = "Пользователь с таким ником уже существует";
            $tmpPartnerId = PartnerProfilesController::isPartnerCardExitst($card);
            if ($tmpPartnerId && $tmpPartnerId != $partnerId)
                $errors[] = "Пользователь с таким номером карты уже существует";

            
            if (count($errors) > 0) {
                return ResponseController::jsonResponseFail($errors);
            }

            $partnerId = self::validateField($partnerId);
            $firstName = self::validateField($firstName);
            $lastName = self::validateField($lastName);
            $phone = self::validateField($phone);
            $email = self::validateField($email);
            $card = self::validateField($card);
            $cardOwner = self::validateField($cardOwner);
            $isCardOwnerSelf = self::validateField($isCardOwnerSelf);
            $nickname = self::validateField($nickname);
            if (isset($request->statusId)) {
                $statusId = self::validateField($statusId);
            }
//            $statusId = self::validateField($statusId);

            $currPartnerData = array(
                "id" => $partnerId,
                "firstName" => $firstName,
                "lastName" => $lastName,
                "phone" => $phone,
                "email" => $email,
                "card" => $card,
                "cardOwner" => $cardOwner,
                "nickname" => $nickname,
            );


            DB::update("UPDATE PartnerProfiles SET firstName = '$firstName', lastName = '$lastName', phone = '$phone', email = '$email', contacts = '$contacts', card = '$card', cardOwner = '$cardOwner', isCardOwnerSelf = $isCardOwnerSelf, nickname = '$nickname' WHERE partnerId = $partnerId");

            if ($password !== false) {
                $password = md5(self::validateField($password));
                DB::update("UPDATE `Partners` SET `password` = '$password' WHERE `id` = $partnerId");
            }

            if (Session::get("user")->groupId == 3) {
                DB::update("UPDATE Partners SET statusId = 2 WHERE id = $partnerId");
            }
            else {
                if (!empty($statusId)) {
                    DB::update("UPDATE Partners SET statusId = {$statusId} WHERE id = $partnerId");
                }
            }


            return ResponseController::jsonResponseDone();
        } catch (Exception $ex) {
            return $ex;
        }
    }


    public static function removePartners(Request $request)
    {
        $idList = json_decode($request->idList);
        $del_id = implode(", ", $idList);

        PartnersModel::remove($idList);

        return ResponseController::jsonResponseDone();
    }

    public static function getPartnerByLoginPass($login, $password)
    {
        return PartnersModel::getPartnerByLoginPass($login, $password);
    }

    public static function getPartnerByLogin($login)
    {
        return PartnersModel::getPartnerByLogin($login);
    }

    public static function setPartnerStatus($partnerId, $statusId)
    {
        PartnersModel::setPartnerStatus($partnerId, $statusId);
    }

    public static function getPartners($id = 0)
    {
        return PartnersModel::get($id);
    }

    public static function isPartnerLoginExitst($login)
    {
        $partner = PartnersModel::getPartnerByLogin($login);
        return (isset($partner)) ? true : false;
    }


    public static function getMainPageStat()
    {
        $stat = array();
        $stat["online"] = PartnersModel::getOnlineCount();
        $stat["newPerWeek"] = PartnersModel::getNewPerWeekCount();
        $stat["activePerWeek"] = PartnersModel::getActivePerWeekCount();
        $stat["hasOrdersPerWeek"] = PartnersModel::getParthersCreatedOrderPerWeekCount();
        $stat["partnersCount"] = PartnersModel::get()->count();

        return $stat;
    }

    public static function getPartnersByStatus($statusId)
    {
        return PartnersModel::getPartnersByStatus($statusId);
    }
}
