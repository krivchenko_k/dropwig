<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\PartnerBalancesModel;

class PartnerBalancesController extends Controller
{
    public static function index()
    {
        $viewParams = array();

        $partners = PartnersController::getPartners();

        $viewParams['maxBalance'] = 0;
        $viewParams['totalBalancesSum'] = 0;
        foreach ($partners as $partner) {
            $viewParams['totalBalancesSum'] += $partner->balance->amount;
            $bonus = 0;
            foreach ($partner->orders as $order) {
                if ($order->statusId == 6) {
                    foreach ($order->goodsInOrder as $goodsInOrder)
                        $bonus += $goodsInOrder->markup * $goodsInOrder->quantity;
                }
            }
            $viewParams['maxBalance'] += $partner->balance->amount + $bonus;
        }

        $viewParams['partners'] = $partners;
//        $viewParams['totalBalancesSum'] = self::getTotalBalancesSum();
        return view('balances.index', $viewParams);
    }

    public static function getBalance($id = null, $params = null)
    {
        return PartnerBalancesModel::get($id, $params);
    }

    public static function updateBalance($partnerId, $value)
    {
        PartnerBalancesModel::updateBalance($partnerId, $value);
    }

    public static function createBalance($partnerId)
    {
        return PartnerBalancesModel::add($partnerId);
    }

    public static function getTotalBalancesSum()
    {
        return PartnerBalancesModel::getTotalBalancesSum();
    }
}
