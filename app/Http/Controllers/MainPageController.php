<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use DateTime;

class MainPageController extends Controller
{
    public function index()
    {
        switch (Session::get("user")->groupId) {
            case 1:
            case 2:
                return view("mainPage.admin.index");
            case 3:
                $viewParams = [];
                $viewParams["newsList"] = NewsController::getNews()->sortByDesc("id")->slice(0,5);
                return view("mainPage.partner.index", $viewParams);
        }
    }

    public static function getTopGoods()
    {
        $user = Session::get("user");
        $date = date("Y-m-d", time() - 7 * 24 * 60 * 60);
        $viewParams = array();

        if ($user->groupId == 3)
            $limit = 5;
        else
            $limit = 10;

        $viewParams["goodsList"] = DB::select("SELECT G.name, SUM(O_G.quantity) AS totalQuantity
                                FROM Goods AS G
                                LEFT JOIN Orders_Goods AS O_G ON G.id = O_G.goodsId
                                LEFT JOIN Orders AS O ON O_G.orderId = O.id 
                                WHERE O.statusId IN (3, 5, 6, 8, 9)
                                AND O.created_at >= '$date'
                                GROUP BY G.id, G.name
                                ORDER BY totalQuantity DESC
                                LIMIT {$limit}
        ");

        if ($user->groupId == 3)
            return view("mainPage.partner.top-goods", $viewParams);
        else
            return view("mainPage.admin.top-goods", $viewParams);
    }

    public static function getTopPartners()
    {
        $user = Session::get("user");

        $date = date("Y-m-d", time() - 7 * 24 * 60 * 60);
        $viewParams = array();

        if ($user->groupId == 3) {
            $viewParams["partners"] = DB::select("SELECT PP.nickname, PP.partnerId, (
                                        SELECT SUM(OG.markup) 
                                        FROM Orders AS O
                                        LEFT JOIN Orders_Goods AS OG ON O.id = OG.orderId
                                        WHERE O.partnerId = PP.partnerId
                                        AND O.statusId IN (3, 5, 6, 8, 9)
                                        AND O.created_at >= '$date'
                                        ) AS markup
                                FROM PartnerProfiles AS PP
                                ORDER BY markup DESC
                                LIMIT 5
            ");
            return view("mainPage.partner.top-partners", $viewParams);
        } else {
            $viewParams["partners"] = DB::select("SELECT PP.firstName, PP.lastName, (
                                        SELECT COUNT(id) 
                                        FROM Orders AS O
                                        WHERE O.partnerId = PP.partnerId
                                        AND O.statusId IN (3, 5, 6, 8, 9)
                                        AND O.created_at >= '$date'
                                        ) AS ordersCount
                                FROM PartnerProfiles AS PP
                                ORDER BY ordersCount DESC
                                LIMIT 10
            ");
            return view("mainPage.admin.top-partners", $viewParams);

        }
    }

    public static function getPartnersOnline()
    {
        $viewParams = array();
        $date = new DateTime();
        $date->modify('-30 minutes');
        $time = $date->format('Y-m-d H:i:s');

        //return dd($time);

        $viewParams["partnersOnline"] = DB::select("SELECT firstName, lastName, lastActivity 
                                                FROM  `PartnerProfiles` 
                                                WHERE  `lastActivity` >=  '$time'
                                                ORDER BY  `PartnerProfiles`.`lastActivity` DESC ");

        return view("mainPage.admin.partners-online", $viewParams);

    }

    public static function getPartnersStat()
    {
        $viewParams = array();
        $viewParams["stat"] = PartnersController::getMainPageStat();
        return view("mainPage.admin.partners-stat", $viewParams);
    }

    public static function getOrdersStat()
    {
        $viewParams = array();
        $viewParams["stat"] = OrdersController::getMainPageStat();
        return view("mainPage.admin.orders-stat", $viewParams);
    }

    public static function getPersonalStat(){
        $viewParams = array();
        $user = Session::get("user");
        $queryParams = [
            ["partnerId", "=", $user->id]
        ];
        $orders = OrdersController::getOrders(null, $queryParams);

        $viewParams["ordersTotal"] = $orders->count();
        $orders = $orders->filter(function ($order){
            return date("Y-m-d", strtotime($order->created_at)) == date("Y-m-d");
        });
        $viewParams["ordersToday"] = $orders->count();
        $viewParams["balance"] = $user->balance->amount;

        $queryParams = [
            ["partnerId", "=", $user->id],
            ["paymentOperationId", "=", 4],
        ];
        $paymentHistory = PaymentHistoryController::getPaymentHistory(null, $queryParams);
        $viewParams["totalPayout"] = $paymentHistory->sum("price");

        return view("mainPage.partner.personal-stat",$viewParams);
    }

    public static function getNews(){
        return view("mainPage.partner.news");
    }

}