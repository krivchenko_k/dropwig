<?php

namespace App\Http\Controllers;


class ResponseController extends Controller
{
    public static function jsonResponseDone($responseData = []){
        $response = array(
            "success" => true,
            "code" => 200,
            "data" => $responseData
        );

        return json_encode($response);
    }

    public static function jsonResponseFail($errors, $warnings = [], $responseData = []){
        $response = array(
            "success" => false,
            "errors" => $errors,
            "warnings" => $warnings,
            "data" => $responseData
        );

        return json_encode($response);
    }
}
