<?php

namespace App\Http\Controllers;

use App\Models\Orders_GoodsModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class Orders_GoodsController extends Controller
{
    public static function getRowNumbersByOrderId($orderId) {
        return Orders_GoodsModel::getRowNumbersByOrderId($orderId);
    }

    public static function updateGoodsInOrder($orderId, $goodsData) {
        return Orders_GoodsModel::updateGoodsInOrder($orderId, $goodsData);
    }

    public static function addGoodsInOrder($orderId, $goodsData) {
        return Orders_GoodsModel::addGoodsInOrder($orderId, $goodsData);
    }

    public static function deleteGoodsById($rowNumbers) {
        return Orders_GoodsModel::deleteGoodsById($rowNumbers);
    }

}
