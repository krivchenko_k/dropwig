<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\NewsModel;

/**
 * Description of NewsController
 *
 * @author Alexander Alexeev
 */
class NewsController extends Controller {

//    public static function show_news ($id){
//        $news = NewsModel::showNews($id);
//
//        return view('news.index', array(
//                    'news' => $news
//        ));
//    }

    public function index () {
        $viewParams["news"] = NewsModel::getNewsList();
        return view("news.index", $viewParams);
    }


    public static function getNewsTable(){
        $viewParams["news"] = NewsModel::getNewsList();
        return view("news.tableBody", $viewParams);
    }


    public static function getPopup($id)
    {
        $viewsParams = array();
        if ($id != 0)
            $viewsParams["news"] = NewsModel::getNews($id);

        return view("news.popup", $viewsParams);
    }

    public static function addNews(Request $request){
        $data = (object)$request->all();
        $errors = array();
        if (NewsModel::isNewsExist($data->title)){
            $errors[] = "Такая новость уже существует";
        }
        if (empty($data->text)){
            $errors[] = "Новость не может быть пустой";
        }
        if (strlen($data->title) > 255){
            $errors[] = "Длинна заголовка не должна превышать 255 символов";
        }

        if (empty($errors)){
            $newId = NewsModel::addNews($data);
            $responseData = array(
                "id" => $newId
            );

            return ResponseController::jsonResponseDone($responseData);
        }
        else{
            return ResponseController::jsonResponseFail($errors);
        }

    }

    public static function editNews(Request $request){
        $data = (object)$request->all();
        NewsModel::editNews($data);

        return ResponseController::jsonResponseDone();
    }

    public static function deleteNews(Request $request){
        $newsId = json_decode($request->newsId);
        NewsModel::deleteNews($newsId);

        $del_id = implode(", ", $newsId);

        return ResponseController::jsonResponseDone();
    }

    /**
     * @return string
     */
    public static function getNewsList()
    {
        $news = NewsModel::query()->get();
        $responseData = array(
            "news" => $news
        );

        return ResponseController::jsonResponseDone($responseData);
    }
}
