<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpFoundation\Str;

class SSENotificationsController extends Controller
{
    public function index()
    {
        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');

        $registrationRequestsCount = PartnersController::getPartnersByStatus(2)->count();
        $moderationRequestsCount = OrdersController::getOrdersByStatus(2)->count();
        $paymentRequestsCount = PaymentRequestsController::getActiveRequestsCount();

        echo "retry: 5000" . PHP_EOL;
        echo 'data: { "registrationRequestsCount" : "' . $registrationRequestsCount
             . '", "moderationRequestsCount" : "' . $moderationRequestsCount
             . '", "paymentRequestsCount" : "' . $paymentRequestsCount . '"';
        echo "}" . PHP_EOL . PHP_EOL;

//        ob_flush();
        flush();
    }
}
