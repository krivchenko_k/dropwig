<?php

namespace App\Http\Controllers;

use App\Models\DeliveryServicesModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class DeliveryServicesController extends Controller
{
    public static function index()
    {
        $viewParams = array();
        $viewParams["deliveryServices"] = DeliveryServicesModel::getDeliveryServices();
        return view("deliveryServices.index", $viewParams);
    }

    public static function getPopup($id)
    {
        $viewsParams = array();
        if ($id != 0)
            $viewsParams["deliveryService"] = DeliveryServicesModel::getDeliveryService($id);

        return view("deliveryServices.popup", $viewsParams);
    }

    public static function addDeliveryService(Request $request){
        $errors = array();
        if (DeliveryServicesModel::isDeliveryServiceExist($request->name)){
            $errors[] = "Такой статус уже существует";
        }
        elseif (empty($request->name)){
            $errors[] = "Название статуса не может быть пустым";
        }

        if (empty($errors)){
            $DeliveryService = DeliveryServicesModel::addDeliveryService($request->name);
            $responseData = array(
                "id" => $DeliveryService->id
            );


            return ResponseController::jsonResponseDone($responseData);
        }
        else{
            return ResponseController::jsonResponseFail($errors);
        }

    }

    public static function editDeliveryService(Request $request){
        $data = (object)$request->all();

        $prevDeliveryService = DeliveryServicesModel::getDeliveryService($data->id);

        $currDeliveryService = DeliveryServicesModel::editDeliveryService($data);


        return ResponseController::jsonResponseDone();
    }

    public static function deleteDeliveryServices(Request $request){
        $deliveryServicesId = json_decode($request->deliveryServicesId);
        DeliveryServicesModel::deleteDeliveryServices($deliveryServicesId);


        return ResponseController::jsonResponseDone();
    }

    public static function getDeliveryServicesTable(){
        $viewParams["deliveryServices"] = DeliveryServicesModel::getDeliveryServices();
        return view("deliveryServices.tableBody", $viewParams);
    }

    public static function getDeliveryServices() {
        return DeliveryServicesModel::getDeliveryServices();
    }
}
