<?php

namespace App\Http\Controllers;

use App\Models\UserStatusesModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class UserStatusesController extends Controller
{
    public static function getUserStatuses(){
        return UserStatusesModel::get();
    }
}
