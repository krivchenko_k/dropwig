<?php

namespace App\Http\Controllers;

use App\Models\PartnerProfilesModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class PartnerProfilesController extends Controller
{
    public static function isPartnerEmailExitst($email){
        return PartnerProfilesModel::isEmailExist($email);
    }
    public static function isPartnerPhoneExitst($phone){
        return PartnerProfilesModel::isPhoneExist($phone);
    }
    public static function isPartnerNicknameExitst($nickname){
        return PartnerProfilesModel::isNicknameExist($nickname);
    }
    public static function isPartnerCardExitst($card){
        return PartnerProfilesModel::isCardExist($card);
    }

    public static function updateLastActivity($partnerId){
        PartnerProfilesModel::updateLastActivity($partnerId);
    }
}
