<?php

namespace App\Http\Controllers;

use App\Models\DeliveryServicesModel;
use App\Models\GoodsCategoriesModel;
use App\Models\OrdersModel;
use App\Models\OrderStatusesModel;
use App\Models\PaymentTypesModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

define('NP_DELIVERY_SERVICE', 2);

class OrdersController extends Controller
{
    //should be moved to the commons static class:
    private static function validateField($field)
    {
        $result = trim($field);
        $result = stripslashes($result);
        $result = htmlspecialchars($result);
        return $result;
    }

    //should be moved to the commons static class:
    public static function getValidDataFromRequestObject($request, $keys, $exceptions, &$errors)
    {

        $data = array();
        foreach ($keys as $key) {
            $value = $request->get($key);
            if (gettype($value) == 'boolean') {
                $data[$key] = $value;
            } else if (isset($value) && ($value != "")) {
                $data[$key] = self::validateField($value);
            } else {
                if (in_array($key, $exceptions)) {
                    $data[$key] = null;
                } else {
                    $errors[] = "Не заполнено: " . $key;
                }
            }
        }
        return $data;
    }

    //should be moved to the commons static class:
    public static function getValidDataFromRequestArray($request, $keys, $exceptions, &$errors)
    {

        $data = array();
        foreach ($keys as $key) {
            if (array_key_exists($key, $request)) {
                $value = $request[$key];
                if (gettype($value) == 'boolean') {
                    $data[$key] = $value;
                } else if (isset($value) && ($value != "")) {
                    $data[$key] = self::validateField($value);
                } else {
                    if (in_array($key, $exceptions)) {
                        $data[$key] = null;
                    } else {
                        $errors[] = "Не заполнено: " . $key;
                    }
                }
            } else {
                $data[$key] = null;
            }
        }
        return $data;
    }

    public function index()
    {
        $viewParams = array();
        $viewParams["selectedStatus"] = 1;
        $viewParams["orderStatuses"] = OrderStatusesController::getOrderStatuses();
        $viewParams["paymentTypes"] = PaymentTypeController::getPaymentTypes();
        $viewParams["deliveryServices"] = DeliveryServicesController::getDeliveryServices();
        $viewParams["partners"] = PartnersController::getPartners();

        $user = Session::get("user");
        if ($user->groupId == 1 || $user->groupId == 2)
            return view('orders.index', $viewParams);
        else
            return view('orders.partnerIndex', $viewParams);
    }

    public function getTableBody(Request $request)
    {
        $statusId = $request->statusId;

        $params = [];

        if (!empty($statusId)) {
            $params[] = ['statusId', '=', $statusId];
        }

        $viewParams = array();
        $viewParams["selectedStatus"] = $statusId;
        //$orders = OrdersModel::getOrdersByStatusId($statusId);


        if (!empty($request->startDate)) {
            $params[] = [DB::raw('DATE(created_at)'), '>=', date('Y-m-d', strtotime($request->startDate))];
        }

        if (!empty($request->endDate)) {
            $params[] = [DB::raw('DATE(created_at)'), '<=', date('Y-m-d', strtotime($request->endDate))];
        }

        $orders = OrdersModel::get(null, $params);

        if (!empty($request->deliveryServiceId)) {
            $orders = $orders->filter(function ($order) use ($request) {
                return $order->deliveryParams->deliveryServiceId == $request->deliveryServiceId;
            });
        }

        if (!empty($request->paymentTypeId)) {
            $orders = $orders->filter(function ($order) use ($request) {
                return $order->paymentTypeId == $request->paymentTypeId;
            });
        }

        if (!empty($request->partnerId)) {
            $orders = $orders->filter(function ($order) use ($request) {
                return $order->partnerId == $request->partnerId;
            });
        }

        $user = Session::get("user");
        if ($user->groupId == 3) {
            $orders = $orders->filter(function ($order) {
                return $order->partnerId == Session::get("user")->id;
            });
        }

        $orders = $orders->sortByDesc("id");
        foreach ($orders as $order) {
            $order->price = 0;
            foreach ($order->goodsInOrder as $goods) {
                $order->price += ($goods->goodsPrice + $goods->markup) * $goods->quantity;
                $order->goodsQuantity += $goods->quantity;
            }
        }
        $viewParams['orders'] = $orders;
        $user = Session::get("user");
        if ($user->groupId == 1 || $user->groupId == 2)
            return view('orders.tableBody', $viewParams);
        else
            return view('orders.partnerTableBody', $viewParams);


    }

    public function getOrderById($orderId)
    {
        $user = Session::get("user");

        $viewParams = array();

        $viewParams['paymentTypes'] = PaymentTypesModel::getPaymentTypes();
        $viewParams['deliveryServices'] = DeliveryServicesModel::getDeliveryServices();
        $viewParams['categories'] = GoodsCategoriesModel::getGoodsCategories();
        $viewParams['orderStatuses'] = OrderStatusesModel::get();

        $viewParams['cities'] = NPCitiesController::getCities();
        if ($orderId == 0) {
            $viewParams['offices'] = NPWarehousesController::getWarehousesByCityId(1);
            $viewParams['goodsList'] = GoodsController::getGoods();
            $viewParams['partners'] = PartnersController::getPartners();

            if ($user->groupId == 1 || $user->groupId == 2)
                return view('orders.popup', $viewParams);
            else
                return view('orders.partnerPopup', $viewParams);
        }
        $order = OrdersModel::getOrderById($orderId);
        $viewParams['order'] = $order;
        $viewParams['goodsList'] = GoodsController::getGoods();
        $goodsIdArray = array();
        foreach ($viewParams['order']->goodsInOrder as $goods) {
            $goodsIdArray[] = $goods->goodsId;
        }

        if (!empty($viewParams['order']) && !empty($viewParams['order']->deliveryParams)) {
            if (!empty($viewParams['order']->deliveryParams->deliveryServiceId)){
                if ($viewParams['order']->deliveryParams->deliveryServiceId == NP_DELIVERY_SERVICE){
                    if (!empty($viewParams['order']->deliveryParams->NPCityId)){
                        $viewParams['offices'] = NPWarehousesController::getWarehousesByCityId($viewParams['order']->deliveryParams->NPCityId);
                    }
                }
            }
        }
        if ($user->groupId == 1 || $user->groupId == 2)
            return view('orders.popup', $viewParams);
        else {
            if ($order->statusId == 1 || $order->statusId == 4) {
                return view('orders.partnerPopup', $viewParams);
            } else
                return view('orders.partnerReadOnlyPopup', $viewParams);
        }
    }

    public function getOrdersByStatusId($statusId)
    {
        $viewParams = array();
        $viewParams["selectedStatus"] = $statusId;
        $orders = OrdersModel::getOrdersByStatusId($statusId);

        $user = Session::get("user");
        if ($user->groupId == 3) {
            $orders = $orders->filter(function ($order) {
                return $order->partnerId == Session::get("user")->id;
            });
        }

        $orders = $orders->sortByDesc("id");
        foreach ($orders as $order) {
            $order->price = 0;
            foreach ($order->goodsInOrder as $goods) {
                $order->price += ($goods->goodsPrice + $goods->markup) * $goods->quantity;
                $order->goodsQuantity += $goods->quantity;
            }
        }
        $viewParams['orders'] = $orders;
        $user = Session::get("user");
        if ($user->groupId == 1 || $user->groupId == 2)
            return view('orders.tableBody', $viewParams);
        else
            return view('orders.partnerTableBody', $viewParams);


    }


    public function sendOrderToModeration($orderId)
    {
        OrdersModel::setStatus($orderId, 2);
    }

    public function addOrder(Request $request)
    {
        try {
            $insertData = json_decode($request->insertData);
            if (isset($insertData) && !empty($insertData)) {
                $errors = array();
                $keys = array();
                $exceptions = array();

                if (Session::get("user")->groupId == 3) {
                    $insertData->orderData->orderStatusId = 1;
                    $insertData->orderData->partnerId = Session::get("user")->id;
                }

                if (isset($insertData->orderData->deliveryServiceId)) {
                    if ($insertData->orderData->deliveryServiceId == NP_DELIVERY_SERVICE) {
                        $keys = [
                            'partnerId', 'orderStatusId', 'clientFirstName', 'clientMiddleName', 'clientLastName', 'orderPhone',
                            'paymentTypeId', 'deliveryServiceId', 'orderNPCityId', 'orderNPAddressId',
                            'orderComment', "invoice"
                        ];
                    } else {
                        $keys = [
                            'partnerId', 'orderStatusId', 'clientFirstName', 'clientMiddleName', 'clientLastName', 'orderPhone',
                            'paymentTypeId', 'deliveryServiceId', 'orderCity', 'orderAddress',
                            'orderComment', "invoice"
                        ];
                    }
                }
                $exceptions = ['clientMiddleName', 'orderNPCityId', 'orderNPAddressId', 'orderCity', 'orderAddress', 'orderComment', "invoice"];

                $orderData = (object)self::getValidDataFromRequestArray(get_object_vars($insertData->orderData), $keys, $exceptions, $errors);
                if (count($errors)) {
                    return ResponseController::jsonResponseFail($errors);
                }

                $goodsInOrder = array();
                $keys = ['goodsId', 'categoryId', 'markup', 'quantity'];


                foreach ($insertData->goods as $key => $goods) {
                    $goodsInOrder[] = (object)self::getValidDataFromRequestArray(get_object_vars($goods), $keys, $exceptions, $errors);
                }
                if (count($errors)) {
                    return ResponseController::jsonResponseFail($errors);
                }

                $orderPrice = $orderMarkup = $orderGoodsQuantity = 0;
                foreach ($goodsInOrder as $goods) {
                    $dropPrice = GoodsController::getGoodsPriceById($goods->goodsId);
                    $orderPrice += ($dropPrice + $goods->markup) * $goods->quantity;
                    $orderMarkup += $goods->markup * $goods->quantity;
                    $goods->goodsPrice = $dropPrice;
                }
                $orderData->orderPrice = $orderPrice;
                $orderData->orderMarkup = $orderMarkup;

//                if (empty($orderData->clientMiddleName))
//                    $orderData->clientMiddleName = "";
//                $clientId = ClientsController::getClientIdByName($orderData->clientFirstName, $orderData->clientMiddleName, $orderData->clientLastName);
//                if ($clientId < 0) {
                $clientId = ClientsController::addClient([
                    'firstName' => $orderData->clientFirstName,
                    'middleName' => $orderData->clientMiddleName,
                    'lastName' => $orderData->clientLastName,
                ]);
//                }

                if (empty($orderData->invoice)) {
                    $orderData->invoice = null;
                }

                $orderData->clientId = $clientId;

                if (Session::get("user")->groupId == 1 || Session::get("user")->groupId == 2) {
                    $orderData->staffId = Session::get("user")->id;
                } else {
                    $orderData->staffId = null;
                }


                $orderId = OrdersModel::addOrder($orderData);
                $orderData->orderId = $orderId;
                OrderDetailsController::addOrderDetails($orderData);

                if ($orderData->orderStatusId == 3 && $orderData->paymentTypeId == 3) {
                    PaymentHistoryController::addPaymentHistoryRecord((object)['partnerId' => $orderData->partnerId, 'orderId' => $orderData->orderId, 'paymentOperationId' => 6, 'price' => $orderData->orderPrice]);
                    PartnerBalancesController::updateBalance($orderData->partnerId, (-1) * $orderData->orderPrice);
                }
                if ($orderData->orderStatusId == 7) {
                    PaymentHistoryController::addPaymentHistoryRecord((object)['partnerId' => $orderData->partnerId, 'orderId' => $orderData->orderId, 'paymentOperationId' => 2, 'price' => $orderData->orderMarkup]);
                    PartnerBalancesController::updateBalance($orderData->partnerId, -60);
                }
                if ($orderData->orderStatusId == 8) {
                    PaymentHistoryController::addPaymentHistoryRecord((object)['partnerId' => $orderData->partnerId, 'orderId' => $orderData->orderId, 'paymentOperationId' => 1, 'price' => $orderData->orderMarkup]);
                    PartnerBalancesController::updateBalance($orderData->partnerId, $orderData->orderMarkup);
                }

                foreach ($goodsInOrder as $goods) {
                    Orders_GoodsController::addGoodsInOrder($orderId, $goods);
                }

                if ($orderData->deliveryServiceId == NP_DELIVERY_SERVICE) {
                    OrderDeliveryParamsController::addNPDeliveryParams($orderData);
                } else {
                    if (!empty($orderData->orderCity)) {
                        $commonCityId = CommonCitiesController::getCityIdByName($orderData->orderCity);
                        if ($commonCityId < 0) {
                            $commonCityId = CommonCitiesController::addCity($orderData->orderCity);
                        }
                        $orderData->commonCityId = $commonCityId;
                    } else {
                        $orderData->commonCityId = null;
                    }
                    if (!empty($orderData->orderAddress)) {
                        $commonAddressId = CommonAddressesController::getAddressIdByName($orderData->orderAddress);
                        if ($commonAddressId < 0) {
                            $commonAddressId = CommonAddressesController::addAddress($orderData->orderAddress);
                        }
                        $orderData->commonAddressId = $commonAddressId;
                    } else {
                        $orderData->commonAddressId = null;
                    }
                    OrderDeliveryParamsController::addCommonDeliveryParams($orderData);

                }

                return ResponseController::jsonResponseDone();
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public static function updateOrder(Request $request)
    {

        try {
            $updateData = json_decode($request->updateData);
            if (isset($updateData) && !empty($updateData)) {
                $errors = array();
                $keys = array();
                $exceptions = array();

                if (isset($updateData->orderData->deliveryServiceId)) {
                    if ($updateData->orderData->deliveryServiceId == NP_DELIVERY_SERVICE) {
                        $keys = [
                            'orderStatusId', 'orderId', 'clientFirstName', 'clientMiddleName', 'clientLastName', 'orderPhone',
                            'paymentTypeId', 'deliveryServiceId', 'orderNPCityId', 'orderNPAddressId',
                            'orderComment', 'invoice'
                        ];
                    } else {
                        $keys = [
                            'orderStatusId', 'orderId', 'clientFirstName', 'clientMiddleName', 'clientLastName', 'orderPhone',
                            'paymentTypeId', 'deliveryServiceId', 'orderCity', 'orderAddress',
                            'orderComment', 'invoice'
                        ];
                    }
                }
                $exceptions = ['clientMiddleName', 'orderNPCityId', 'orderNPAddressId', 'orderCity', 'orderAddress', 'orderComment', 'invoice'];


                $prevOrder = self::getOrder($updateData->orderData->orderId);
                $orderData = (object)self::getValidDataFromRequestArray(get_object_vars($updateData->orderData), $keys, $exceptions, $errors);

                if (count($errors)) {
                    return ResponseController::jsonResponseFail($errors);
                }
                if (!isset($orderData->orderId) || $orderData->orderId <= 0) {
                    $errors[] = "There is no order related to the request data";
                    return ResponseController::jsonResponseFail($errors);
                }

                $goodsInOrder = array();
                $keys = ['goodsId', 'rowNumber', 'categoryId', 'markup', 'quantity'];


                foreach ($updateData->goods as $key => $goods) {
                    $goodsInOrder[] = (object)self::getValidDataFromRequestArray(get_object_vars($goods), $keys, $exceptions, $errors);
                }
                if (count($errors)) {
                    return ResponseController::jsonResponseFail($errors);
                }

                $orderPrice = $orderMarkup = $orderGoodsQuantity = 0;
                $goodsRowNumbers = Orders_GoodsController::getRowNumbersByOrderId($orderData->orderId);

                foreach ($goodsInOrder as $goods) {
                    $dropPrice = GoodsController::getGoodsPriceById($goods->goodsId);
                    $orderPrice += ($dropPrice + $goods->markup) * $goods->quantity;
                    $orderMarkup += $goods->markup * $goods->quantity;
                    $goods->goodsPrice = $dropPrice;

                    if (in_array($goods->rowNumber, $goodsRowNumbers)) {
                        //update
                        Orders_GoodsController::updateGoodsInOrder($orderData->orderId, $goods);
                        $key = array_search($goods->rowNumber, $goodsRowNumbers);
                        unset($goodsRowNumbers[$key]);
                    } else if ($goods->rowNumber == '0') {
                        //insert
                        Orders_GoodsController::addGoodsInOrder($orderData->orderId, $goods);
                    }
                }
                $orderData->orderPrice = $orderPrice;
                $orderData->orderMarkup = $orderMarkup;

                if (count($goodsRowNumbers)) {
                    Orders_GoodsController::deleteGoodsById($goodsRowNumbers);
                }

                $currentOrderData = OrdersModel::getOrderById($orderData->orderId);

                if (empty($orderData->orderStatusId)){
//                if (Session::get("user")->groupId == 3 && $orderData->orderStatusId != 2){
                    $orderData->orderStatusId = $currentOrderData->statusId;
                }

                if (!isset($currentOrderData) || empty($currentOrderData)) {
                    $errors[] = "Заказ №" . $orderData->orderId . " не существует";
                    return ResponseController::jsonResponseFail($errors);
                }


                ClientsController::editClient($currentOrderData->clientId, [
                    'firstName' => $orderData->clientFirstName,
                    'middleName' => $orderData->clientMiddleName,
                    'lastName' => $orderData->clientLastName,
                ]);
                $orderData->clientId = $currentOrderData->clientId;
                $orderData->partnerId = $currentOrderData->partnerId;

                if (empty($orderData->invoice)) {
                    $orderData->invoice = null;
                }

                if (Session::get("user")->groupId == 1 || Session::get("user")->groupId == 2) {
                    $orderData->staffId = Session::get("user")->id;
                } else {
                    $orderData->staffId = null;
                }

                OrdersModel::updateOrder($orderData);
                OrderDetailsController::updateOrderDetails($orderData);

                if ($currentOrderData->statusId != $orderData->orderStatusId){
                    if ($currentOrderData->statusId != 3 && $orderData->orderStatusId == 3 && $orderData->paymentTypeId == 3) {
                        PaymentHistoryController::addPaymentHistoryRecord((object)['partnerId' => $orderData->partnerId, 'orderId' => $orderData->orderId, 'paymentOperationId' => 6, 'price' => $orderData->orderPrice]);
                        PartnerBalancesController::updateBalance($orderData->partnerId, (-1) * $orderData->orderPrice);
                    }
                    elseif ($currentOrderData->statusId == 3 && ($orderData->orderStatusId == 1 || $orderData->orderStatusId == 4) && $currentOrderData->paymentTypeId == 3) {
                        PaymentHistoryController::addPaymentHistoryRecord((object)['partnerId' => $orderData->partnerId, 'orderId' => $orderData->orderId, 'paymentOperationId' => 6, 'price' => $orderData->orderPrice]);
                        PartnerBalancesController::updateBalance($orderData->partnerId, $orderData->orderPrice);
                    }
                    elseif ($currentOrderData->statusId != 7 && $orderData->orderStatusId == 7) {
                        PaymentHistoryController::addPaymentHistoryRecord((object)['partnerId' => $orderData->partnerId, 'orderId' => $orderData->orderId, 'paymentOperationId' => 2, 'price' => $orderData->orderMarkup]);
                        PartnerBalancesController::updateBalance($orderData->partnerId, -60);
                    }
                    elseif ($currentOrderData->statusId == 7 && $orderData->orderStatusId != 7) {
                        PaymentHistoryController::addPaymentHistoryRecord((object)['partnerId' => $orderData->partnerId, 'orderId' => $orderData->orderId, 'paymentOperationId' => 2, 'price' => $orderData->orderMarkup]);
                        PartnerBalancesController::updateBalance($orderData->partnerId, 60);
                    }
                    elseif ($currentOrderData->statusId != 8 && $orderData->orderStatusId == 8) {
                        PaymentHistoryController::addPaymentHistoryRecord((object)['partnerId' => $orderData->partnerId, 'orderId' => $orderData->orderId, 'paymentOperationId' => 1, 'price' => $orderData->orderMarkup]);
                        PartnerBalancesController::updateBalance($orderData->partnerId, $orderData->orderMarkup);
                    }
                    elseif ($currentOrderData->statusId == 8 && $orderData->orderStatusId != 8 && $orderData->orderStatusId != 9 && $orderData->orderStatusId != 10) {
                        PaymentHistoryController::addPaymentHistoryRecord((object)['partnerId' => $orderData->partnerId, 'orderId' => $orderData->orderId, 'paymentOperationId' => 1, 'price' => $orderData->orderMarkup]);
                        PartnerBalancesController::updateBalance($orderData->partnerId, (-1) * $orderData->orderMarkup);
                    }
                }

                if ($orderData->deliveryServiceId == NP_DELIVERY_SERVICE) {
                    OrderDeliveryParamsController::updateNPDeliveryParams($orderData);
                } else {
                    if (!empty($orderData->orderCity)) {
                        $commonCityId = CommonCitiesController::getCityIdByName($orderData->orderCity);
                        if ($commonCityId < 0) {
                            $commonCityId = CommonCitiesController::addCity($orderData->orderCity);
                        }
                        $orderData->commonCityId = $commonCityId;
                    } else {
                        $orderData->commonCityId = null;
                    }
                    if (!empty($orderData->orderAddress)) {
                        $commonAddressId = CommonAddressesController::getAddressIdByName($orderData->orderAddress);
                        if ($commonAddressId < 0) {
                            $commonAddressId = CommonAddressesController::addAddress($orderData->orderAddress);
                        }
                        $orderData->commonAddressId = $commonAddressId;
                    } else {
                        $orderData->commonAddressId = null;
                    }
                    OrderDeliveryParamsController::updateCommonDeliveryParams($orderData);

                }

                return ResponseController::jsonResponseDone();
            }
        } catch
        (Exception $ex) {
            return $ex;
        }
    }


    public static function removeOrders(Request $request)
    {
        $removalOrders = json_decode($request->removalOrders);
        if (!isset($removalOrders) || empty($removalOrders)) {
            return;
        }

        OrdersModel::remove($removalOrders);
    }

    public static function recoverOrders($idArray)
    {
        $recoveryId = implode(", ", $idArray);

        OrdersModel::recover($idArray);
    }

    public static function eraseOrders($idArray)
    {
        $eraseId = implode(", ", $idArray);

        OrdersModel::erase($idArray);
    }

    public static function getOrders($idArray = "", $params = "")
    {
        $orders = OrdersModel::get($idArray, $params);
        return $orders;
    }

    public static function editOrders($idArray, $params)
    {
        OrdersModel::edit($idArray, $params);
    }

    public static function updateStatusForGroup($ordersIdArray, $statusId)
    {
        OrdersModel::setStatusForGroup($ordersIdArray, $statusId);
    }

    public static function updateOrderStatus($orderId, $statusId)
    {
        $order = OrdersController::getOrders($orderId, null);
        if ($order->statusId != $statusId) {
            OrdersController::calculateSummaries($order);
            if ($statusId == 3 && $order->paymentTypeId == 3) {
                PaymentHistoryController::addPaymentHistoryRecord((object)['partnerId' => $order->partnerId, 'orderId' => $order->id, 'paymentOperationId' => 6, 'price' => $order->price]);
                PartnerBalancesController::updateBalance($order->partnerId, (-1) * $order->price);
            }
            if ($statusId == 7) {
                PaymentHistoryController::addPaymentHistoryRecord((object)['partnerId' => $order->partnerId, 'orderId' => $order->id, 'paymentOperationId' => 2, 'price' => $order->markup]);
                PartnerBalancesController::updateBalance($order->partnerId, -60);
            }
            if ($statusId == 8) {
                PaymentHistoryController::addPaymentHistoryRecord((object)['partnerId' => $order->partnerId, 'orderId' => $order->id, 'paymentOperationId' => 1, 'price' => $order->markup]);
                PartnerBalancesController::updateBalance($order->partnerId, $order->markup);
            }
            OrdersModel::setStatus($orderId, $statusId);
        }
    }

    public static function getMainPageStat()
    {
        $stat = array();
        $dateFrom = date("Y-m-d");
        $dateTo = date("Y-m-d H:i:s");
        $stat["ordersPerDay"] = OrdersModel::getOrdersByPeriod($dateFrom, $dateTo)->count();
        $dateFrom = date('Y-m-d', time() - (date('N') - 1) * 24 * 60 * 60);
        $stat["ordersPerWeek"] = OrdersModel::getOrdersByPeriod($dateFrom, $dateTo)->count();
        $dateFrom = date('Y-m-d', time() - (date('j') - 1) * 24 * 60 * 60);
        $stat["ordersPerMonth"] = OrdersModel::getOrdersByPeriod($dateFrom, $dateTo)->count();

        $systemStartDate = strtotime("2016-09-29");
        $countOfDays = ceil((time() - $systemStartDate) / (24 * 60 * 60));
        $countOfDays = ($countOfDays < 30) ? $countOfDays : 30;
        $dateFrom = date('Y-m-d', time() - $countOfDays * 24 * 60 * 60);
        $stat["ordersPerDayAvg"] = floor(OrdersModel::getOrdersByPeriod($dateFrom, $dateTo)->count() / $countOfDays);

        return $stat;
    }

    public static function getOrdersByPeriod($dateFrom = "", $dateTo = "")
    {
        return OrdersModel::getOrdersByPeriod($dateFrom, $dateTo);
    }

    public static function getParthersCreatedOrderPerWeekCount()
    {
        return OrdersModel::getParthersCreatedOrderPerWeekCount();
    }

    public static function getOrdersForPartnerByPeriod($partnerId, $dateFrom, $dateTo)
    {
        return OrdersModel::getOrdersForPartnerByPeriod($partnerId, $dateFrom, $dateTo);
    }

    public static function getOrdersByStatus($statusId)
    {
        return OrdersModel::getOrdersByStatusId($statusId);
    }

    public static function getOrder($orderId)
    {
        return OrdersModel::getOrderById($orderId);
    }

    public static function getTrashedOrder($orderId)
    {
        return OrdersModel::getOrderById($orderId, true);
    }

    public static function setOrderAcceptorId($orderId, $staffId)
    {
        OrdersModel::setOrderAcceptorId($orderId, $staffId);
    }

    public static function getTrashedOrders()
    {
        return OrdersModel::getTrashedOrders();
    }

    public static function getOrdersByIdArray($orderIdArray)
    {
        return OrdersModel::getOrdersByIdArray($orderIdArray);
    }

    public static function calculateSummaries($orders)
    {
        if (is_array($orders) || get_class($orders) == 'Illuminate\Database\Eloquent\Collection') {
            foreach ($orders as $order) {
                $order->price = 0;
                $order->markup = 0;
                foreach ($order->goodsInOrder as $goods) {
                    $order->price += ($goods->goodsPrice + $goods->markup) * $goods->quantity;
                    $order->markup += $goods->markup * $goods->quantity;
                    $order->goodsQuantity += $goods->quantity;
                }
            }
        } else {
            $orders->price = 0;
            $orders->markup = 0;
            foreach ($orders->goodsInOrder as $goods) {
                $orders->price += ($goods->goodsPrice + $goods->markup) * $goods->quantity;
                $orders->markup += $goods->markup * $goods->quantity;
                $orders->goodsQuantity += $goods->quantity;
            }
        }
    }

    public static function getOrdersByNPUpdate($statuses, $dateFrom, $dateTo)
    {
        return OrdersModel::getOrdersByNPUpdate($statuses, $dateFrom, $dateTo);
    }

    public static function setOrderBackwardInvoice($orderId, $backwardInvoice)
    {
        return OrdersModel::setOrderBackwardInvoice($orderId, $backwardInvoice);
    }

}
