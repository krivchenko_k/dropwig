<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Exception;
use App\Classes\NovaPoshtaHandler;

define('API_NP', '5c08c94a73f622fc74ecca7032cd9400'); 
define('API2_NP', '50edfc46daa5531ec523ab6cb2334406'); //ee684cfc3842d8d0cfd047b05013e84c //4751e0e74f8ec3bd1f60133e3df5c7dd

class NPController extends Controller
{
    public static function makeRequest($url, $request)
    {
        if (!isset($request) || empty($request)) {
            throw new Exception('request data was not set correctly (NPController -> makeRequest()).');
        }

        try {
            $cUrlHandler = curl_init();
            curl_setopt($cUrlHandler, CURLOPT_URL, $url);
            curl_setopt($cUrlHandler, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($cUrlHandler, CURLOPT_HTTPHEADER, Array("Content-Type: application/json"));
            curl_setopt($cUrlHandler, CURLOPT_HEADER, 0);
            curl_setopt($cUrlHandler, CURLOPT_POSTFIELDS, $request);
            curl_setopt($cUrlHandler, CURLOPT_POST, 1);
            curl_setopt($cUrlHandler, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($cUrlHandler);
            curl_close($cUrlHandler);

            return json_decode($response);

        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public static function getCities()
    {
        $request = array();
        $request['modelName'] = 'Address';
        $request['calledMethod'] = 'getCities';
        $request['apiKey'] = API2_NP;

        $url = 'https://api.novaposhta.ua/v2.0/json/';
        return NPController::makeRequest($url, json_encode($request));
    }

    public static function getWarehouses()
    {
        $request = array();
        $request['modelName'] = 'AddressGeneral';
        $request['calledMethod'] = 'getWarehouses';
        $request['apiKey'] = API2_NP;
        $url = 'https://api.novaposhta.ua/v2.0/json/';
        return NPController::makeRequest($url, json_encode($request));
    }

    public static function getWarehouse($cityName, $cityRef)
    {
        if (isset($cityName, $cityRef) && !empty($cityName) && !empty($cityRef)) {
            $request = array();
            $request['modelName'] = 'AddressGeneral';
            $request['calledMethod'] = 'getWarehouses';
            $request['apiKey'] = API2_NP;
            $request['methodProperties'] = array();
            $request['methodProperties']['CityName'] = $cityName;
            $request['methodProperties']['CityRef'] = $cityRef;
            $url = 'https://api.novaposhta.ua/v2.0/json/';
            return NPController::makeRequest($url, json_encode($request));
        }
        return null;
    }

    public static function getStatusByNPCode($code)
    {

        //тут статусы $status заменить на наши

        $status = 0;
        switch ($code) {
            case NovaPoshtaHandler::NP2_STATUS_ORDER_IN_PROCESSING : //1 - Замовлення в обробці
            case NovaPoshtaHandler::NP2_STATUS_DELETED : //2 - Видалено
            case NovaPoshtaHandler::NP2_STATUS_PHONE_NOT_FOUND : //3 - Номер не знайдено
            case NovaPoshtaHandler::NP2_STATUS_PREPARING_TO_DISPATCH : //4 - Готується до відправлення
            case NovaPoshtaHandler::NP2_STATUS_DISPATCHED : //5 - Відправлено
            case NovaPoshtaHandler::NP2_STATUS_GOING_TO_RECIPIENT : //101 - На шляху до одержувача
                $status = 5; //отправлено
                break;
            case NovaPoshtaHandler::NP2_STATUS_RECEIVED1 : //9 - Одержаний
            case NovaPoshtaHandler::NP2_STATUS_RECEIVED2 : //10 - Одержаний
            case 11 : //11 - Одержаний
            case NovaPoshtaHandler::NP2_STATUS_RECEIVED3 : //106 - Одержаний
                $status = 8; //К выплате
                break;
            case NovaPoshtaHandler::NP2_STATUS_RECIPIENT_REFUSED1 : //102 - Відмова
            case NovaPoshtaHandler::NP2_STATUS_RECIPIENT_REFUSED2 : //103 - Відмова
                $status = 7; //НЗ
                break;
            case NovaPoshtaHandler::NP2_STATUS_PREPARING_TO_ISSUE : //6 - Готується до видачі
            case NovaPoshtaHandler::NP2_STATUS_ARRIVED_TO_OFFICE1 : //7 - Прибув у відділення
            case NovaPoshtaHandler::NP2_STATUS_ARRIVED_TO_OFFICE2 : //8 - Прибув у відділення
            case NovaPoshtaHandler::NP2_STATUS_ARRIVED_TO_OFFICE2 : //8 - Прибув у відділення
            case NovaPoshtaHandler::NP2_STATUS_FEE_BILLED_FOR_STORAGE : //107 - Нараховується плата за зберігання
            case NovaPoshtaHandler::NP2_STATUS_STORAGE_STOPPED :
                $status = 6; //отделение
                break;
            case NovaPoshtaHandler::NP2_STATUS_ADDRESS_CHANGED :
                $status = 5; //переадресация
                break;
        }
        return $status;
    }

    public static function updateOrderStatusInDB($npResponse)
    {
        try {
            foreach ($npResponse->data as $item) {
                if (!empty($item->StatusCode)) {
                    $newOrderStatus = self::getStatusByNPCode($item->StatusCode);

                    $queryParams = [
                        ["invoice", "=", $item->Number]
                    ];

                    $order = OrdersController::getOrders(null, $queryParams)[0];

                    if (!empty($item->LastCreatedOnTheBasisNumber)) {
                        OrdersController::setOrderBackwardInvoice($order->id, $item->LastCreatedOnTheBasisNumber);
                    }

                    OrdersController::updateOrderStatus($order->id, $newOrderStatus);

                    echo "Заказ №".$order->id." ТТН: ".$item->Number." Новый статус: " . $newOrderStatus."<br>";
                }
              //  print_r($item); die();
            }
        } catch (Exception $ex) {
            throw $ex;
        }

    }

    public static function getTracking($orders)
    {
        $counter = 0;
        $orderCount = count($orders);
        $updatedOrdersCount = 0;
        $invoiceList = array();

        foreach ($orders as $order) {

            $orderCount--;
            $counter++;

            if ($order->deliveryParams->deliveryServiceId == 2) {
                if (!empty($order->invoice)) {
                    $invoiceList[] = array(
                        'ttn' => $order->invoice,
                        'phone' => $order->phone,
                    );
                }
            }                                                                                                   
            if ($counter % 100 == 0) {
                $npResponse = NovaPoshtaHandler::invoiceListTracking($invoiceList);

                self::updateOrderStatusInDB($npResponse);
                $updatedOrdersCount += count($invoiceList);
                $invoiceList = array();
                $counter = 0;

            }
            if ($orderCount == 0 && !empty($invoiceList)) {
                $npResponse = NovaPoshtaHandler::invoiceListTracking($invoiceList);
                self::updateOrderStatusInDB($npResponse);
                $updatedOrdersCount += count($invoiceList);
                break;
            }
        }
    }


    public static function updateNPOrderStatuses()
    {
        try {
            
            $statuses = [5, 6];
            $dateFrom = date("Y-m-d", time() - 30 * 24 * 60 * 60);
            $dateTo = date("Y-m-d");

            $orders = OrdersController::getOrdersByNPUpdate($statuses, $dateFrom, $dateTo);

            echo "Заказов: ".count($orders)."<br>";
            self::getTracking($orders);

        } catch (Exception $ex) {
            echo $ex;
        }
    }
}
