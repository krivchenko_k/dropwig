<?php

namespace App\Http\Controllers;

use App\Models\OrderStatusesModel;
use Illuminate\Http\Request;

class OrderStatusesController extends Controller
{
    public static function index()
    {
        $viewParams = array();
        $viewParams["orderStatuses"] = OrderStatusesModel::get();
        return view("orderStatuses.index", $viewParams);
    }

    public static function getPopup($id)
    {
        $viewsParams = array();
        if ($id != 0)
            $viewsParams["orderStatus"] = OrderStatusesModel::get($id);

        return view("orderStatuses.popup", $viewsParams);
    }

    public static function addOrderStatus(Request $request){
        $data = (object)$request->all();
        $errors = array();
        if (OrderStatusesModel::isOrderStatusExist($data->name)){
            $errors[] = "Такой статус уже существует";
        }
        elseif (empty($data->name)){
            $errors[] = "Название статуса не может быть пустым";
        }

        if (empty($errors)){
            $newId = OrderStatusesModel::addOrderStatus($data);
            $responseData = array(
                "id" => $newId
            );

            return ResponseController::jsonResponseDone($responseData);
        }
        else{
            return ResponseController::jsonResponseFail($errors);
        }

    }

    public static function editOrderStatus(Request $request){
        $data = (object)$request->all();
        OrderStatusesModel::editOrderStatus($data);

        return ResponseController::jsonResponseDone();
    }

    public static function deleteOrderStatuses(Request $request){
        $orderStatusesId = json_decode($request->orderStatusesId);
        OrderStatusesModel::deleteOrderStatuses($orderStatusesId);

        return ResponseController::jsonResponseDone();
    }

    public static function getOrderStatusesTable(){
        $viewParams["orderStatuses"] = OrderStatusesModel::get();
        return view("orderStatuses.tableBody", $viewParams);
    }

    public static function getOrderStatuses(){
        return OrderStatusesModel::get();
    }
}
