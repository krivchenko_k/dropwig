<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AgreementModel;

/**
 * Description of NewsController
 *
 * @author Alexander Alexeev
 */
class AgreementController extends Controller {

    public function index () {
        $viewParams = array();
        $viewParams["agreement"] = AgreementModel::get();

        return view("settings.agreement.index", $viewParams);
    }

    public function getPopup() {
        $viewsParams = array();
        $viewsParams["agreement"] = AgreementModel::get();

        return view("settings.agreement.popup", $viewsParams);
    }

    public function editAgreement(Request $request) {
        $data = (object)$request->all();
        $errors = array();

        if(empty($data->text)) {
            $errors[] = "Текст соглашения не может быть пустым";
        }

        if(empty($errors)) {
            $prevRejectionReasons = AgreementModel::get();;

            $currRejectionReasons = AgreementModel::saveAgreement($data);

            $edit = AgreementModel::saveAgreement($data);
            $responseData = array(
                "id" => $edit
            );

            return ResponseController::jsonResponseDone($responseData);
        }
        else {
            return ResponseController::jsonResponseFail($errors);
        }
    }


}