<?php

namespace App\Http\Controllers;

use App\Models\Partners_RejectionReasonsModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class Partners_RejectionReasonsController extends Controller
{
    public static function addRejection($partnerId, $rejectionReasonId = null, $text = null) {
        return Partners_RejectionReasonsModel::add($partnerId, $rejectionReasonId, $text);
    }
}
