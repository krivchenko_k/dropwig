<?php

namespace App\Http\Controllers;

use App\Models\CommonCitiesModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class CommonCitiesController extends Controller
{
    public static function getCities() {
        return CommonCitiesModel::getAddresses();
    }

    public static function addCity($cityName) {
        return CommonCitiesModel::addCity($cityName);
    }

    public static function getCityById($cityId) {
        return CommonCitiesModel::getCityById($cityId);
    }

    public static function getCityIdByName($cityName) {
        return CommonCitiesModel::getCityIdByName($cityName);
    }
}