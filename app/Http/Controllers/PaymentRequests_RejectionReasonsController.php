<?php

namespace App\Http\Controllers;

use App\Models\PaymentRequests_RejectionReasonsModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class PaymentRequests_RejectionReasonsController extends Controller
{
    public static function addRejection($paymentRequestId, $rejectionReasonId = null, $text = null) {
        return PaymentRequests_RejectionReasonsModel::add($paymentRequestId, $rejectionReasonId, $text);
    }
}
