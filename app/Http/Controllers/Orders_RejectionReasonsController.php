<?php

namespace App\Http\Controllers;

use App\Models\Orders_RejectionReasonsModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class Orders_RejectionReasonsController extends Controller
{
    public static function addRejection($orderId, $rejectionReasonId = null, $text = null) {
        return Orders_RejectionReasonsModel::add($orderId, $rejectionReasonId, $text);
    }
}
