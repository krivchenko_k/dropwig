<?php

namespace App\Http\Controllers;

use App\Models\OrderDetailsModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class OrderDetailsController extends Controller
{
    public static function addOrderDetails($orderData) {
        return OrderDetailsModel::addOrderDetails($orderData);
    }

    public static function updateOrderDetails($orderData) {
        return OrderDetailsModel::updateOrderDetails($orderData);
    }
}
