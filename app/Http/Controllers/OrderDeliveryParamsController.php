<?php

namespace App\Http\Controllers;

use App\Models\OrderDeliveryParamsModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class OrderDeliveryParamsController extends Controller
{

    public static function addNPDeliveryParams($orderData) {
        return OrderDeliveryParamsModel::addNPDeliveryParams($orderData);
    }

    public static function addCommonDeliveryParams($orderData) {
        return OrderDeliveryParamsModel::addCommonDeliveryParams($orderData);
    }

    public static function updateNPDeliveryParams($orderData) {
        return OrderDeliveryParamsModel::updateNPDeliveryParams($orderData);
    }

    public static function updateCommonDeliveryParams($orderData) {
        return OrderDeliveryParamsModel::updateCommonDeliveryParams($orderData);
    }

    public static function parametricDeleteDeliveryParams($params){
        return OrderDeliveryParamsModel::parametricDelete($params);
    }

}
