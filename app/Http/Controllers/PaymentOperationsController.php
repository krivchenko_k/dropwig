<?php

namespace App\Http\Controllers;

use App\Models\PaymentHistoryModel;
use App\Models\PaymentOperationsModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class PaymentOperationsController extends Controller
{
    public static function get($id = 0) {
        return PaymentOperationsModel::get($id);
    }
}
