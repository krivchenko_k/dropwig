<?php

namespace App\Http\Controllers;

use App\Models\ClientsModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class ClientsController extends Controller
{
    
    public static function checkExistance($userFirstName, $userMiddleName, $userLastName) {
        return ClientsModel::isExist($userFirstName, $userMiddleName, $userLastName);
    }

    public static function getClientIdByName($userFirstName, $userMiddleName, $userLastName) {
        return ClientsModel::getClientIdByName($userFirstName, $userMiddleName, $userLastName);
    }

    public static function addClient($clientData) {
        return ClientsModel::addClient((object)$clientData);
    }

    public static function editClient($clientId, $clientData){
        return ClientsModel::editClient($clientId, (object)$clientData);
    }
}
