<?php

namespace App\Http\Controllers;

use App\Models\RejectionTypesModel;
use Illuminate\Http\Request;
use App\Http\Requests;

class RejectionTypesController extends Controller
{
    public static function index()
    {
        $viewParams = array();
        $viewParams["rejectionTypes"] = RejectionTypesModel::getRejectionTypes();
        return view("rejectionTypes.index", $viewParams);
    }

    public static function getRejectionTypesTable(){
        $viewParams["rejectionTypes"] = RejectionTypesModel::getRejectionTypes();
        return view("rejectionTypes.tableBody", $viewParams);
    }

    public static function getPopup($id)
    {
        $viewsParams = array();
        if ($id != 0)
            $viewsParams["rejectionType"] = RejectionTypesModel::getRejectionType($id);

        return view("rejectionTypes.popup", $viewsParams);
    }

    public static function addRejectionType(Request $request){
        $data = (object)$request->all();
        $errors = array();
        if (RejectionTypesModel::isRejectionTypeExist($data->name)){
            $errors[] = "Такой тип отказа уже существует";
        }
        elseif (empty($data->name)){
            $errors[] = "Название типа отказа не может быть пустым";
        }

        if (empty($errors)){
            $RejectionType = RejectionTypesModel::addRejectionType($data);
            $responseData = array(
                "id" => $RejectionType->id
            );


            return ResponseController::jsonResponseDone($responseData);
        }
        else{
            return ResponseController::jsonResponseFail($errors);
        }

    }

    public static function editRejectionType(Request $request){
        $data = (object)$request->all();

        $prevRejectionType = RejectionTypesModel::getRejectionType($data->id);

        $currRejectionType = RejectionTypesModel::editRejectionType($data);


        return ResponseController::jsonResponseDone();
    }

    public static function deleteRejectionTypes(Request $request){
        $rejectionTypesId = json_decode($request->rejectionTypesId);
        RejectionTypesModel::deleteRejectionTypes($rejectionTypesId);

        $del_id = implode(", ", $rejectionTypesId);

        return ResponseController::jsonResponseDone();
    }


    public static function getRejectionTypes(){
        return RejectionTypesModel::getRejectionTypes();
    }

    public static function getRejectionType($id){
        return RejectionTypesModel::getRejectionType($id);
    }

}
