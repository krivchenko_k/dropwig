<?php

namespace App\Http\Controllers;

use App\Models\GoodsCategoriesModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class GoodsCategoriesController extends Controller
{
    public static function index()
    {
        $viewParams = array();
        $viewParams["goodsCategories"] = GoodsCategoriesModel::getGoodsCategories();
        return view("goodsCategories.index", $viewParams);
    }

    public static function getGoodsCategoriesTable(){
        $viewParams["goodsCategories"] = GoodsCategoriesModel::getGoodsCategories();
        return view("goodsCategories.tableBody", $viewParams);
    }

    public static function getPopup($id)
    {
        $viewsParams = array();
        if ($id != 0)
            $viewsParams["goodsCategory"] = GoodsCategoriesModel::getGoodsCategory($id);

        return view("goodsCategories.popup", $viewsParams);
    }

    public static function addGoodsCategory(Request $request){
        $data = (object)$request->all();
        $errors = array();
        if (GoodsCategoriesModel::isGoodsCategoryExist($data->name)){
            $errors[] = "Такая категория товаров уже существует";
        }
        elseif (empty($request->name)){
            $errors[] = "Название категории товаров не может быть пустым";
        }

        if (empty($errors)){
            $GoodsCategory = GoodsCategoriesModel::addGoodsCategory($data);
            $responseData = array(
                "id" => $GoodsCategory->id
            );


            return ResponseController::jsonResponseDone($responseData);
        }
        else{
            return ResponseController::jsonResponseFail($errors);
        }

    }

    public static function editGoodsCategory(Request $request){
        $data = (object)$request->all();

        $prevDeliveryService = GoodsCategoriesModel::getGoodsCategory($data->id);

        $currDeliveryService = GoodsCategoriesModel::editGoodsCategory($data);

        return ResponseController::jsonResponseDone();
    }

    public static function deleteGoodsCategories(Request $request){
        $goodsCategoriesId = json_decode($request->goodsCategoriesId);
        GoodsCategoriesModel::deleteGoodsCategories($goodsCategoriesId);

        return ResponseController::jsonResponseDone();
    }


    public static function getGoodsCategories(){
        return GoodsCategoriesModel::getGoodsCategories();
    }
}
