<?php

namespace App\Http\Controllers;

use App\Models\CommonAddressesModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class CommonAddressesController extends Controller
{
    public static function getAddresses() {
        return CommonAddressesModel::getAddresses();
    }

    public static function addAddress($addressName) {
        return CommonAddressesModel::addAddress($addressName);
    }

    public static function getAddressById($addressId) {
        return CommonAddressesModel::getAddressById($addressId);
    }

    public static function getAddressIdByName($addressName) {
        return CommonAddressesModel::getAddressIdByName($addressName);
    }
}