<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\NPController;
use App\Models\NPWarehousesModel;
use App\Models\NPCitiesModel;
use Illuminate\Http\Request;

//define('API_NP', '5c08c94a73f622fc74ecca7032cd9400');
//define('API2_NP', 'ee684cfc3842d8d0cfd047b05013e84c');

class NPWarehousesController extends Controller
{
    public static function index() {

    }

    public static function updateWarehouses() {

        $cities = NPCitiesModel::getCities();
        $response = NPController::getWarehouses();
        echo count($response->data);
        if($response->success) {
            $offices = $response->data;
            echo count($offices);
            foreach($offices as $index => $office) {
                if(NPWarehousesModel::updateWarehouse($office)) {
                    unset($offices[$index]);
                }
            }
            if(count($offices)) {
                foreach($cities as $city) {
                    foreach($offices as $office) {
                        if(strcmp($office->CityRef, $city->ref) == 0) {
                            NPWarehousesModel::insertWarehouse($city->id, $office);
                        }
                    }
                }
            }
        }
        NPWarehousesModel::updateActivity();
    }

    public static function getWarehousesByCityId($cityId) {
        return NPWarehousesModel::getWarehousesByCityId($cityId);
    }

    public static function getNPAddressesByCityId($cityId) {

        if(isset($cityId) && !empty($cityId)) {
            $result = NPWarehousesModel::getWarehousesByCityId($cityId);
            return json_encode($result);
        }
    }


}
