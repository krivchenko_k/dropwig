<?php

namespace App\Http\Controllers;

use App\Models\NPCitiesModel;
use Illuminate\Database\Eloquent\Model;


class NPCitiesController extends Model
{

    public static function getCities() {
        return NPCitiesModel::getCities();
    }

    public static function updateCities() {
        $response = NPController::getCities();
        if($response->success) {
            $cities = $response->data;
            echo count($cities);
            foreach($cities as $index => $city) {
                if(NPCitiesModel::updateCity($city)) {
                    unset($cities[$index]);
                }
            }
            if(count($cities)) {
                foreach($cities as $city) {
                    NPCitiesModel::insertCity($city);
                }
            }
            NPCitiesModel::updateActivity();
        }
    }
}
