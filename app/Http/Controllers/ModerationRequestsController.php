<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;

class ModerationRequestsController extends Controller
{
    public static function index() {

        return view('moderationRequests.index');
    }

    public static function getTableBody() {

        $viewParams = array();
        $orders = OrdersController::getOrdersByStatus(2);

        foreach ($orders as $order) {
            foreach ($order->goodsInOrder as $goods){
                $order->goodsQuantity += $goods->quantity;
            }
        }

        $viewParams['orders'] = $orders;
        return view('moderationRequests.tableBody', $viewParams);
    }


    public static function getDeclinePopup(Request $request) {
        $orderId = $request->orderId;
        if(!isset($orderId) || empty($orderId)) {
            return ResponseController::jsonResponseFail([ 'Не выбран заказ' ]);
        }
        $viewParams = array();
        $viewParams['rejectionReasons'] = RejectionReasonsController::getRejectionReasonsByType(5);
        $viewParams['orderId'] = $orderId;
        return view('moderationRequests.decline-popup', $viewParams);
    }


    public static function getOrderById($orderId)
    {
        $viewParams = array();

        $viewParams['paymentTypes'] = PaymentTypeController::getPaymentTypes();
        $viewParams['deliveryServices'] = DeliveryServicesController::getDeliveryServices();
        $viewParams['categories'] = GoodsCategoriesController::getGoodsCategories();
        $viewParams['orderStatuses'] = OrderStatusesController::getOrderStatuses();


        $order = OrdersController::getOrder($orderId);
        $viewParams['order'] = $order;
        $viewParams['cities'] = NPCitiesController::getCities();

        $viewParams['goodsList'] = GoodsController::getGoods();
        $goodsIdArray = array();
        foreach($viewParams['order']->goodsInOrder as $goods) {
            $goodsIdArray[] = $goods->goodsId;
        }
        $viewParams['recommendedGoods'] = RecomendedGoodsController::getRecommendedGoodsForGoodsList($goodsIdArray);

        if (isset($viewParams['order']) && $viewParams['order']->deliveryParams->deliveryServiceId == NP_DELIVERY_SERVICE && !empty($viewParams['order']->deliveryParams->NPCityId)) {
            $viewParams['offices'] = NPWarehousesController::getWarehousesByCityId($viewParams['order']->deliveryParams->NPCityId);
        }

        return view('moderationRequests.popup', $viewParams);
    }

    public static function updateOrder(Request $request) {
        if(isset($request->updateData) && !empty($request->updateData)) {
            $ordersController = new OrdersController();
            $ordersController->updateOrder($request);

            return ResponseController::jsonResponseDone();
        }
        else {
            return ResponseController::jsonResponseFail([ 'Данные о заказе не приняты' ]);
        }
    }

    public static function acceptOrder(Request $request) {
        if(isset($request->updateData) && !empty($request->updateData)) {
            OrdersController::updateOrder($request);
            $updateData = json_decode($request->updateData);
            OrdersController::updateOrderStatus($updateData->orderData->orderId, 3);
            OrdersController::setOrderAcceptorId($updateData->orderData->orderId, Session::get("user")->id);


            return ResponseController::jsonResponseDone();
        }
        else {
            return ResponseController::jsonResponseFail([ 'Данные о заказе не приняты' ]);
        }
    }

    public static function declineOrder(Request $request) {
        $orderId = $request->orderId;
        $rejectionReasonId = $request->rejectionReasonId;
        $rejectionText = !empty($request->rejectionText) ? $request->rejectionText : null;
        if(!isset($orderId, $rejectionReasonId) || empty($orderId) || empty($rejectionReasonId)) {
            return ResponseController::jsonResponseFail([ 'Не выбран заказ' ]);
        }
        if($rejectionReasonId < 0) {
            if(empty($rejectionText)) {
                return ResponseController::jsonResponseFail([ 'Не указана причина отказа' ]);
            }
            $rejectionReasonId = null;
        }

        OrdersController::updateOrderStatus($request->orderId, 4);
        Orders_RejectionReasonsController::addRejection($orderId, $rejectionReasonId, $rejectionText);


        return ResponseController::jsonResponseDone();
    }

    public static function deleteOrders(Request $request) {
        if(isset($request->removalOrders) && !empty($request->removalOrders)) {
            OrdersController::removeOrders($request);

            return ResponseController::jsonResponseDone();
        }
        return ResponseController::jsonResponseFail([ 'Не заданы заказы для удаления' ]);
    }

}
