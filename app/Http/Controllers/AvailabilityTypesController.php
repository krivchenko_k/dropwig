<?php

namespace App\Http\Controllers;

use App\Models\AvailabilityTypesModel;
use Illuminate\Http\Request;

class AvailabilityTypesController extends Controller
{
    public static function index()
    {
        $viewParams = array();
        $viewParams["availabilityTypes"] = AvailabilityTypesModel::getAvailabilityTypes();
        return view("availabilityTypes.index", $viewParams);
    }

    public static function getAvailabilityTypesTable(){
        $viewParams["availabilityTypes"] = AvailabilityTypesModel::getAvailabilityTypes();
        return view("availabilityTypes.tableBody", $viewParams);
    }

    public static function getPopup($id)
    {
        $viewsParams = array();
        if ($id != 0)
            $viewsParams["availabilityType"] = AvailabilityTypesModel::getAvailabilityType($id);

        return view("availabilityTypes.popup", $viewsParams);
    }

    public static function addAvailabilityType(Request $request){
        $data = (object)$request->all();
        $errors = array();
        if (AvailabilityTypesModel::isAvailabilityTypeExist($data->name)){
            $errors[] = "Такая тип оплаты уже существует";
        }
        elseif (empty($request->name)){
            $errors[] = "Название типа оплаты не может быть пустым";
        }

        if (empty($errors)){
            $AvailabilityType = AvailabilityTypesModel::addAvailabilityType($data);
            $responseData = array(
                "id" => $AvailabilityType->id
            );

            return ResponseController::jsonResponseDone($responseData);
        }
        else{
            return ResponseController::jsonResponseFail($errors);
        }

    }

    public static function editAvailabilityType(Request $request){
        $data = (object)$request->all();

        $prevAvailabilityType = AvailabilityTypesModel::getAvailabilityType($data->id);

        $currAvailabilityType = AvailabilityTypesModel::editAvailabilityType($data);


        return ResponseController::jsonResponseDone();
    }

    public static function deleteAvailabilityTypes(Request $request){
        $availabilityTypesId = json_decode($request->availabilityTypesId);
        AvailabilityTypesModel::deleteAvailabilityTypes($availabilityTypesId);

        $del_id = implode(", ", $availabilityTypesId);

        return ResponseController::jsonResponseDone();
    }


    public static function getAvailabilityTypes(){
        return AvailabilityTypesModel::getAvailabilityTypes();
    }
}
