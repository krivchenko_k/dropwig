<?php

namespace App\Http\Controllers;

use App\Models\StaffModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class StaffController extends Controller
{
    public function index() {
        $viewParams = array();
        $viewParams['staff'] = StaffModel::get();
        return view('staff.index', $viewParams);
    }

    public static function getTableBody(){
        $viewParams = array();
        $viewParams['staff'] = StaffModel::get();
        return view('staff.tableBody', $viewParams);
    }

    /**
     * Вывод всплывающего меню страницы настроек
     */
    public function getPopup($id){
        $viewParams = array();
        if($id != 0)
            $viewParams["employee"] = StaffModel::get($id);

        $viewParams["userGroups"] = UserGroupsController::getUserGroups();

        return view("staff.popup", $viewParams);
    }

    /**
     * Добавление нового сотрудника
     */
    public function addEmployee (Request $request) {
        $data = (object)$request->all();
        $errors = array();

        if (StaffModel::isLoginExist($data->login)) {
            $errors[] = "Пользователь с таким логином уже существует";
        }
        if (PartnersController::isPartnerLoginExitst($data->login)){
            $errors[] = "Пользователь с таким логином уже существует";
        }
        if (empty($data->login)){
            $errors[] = "Логин не может быть пустым";
        }
        if (empty($data->groupId)){
            $errors[] = "Выберите группу пользователя";
        }

        if(empty($errors)) {
            $data->id = StaffModel::add($data);
            StaffProfilesController::addStaffProfile($data);
            $responseData = array(
                "id" => $data->id
            );

            return ResponseController::jsonResponseDone($responseData);
        }
        else {
            return ResponseController::jsonResponseFail($errors);
        }

    }

    /**
     * Редактирование существующего сотрудника
     */
    public function editEmployee (Request $request) {
        $data = (object)$request->all();
        $errors = array();

        if (empty($data->groupId)){
            $errors[] = "Выберите группу пользователя";
        }
        if(empty($errors)) {

            $prevStaffData = StaffModel::get($data->id);

            $currStaffData = StaffModel::edit($data);
            StaffProfilesController::editStaffProfile($data);


            return ResponseController::jsonResponseDone();
        }
        else {
            return ResponseController::jsonResponseFail($errors);
        }
    }

    /**
     * Удаление сотрудника
     */
    public function deleteStaff (Request $request) {
        $idArray = json_decode($request->idArray);
        StaffModel::remove($idArray);

        $del_id = implode(", ", $idArray);

        return ResponseController::jsonResponseDone();
    }

    public static function getEmployeeByLoginPass($login, $password){
        return StaffModel::getEmployeeByLoginPass($login, $password);
    }


    public static function isStaffLoginExist($login)
    {
        return StaffModel::isLoginExist($login);
    }
}
