<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FAQModel;

/**
 * Description of FAQController
 *
 * @author alexander
 */
class FAQController extends Controller {

    /**
     * Получение списка FAQ для страницы настроек
     */
    public function index(){
        $faqData = array();
        $faqData["FAQList"] = FAQModel::getFAQList();
        return view("faq.index", $faqData);
    }

    public function settingsIndex(){
        $faqData = array();
        $faqData["FAQList"] = FAQModel::getFAQList();
        return view("settings.faq.index", $faqData);
    }

    public static function getFAQTable(){
        $viewParams["FAQList"] = FAQModel::getFAQList();
        return view("settings.faq.tableBody", $viewParams);
    }

    /**
     * Вывод всплывающего меню страницы настроек
     */
    public static function getPopup($id){
        $viewsParams = array();
        if ($id != 0)
            $viewsParams["FAQ"] = FAQModel::getFAQ($id);

        return view("settings.faq.popup", $viewsParams);
    }

    public static function getLoginPagePopup(){
        $viewParams = array();
        $viewParams["FAQList"] = FAQModel::getFAQList();
        return view('faq.login-page-popup', $viewParams);
    }
    
    /**
     * Добавление новой записи FAQ
     */
    public static function addFAQ(Request $request){
        $data = (object)$request->all();
        $errors = array();
        if (FAQModel::isQuestionExist($data->question)){
            $errors[] = "Такой вопрос уже существует";
        }
        elseif (empty($request->question)){
            $errors[] = "Вопрос не может быть пустым";
        }

        if (empty($errors)){
            $FAQ = FAQModel::addFAQ($data);
            $responseData = array(
                "id" => $FAQ->id
            );

            return ResponseController::jsonResponseDone($responseData);
        }
        else{
            return ResponseController::jsonResponseFail($errors);
        }

    }
    
    /**
     * Редактирование существующей записи FAQ
     */
    public static function editFAQ(Request $request){
        $data = (object)$request->all();

        $prevFAQ = FAQModel::getFAQ($data->id);

        $currFAQ = FAQModel::editFaq($data);

        return ResponseController::jsonResponseDone();
    }
    
    /**
     * Удаление записей FAQ
     */
    public static function deleteFAQList(Request $request){
        $FAQId = json_decode($request->FAQId);
        FAQModel::deleteFAQList($FAQId);

        return ResponseController::jsonResponseDone();
    }

}
