<?php

namespace App\Http\Controllers;

use App\Models\Orders_PaymentRequestsModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class Orders_PaymentRequestsController extends Controller
{
    public static function add($data) {
        return Orders_PaymentRequestsModel::add($data);
    }

    public static function erase($idArray) {
        return Orders_PaymentRequestsModel::erase($idArray);
    }

    public static function eraseByRequest($requestId) {
        return Orders_PaymentRequestsModel::eraseByRequest($requestId);
    }

    public static function getOrdersByRequest($requestId) {
        return Orders_PaymentRequestsModel::getOrdersByRequest($requestId);
    }

}
