<?php

namespace App\Http\Controllers;

use App\Models\RejectionReasonsModel;
use Illuminate\Http\Request;
use App\Http\Requests;

class RejectionReasonsController extends Controller
{
    public static function index()
    {
        $viewParams = array();
        $viewParams["rejectionReasons"] = RejectionReasonsModel::getRejectionReasons();
        return view("rejectionReasons.index", $viewParams);
    }

    public static function getRejectionReasonsTable(){
        $viewParams["rejectionReasons"] = RejectionReasonsModel::getRejectionReasons();
        return view("rejectionReasons.tableBody", $viewParams);
        return view("rejectionReasons.tableBody", $viewParams);
    }

    public static function getPopup($id)
    {
        $viewsParams = array();
        $viewsParams["rejectionTypes"] = RejectionTypesController::getRejectionTypes();
        if ($id != 0)
            $viewsParams["rejectionReason"] = RejectionReasonsModel::getRejectionReason($id);

        return view("rejectionReasons.popup", $viewsParams);
    }

    public static function addRejectionReason(Request $request){
        $data = (object)$request->all();
        $errors = array();
        if (RejectionReasonsModel::isRejectionReasonExist($data->text)){
            $errors[] = "Такая причина отказа уже существует";
        }
        elseif (empty(RejectionTypesController::getRejectionType($data->typeId))){
            $errors[] = "Такого типа отказа не существует";
        }
        elseif (empty($data->text)){
            $errors[] = "Текст причины отказа не может быть пустым";
        }

        if (empty($errors)){
            $RejectionReasons = RejectionReasonsModel::addRejectionReason($data);
            $responseData = array(
                "id" => $RejectionReasons->id
            );

            return ResponseController::jsonResponseDone($responseData);
        }
        else{
            return ResponseController::jsonResponseFail($errors);
        }

    }

    public static function editRejectionReason(Request $request){
        $data = (object)$request->all();

        $prevRejectionReasons = RejectionReasonsModel::getRejectionReason($data->id);

        $currRejectionReasons = RejectionReasonsModel::editRejectionReason($data);

        return ResponseController::jsonResponseDone();
    }

    public static function deleteRejectionReasons(Request $request){
        $rejectionReasonsId = json_decode($request->rejectionReasonsId);
        RejectionReasonsModel::deleteRejectionReasons($rejectionReasonsId);

        $del_id = implode(", ", $rejectionReasonsId);

        return ResponseController::jsonResponseDone();
    }


    public static function getRejectionReason($id){
        return RejectionReasonsModel::getRejectionReason($id);
    }

    public static function getRejectionReasonsByType($typeId){
        return RejectionReasonsModel::getRejectionReasonsByType($typeId);
    }
}
