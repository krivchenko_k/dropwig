<?php

namespace App\Http\Controllers;

use App\Models\OrdersModel;
use App\Models\PartnerBalancesModel;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Str;

class SSEController extends Controller
{
    public function getAdminNotifications()
    {
        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');

        $registrationRequestsCount = PartnersController::getPartnersByStatus(2)->count();
        $moderationRequestsCount = OrdersController::getOrdersByStatus(2)->count();
        $paymentRequestsCount = PaymentRequestsController::getActiveRequestsCount();

        echo "retry: 10000" . PHP_EOL;
        echo 'data: { "registrationRequestsCount" : "' . $registrationRequestsCount
             . '", "moderationRequestsCount" : "' . $moderationRequestsCount
             . '", "paymentRequestsCount" : "' . $paymentRequestsCount . '"';
        echo "}" . PHP_EOL . PHP_EOL;

//        ob_flush();
        flush();
    }

    public function getPartnerBalance()
    {
        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');

        $queryParams = [
            [ "partnerId", "=", Session::get("user")->id]
        ];
        $balance = number_format(PartnerBalancesController::getBalance(null, $queryParams)[0]->amount, 2);

        echo "retry: 60000" . PHP_EOL;
        echo "data: {\"balance\" : \"{$balance}\"}" . PHP_EOL . PHP_EOL;

        flush();
    }
}
