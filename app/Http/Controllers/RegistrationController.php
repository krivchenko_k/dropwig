<?php

namespace App\Http\Controllers;

use App\Classes\ipApi;
use App\Models\PartnersModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class RegistrationController extends Controller
{
    private static function validateField($field)
    {
        $result = trim($field);
        $result = stripslashes($result);
        $result = htmlspecialchars($result);
        return $result;
    }

    public function index()
    {
        if (isset($_GET['login'], $_GET['hashcode'])) {
            $login = $_GET['login'];
            $hashcode = $_GET['hashcode'];
            $partner = PartnersController::getPartnerByLogin($login);
            if ($partner) {
                if ($partner->hashcode->hashcode == $hashcode && $partner->statusId == 2) {
                    PartnersController::setPartnerStatus($partner->id, 2);
                    return redirect("/login")->with("login", $login)->with("reg-confirm", "Спасибо за регистрацию! C Вами свяжутся в ближайшее время");
                } else {
                    return redirect("/login")->with("login", $login);
                }
            }
        }
        return view('registration.index');
    }

    public function register(Request $request)
    {

        $errors = array();

        $data = (object)json_decode($request->data);

        if (!isset($data->login))
            $errors[] = 'Не задано поле ';
        if (!isset($data->firstName))
            $errors[] = 'Не задано поле ';
        if (!isset($data->lastName))
            $errors[] = 'Не задано поле ';
        if (!isset($data->phone))
            $errors[] = 'Не задано поле ';
        if (!isset($data->email))
            $errors[] = 'Не задано поле ';
//        if (!isset($data->card))
//            $errors[] = 'Не задано поле ';
//        if (!isset($data->cardOwner))
//            $errors[] = 'Не задано поле ';
        if (empty($data->card))
            $data->card = "";
        if (empty($data->cardOwner))
            $data->cardOwner = "";
        if ($data->isCardOwnerSelf) {
            $data->isCardOwnerSelf = 1;
        } else {
            $data->isCardOwnerSelf = 0;
        }
        if (!isset($data->nickname))
            $errors[] = 'Не задано поле ';

        if (!isset($data->password))
            $errors[] = 'Не задано поле ';

        if (count($errors) != 0) {
            return ResponseController::jsonResponseFail($errors);
        }

        $data->firstName = $this->validateField($data->firstName);
        $data->lastName = $this->validateField($data->lastName);
        $data->phone = $this->validateField($data->phone);
        $data->email = $this->validateField($data->email);
        $data->card = $this->validateField($data->card);
        $data->cardOwner = $this->validateField($data->cardOwner);
        $data->isCardOwnerSelf = $this->validateField($data->isCardOwnerSelf);
        $data->nickname = $this->validateField($data->nickname);
        $data->password = $this->validateField($data->password);

        if (PartnersController::isPartnerLoginExitst($data->login))
            $errors[] = "Пользователь с таким логином уже существует";
        if (StaffController::isStaffLoginExist($data->login))
            $errors[] = "Пользователь с таким логином уже существует";
        if (PartnerProfilesController::isPartnerEmailExitst($data->email))
            $errors[] = "Пользователь с таким E-mail-адресом уже существует";
        if (PartnerProfilesController::isPartnerPhoneExitst($data->phone))
            $errors[] = "Пользователь с таким телефоном уже существует";
        if (PartnerProfilesController::isPartnerNicknameExitst($data->nickname))
            $errors[] = "Пользователь с таким ником уже существует";
        if (!empty($data->card)) {
            if (PartnerProfilesController::isPartnerCardExitst($data->card))
                $errors[] = "Пользователь с такой картой уже существует";
        }
        if (count($errors) != 0) {
            return ResponseController::jsonResponseFail($errors);
        }

        $geoInfo = ipApi::getGeoInfo($_SERVER["REMOTE_ADDR"]);

        try {
            $partnerId = DB::table('Partners')->insertGetId(['login' => $data->login, 'password' => md5($data->password), 'groupId' => 3, "statusId" => 2]);

        } catch (\Exception $ex) {
        }

        if (isset($partnerId)) {
            try {
                DB::table('PartnerProfiles')->insert([
                    'partnerId' => $partnerId,
                    'firstName' => $data->firstName,
                    'lastName' => $data->lastName,
                    'phone' => $data->phone,
                    'email' => $data->email,
                    'contacts' => $data->contacts,
                    'card' => $data->card,
                    'cardOwner' => $data->cardOwner,
                    'ip' => $_SERVER["REMOTE_ADDR"],
                    "cityByIp" => (isset($geoInfo["city_rus"])) ? $geoInfo["city_rus"] : "Н/Д",
                    'isCardOwnerSelf' => $data->isCardOwnerSelf,
                    'nickname' => $data->nickname,
                ]);

                PartnerBalancesController::createBalance($partnerId);
            } catch (\Exception $ex) {
                $partner = PartnersModel::find($partnerId);
                $partner->forceDelete();
            }
        } else {
            $errors[] = "Не удалось создать профиль";
            return ResponseController::jsonResponseFail($errors);
        }


        return redirect("/login")->with("reg-confirm", "Спасибо за регистрацию! C Вами свяжутся в ближайшее время");
//        return ResponseController::jsonResponseDone();
    }
}