<?php

namespace App\Http\Controllers;

use App\Models\StaffProfilesModel;
use Illuminate\Http\Request;

use App\Http\Requests;

class StaffProfilesController extends Controller
{
    public static function addStaffProfile($data){
        StaffProfilesModel::add($data);
    }

    public static function editStaffProfile($data){
        StaffProfilesModel::edit($data);
    }
}
