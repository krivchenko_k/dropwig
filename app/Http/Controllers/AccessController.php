<?php

namespace App\Http\Controllers;

use App\Models\AccessesModel;
use App\Models\UserGroupsModel;
use App\Models\RoutesModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AccessController extends Controller
{
    public static function checkAccess(Request $request)
    {
        $groupId = Session::get("user")->groupId;

        $routeSegments = explode("/", $request->path());
        $segmentsCount = count($routeSegments);

        for($i = 0; $i < $segmentsCount; $i++)
            if (is_numeric($routeSegments[$i]))
                $routeSegments[$i] = "#";

        $route = implode("/", $routeSegments);

        if ($route != "/")
            $route = "/".$route;
        
        $routeId = RoutesModel::getRouteId($route);
        if (!AccessesModel::isAccessibleRouteById($groupId, $routeId)) {
            return false;
        }
        else {
            return true;
        }
    }

}


