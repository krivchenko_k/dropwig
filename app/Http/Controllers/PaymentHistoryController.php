<?php

namespace App\Http\Controllers;

use App\Models\PaymentHistoryModel;
use App\Models\PartnerProfilesModel;
use App\Models\PaymentOperationsModel;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;

class PaymentHistoryController extends Controller
{
    public static function index()
    {
        $viewParams = array();
        $user = Session::get('user');
        if ($user->groupId == 1 || $user->groupId == 2) {
            $viewParams['paymentHistory'] = PaymentHistoryModel::get();
        } else {
            $queryParams = [
                ["partnerId", "=", $user->id]
            ];
            $viewParams['paymentHistory'] = PaymentHistoryModel::get(null, $queryParams);
        }


        return view('paymentHistory.index', $viewParams);
    }

    public static function getPaymentHistory($id = null, $queryParams = null)
    {
        return PaymentHistoryModel::get($id, $queryParams);
    }

    public static function addPaymentHistoryRecord($data)
    {
        if (!empty($data->paymentOperationId)) {
            PaymentHistoryModel::add($data);

            switch ($data->paymentOperationId) {
                case 1:
                    $orderData = OrdersController::getOrder($data->orderId);
                    $partnerData = PartnersController::getPartners($data->partnerId);
                    break;
                case 2:
                    $orderData = OrdersController::getOrder($data->orderId);
                    $partnerData = PartnersController::getPartners($data->partnerId);
                    break;
                case 3:
                    $partnerData = PartnersController::getPartners($data->partnerId);
                    break;
                case 4:
                    $partnerData = PartnersController::getPartners($data->partnerId);
                    break;;
                case 5:
                    $partnerData = PartnersController::getPartners($data->partnerId);
                    break;
                case 6:
                    $partnerData = PartnersController::getPartners($data->partnerId);
                    break;
            }


        }

    }
}
