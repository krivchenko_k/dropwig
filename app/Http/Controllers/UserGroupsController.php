<?php

namespace App\Http\Controllers;

use App\Models\UserGroupsModel;
use Illuminate\Http\Request;

class UserGroupsController extends Controller
{

    public static function getUserGroups(){
        return UserGroupsModel::getGroups();
    }


}
