<?php

namespace App\Http\Controllers;

use App\Models\Orders_PaymentRequestsModel;
use App\Models\PartnerProfilesModel;
use App\Models\OrdersModel;
use App\Models\PartnerBalancesModel;
use App\Models\PaymentHistoryModel;
use App\Models\PaymentRequestsModel;
use App\Models\UserModel;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class PaymentRequestsController extends Controller
{
    public static function index() {
        $user = Session::get('user');
        if($user->groupId == 3) {
            return view('paymentRequests.partners.index');
        }
        else {
            return view('paymentRequests.index');
        }
    }

    public static function getConfirmPopup($requestId) {
        if(isset($requestId)) {
            $viewParams = array();
            $viewParams['request'] = PaymentRequestsModel::get($requestId);
            return view('paymentRequests.confirm-popup', $viewParams);
        }
        else {
            return ResponseController::jsonResponseFail([ 'Не указан идентификатор запроса' ]);
        }
    }

    public static function confirmRequest() {
        $data = json_decode($_POST['data']);
        if(isset($data) && !empty($data)) {
            $requestId = $data->requestId;
            $request = PaymentRequestsModel::find($requestId);
            if((double)$request->price > (double)$request->partnerProfile->balance->amount) {
                return ResponseController::jsonResponseFail([ 'Сумма к выплате превышает сумму на балансе партнера' ]);
            }
            //Saving the files:
            if (!isset($_FILES['file']) && $_FILES['file']['error'] > 0) {
                return ResponseController::jsonResponseFail([ 'Сбой загрузки файла.' ]);
            }
            else {
                $fileName = array_last(explode('.', $_FILES['file']['name']));
                $fileName = "$requestId" . "_" . date('d-m-Y(H-i-s)') . "." . $fileName;
                move_uploaded_file($_FILES['file']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . '/public/images/paymentRequests/' . $fileName);
                //sending mail with attachment here...
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                $message = "
                <html>
                    <head>
                        <title>Информация о выплате</title>
                    </head>
                    <body>
                        <img src=\"http://" . $_SERVER['SERVER_NAME'] . '/images/paymentRequests/' . $fileName . "\">
                    </body>
                </html>";
                mail($request->partnerProfile->email, 'Информация о выплате', $message, $headers);
                //updating partner's balance here...
                $request->isActive = 0;
                $request->imageLink = '/images/paymentRequests/' . $fileName;
                $request->partnerProfile->balance->amount -= $request->price;
                $request->partnerProfile->balance->save();
                $request->save();
                //updating orders
                $ordersIdArray = Orders_PaymentRequestsController::getOrdersByRequest($requestId);
                if(!empty($ordersIdArray)) {
                    OrdersController::updateStatusForGroup($ordersIdArray, 10);
                }

                PaymentHistoryController::addPaymentHistoryRecord((object)[ 'partnerId' => $request->partnerId, 'paymentOperationId' => 4, 'paymentRequestId' => $request->id, 'price' => $request->price ]);
            }
            return ResponseController::jsonResponseDone();
        }
    }

    public static function getDeclinePopup($requestId) {
        if(isset($requestId)) {
            $viewParams = array();
            $viewParams['requestId'] = $requestId;
            $viewParams['rejectionReasons'] = RejectionReasonsController::getRejectionReasonsByType(2);
            return view('paymentRequests.decline-popup', $viewParams);
        }
        else {
            return ResponseController::jsonResponseFail([ 'Не указан идентификатор запроса' ]);
        }
    }

    public static function declineRequest(Request $request) {

        $rejectionData = json_decode($request->rejectionData);
        if(isset($rejectionData) && !empty($rejectionData)) {
            if (!isset($rejectionData->requestId) || ($rejectionData->reasonId == -1 && empty($rejectionData->reasonText))) {
                $errors[] = "Не указана причина отказа";
                return ResponseController::jsonResponseFail($errors);
            }
            $request = PaymentRequestsModel::find($rejectionData->requestId);
            $ordersIdArray = Orders_PaymentRequestsController::getOrdersByRequest($request->id);
            $email = $request->partnerProfile->email;

            //payment history actions
            PaymentHistoryController::addPaymentHistoryRecord((object)[ 'partnerId' => $request->partnerId, 'paymentOperationId' => 5, 'paymentRequestId' => $request->id, 'price' => $request->price ]);

            if (!empty($ordersIdArray)){
                $queryParams = [
                    "statusId" => 8
                ];
                OrdersController::editOrders($ordersIdArray, $queryParams);
            }

            $rejectionReason = $rejectionData->reasonText;
            if($rejectionData->reasonId != -1) {
                $rejectionReason = RejectionReasonsController::getRejectionReason($rejectionData->reasonId)->text;
                PaymentRequests_RejectionReasonsController::addRejection($request->id, $rejectionData->reasonId, null);
            }
            else {
                PaymentRequests_RejectionReasonsController::addRejection($request->id, null, $rejectionReason);
            }

            PaymentRequestsModel::deactivate($request->id);
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $message = "В выплате отказано. " . $rejectionReason;
            mail($email, 'Информация о выплате', $message, $headers);

            return ResponseController::jsonResponseDone();
        }
        else {
            return ResponseController::jsonResponseFail([ 'Не задана причина отказа в выплате' ]);
        }
    }

    public static function getTableBody(Request $request) {
        $viewParams = array();
        $user = Session::get('user');

        if($user->groupId == 3) {
            $queryParams = [
                ["partnerId", "=", $user->id],
                ["statusId", "=", 8]
            ];
            if (!empty($request->dateFrom)){
                $queryParams[] = [DB::raw("DATE(created_at)"), ">=", date("Y-m-d", strtotime($request->dateFrom))];
            }
            if (!empty($request->dateTo)){
                $queryParams[] = [DB::raw("DATE(created_at)"), "<=", date("Y-m-d", strtotime($request->dateTo))];
            }
            $orders = OrdersController::getOrders(null, $queryParams);
            $orders = $orders->sortByDesc("id");

            OrdersController::calculateSummaries($orders);

            $viewParams["orders"] = $orders;
            return view('paymentRequests.partners.tableBody', $viewParams);
        }
        else {
            $queryParams = [];
            if (!empty($request->dateFrom)){
                $queryParams[] = [DB::raw("DATE(created_at)"), ">=", date("Y-m-d", strtotime($request->dateFrom))];
            }
            if (!empty($request->dateTo)){
                $queryParams[] = [DB::raw("DATE(created_at)"), "<=", date("Y-m-d", strtotime($request->dateTo))];
            }
            $viewParams['requests'] = PaymentRequestsModel::get(null, $queryParams);
            return view('paymentRequests.tableBody', $viewParams);
        }
    }

    public static function makePaymentRequest(Request $request) {
        $ordersIdArray = json_decode($request->idArray, true);
        if(isset($ordersIdArray) && !empty($ordersIdArray)) {
            $partnerId = Session::get('user')->id;

            $orders = OrdersController::getOrders($ordersIdArray);
            $totalPrice = 0;
            $partnerBalance = (double)PartnerBalancesController::getBalance(null, [ [ 'partnerId', '=', $partnerId ] ])->first()->amount;
            OrdersController::calculateSummaries($orders);
            foreach($orders as $order) {
                $totalPrice += $order->markup;
            }
            Orders_PaymentRequestsModel::deleteIfExists($ordersIdArray);

            if($totalPrice > $partnerBalance) {
                $totalPrice = $partnerBalance;
            }

            $request = new PaymentRequestsModel();
            $request->partnerId = $partnerId;
            $request->price = $totalPrice;
            $request->save();

            foreach($orders as $order) {
                Orders_PaymentRequestsController::add((object)[ 'paymentRequestId' => $request->id, 'orderId' => $order->id ]);
            }

            PaymentHistoryController::addPaymentHistoryRecord((object)[ 'partnerId' => $partnerId, 'paymentOperationId' => 3, 'paymentRequestId' => $request->id, 'price' => $totalPrice ]);

            $updateParams = [
                "statusId" => 9
            ];
            OrdersController::editOrders($ordersIdArray, $updateParams);

            return ResponseController::jsonResponseDone();
        }
        else {
            return ResponseController::jsonResponseFail([ 'Не выбраны заказы для оформления заявки на выплату' ]);
        }
    }

    public static function erase($idArray) {
        PaymentRequestsModel::erase($idArray);
    }

    public static function getActiveRequestsCount() {
        return PaymentRequestsModel::getActiveRequestsCount();
    }

}
