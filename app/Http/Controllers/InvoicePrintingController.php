<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use LisDev\Delivery\NovaPoshtaApi2;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Worksheet;
use PHPExcel_Style_Alignment;

class InvoicePrintingController extends Controller
{
    private static $NPHandler;

    public static function index()
    {
        return view('invoicePrinting.index');
    }

    public static function getTableBody()
    {
        $viewParams = array();
        $orders = OrdersController::getOrdersByStatus(3);

        $orders = $orders->filter(function ($order) {
            return ($order->deliveryParams->deliveryServiceId == 2);
        });

        foreach ($orders as $order) {
            foreach ($order->goodsInOrder as $goods) {
                $order->goodsQuantity += $goods->quantity;
            }
        }

        $viewParams['orders'] = $orders;
        return view('invoicePrinting.tableBody', $viewParams);
    }

    public static function ExcelExport ()
    {

        $phpexcel = new PHPExcel();

        $page = $phpexcel->setActiveSheetIndex(0);

        $page->getColumnDimension('A')->setWidth(20);
        $page->getColumnDimension('B')->setWidth(15);
        $page->getColumnDimension('C')->setWidth(55);
        $page->getColumnDimension('D')->setWidth(15);
        $page->getColumnDimension('E')->setWidth(75);
        $page->getColumnDimension('F')->setWidth(15);
        $page->getColumnDimension('G')->setWidth(10);
        $page->getColumnDimension('H')->setWidth(15);
        $page->getColumnDimension('I')->setWidth(15);

        $arHeadStyle = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '778899'),
                'size'  => 12,
                'name'  => 'Verdana'
            )
        );

        $page->getStyle('A1')->applyFromArray($arHeadStyle);
        $page->getStyle('B1')->applyFromArray($arHeadStyle);
        $page->getStyle('C1')->applyFromArray($arHeadStyle);
        $page->getStyle('D1')->applyFromArray($arHeadStyle);
        $page->getStyle('E1')->applyFromArray($arHeadStyle);
        $page->getStyle('F1')->applyFromArray($arHeadStyle);
        $page->getStyle('G1')->applyFromArray($arHeadStyle);
        $page->getStyle('H1')->applyFromArray($arHeadStyle);
        $page->getStyle('I1')->applyFromArray($arHeadStyle);

        $page->setTitle("invoices_('.date('d.m.Y').')");
        $page->getDefaultStyle()->getFont()->setName('Arial')->setSize('9');
        $page->setCellValue("A1", "Покупатель");
        $page->setCellValue("B1", "Номер ТТН");
        $page->setCellValue("C1", "Данные поставки");
        $page->setCellValue("D1", "Контактный телефон");
        $page->setCellValue("E1", "Товар");
        $page->setCellValue("F1", "Сумма");
        $page->setCellValue("G1", "Статус");
        $page->setCellValue("H1", "Дата добавления");
        $page->setCellValue("I1", "Дата изменения");

        $orders = OrdersController::getOrdersByStatus(3);

        $orders = $orders->filter(function ($order) {
            return ($order->deliveryParams->deliveryServiceId == 2);
        });

        foreach ($orders as $order) {
            foreach ($order->goodsInOrder as $goods) {
                $order->goodsQuantity += $goods->quantity;
            }
        }


        $i = 2;
        foreach($orders as $order) {
            if (isset($order->goodsInOrder)) {
                $array = array();
                foreach ($order->goodsInOrder as $goodsInOrder) {
                    array_push($array, $goodsInOrder->goods->name .
                        " (" .  $goodsInOrder->quantity . " * " . $goodsInOrder->goods->price . ") = "
                        . ($goodsInOrder->goods->price + $goodsInOrder->markup) * $goodsInOrder->quantity .
                        " грн. (+ наценка " . $goodsInOrder->markup . " грн.)");
                    $goods = implode("\n", $array);
                }
            }
            else {
                array_push($array, "Нет товаров");
            }

            $page->getCell("A" . $i . "")->setValue($order->client->lastName . " " . $order->client->firstName);
            $page->getCell("B" . $i . "")->setValue("#" . $order->invoice);
            if ($order->deliveryParams->deliveryService->id == 2) {
                $page->getCell("C" . $i . "")->setValue($order->deliveryParams->deliveryService->name . ": " . $order->deliveryParams->NPCity["name"] . ", Отделение " . $order->deliveryParams->NPWarehouse["name"]);
            } else {
                $page->getCell("C" . $i . "")->setValue($order->deliveryParams->deliveryService->name . ": " . $order->deliveryParams->commonCity["name"] . ", " . $order->deliveryParams->commonAddress["name"]);
            }
            $page->getCell("E" . $i . "")->setValue($goods);
            $page->getStyle("E" . $i . "")->getAlignment()->setWrapText(true);
            $page->getCell("D" . $i . "")->setValue($order->phone);
            $page->getCell("F" . $i . "")->setValue($order->price . " грн.");
            $page->getCell("G" . $i . "")->setValue($order->status->name);
            $page->getCell("H" . $i . "")->setValue(date("d.m.Y", strtotime($order->created_at)));
            $page->getCell("I" . $i . "")->setValue(date("d.m.Y", strtotime($order->updated_at)));
            $i++;

        }

        //$objWriter = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel2007');

        /* Начинаем готовиться к записи информации в xlsx-файл */
        $objWriter = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel2007');
        /* Записываем в файл */

        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="invoices_export_ '.  date('d-m-Y_H-i') .'.xlsx"');

        //return $objWriter->save("example.xls");

        //$temp = tmpfile();

        //dd($temp);

        //return $objWriter->save($temp);


        InvoicePrintingController::SaveViaTempFile($objWriter);

    }

    public static function SaveViaTempFile($objWriter){
        $filePath = '/tmp/' . rand(0, getrandmax()) . rand(0, getrandmax()) . ".tmp";
        $objWriter->save($filePath);
        readfile($filePath);
        unlink($filePath);
    }

    public static function getOrderById($orderId)
    {
        $viewParams = array();

        $viewParams['paymentTypes'] = PaymentTypeController::getPaymentTypes();
        $viewParams['deliveryServices'] = DeliveryServicesController::getDeliveryServices();
        $viewParams['categories'] = GoodsCategoriesController::getGoodsCategories();
        $viewParams['orderStatuses'] = OrderStatusesController::getOrderStatuses();

        $viewParams['cities'] = NPCitiesController::getCities();
        if ($orderId == 0) {
            $viewParams['offices'] = NPWarehousesController::getWarehousesByCityId(1);
            $viewParams['goodsList'] = GoodsController::getGoods();
            $viewParams['partners'] = PartnersController::getPartner();
            return view('orders.empty-popup', $viewParams);
        }
        $viewParams['order'] = OrdersController::getOrder($orderId);
        $viewParams['goodsList'] = GoodsController::getGoods();
        $goodsIdArray = array();
        foreach ($viewParams['order']->goodsInOrder as $goods) {
            $goodsIdArray[] = $goods->goodsId;
        }
        $viewParams['recommendedGoods'] = RecomendedGoodsController::getRecommendedGoodsForGoodsList($goodsIdArray);

        if (isset($viewParams['order']) && $viewParams['order']->deliveryParams->deliveryServiceId == NP_DELIVERY_SERVICE && !empty($viewParams['order']->deliveryParams->NPCityId)) {
            $viewParams['offices'] = NPWarehousesController::getWarehousesByCityId($viewParams['order']->deliveryParams->NPCityId);
        }

        return view('invoicePrinting.popup', $viewParams);
    }

    public static function updateOrder(Request $request)
    {
        if (isset($request->updateData) && !empty($request->updateData)) {
            $ordersController = new OrdersController();
            $ordersController->updateOrder($request);


            return ResponseController::jsonResponseDone();
        } else {
            return ResponseController::jsonResponseFail(['Данные о заказе не приняты']);
        }
    }

    public static function deleteOrders(Request $request)
    {
        if (isset($request->removalOrders) && !empty($request->removalOrders)) {
            OrdersController::deleteOrders($request);


            return ResponseController::jsonResponseDone();
        }
        return ResponseController::jsonResponseFail(['Не заданы заказы для удаления']);
    }

    public static function makeInvoiceCreationRequest($order)
    {

        //date_default_timezone_set(TIME_ZONE);
        $goodsInOrder = $order->goodsInOrder;
        $description = '';
        for ($i = 0; $i < count($goodsInOrder); $i++) {
            if ($i != 0) {
                $description .= ' / ';
            }
            $description .= $goodsInOrder[$i]->goods->params->shortDescription . ' - ' . $goodsInOrder[$i]->quantity;
        }
        if (strlen($description) > 100) {
            $description = mb_strimwidth($description, 0, 97, '...');
        }

        $weight = 0;
        for ($i = 0; $i < count($goodsInOrder); $i++) {
            $weight += $goodsInOrder[$i]->goods->params->weight * $goodsInOrder[$i]->quantity;
        }
        if ($weight == 0 || empty($weight)) {
            $weight = 0.2;
        }

        $cargoSizes = self::getCargoSizes($goodsInOrder);

        self::$NPHandler = new NovaPoshtaApi2(Config::get('np.API2_NP'));

        $recipientCity = '';
        $recipientWarehouse = '';

        $commonCity = (isset($order->deliveryParams->commonCity) ? $order->deliveryParams->commonCity->name : '');
        $commonAddress = (isset($order->deliveryParams->commonAddress) ? $order->deliveryParams->commonAddress->name : '');
        if (isset($commonCity, $commonAddress) && !empty($commonCity) & !empty($commonAddress)) {
            $recipientCity = $commonCity;
            $recipientWarehouse = $commonAddress;
        } else {
            $NPCity = $order->deliveryParams->NPCity->name;
            $NPWarehouse = $order->deliveryParams->NPWarehouse->name;
            if (isset($NPCity, $NPWarehouse) && !empty($NPCity) & !empty($NPWarehouse)) {
                $recipientCity = $NPCity;
                $recipientWarehouse = $NPWarehouse;
            }
        }

        $cost = 0;
        if ($order->price < 300) {                          //если стоимость товара <300
            $cost = 300;                                    //стоимость груза = 300
        } else {                                              //иначе
            $cost = $order->price;                          //стоимость груза = стоимости товаров
        }
        $redeliveryCost = $order->price;                    //возвращаемая сумма (наложенный платеж)(стоимость товаров)

        $sender = array(
            'FirstName' => 'Станіслав',//$sender['FirstName'],
            'MiddleName' => 'Едуардович',//$sender['MiddleName'],
            'LastName' => 'Головатий',//$sender['LastName'],
            'Phone' => '380989802953', //0675682276
            'City' => 'Дніпро',
            'Warehouse' => 'Відділення №16 (до 30 кг): пл. Героїв Майдану, 1', //$senderWarehouses['data'][14]['DescriptionRu'],
        );
        //preg_replace('|[\s]+|s', ' ', $row['bayer_name']);

        $recipient = array(
            'LastName' => $order->client->lastName,
            'FirstName' => $order->client->firstName,
            'MiddleName' => !empty($order->client->middleName) ? $order->client->middleName : "",
            'Phone' => $order->phone,
            'City' => $recipientCity,
            'Region' => 'Киевская',
            'Warehouse' => $recipientWarehouse,
        );

        $package = array(
            // Дата отправления
            'DateTime' => date('d.m.Y'),
            // Тип доставки, дополнительно - getServiceTypes()
            'ServiceType' => 'WarehouseWarehouse',
            // Тип оплаты, дополнительно - getPaymentForms()
            'PaymentMethod' => 'Cash',
            // Кто оплачивает за доставку
            'PayerType' => 'Recipient',
            // Стоимость груза в грн
            'Cost' => $cost,
            // Кол-во мест
            'SeatsAmount' => '1',
            // Описание груза
            'Description' => 'Побутові товари',
            // Тип доставки, дополнительно - getCargoTypes
            'CargoType' => 'Cargo',
            // Вес груза
            'Weight' => $weight,
            'VolumeWeight' => $cargoSizes['volume'],
            //дополнительная информация
            'AdditionalInformation' => $description,
            // Обратная доставка
        );

        $redeliveryParam = array(
            'BackwardDeliveryData' => array(
                array(
                    // Кто оплачивает обратную доставку
                    'PayerType' => 'Recipient',
                    // Тип доставки
                    'CargoType' => 'Money',
                    // Значение обратной доставки
                    'RedeliveryString' => $redeliveryCost,
                )
            )
        );

        $optionsSeatParam = array(
            'OptionsSeat' => array(
                array(
                    'volumetricVolume' => $cargoSizes['volume'],
                    'volumetricWidth' => $cargoSizes['width'],
                    'volumetricLength' => $cargoSizes['length'],
                    'volumetricHeight' => $cargoSizes['height'],
                    'weight' => $weight
                )
            )
        );


        if ($order->paymentTypeId == 1) {
            $package = array_merge($package, $redeliveryParam);
        }
        $package = array_merge($package, $optionsSeatParam);

        return self::$NPHandler->newInternetDocument($sender, $recipient, $package);
    }

    public static function printInvoices(Request $request)
    {
        $orderIdArray = json_decode($request->orderIdArray, true);

        $successed = $failed = 0;

        if (isset($orderIdArray) && !empty($orderIdArray)) {
            $orders = OrdersController::getOrdersByIdArray($orderIdArray);
            $link = 'http://my.novaposhta.ua/orders/printDocument';
            $errors = array();

            foreach ($orders as $order) {
                if ($order->deliveryParams->deliveryServiceId == 2) {
                    $npResponse = (object)self::makeInvoiceCreationRequest($order);
                    if ($npResponse->success == 1) {
                        $npResponse = (object)$npResponse->data[0];
                        $successed++;
//                        $estimatedDeliveryDate = date("Y-m-d H:i:s", strtotime($npResponse->EstimatedDeliveryDate));
                        $order->statusId = 5;
                        $order->invoice = $npResponse->IntDocNumber;
                        $order->delivered_at = date('Y-m-d H:i:s');
                        $order->save();
                        $link .= "/orders[]/".$npResponse->IntDocNumber;
                    } else {
                        $failed++;
                        $error = ["orderId" => $order->id, "reasons" => []];
                        if (isset($npResponse->errors))
                            $error["reasons"] = $npResponse->errors;
                        array_push($errors, $error);
                    }
                }
                $npApiKey = Config::get('np.API2_NP');
                $link .= "/type/html/apiKey/".$npApiKey;
            }

            $data = array(
                "link" => $link,
                "successedCount" => $successed,
                "failedCount" => $failed,
                "errors" => $errors
            );

            return ResponseController::jsonResponseDone($data);
        } else {
            return ResponseController::jsonResponseFail(['Не указаны заказы для печати ТТН']);
        }
    }

    public static function getCargoSizes($goodsInOrder)
    {

        $cargoSizes = array();
        $cargoSizes['width'] = $goodsInOrder[0]->goods->params->width;
        $cargoSizes['height'] = $goodsInOrder[0]->goods->params->height;
        $cargoSizes['length'] = $goodsInOrder[0]->goods->params->length;

        for ($i = 0; $i < count($goodsInOrder); $i++) {
            $i == 0 ? $s = 1 : $s = 0;
            for ($k = $s; $k < $goodsInOrder[$i]->quantity; $k++) {
                $pr_sizes = array(
                    'width' => $goodsInOrder[$i]->goods->params->width,
                    'height' => $goodsInOrder[$i]->goods->params->height,
                    'length' => $goodsInOrder[$i]->goods->params->length
                );

                if ($cargoSizes['width'] == min($cargoSizes))
                    $cargoSizes['width'] += min($pr_sizes);

                elseif ($cargoSizes['height'] == min($cargoSizes))
                    $cargoSizes['height'] += min($pr_sizes);

                elseif ($cargoSizes['length'] == min($cargoSizes))
                    $cargoSizes['length'] += min($pr_sizes);

                if ($pr_sizes['width'] > $cargoSizes['width']) $cargoSizes['width'] = $pr_sizes['width'];
                if ($pr_sizes['height'] > $cargoSizes['height']) $cargoSizes['height'] = $pr_sizes['height'];
                if ($pr_sizes['length'] > $cargoSizes['length']) $cargoSizes['length'] = $pr_sizes['length'];
            }

        }
        $cargoSizes['volume'] = ($cargoSizes['width'] * $cargoSizes['height'] * $cargoSizes['length']) / 4000;
        return $cargoSizes;
    }


}
