<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PrivacyModel;

/**
 *
 * @author Alexander Alexeev
 */
class PrivacyController extends Controller {

    public function index () {
        $viewParams = array();
        $viewParams["privacy"] = PrivacyModel::get();

        return view("settings.privacy-policy.index", $viewParams);
    }

    public function getPopup() {
        $viewsParams = array();
        $viewsParams["privacy"] = PrivacyModel::get();

        return view("settings.privacy-policy.popup", $viewsParams);
    }

    public function editPrivacy(Request $request) {
        $data = (object)$request->all();
        $errors = array();

        if(empty($data->text)) {
            $errors[] = "Текст политики конфиденциальности не может быть пустым";
        }

        if(empty($errors)) {
            $edit = PrivacyModel::savePrivacy($data);
            $responseData = array(
                "id" => $edit
            );


            return ResponseController::jsonResponseDone($responseData);
        }
        else {
            return ResponseController::jsonResponseFail($errors);
        }
    }
}