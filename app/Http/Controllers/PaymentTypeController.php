<?php

namespace App\Http\Controllers;

use App\Models\PaymentTypesModel;
use Illuminate\Http\Request;

class PaymentTypeController extends Controller
{
    public static function index()
    {
        $viewParams = array();
        $viewParams["paymentTypes"] = PaymentTypesModel::getPaymentTypes();
        return view("paymentTypes.index", $viewParams);
    }

    public static function getPaymentTypesTable(){
        $viewParams["paymentTypes"] = PaymentTypesModel::getPaymentTypes();
        return view("paymentTypes.tableBody", $viewParams);
    }

    public static function getPopup($id)
    {
        $viewsParams = array();
        if ($id != 0)
            $viewsParams["paymentType"] = PaymentTypesModel::getPaymentType($id);

        return view("paymentTypes.popup", $viewsParams);
    }

    public static function addPaymentType(Request $request){
        $data = (object)$request->all();
        $errors = array();
        if (PaymentTypesModel::isPaymentTypeExist($data->name)){
            $errors[] = "Такая тип оплаты уже существует";
        }
        elseif (empty($request->name)){
            $errors[] = "Название типа оплаты не может быть пустым";
        }

        if (empty($errors)){
            $paymentType = PaymentTypesModel::addPaymentType($data);
            $responseData = array(
                "id" => $paymentType->id
            );

            return ResponseController::jsonResponseDone($responseData);
        }
        else{
            return ResponseController::jsonResponseFail($errors);
        }

    }

    public static function editPaymentType(Request $request){
        $data = (object)$request->all();

        $prevPaymentType = PaymentTypesModel::getPaymentType($data->id);

        $currPaymentType = PaymentTypesModel::editPaymentType($data);

        return ResponseController::jsonResponseDone();
    }

    public static function deletePaymentTypes(Request $request){
        $paymentTypesId = json_decode($request->paymentTypesId);
        PaymentTypesModel::deletePaymentTypes($paymentTypesId);

        $del_id = implode(", ", $paymentTypesId);

        return ResponseController::jsonResponseDone();
    }

    public static function getPaymentTypes() {
        return PaymentTypesModel::getPaymentTypes();
    }
}
