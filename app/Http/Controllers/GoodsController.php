<?php

namespace App\Http\Controllers;

use App\Models\GoodsAvailabilityModel;
use App\Models\GoodsModel;
use App\Models\GoodsParamsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class GoodsController extends Controller
{
    private static function validateField($field)
    {
        $result = trim($field);
        $result = stripslashes($result);
        $result = htmlspecialchars($result);
        return $result;
    }

    //should be moved to the commons static class:
    public static function getValidDataFromRequestObject($request, $keys, $exceptions, &$errors)
    {

        $data = array();
        foreach ($keys as $key) {
            $value = $request->get($key);
            if (gettype($value) == 'boolean') {
                $data[$key] = $value;
            } else if (isset($value) && ($value != "")) {
                $data[$key] = self::validateField($value);
            } else {
                if (in_array($key, $exceptions)) {
                    $data[$key] = null;
                } else {
                    $errors[$key] = "Вы не указали параметр '$key'";
                }
            }
        }
        return $data;
    }

    //should be moved to the commons static class:
    public static function getValidDataFromRequestArray($request, $keys, $exceptions, &$errors)
    {

        $data = array();
        foreach ($keys as $key) {
            if (array_key_exists($key, $request)) {
                $value = $request[$key];
                if (gettype($value) == 'boolean') {
                    $data[$key] = $value;
                } else if (isset($value) && ($value != "")) {
                    $data[$key] = self::validateField($value);
                } else {
                    if (in_array($key, $exceptions)) {
                        $data[$key] = null;
                    } else {
                        $errors[] = "Вы не указали параметр '$key' для успешного добавления товара";
                    }
                }
            } else {
                $data[$key] = null;
            }
        }
        return $data;
    }

    public static function index()
    {
        $viewParams = array();
        $user = Session::get("user");
        if ($user->groupId == 1 || $user->groupId == 2){
            $viewParams["goodsList"] = GoodsModel::get();
            return view("goods.index", $viewParams);
        }
        else{
            $viewParams["goodsList"] = GoodsModel::get();
            return view("goods.partners.index", $viewParams);
        }
    }

    public static function getPopup($id)
    {
        $viewsParams = array();
        $viewsParams["categories"] = GoodsCategoriesController::getGoodsCategories();
        $viewsParams["measures"] = GoodsMeasuresController::getGoodsMeasures();
        $viewsParams["availabilityTypes"] = AvailabilityTypesController::getAvailabilityTypes();

        $user = Session::get("user");

        if ($user->groupId == 1 || $user->groupId == 2){
            if ($id != 0)
                $viewsParams["goods"] = GoodsModel::get($id);
            return view("goods.popup", $viewsParams);
        }
        else{
            $viewsParams["goods"] = GoodsModel::getActive($id);
            return view("goods.partners.popup", $viewsParams);

        }

    }

    public static function checkRanges (&$data, &$errors)
    {
        if($data['weight'] <= 0 || $data['weight'] > 10000) {
            $errors[] = "Недопустимое значение поля вес";
        }
        if($data['width'] <= 0 || $data['width'] > 200) {
            $errors[] = "Недопустимое значение поля ширина";
        }
        if($data['height'] <= 0 || $data['height'] > 200) {
            $errors[] = "Недопустимое значение поля высота";
        }
        if($data['length'] <= 0 || $data['length'] > 200) {
            $errors[] = "Недопустимое значение поля длинна";
        }
        if($data['volume'] <= 0 || $data['volume'] > 10000) {
            $errors[] = "Недопустимое значение поля количество";
        }
        if($data['price'] <= 0 || $data['price'] > 10000) {
            $errors[] = "Недопустимое значение поля цена";
        }
        if($data['availability'] <= 0 || $data['availability'] > 10000) {
            $errors[] = "Недопустимое значение поля в наличии";
        }
    }


    public static function addGoods()
    {
        try {
            $formData = json_decode($_POST['data'], true);
            $keys = [
                'name',
                'categoryId',
                'price',
                'weight',
                'length',
                'width',
                'height',
                'volume',
                'measureId',
                'availability',
                'availabilityTypeId',
                'link',
                'shortDesc',
                'fullDesc'
            ];

            $exceptions = array(
                "link", "shortDesc", "fullDesc"
            );

            $errors = array();
            $data = self::getValidDataFromRequestArray($formData, $keys, $exceptions, $errors);

            if (count($errors) > 0) {
                return ResponseController::jsonResponseFail($errors);
            }

            self::checkRanges($data, $errors);

            if (count($errors) > 0) {
                return ResponseController::jsonResponseFail($errors);
            }

            $data['imageLink'] = '';
            if(isset($_FILES['file'])) {
                if ($_FILES['file']['error'] > 0) {
                    echo 'Error: ' . $_FILES['file']['error'];
                } else {
                    move_uploaded_file($_FILES['file']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . '/public/images/goods/' . $_FILES['file']['name']);
                    $data['imageLink'] = '/images/goods/' . $_FILES['file']['name'];
                }
            }

            $data['goodsId'] = GoodsModel::insertGetId([
                'categoryId' => $data['categoryId'],
                'name' => $data['name'],
                'price' => $data['price']
            ]);

            GoodsParamsModel::insert([
                'goodsId' => $data['goodsId'],
                'weight' => $data['weight'],
                'width' => $data['width'],
                'height' => $data['height'],
                'length' => $data['length'],
                'volume' => $data['volume'],
                'measureId' => $data['measureId'],
                'link' => $data['link'],
                'imageLink' => $data['imageLink'],
                'shortDescription' => $data['shortDesc'],
                'largeDescription' => $data['fullDesc']
            ]);

            GoodsAvailabilityModel::insert([
                'goodsId' => $data['goodsId'],
                'availabilityTypeId' => $data['availabilityTypeId'],
                'availability' => $data['availability']
            ]);


            return ResponseController::jsonResponseDone();
        } catch (Exception $ex) {
            return $ex;
        }
    }



    public static function editGoods()
    {
        try {
            $formData = json_decode($_POST['data'], true);
            $keys = [
                "id",
                'name',
                'categoryId',
                'price',
                'weight',
                'length',
                'width',
                'height',
                'volume',
                'measureId',
                'availability',
                'availabilityTypeId',
                'link',
                'shortDesc',
                'fullDesc'
            ];

            $exceptions = array(
                "link", "shortDesc", "fullDesc"
            );

            $errors = array();


            $data = self::getValidDataFromRequestArray($formData, $keys, $exceptions, $errors);

            if (count($errors) > 0) {
                return ResponseController::jsonResponseFail($errors);
            }

            self::checkRanges($data, $errors);

            if (count($errors) > 0) {
                return ResponseController::jsonResponseFail($errors);
            }

            $data['imageLink'] = '';
            if (isset($_FILES['file'])) {
                if ($_FILES['file']['error'] > 0) {
                    echo 'Error: ' . $_FILES['file']['error'];
                } else {
                    move_uploaded_file($_FILES['file']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . '/public/images/goods/' . $_FILES['file']['name']);
                    $data['imageLink'] = '/images/goods/' . $_FILES['file']['name'];
                }
            }

            $data = (object)$data;
            $goods = GoodsModel::find($data->id);
            $prevGoods = GoodsModel::find($data->id);

            $goods->name = $data->name;
            $goods->categoryId = $data->categoryId;
            $goods->price = $data->price;

            $goods->params->weight = $data->weight;
            $goods->params->length = $data->length;
            $goods->params->width = $data->width;
            $goods->params->height = $data->height;
            $goods->params->volume = $data->volume;
            $goods->params->measureId = $data->measureId;
            $goods->params->link = $data->link;
            $goods->params->imageLink = !empty($data->imageLink) ? $data->imageLink : $goods->params->imageLink;
            $goods->params->shortDescription = $data->shortDesc;
            $goods->params->largeDescription = $data->fullDesc;

            $goods->availability->availabilityTypeId = $data->availabilityTypeId;
            $goods->availability->availability = $data->availability;

            $goods->save();
            $goods->params->save();
            $goods->availability->save();


            return ResponseController::jsonResponseDone();
        } catch (Exception $ex) {
            return $ex;
        }
    }



    public static function deleteGoods(Request $request)
    {
        $goodsIdList = json_decode($request->goodsIdList);
        GoodsModel::deleteGoods($goodsIdList);

        $del_id = implode(", ", $goodsIdList);
        return ResponseController::jsonResponseDone();
    }

    public static function getTableBody()
    {
        $viewParams = array();

        $user = Session::get("user");
        if ($user->groupId == 1 || $user->groupId == 2){
            $viewParams["goodsList"] = GoodsModel::get();
            return view("goods.tableBody", $viewParams);
        }
        else{
            $viewParams["goodsList"] = GoodsModel::getActive();
            return view("goods.partners.tableBody", $viewParams);
        }
    }

    public static function getGoods($id = 0)
    {
        return GoodsModel::getActive($id);
    }

    public static function changeGoodsActivity($id)
    {
        try {
            $goods = GoodsModel::get($id);
            $goods->isActive = !$goods->isActive;
            $goods->save();
            return ResponseController::jsonResponseDone();
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public static function getGoodsByCategoryId($categoryId)
    {
        $goodsData = GoodsModel::getGoodsByCategoryId($categoryId);
        $result = array();
        foreach ($goodsData as $goods) {
            $result[$goods->goodsId] = $goods;
        }
        return json_encode($result);
    }

    public static function getGoodsPriceById($goodsId) {
        return GoodsModel::getGoodsPriceById($goodsId);
    }
}
