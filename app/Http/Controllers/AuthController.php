<?php

namespace App\Http\Controllers;

use App\Models\PrivacyModel;
use App\Models\AgreementModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function index()
    {
        if (Session::has("user"))
            return redirect('/');
        else
            return view('auth.index');
    }

    public function authenticate(Request $request)
    {
        $login = $request->input("login");
        $password = $request->input("password");

        $partner = $employee = array();

        $employee = StaffController::getEmployeeByLoginPass($login, $password);

        if (empty($employee))
            $partner = PartnersController::getPartnerByLoginPass($login, $password);

        if ($employee) {
            Session::put("user", $employee);
            Session::forget("error");

            return redirect('/');
        } elseif ($partner) {
            if ($partner->statusId == 1) {
                Session::forget("user");
                return redirect('login')->with('error', 'Ваш аккаунт еще не активирован. Подтвердите активацию, перейдя по ссылке, которая была отправлена на Ваш e-mail при регистрации')->with("login", $login);
            }
//            elseif ($partner->statusId == 4) {
//                Session::forget("user");
//                return redirect('login')->with('error', 'Ваш аккаунт временно заблокирован')->with("login", $login);
//            } elseif ($partner->statusId == 5) {
//                Session::forget("user");
//                return redirect('login')->with('error', 'Ваш аккаунт заблокирован')->with("login", $login);
//            } elseif ($partner->statusId == 6) {
//                Session::forget("user");
//                return redirect('login')->with('error', 'Вам отказано в регистрации')->with("login", $login);
//            }
            else {
                Session::put("user", $partner);
                Session::forget("error");
                return redirect('/');
            }
        } else
            return redirect('login')->with('error', 'Ошибка логина или пароля')->with("login", $login);
    }

    public static function getFAQPopup()
    {
        return FAQController::getLoginPagePopup();
    }

    public static function getContactsPopup()
    {
        return ContactsController::getLoginPagePopup();
    }

    public static function getAgreementPopup(){
        $viewParams = array();
        $viewParams["agreement"] = AgreementModel::get();

        return view("auth.agreementPopup", $viewParams);
    }

    public static function getPolicyPopup(){
        $viewParams = array();
        $viewParams["privacy"] = PrivacyModel::get();

        return view("auth.policyPopup", $viewParams);
    }

    public function logout()
    {
        $user = Session::get("user");
        isset($user) ? $login = $user->login : $login = "";
        Session::forget("user");
        Session::forget("error");
        return redirect('login')->with("login", $login);
    }
}
