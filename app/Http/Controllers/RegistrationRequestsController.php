<?php

namespace App\Http\Controllers;

use App\Models\PartnersModel;
use App\Models\RejectionReasonsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;

class Mail
{

    public static function send($to, $from, $subject, $text)
    {
        if (isset($to, $from, $subject, $text)) {
            $subject = "=?utf-8?b?" . base64_encode($subject) . "?=";
            $headers = "Content-type: text/html; charset=\"utf-8\"\r\n";
            $headers .= "From: " . $from . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Date: " . date('D, d M Y h:i:s O') . "\r\n";
            $headers .= "Precedence: bulk\r\n";

            return mail($to, $subject, $text, $headers);
        }
        return -1;
    }
}

class RegistrationRequestsController extends Controller
{
    private static function validateField($field)
    {
        $result = trim($field);
        $result = stripslashes($result);
        $result = htmlspecialchars($result);
        return $result;
    }

    public static function index()
    {
        $viewParams = array();
        $viewParams["partners"] = PartnersModel::getUnregisteredPartners();
        return view('registrationRequests.index', $viewParams);
    }

    public static function getTableBody(){
        $viewParams = array();
        $viewParams["partners"] = PartnersModel::getUnregisteredPartners();
        return view('registrationRequests.tableBody', $viewParams);
    }

    public static function getPopup($partnerId)
    {
        $viewParams = array();
        $viewParams["partner"] = PartnersController::getPartners($partnerId);
        return view("registrationRequests.popup", $viewParams);
    }

    public static function getDeclinePopup($partnerId)
    {
        $viewParams = array();
        $viewParams["partnerId"] = $partnerId;
        $viewParams["declineReasons"] = RejectionReasonsModel::getRejectionReasonsByType(1);
        return view("registrationRequests.decline-popup", $viewParams);
    }

    public static function confirmRegistration(Request $request)
    {
        try {
            $id = '';
            $login = '';
            $firstName = '';
            $lastName = '';
            $phone = '';
            $email = '';
            $contacts = '';
            $card = '';
            $cardOwner = '';
            $isCardOwnerSelf = '';
            $nickname = '';
            $formData = array();
            parse_str($request->formData, $formData);

            $errors = array();

            if (isset($request->id))
                $id = $request->id;
            else
                $errors[] = 'Не задано поле ';

            if (!empty($formData['login']))
                $login = $formData['login'];
            else
                $errors[] = 'Не задано поле ';

            if (!empty($formData['firstName']))
                $firstName = $formData['firstName'];
            else
                $errors[] = 'Не задано поле ';

            if (!empty($formData['lastName']))
                $lastName = $formData['lastName'];
            else
                $errors[] = 'Не задано поле ';

            if (!empty($formData['phone']))
                $phone = $formData['phone'];
            else
                $errors[] = 'Не задано поле ';

            if (!empty($formData['email']))
                $email = $formData['email'];
            else
                $errors[] = 'Не задано поле ';

            if (!empty($formData['contacts']))
                $contacts = $formData['contacts'];

//            if (!empty($formData['card']))
//                $card = $formData['card'];
//            else
//                $errors[] = 'Не задано поле ';
//
//            if (!empty($formData['cardOwner']))
//                $cardOwner = $formData['cardOwner'];
//            else
//                $errors[] = 'Не задано поле ';

            if (!empty($formData['card']))
                $card = $formData['card'];
            else
                $card = "";

            if (!empty($formData['cardOwner']))
                $cardOwner = $formData['cardOwner'];
            else
                $cardOwner = "";

            if (!empty($formData['isCardOwnerSelf'])) {
                $isCardOwnerSelf = $formData['isCardOwnerSelf'];
                $isCardOwnerSelf = ($isCardOwnerSelf == "on") ? 1 : 0;
            } else {
                $isCardOwnerSelf = 0;
            }


            if (!empty($formData['nickname']))
                $nickname = $formData['nickname'];
            else
                $errors['nickname'] = 'Не задано поле ';

            if (count($errors) > 0) {
                return ResponseController::jsonResponseFail($errors);
            }

            $id = self::validateField($id);
            $login = self::validateField($login);
            $firstName = self::validateField($firstName);
            $lastName = self::validateField($lastName);
            $phone = self::validateField($phone);
            $email = self::validateField($email);
            $card = self::validateField($card);
            $cardOwner = self::validateField($cardOwner);
            $isCardOwnerSelf = self::validateField($isCardOwnerSelf);
            $nickname = self::validateField($nickname);

            DB::update("UPDATE PartnerProfiles SET firstName = '$firstName', lastName = '$lastName', phone = '$phone', email = '$email', contacts = '$contacts', card = '$card', cardOwner = '$cardOwner', isCardOwnerSelf = $isCardOwnerSelf, nickname = '$nickname' WHERE partnerId = $id");
            DB::update("UPDATE Partners SET login = '$login', statusId = 3 WHERE id = $id");

            Mail::send($email, '', "Регистрация завершена", "Вы успешно зарегистрированы. Войдите в систему, используя Ваши логин и пароль.");

            return ResponseController::jsonResponseDone();
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function declineRegistration(Request $request)
    {
        try {
            $errors = array();
            $partnerId = $request->partnerId;
            $reasonId = $request->reasonId;
            $reasonText = $request->reasonText;

            if (!isset($partnerId)) {
                $errors[] = "Заявка не найдена";
            }
            if (!isset($reasonId) || ($reasonId == -1 && empty($reasonText))) {
                $errors[] = "Не указана причина отказа";
            }

            if (!empty($errors)) {
                return ResponseController::jsonResponseFail($errors);
            } else {
                $email = PartnersController::getPartners($partnerId)->profile->email;

                if ($reasonId != -1) {
                    $reasonText = RejectionReasonsController::getRejectionReason($reasonId)->text;
                    Partners_RejectionReasonsController::addRejection($partnerId, $reasonId, null);
                }
                else {
                    Partners_RejectionReasonsController::addRejection($partnerId, null, $reasonText);
                }


                mail($email, "Отказ в регистрации", $reasonText);
                PartnersController::setPartnerStatus($partnerId, 6);

                return ResponseController::jsonResponseDone();
            }
        } catch (Exception $ex) {
            die($ex);
        }
    }

}
