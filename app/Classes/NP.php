<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Classes;

use Exception;
/**
 * Description of NP
 *
 * @author Delevoper
 */
class NovaPoshtaHandler
{

    const NP2_STATUS_ORDER_IN_PROCESSING = 1;
    const NP2_STATUS_DELETED = 2;
    const NP2_STATUS_PHONE_NOT_FOUND = 3;
    const NP2_STATUS_PREPARING_TO_DISPATCH = 4;
    const NP2_STATUS_DISPATCHED = 5;
    const NP2_STATUS_PREPARING_TO_ISSUE = 6;
    const NP2_STATUS_ARRIVED_TO_OFFICE1 = 7;
    const NP2_STATUS_ARRIVED_TO_OFFICE2 = 8;
    const NP2_STATUS_RECEIVED1 = 9;
    const NP2_STATUS_RECEIVED2 = 10;
    const NP2_STATUS_RECEIVED3 = 106;
    const NP2_STATUS_GOING_TO_RECIPIENT = 101;
    const NP2_STATUS_RECIPIENT_REFUSED1 = 102;
    const NP2_STATUS_RECIPIENT_REFUSED2 = 103;
    const NP2_STATUS_ADDRESS_CHANGED = 104;
    const NP2_STATUS_STORAGE_STOPPED = 105;
    const NP2_STATUS_FEE_BILLED_FOR_STORAGE = 107;

    private static function makeRequest($url, $request)
    {

        if (!isset($request) || empty($request)) {
            throw new Exception('request data was not set correctly (NovaPoshtaHandler -> makeRequest()).');
        }

        try {

            $cUrlHandler = curl_init();
            curl_setopt($cUrlHandler, CURLOPT_URL, $url);
            curl_setopt($cUrlHandler, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($cUrlHandler, CURLOPT_HTTPHEADER, Array("Content-Type: application/json"));
            curl_setopt($cUrlHandler, CURLOPT_HEADER, 0);
            curl_setopt($cUrlHandler, CURLOPT_POSTFIELDS, $request);
            curl_setopt($cUrlHandler, CURLOPT_POST, 1);
            curl_setopt($cUrlHandler, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($cUrlHandler);
            curl_close($cUrlHandler);

            return json_decode($response);

        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public static function invoiceTracking($invoiceId, $invoicePhone)
    {

        if (empty($invoiceId)) {
            throw new Exception('invoice id was not set correctly (NovaPoshtaHandler -> invoiceTracking()).');
        }

        if (empty($invoicePhone)) {
            $invoicePhone = '380XXXXXXXXX';
        }

        $request = array();
        $request['apiKey'] = API2_NP;
        $request['modelName'] = 'TrackingDocument';
        $request['calledMethod'] = 'getStatusDocuments';

        $request['methodProperties'] = array();
        $request['methodProperties']['Documents'] = array();
        array_push($request['methodProperties']['Documents'], array('DocumentNumber' => $invoiceId, 'Phone' => $invoicePhone));

        $request['methodProperties']['Language'] = 'UA';

        try {
            $url = 'https://api.novaposhta.ua/v2.0/json/';
            $response = self::makeRequest($url, json_encode($request));
            return $response;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public static function invoiceListTracking($invoiceData)
    {                                                                               

        if (!isset($invoiceData) || empty($invoiceData)) {
            throw new Exception('invoice codes was not set correctly (NovaPoshtaHandler -> invoiceListTracking()).');
        }

        $request = array();
        $request['apiKey'] = API2_NP;
        $request['modelName'] = 'TrackingDocument';
        $request['calledMethod'] = 'getStatusDocuments';

        $request['methodProperties'] = array();
        $request['methodProperties']['Documents'] = array();

        foreach ($invoiceData as $invoice) {
            if (!empty($invoice['ttn'])) {
                $phone = (!empty($invoice['phone'])) ? $invoice['phone'] : '380XXXXXXXXX';
                array_push($request['methodProperties']['Documents'], array('DocumentNumber' => $invoice['ttn'], 'Phone' => $phone));
            }
        }

        $request['methodProperties']['Language'] = 'UA';

        try {
            $url = 'https://api.novaposhta.ua/v2.0/json/';
            $response = self::makeRequest($url, json_encode($request));
            return $response;
        } catch (Exception $ex) {
            throw $ex;
        }
    }
}
