<?php

namespace App\Classes;

class ipApi
{
    public static function getGeoInfo($ip)
    {
        $url = "http://api.2ip.ua/geo.json?ip=".$ip;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, true);
    }
}