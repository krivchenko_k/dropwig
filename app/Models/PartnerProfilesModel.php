<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PartnerProfilesModel extends Model
{
    protected $table = "PartnerProfiles";
    public static $publicTableName = 'PartnerProfiles';

    public static function getPartnerData($partnerId) {
        return self::find($partnerId);
    }

    public static function isEmailExist($email){
        return self::where("email", $email)->value("partnerId");
    }

    public static function isPhoneExist($phone){
        return self::where("phone", $phone)->value("partnerId");
    }

    public static function isNicknameExist($nickname){
        return self::where("nickname", $nickname)->value("partnerId");
    }

    public static function isCardExist($card){
        return self::where("card", $card)->value("partnerId");
    }

    public static function updateLastActivity($partnerId){
        self::where("partnerId", $partnerId)->update(["lastActivity" => DB::raw("CURRENT_TIMESTAMP")]);
    }

    public function balance(){
        return $this->hasOne('App\Models\PartnerBalancesModel', "partnerId", "partnerId");
    }
}
