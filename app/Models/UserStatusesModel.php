<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserStatusesModel extends Model
{
    protected $table = 'UserStatuses';
    public static $publicTableName = 'UserStatuses';
    public $timestamps = false;

    public static function get($id = 0){
        if (!$id){
            return self::all();
        }
        else{
            return self::find($id);
        }
    }
}
