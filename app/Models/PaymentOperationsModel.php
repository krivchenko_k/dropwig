<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentOperationsModel extends Model
{
    protected $table = "PaymentOperations";
    public $timestamps = false;

    public static $publicTableName = "PaymentOperations";

    public static function get($id = 0) {
        if($id) {
            return self::where('id', $id);
        }
        else {
            return self::all();
        }
    }

    public static function getPaymentOperationData($operationId) {
        return self::find($operationId);
    }

}
