<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgreementModel extends Model
{
    protected $table = "Agreement";
    public static $publicTableName = "Agreement";

    public $timestamps = false;

    public static function get()
    {
        return self::first();
    }

    public static function saveAgreement($data)
    {
        $agreement = self::first();
        if($agreement == false) {
            $agreement = new self();
            $agreement->text = $data->text;
            $agreement->save();

            return $agreement;
        }
        else {
            $agreement->text = $data->text;
            $agreement->save();

            return $agreement;
        }
    }
}