<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoodsModel extends Model
{
    use SoftDeletes;

    protected $table = "Goods";
    public static $publicTableName = "Goods";
    public $timestamps = false;

    protected $dates = ['deleted_at'];

    public function availability()
    {
        return $this->hasOne("App\Models\GoodsAvailabilityModel", "goodsId");
    }

    public function params()
    {
        return $this->hasOne("App\Models\GoodsParamsModel", "goodsId");
    }

    public function category()
    {
        return $this->hasOne("App\Models\GoodsCategoriesModel", "id", "categoryId");
    }

    public function relatedOrders() {
        return $this->hasMany('App\Models\Orders_GoodsModel', 'goodsId');
    }

    public static function getActive($id = 0)
    {
        if (!$id)
            return self::where("isActive", 1)
                ->with('params')
                ->with('category')
                ->with('params.measure')
                ->with('availability')
                ->with('availability.availabilityType')
                ->get();
        else
            return self::where("id", $id)->where("isActive", 1)->first();
    }

    public static function get($id = 0)
    {
        if (!$id)
            return self::with('params')
                ->with('category')
                ->with('params.measure')
                ->with('availability')
                ->with('availability.availabilityType')
                ->get();
        else
            return self::where("id", $id)->first();
    }

    public static function deleteGoods($goodsIdList)
    {
        self::whereIn("id", $goodsIdList)->delete();
    }

    public static function getGoodsByCategoryId($categoryId)
    {
        return self::where('categoryId', $categoryId)
            ->with('params')
            ->with('category')
            ->with('params.measure')
            ->with('availability')
            ->with('availability.availabilityType')
            ->get();
    }

    public static function getGoodsPriceById($goodsId)
    {
        return self::where('id', '=', $goodsId)->value('price');
    }

}
