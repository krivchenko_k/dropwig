<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class OrdersModel
 * @package App\Models
 */
class OrdersModel extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = "Orders";
    /**
     * @var string
     */
    public static $publicTableName = "Orders";
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partner()
    {
        return $this->belongsTo('App\Models\PartnersModel', 'partnerId');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partnerProfile()
    {
        return $this->belongsTo('App\Models\PartnerProfilesModel', 'partnerId', 'partnerId');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo('App\Models\ClientsModel', 'clientId', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function details()
    {
        return $this->hasOne('App\Models\OrderDetailsModel', "orderId");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paymentType()
    {
        return $this->belongsTo('App\Models\PaymentTypesModel', "paymentTypeId");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function goodsInOrder()
    {
        return $this->hasMany('App\Models\Orders_GoodsModel', "orderId");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo('App\Models\OrderStatusesModel', "statusId");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function deliveryParams()
    {
        return $this->hasOne('App\Models\OrderDeliveryParamsModel', 'orderId');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function staff()
    {
        return $this->belongsTo('App\Models\StaffModel', "staffId");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rejections()
    {
        return $this->hasMany('App\Models\Orders_RejectionReasonsModel', 'orderId');
    }


    public static function get($idArray = null, $params = null)
    {
        if (!empty($idArray)) {
            if (is_array($idArray)) {
                if (!empty($params))
                    return self::whereIn("id", $idArray)->where($params)->with("goodsInOrder")->with("goodsInOrder.goods")->with("status")->with("partner")->with("details")->with("staff")->with("deliveryParams")->with("paymentType")->get();
                else
                    return self::whereIn("id", $idArray)->with("goodsInOrder")->with("goodsInOrder.goods")->with("status")->with("partner")->with("details")->with("staff")->with("deliveryParams")->with("paymentType")->get();
            } else
                if (!empty($params))
                    return self::where("id", $idArray)->where($params)->with("goodsInOrder")->with("goodsInOrder.goods")->with("status")->with("partner")->with("details")->with("staff")->with("deliveryParams")->with("paymentType")->first();
                else
                    return self::where("id", $idArray)->with("goodsInOrder")->with("goodsInOrder.goods")->with("status")->with("partner")->with("details")->with("staff")->with("deliveryParams")->with("paymentType")->first();
        } else {
            if (!empty($params))
                return self::where($params)->with("goodsInOrder")->with("goodsInOrder.goods")->with("status")->with("partner")->with("details")->with("staff")->with("deliveryParams")->with("paymentType")->get();
            else
                return self::with("goodsInOrder")->with("goodsInOrder.goods")->with("status")->with("partner")->with("details")->with("staff")->with("deliveryParams")->with("paymentType")->get();
        }
    }


    public static function edit($idArray, $params)
    {
        if (is_array($idArray)) {
            return self::whereIn("id", $idArray)->update($params);
        } else
            return self::where("id", $idArray)->update($params);
    }

    /**
     * @param $statusId
     * @return mixed
     */
    public static function getOrdersByStatusId($statusId)
    {
        if ($statusId == 0){
            return self::with("goodsInOrder")->with("goodsInOrder.goods")->with("status")->with("partner")->with("details")->get();
        }
        else{
            return self::where("statusId", $statusId)->with("goodsInOrder")->with("goodsInOrder.goods")->with("status")->with("partner")->with("details")->get();
            return self::where("statusId", $statusId)->with("goodsInOrder")->with("goodsInOrder.goods")->with("status")->with("partner")->with("details")->limit(10)->get();
        }
    }


    /**
     * @param $orderId
     * @param bool $isTrashed
     * @return mixed
     */
    public static function getOrderById($orderId, $isTrashed = false)
    {
        if (!$isTrashed)
            return self::where('id', $orderId)->first();
        else
            return self::where('id', $orderId)->withTrashed()->first();
    }

    /**
     * @param $orderData
     * @return mixed
     */
    public static function updateOrder($orderData)
    {
        return self::where('id', '=', $orderData->orderId)->update([
            'statusId' => $orderData->orderStatusId,
            'paymentTypeId' => $orderData->paymentTypeId,
            'clientId' => $orderData->clientId,
            'staffId' => $orderData->staffId,
            'phone' => $orderData->orderPhone,
            'price' => $orderData->orderPrice,
            'invoice' => $orderData->invoice,
        ]);
    }

    /**
     * @param $orderData
     * @return mixed
     */
    public static function addOrder($orderData)
    {
        return self::insertGetId([
            'statusId' => $orderData->orderStatusId,
            'paymentTypeId' => $orderData->paymentTypeId,
            'partnerId' => $orderData->partnerId,
            'clientId' => $orderData->clientId,
            'staffId' => $orderData->staffId,
            'phone' => $orderData->orderPhone,
            'price' => $orderData->orderPrice,
            'invoice' => $orderData->invoice
        ]);
    }

    /**
     * @param $removalOrders
     * @return mixed
     */
    public static function remove($removalOrders)
    {
        return self::whereIn("id", $removalOrders)->delete();
    }

    /**
     * @param $recoveredOrders
     * @return mixed
     */
    public static function recover($recoveredOrders)
    {
        return self::whereIn("id", $recoveredOrders)->restore();
    }

    /**
     * @param $erasedlOrders
     * @return mixed
     */
    public static function erase($erasedlOrders)
    {
        $orders = self::whereIn("id", $erasedlOrders)->onlyTrashed()->get();

        foreach ($orders as $order) {
            if (!empty($order->details)){
                $order->details->delete();
            }
            if (!empty($order->deliveryParams)){
                $order->deliveryParams->delete();
            }
            foreach ($order->goodsInOrder as $goodsInOrder) {
                $goodsInOrder->delete();
            }
        }
        return self::whereIn("id", $erasedlOrders)->forceDelete();
    }

    /**
     * @param $partnerId
     * @param $statusId
     * @param int $dateRange
     * @return mixed
     */
    public static function getOrdersForPartner($partnerId, $statusId)
    {
        return self::where("partnerId", $partnerId)->where("statusId", $statusId)->with("goodsInOrder")->with("goodsInOrder.goods")->with("status")->with("partner")->with("details")->with("staff")->with("deliveryParams")->with("paymentType")->get();
    }

    /**
     * @param $idArray
     * @return mixed
     */
    public static function getOrdersDataForIdArray($idArray)
    {
        if (!empty($idArray)) {
            $arrayAsString = implode(',', $idArray);
            return DB::select(DB::raw("SELECT
                                    O.id AS orderId, O.statusId, O.paymentTypeId, O.partnerId, O.clientId, O.isDeleted,
                                    O.phone, O.price, O.invoice, O.backwardInvoice, O.created_at, O.updated_at, O.completed_at,
                                    ODP.deliveryServiceId, ODP.NPCityId, ODP.NPWarehouseId, ODP.commonCityId, ODP.commonAddressId,
                                    CC.name AS commonCityName,
                                    CA.name AS commonAddressName,
                                    (SELECT SUM((G.price + O_G.markup) * O_G.quantity) FROM Orders_Goods AS O_G, Goods AS G WHERE O_G.goodsId = G.id AND O_G.orderId = O.id) AS orderPrice,
                                    (SELECT SUM(G.price * O_G.quantity) FROM Orders_Goods AS O_G, Goods AS G WHERE O_G.goodsId = G.id AND O_G.orderId = O.id) AS totalDropPrice,
                                    (SELECT SUM(O_G.markup * O_G.quantity) FROM Orders_Goods AS O_G WHERE O_G.orderId = O.id) AS totalMarkup,
                                    (SELECT SUM(O_G.quantity) FROM Orders_Goods AS O_G WHERE O_G.orderId = O.id) AS totalGoodsQuantity,
                                    OD.comment, OD.rejectionReason,
                                    OS.name AS statusName,
                                    DS.name AS deliveryServiceName,
                                    PT.name AS paymentTypeName,
                                    UP.firstName AS partnerFirstName, UP.lastName AS partnerLastName,
                                    C.firstName AS clientFirstName, C.lastName AS clientLastName, C.middleName AS clientMiddleName
                                    FROM Orders AS O
                                    LEFT JOIN OrderDeliveryParams AS ODP ON O.id = ODP.orderId
                                    LEFT JOIN CommonCities AS CC ON ODP.commonCityId = CC.id
                                    LEFT JOIN CommonAddresses AS CA ON ODP.commonAddressId = CA.id
                                    LEFT JOIN OrderDetails AS OD ON O.id = OD.orderId
                                    LEFT JOIN OrderStatuses AS OS ON O.statusId = OS.id
                                    LEFT JOIN DeliveryServices AS DS ON ODP.deliveryServiceId = DS.id
                                    LEFT JOIN PaymentTypes AS PT ON O.paymentTypeId = PT.id
                                    LEFT JOIN PartnerProfiles AS UP ON O.partnerId = UP.partnerId
                                    LEFT JOIN Clients AS C ON O.clientId = C.id
                                    WHERE O.isDeleted <> 1
                                    AND O.id IN ($arrayAsString)
                                    ORDER BY O.created_at DESC"));
        }
    }

    /**
     * @param $ordersIdArray
     * @param $statusId
     * @return int
     */
    public static function setStatusForGroup($ordersIdArray, $statusId)
    {
        if (isset($ordersIdArray) && !empty($ordersIdArray)) {
            return self::whereIn('id', $ordersIdArray)->update(['statusId' => $statusId]);
        }
        return 0;
    }

    /**
     * @param $orderId
     * @param $statusId
     * @return mixed
     */
    public static function setStatus($orderId, $statusId)
    {
        return self::where('id', $orderId)->update(['statusId' => $statusId]);
    }

    /**
     * @param string $dateFrom
     * @param string $dateTo
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getOrdersByPeriod($dateFrom = "", $dateTo = "")
    {
        if (!empty($dateFrom) && !empty($dateTo)) {
            return self::whereDate('created_at', '>=', date('Y-m-d', strtotime($dateFrom)))->whereDate('created_at', '<=', date('Y-m-d', strtotime($dateTo)))->get();
        }
        if (!empty($dateFrom)) {
            return self::whereDate('created_at', '>=', date('Y-m-d', strtotime($dateFrom)))->get();
        }
        if (!empty($dateTo)) {
            return self::whereDate('created_at', '<=', date('Y-m-d', strtotime($dateTo)))->get();
        }
        return self::all();
    }

    /**
     * @param $partnerId
     * @param string $dateFrom
     * @param string $dateTo
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getOrdersForPartnerByPeriod($partnerId, $dateFrom = "", $dateTo = "")
    {
        if (!empty($dateFrom) && !empty($dateTo)) {
            return self::where('partnerId', $partnerId)->whereDate('created_at', '>=', date('Y-m-d', strtotime($dateFrom)))->whereDate('created_at', '<=', date('Y-m-d', strtotime($dateTo)))->get();
        }
        if (!empty($dateFrom)) {
            return self::where('partnerId', $partnerId)->whereDate('created_at', '>=', date('Y-m-d', strtotime($dateFrom)))->get();
        }
        if (!empty($dateTo)) {
            return self::where('partnerId', $partnerId)->whereDate('created_at', '<=', date('Y-m-d', strtotime($dateTo)))->get();
        }
        return self::where('partnerId', $partnerId)->get();
    }

    /**
     * @return mixed
     */
    public static function getParthersCreatedOrderPerWeekCount()
    {
        $dateFrom = date('Y-m-d', time() - (date('N') - 1) * 24 * 60 * 60);
        $dateTo = date("Y-m-d");
        return self::whereDate('created_at', '>=', $dateFrom)->whereDate('created_at', '<=', $dateTo)->distinct("partnerId")->count("partnerId");
    }

    /**
     * @param $orderId
     * @param $staffId
     */
    public static function setOrderAcceptorId($orderId, $staffId)
    {
        $order = self::find($orderId);
        $order->staffId = $staffId;
        $order->save();
    }

    /**
     * @return mixed
     */
    public static function getTrashedOrders()
    {
        return self::onlyTrashed()->get();
    }


    public static function getOrdersByIdArray($orderIdArray)
    {
        return self::whereIn('id', $orderIdArray)->get();
    }


    public static function getOrdersByNPUpdate ($statuses, $dateFrom, $dateTo) {
        return self::whereIn("statusId", $statuses)->whereDate('created_at', '>=', date('Y-m-d', strtotime($dateFrom)))->whereDate('created_at', '<=', date('Y-m-d', strtotime($dateTo)))->get();
    }


    public static function getTest($whereParams)
    {
        return self::where([["statusId", "=", 5], ["paymentTypeId", "=", 3]])->get(["id"]);
    }

    public static function setOrderBackwardInvoice ($orderId, $backwardInvoice) {
        $order = self::find($orderId);
        $order->backwardInvoice = $backwardInvoice;
        $order->save();
    }

}

/*

 */