<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerBalancesModel extends Model
{
    protected $table = "PartnerBalances";
    public $timestamps = false;

    public static $publicTableName = "PartnerBalances";

    public function partnerProfile() {
        return $this->belongsTo('App\Models\PartnerProfilesModel', 'partnerId');
    }

    public static function get($id = null,  $params = null) {
        if (!empty($id)) {
            if (is_array($id)) {
                if (!empty($params))
                    return self::whereIn("id", $id)->where($params)->with("partnerProfile")->get();
                else
                    return self::whereIn("id", $id)->with("partnerProfile")->get();
            } else
                if (!empty($params))
                    return self::where("id", $id)->where($params)->with("partnerProfile")->first();
                else
                    return self::where("id", $id)->with("partnerProfile")->first();
        } else {
            if (!empty($params))
                return self::where($params)->with("partnerProfile")->get();
            else
                return self::with("partnerProfile")->get();
        }
    }

    public static function add($partnerId){
        $balance = new self();
        $balance->partnerId = $partnerId;
        $balance->save();
        return $balance->id;
    }

    public static function updateBalance($partnerId, $value) {
        $balance = self::where('partnerId', $partnerId)->first();
        $balance->amount += $value;
        $balance->save();
    }

    public static function getTotalBalancesSum() {
        return self::sum('amount');
    }

}
