<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Orders_GoodsModel extends Model
{
    protected $table = "Orders_Goods";
    public static $publicTableName = 'Orders_Goods';

    public $timestamps = false;

    public function goods(){
        return $this->belongsTo('App\Models\GoodsModel', "goodsId");
    }

    public static function getGoodsInOrders($statusId) {
        $statusWhere = "";
        if(isset($statusId) && !empty($statusId)) {
            $statusWhere = "LEFT JOIN Orders AS O ON O.id = O_G.orderId WHERE O.statusId = ".$statusId;
        }
        return DB::select(DB::raw("SELECT G.id, G.name, G.categoryId, G.price,
                                     O_G.orderId, O_G.markup, O_G.quantity,
                                     ((G.price + O_G.markup) * O_G.quantity) AS totalPrice,
                                     (O_G.markup * O_G.quantity) AS profit
                                     FROM Goods AS G 
                                     LEFT JOIN Orders_Goods AS O_G ON O_G.goodsId = G.id
                                     $statusWhere"));
    }

    public static function getGoodsInOrderById($orderId) {
        return DB::select(DB::raw("SELECT G.id, G.name, G.price, G.categoryId,
                                       GP.largeDescription, GP.imageLink,
                                       O_G.id AS rowNumber, O_G.orderId, O_G.markup, O_G.quantity,
                                       ((G.price + O_G.markup) * O_G.quantity) AS totalPrice,
                                       (O_G.markup * O_G.quantity) AS profit
                                       FROM Goods AS G 
                                       LEFT JOIN GoodsParams AS GP ON GP.goodsId = G.id
                                       LEFT JOIN Orders_Goods AS O_G ON O_G.goodsId = G.id
                                       WHERE O_G.orderId ={$orderId}"));
    }

    public static function getRowNumbersByOrderId($orderId) {
        return self::where('orderId', '=', $orderId)->pluck('id')->toArray();
    }

    public static function updateGoodsInOrder($orderId, $goodsData) {
        $affectedRows = self::where('orderId', '=', $orderId)->where('id', '=', $goodsData->rowNumber)->update([
            'goodsId' => $goodsData->goodsId, 'markup' => $goodsData->markup, 'quantity' => $goodsData->quantity
        ]);
        return $affectedRows;
    }

    public static function addGoodsInOrder($orderId, $goodsData) {
        return self::insertGetId([
            'orderId' => $orderId, 'goodsId' => $goodsData->goodsId,
            'markup' => $goodsData->markup, 'quantity' => $goodsData->quantity,
            'goodsPrice' => $goodsData->goodsPrice
        ]);
    }

    public static function deleteGoodsById($rowNumbers) {
        return self::destroy($rowNumbers);
    }

}
