<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class NPWarehousesModel extends Model
{
    protected $table = "NPWarehouses";
    public static $publicTableName = "NPWarehouses";
    public $timestamps = true;

    public static function getWarehouses() {
        return self::all();
    }

    public static function getWarehouseById($warehouseId) {
        return self::where('id', '=', $warehouseId)->first();
    }

    public static function getWarehouseByRef($warehouseRef) {
        return self::where('ref', '=', $warehouseRef)->first();
    }

    public static function getWarehousesByCityId($cityId) {
        return DB::select(DB::raw("SELECT * FROM NPWarehouses WHERE NPCityId = $cityId"));
        //return DB::table("NPWarehouses")->where("NPCityId", "=", $cityId);
        //return self::where('NPCityId', '=', $cityId);
        //return NPWarehousesModel::where('NPCityId', '=', $cityId);
        //return DB::select(DB::raw("SELECT * FROM NPWarehouses WHERE NPCityId = $cityId"));
    }

    public static function getWarehousesByCityRef($cityRef) {
        return self::where('cityRef', '=', $cityRef);
    }

    public static function insertWarehouse($cityId, $data) {
        $officeId = self::insertGetId([
            'ref' => $data->Ref, 'NPCityId' => $cityId,
            'name' => $data->DescriptionRu, 'type' => $data->TypeOfWarehouse,
            'isActive' => 1, 'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
        return $officeId;
    }

    public static function updateWarehouse($data) {
        $affectedRows = self::where('ref', '=', $data->Ref)->update([
            'name' => $data->DescriptionRu, 'isActive' => 1
        ]);
        return ($affectedRows >= 1) ? true : false;
    }

    public static function updateActivity() {
        DB::update('UPDATE NPWarehouses SET isActive = 0 WHERE DATE(updated_at) < CURRENT_DATE');
    }

}
