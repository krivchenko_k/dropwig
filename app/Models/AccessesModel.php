<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AccessesModel extends Model
{
    protected $table = "Accesses";
    public $timestamps = false;

    public static function getAccessibleRoutes($groupId)
    {
        $accessibleRoutesId = array();
        $routes = self::where("groupId", "=", $groupId)->get();

        foreach ($routes as $route)
            $accessibleRoutesId[] = $route->routeId;

        return $accessibleRoutesId;
    }

    public static function isAccessibleRouteById($groupId, $routeId)
    {
        $accessibleRouteId = self::where("groupId", $groupId)
            ->where("routeId", $routeId)->value("id");              
        return isset($accessibleRouteId) ? true : false;
    }

    public static function grandAccessToRoute($groupId, $routeId){
        $route = new self();
        $route->groupId = $groupId;
        $route->routeId = $routeId;

        $route->save();

        return $route->id;
    }

    public static function revokeAccessToRoute($groupId, $routeId){
        self::where("groupId", "=", $groupId)
            ->where("routeId", "=", $routeId)
            ->delete();
    }
}
