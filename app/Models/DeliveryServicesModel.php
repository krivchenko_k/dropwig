<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryServicesModel extends Model
{
    protected $table = "DeliveryServices";
    public static $publicTableName = "DeliveryServices";

    public $timestamps = false;

    public static function getDeliveryServices(){
        return self::all();
    }

    public static function getDeliveryService($id){
        return self::find($id);
    }

    public static function isDeliveryServiceExist($name){
        $orderStatusId = self::where("name", "=", $name)->value("id");
        return isset($orderStatusId) ? true : false;
    }

    public static function addDeliveryService($name){
        $orderStatus = new self();
        $orderStatus->name = $name;
        $orderStatus->save();

        return $orderStatus;
    }

    public static function editDeliveryService($data){
        $orderStatus = self::find($data->id);
        $orderStatus->name = $data->name;
        $orderStatus->save();

        return $orderStatus;
    }

    public static function deleteDeliveryServices($deliveryServicesId){
        self::destroy($deliveryServicesId);
    }
}
