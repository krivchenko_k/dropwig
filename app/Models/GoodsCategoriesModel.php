<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsCategoriesModel extends Model
{
    protected $table = "GoodsCategories";
    public static $publicTableName = "GoodsCategories";

    public $timestamps = false;

    public static function getGoodsCategories(){
        return self::all();
    }

    public static function getGoodsCategory($id){
        return self::find($id);
    }

    public static function isGoodsCategoryExist($name){
        $goodsCategoryId = self::where("name", "=", $name)->value("id");
        return isset($goodsCategoryId) ? true : false;
    }

    public static function addGoodsCategory($data){
        $goodsCategory = new self();
        $goodsCategory->name = $data->name;
        $goodsCategory->save();

        return $goodsCategory;
    }

    public static function editGoodsCategory($data){
        $goodsCategory = self::find($data->id);
        $goodsCategory->name = $data->name;
        $goodsCategory->save();

        return $goodsCategory;
    }

    public static function deleteGoodsCategories($goodsCategorysId){
        self::destroy($goodsCategorysId);
    }

}
