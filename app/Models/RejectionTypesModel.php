<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RejectionTypesModel extends Model
{
    protected $table = "RejectionTypes";
    public static $publicTableName = "RejectionTypes";

    public $timestamps = false;

    public static function getRejectionTypes(){
        return self::all();
    }

    public static function getRejectionType($id){
        return self::find($id);
    }

    public static function isRejectionTypeExist($name){
        $goodsMeasureId = self::where("name", "=", $name)->value("id");
        return isset($goodsMeasureId) ? true : false;
    }

    public static function addRejectionType($data){
        $goodsMeasure = new self();
        $goodsMeasure->name = $data->name;
        $goodsMeasure->save();

        return $goodsMeasure;
    }

    public static function editRejectionType($data){
        $goodsMeasure = self::find($data->id);
        $goodsMeasure->name = $data->name;
        $goodsMeasure->save();

        return $goodsMeasure;
    }

    public static function deleteRejectionTypes($goodsMeasuresId){
        self::destroy($goodsMeasuresId);
    }
}
