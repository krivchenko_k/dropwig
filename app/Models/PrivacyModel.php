<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrivacyModel extends Model
{
    protected $table = "Privacy-policy";
    public static $publicTableName = "Privacy-policy";

    public $timestamps = false;

    public static function get()
    {
        return self::first();
    }

    public static function savePrivacy($data)
    {
        $agreement = self::first();
        if($agreement == false) {
            $agreement = new self();
            $agreement->text = $data->text;
            $agreement->save();
        }
        else {
            $agreement->text = $data->text;
            $agreement->save();
        }
    }
}