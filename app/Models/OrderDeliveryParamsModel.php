<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDeliveryParamsModel extends Model
{
    protected $table = "OrderDeliveryParams";
    public static $publicTableName = "OrderDeliveryParams";

    public $timestamps = false;

    public function deliveryService() {
        return $this->belongsTo('App\Models\DeliveryServicesModel', 'deliveryServiceId');
    }

    public function commonCity() {
        return $this->belongsTo('App\Models\CommonCitiesModel', 'commonCityId', 'id');
    }

    public function commonAddress() {
        return $this->belongsTo('App\Models\CommonAddressesModel', 'commonAddressId', 'id');
    }

    public function NPCity() {
        return $this->belongsTo('App\Models\NPCitiesModel', 'NPCityId', 'id');
    }

    public function NPWarehouse() {
        return $this->belongsTo('App\Models\NPWarehousesModel', 'NPWarehouseId', 'id');
    }

    public static function addNPDeliveryParams($orderData) {
        return self::insertGetId([
            'orderId' => $orderData->orderId, 'deliveryServiceId' => $orderData->deliveryServiceId,
            'NPCityId' => $orderData->orderNPCityId, 'NPWarehouseId' => $orderData->orderNPAddressId
        ]);
    }

    public static function addCommonDeliveryParams($orderData) {
        return self::insertGetId([
            'orderId' => $orderData->orderId, 'deliveryServiceId' => $orderData->deliveryServiceId,
            'commonCityId' => $orderData->commonCityId, 'commonAddressId' => $orderData->commonAddressId
        ]);
    }

    public static function updateNPDeliveryParams($orderData) {
        return self::where('orderId', '=', $orderData->orderId)->update([
            'deliveryServiceId' => $orderData->deliveryServiceId, 'NPCityId' => $orderData->orderNPCityId, 'NPWarehouseId' => $orderData->orderNPAddressId,
            'commonCityId' => null, 'commonAddressId' => null
        ]);
    }

    public static function updateCommonDeliveryParams($orderData) {
        return self::where('orderId', '=', $orderData->orderId)->update([
            'deliveryServiceId' => $orderData->deliveryServiceId, 'NPCityId' => null, 'NPWarehouseId' => null,
            'commonCityId' => $orderData->commonCityId, 'commonAddressId' => $orderData->commonAddressId ]);
    }


    public static function parametricDelete($params){
        return self::where($params)->delete();
    }
    
}
