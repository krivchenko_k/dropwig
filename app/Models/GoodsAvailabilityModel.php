<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsAvailabilityModel extends Model
{
    protected $table = "GoodsAvailability";
    public static $publicTableName = "GoodsAvailability";
    public $timestamps = false;

    public function availabilityType(){
        return $this->hasOne("App\Models\AvailabilityTypesModel", "id", "availabilityTypeId");
    }
}
