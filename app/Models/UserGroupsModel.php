<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserGroupsModel extends Model
{
    protected $table = "UserGroups";
    public static $tableName = "UserGroups";

    public $timestamps = false;

    public static function getGroups(){
        return self::all();
    }

    public static function getGroup($id){
        return self::find($id);
    }

    public static function isUserGroupExist($name){
        $userGroupId = self::where("name", "=", $name)->value("id");
        return isset($userGroupId) ? true : false;
    }

    public static function addUserGroup($name){
        $userGroup = new self();
        $userGroup->name = $name;
        $userGroup->save();

        return $userGroup;
    }

    public static function editUserGroup($data){
        $userGroup = self::find($data->id);
        $userGroup->name = $data->name;
        $userGroup->save();

        return $userGroup;
    }

    public static function deleteUserGroups($userGroupsId){
        self::destroy($userGroupsId);
    }
}
