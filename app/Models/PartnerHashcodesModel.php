<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerHashcodesModel extends Model
{
    protected $table = "PartnerHashcodes";
    public static $publicTableName = 'PartnerHashcodes';
}
