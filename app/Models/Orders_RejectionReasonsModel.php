<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders_RejectionReasonsModel extends Model
{
    protected $table = 'Orders_RejectionReasons';
    public static $publicTableName = 'Orders_RejectionReasons';

    public $timestamps = false;


    public function rejectionReason() {
        return $this->belongsTo('App\Models\RejectionReasonsModel', 'rejectionReasonId', 'id');
    }

    
    public static function add($orderId, $rejectionReasonId = null, $text = null) {
        if(!isset($orderId) || (!isset($rejectionReasonId) && !isset($text))) {
            return false;
        }
        return self::insert([ 'rejectionReasonId' => $rejectionReasonId, 'orderId' => $orderId, 'text' => $text ]);
    }
}
