<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentRequests_RejectionReasonsModel extends Model
{
    protected $table = 'PaymentRequests_RejectionReasons';
    public static $publicTableName = 'PaymentRequests_RejectionReasons';

    public $timestamps = false;


    public function rejectionReason() {
        return $this->belongsTo('App\Models\RejectionReasonsModel', 'rejectionReasonId', 'id');
    }


    public static function add($paymentRequestId, $rejectionReasonId = null, $text = null) {
        if(!isset($paymentRequestId) || (!isset($rejectionReasonId) && !isset($text))) {
            return false;
        }
        return self::insert([ 'rejectionReasonId' => $rejectionReasonId, 'paymentRequestId' => $paymentRequestId, 'text' => $text ]);
    }
}
