<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class NewsModel extends Model
{
    protected $table = "News";
    public static $publicTableName = "News";

    public $timestamps = true;

    public static function getNewsList(){
        return self::get();
    }

    public static function getNews($id){
        return self::find($id);
    }

    public static function isNewsExist($text){
        $newId = self::where("text", "=", $text)->value("id");
        return isset($newId) ? true : false;
    }

    public static function addNews($data){
        $news = new self();
        $news->title = $data->title;
        $news->text = $data->text;
        $news->save();

        return $news->id;
    }

    public static function editNews($data){
        $news = self::find($data->id);
        $news->title = $data->title;
        $news->text = $data->text;
        $news->save();
    }

    public static function deleteNews($newsId){
        self::destroy($newsId);
    }
}
