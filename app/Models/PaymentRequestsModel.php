<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentRequestsModel extends Model
{
    protected $table = "PaymentRequests";
    public $timestamps = false;

    public static $publicTableName = "PaymentRequests";

    public function partnerProfile()
    {
        return $this->belongsTo('App\Models\PartnerProfilesModel', 'partnerId', 'partnerId');
    }

    public function rejections()
    {
        return $this->hasOne('App\Models\PaymentRequests_RejectionReasonsModel', 'paymentRequestId');
    }

    public static function get($id = null, $queryParams = null)
    {
        if (!empty($id)) {
            if (is_array($id)) {
                if (!empty($queryParams))
                    return self::whereIn('id', $id)->where($queryParams)->orderBy('isActive', 'DESC')->orderBy('created_at', 'DESC')->get();
                else
                    return self::whereIn('id', $id)->orderBy('isActive', 'DESC')->orderBy('created_at', 'DESC')->get();
            } else {
                if (!empty($queryParams))
                    return self::where('id', $id)->where($queryParams)->orderBy('isActive', 'DESC')->orderBy('created_at', 'DESC')->first();
                else
                    return self::where('id', $id)->orderBy('isActive', 'DESC')->orderBy('created_at', 'DESC')->first();
            }
        } else {
            if (!empty($queryParams))
                return self::where($queryParams)->orderBy('isActive', 'DESC')->orderBy('created_at', 'DESC')->get();
            else
                return self::orderBy('isActive', 'DESC')->orderBy('created_at', 'DESC')->get();
        }

    }


    public static function activate($id)
    {
        return self::where('id', $id)->update(['isActive' => 1]);
    }

    public static function deactivate($id)
    {
        return self::where('id', $id)->update(['isActive' => 0]);
    }

    public static function erase($idArray)
    {
        return self::destroy($idArray);
    }

    public static function getActiveRequestsCount()
    {
        return self::where('isActive', 1)->count();
    }
}
