<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RejectionReasonsModel extends Model
{
    protected $table = "RejectionReasons";
    public static $publicTableName = "RejectionReasons";
    public $timestamps = false;

    public function rejectionType(){
        return $this->belongsTo("App\Models\RejectionTypesModel", "typeId");
    }

    public static function getRejectionReasons() {
        return self::all();
    }

    public static function getRejectionReason($rejectionReasonsId){
        return self::find($rejectionReasonsId);
    }

    public static function isRejectionReasonExist($text){
        $RejectionReasonId = self::where("text", "=", $text)->value("id");
        return isset($RejectionReasonId) ? true : false;
    }

    public static function addRejectionReason($data){
        $RejectionReason = new self();
        $RejectionReason->typeId = $data->typeId;
        $RejectionReason->text = $data->text;
        $RejectionReason->save();

        return $RejectionReason;
    }

    public static function editRejectionReason($data){
        $RejectionReason = self::find($data->id);
        $RejectionReason->typeId = $data->typeId;
        $RejectionReason->text = $data->text;
        $RejectionReason->save();

        return $RejectionReason;
    }

    public static function deleteRejectionReasons($RejectionReasonsId){
        self::destroy($RejectionReasonsId);
    }

    public static function getRejectionReasonsByType($typeId) {
        return self::where("typeId", "=", $typeId)->get();
    }
}
