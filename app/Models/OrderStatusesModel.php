<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStatusesModel extends Model
{
    protected $table = "OrderStatuses";
    public static $publicTableName = "OrderStatuses";

    public $timestamps = false;

    public static function get($id = 0){
        if (!$id)
            return self::all();
        else
            return self::find($id);

    }

    public static function isOrderStatusExist($name){
        $orderStatusId = self::where("name", "=", $name)->value("id");
        return isset($orderStatusId) ? true : false;
    }

    public static function addOrderStatus($data){
        $orderStatus = new self();
        $orderStatus->name = $data->name;
        $orderStatus->color = $data->color;
        $orderStatus->save();
    }

    public static function editOrderStatus($data){
        $orderStatus = self::find($data->id);
        $orderStatus->name = $data->name;
        $orderStatus->color = $data->color;
        $orderStatus->save();
    }

    public static function deleteOrderStatuses($orderStatusesId){
        self::destroy($orderStatusesId);
    }
}
