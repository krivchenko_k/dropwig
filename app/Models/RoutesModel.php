<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RoutesModel
 * @package App\Models
 */
class RoutesModel extends Model
{
    /**
     * @var string
     */
    protected $table = "Routes";
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getRoutes(){
        return self::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getRoute($id){
        return RoutesModel::find($id);
    }

    /**
     * @param $route
     * @return bool
     */
    public static function isRouteExist($route){
        $route = self::where("route", "=", $route)->first();
        return isset($route) ? true : false;
    }

    /**
     * @param $route
     * @return mixed
     */
    public static function getRouteId($route){
        return self::where("route", "=", $route)->value("id");
    }

    /**
     * @param $name
     * @param $route
     * @return mixed
     */
    public static function addRoute($name, $route){
        $newRoute = new self();
        $newRoute->name = $name;
        $newRoute->route = $route;
        $newRoute->save();

        return $newRoute->id;
    }

    /**
     * @param $data
     */
    public static function editRoute($data){
        $route = self::find($data->id);

        $route->name = $data->name;
        $route->route = $data->route;
        $route->save();
    }

    /**
     * @param $routesId
     */
    public static function deleteRoutes($routesId){
        self::destroy($routesId);
    }

}
