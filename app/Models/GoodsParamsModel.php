<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsParamsModel extends Model
{
    protected $table = "GoodsParams";
    public static  $publicTableName = "GoodsParams";
    public $timestamps = true;

    public function measure(){
        return $this->belongsTo("App\Models\GoodsMeasuresModel", "measureId");
    }
}
