<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class StaffModel extends Authenticatable
{
    protected $table = "Staff";
    public static $publicTableName = "Staff";

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile()
    {
        return $this->hasOne('App\Models\StaffProfilesModel', "employeeId");
    }

    public function group(){
        return $this->belongsTo('App\Models\UserGroupsModel', "groupId");
    }

    public static function get($id = 0)
    {
        if (!$id) {
            return self::with("profile")->get();
        }
        else{
            return self::where("id", $id)->with("profile")->first();
        }
    }

    public static function getEmployeeByLoginPass($login, $password)
    {
        return self::where("login", "=", $login)->where("password", "=", md5($password))->first();
    }

    public static function isLoginExist($login)
    {
        $id = self::where("login", "=", $login)->value("id");
        return !empty($id) ? true : false;
    }

    public static function add($data)
    {
        $employee = new self();
        $employee->login = $data->login;
        $employee->password = md5($data->password);
        $employee->groupId = $data->groupId;
        $employee->save();

        return $employee->id;
    }

    public static function edit($data)
    {
        $employee = self::find($data->id);
//        $employee->login = $data->login;
        if (!empty($data->password))
            $employee->password = md5($data->password);
        $employee->groupId = $data->groupId;

        $employee->save();

        return $employee;
    }


    public static function remove($staff)
    {
        self::destroy($staff);
    }
}
