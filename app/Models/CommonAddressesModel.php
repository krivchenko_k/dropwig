<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommonAddressesModel extends Model
{
    protected $table = "CommonAddresses";
    public static $publicTableName = 'CommonAddresses';

    public $timestamps = false;

    public static function getAddresses() {
        return self::all();
    }

    public static function getAddressById($addressId) {
        return self::where('id', '=', $addressId)->first();
    }

    public static function getAddressIdByName($addressName) {
        $selectedRow = self::where('name', 'like', $addressName)->first();
        if(!isset($selectedRow) || empty($selectedRow)) {
            return -1;
        }
        return $selectedRow->id;
    }

    public static function addAddress($addressName) {
        return self::insertGetId([ 'name' => $addressName ]);
    }

    public static function isExist($addressName) {
        $selectedRowsCount = self::where('name', 'like', $addressName)->count();
        return ($selectedRowsCount > 0) ? true : false;
    }

}
