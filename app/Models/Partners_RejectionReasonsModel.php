<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Partners_RejectionReasonsModel extends Model
{
    protected $table = 'Partners_RejectionReasons';
    public static $publicTableName = 'Partners_RejectionReasons';

    public $timestamps = false;


    public function rejectionReason() {
        return $this->belongsTo('App\Models\RejectionReasonsModel', 'rejectionReasonId', 'id');
    }


    public static function add($partnerId, $rejectionReasonId = null, $text = null) {
        if(!isset($partnerId) || (!isset($rejectionReasonId) && !isset($text))) {           
            return false;
        }
        return self::insert([ 'rejectionReasonId' => $rejectionReasonId, 'partnerId' => $partnerId, 'text' => $text ]);
    }

}
