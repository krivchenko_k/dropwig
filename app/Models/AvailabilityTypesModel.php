<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AvailabilityTypesModel extends Model
{
    protected $table = "AvailabilityTypes";
    public static $publicTableName = "AvailabilityTypes";

    public $timestamps = false;

    public static function getAvailabilityTypes(){
        return self::all();
    }

    public static function getAvailabilityType($id){
        return self::find($id);
    }

    public static function isAvailabilityTypeExist($name){
        $availabilityTypeId = self::where("name", "=", $name)->value("id");
        return isset($availabilityTypeId) ? true : false;
    }

    public static function addAvailabilityType($data){
        $availabilityType = new self();
        $availabilityType->name = $data->name;
        $availabilityType->save();

        return $availabilityType;
    }

    public static function editAvailabilityType($data){
        $availabilityType = self::find($data->id);
        $availabilityType->name = $data->name;
        $availabilityType->save();

        return $availabilityType;
    }

    public static function deleteAvailabilityTypes($availabilityTypesId){
        self::destroy($availabilityTypesId);
    }

}
