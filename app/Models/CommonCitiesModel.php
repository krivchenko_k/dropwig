<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommonCitiesModel extends Model
{
    protected $table = "CommonCities";
    public static $publicTableName = 'CommonCities';
    public $timestamps = false;

    public static function getCities() {
        return self::all();
    }

    public static function addCity($cityName) {
        return self::insertGetId([ 'name' => $cityName ]);
    }

    public static function getCityById($cityId) {
        return self::where('id', '=', $cityId)->first();
    }

    public static function getCityIdByName($cityName) {
        $selectedRow = self::where('name', 'like', $cityName)->first();
        if(!isset($selectedRow) || empty($selectedRow)) {
            return -1;
        }
        return $selectedRow->id;
    }

    public static function isExist($cityName) {
        $selectedRowsCount = self::where('name', 'like', $cityName)->count();
        return ($selectedRowsCount > 0) ? true : false;
    }

}
