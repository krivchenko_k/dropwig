<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetailsModel extends Model
{
    protected $table = "OrderDetails";
    public static $publicTableName = 'OrderDetails';

    public $timestamps = false;

    public static function addOrderDetails($orderData) {
        return self::insert([ 'orderId' => $orderData->orderId, 'comment' => $orderData->orderComment, "ip" => $_SERVER["REMOTE_ADDR"] ]);
    }

    public static function updateOrderDetails($orderData) {
        return self::where('orderId', '=', $orderData->orderId)->update([ 'comment' => $orderData->orderComment ]);
    }

}
