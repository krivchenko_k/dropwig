<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactsModel extends Model
{
    protected $table = "Contacts";
    public static $publicTableName = "Contacts";

    public $timestamps = false;

    public static function getContacts(){
        return self::all();
    }

    public static function getContact($id){
        return self::find($id);
    }

    public static function isContactExist($text){
        $contactId = self::where("text", "=", $text)->value("id");
        return isset($contactId) ? true : false;
    }

    public static function addContact($data){
        $contact = new self();
        $contact->text = $data->text;
        $contact->save();

        return $contact;
    }

    public static function editContact($data){
        $contact = self::find($data->id);
        $contact->text = $data->text;
        $contact->save();

        return $contact;
    }

    public static function deleteContacts($contactsId){
        self::destroy($contactsId);
    }
}