<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentTypesModel extends Model
{
    protected $table = "PaymentTypes";
    public static $publicTableName = "PaymentTypes";

    public $timestamps = false;

    public static function getPaymentTypes(){
        return self::all();
    }

    public static function getPaymentType($id){
        return self::find($id);
    }

    public static function isPaymentTypeExist($name){
        $paymentTypeId = self::where("name", "=", $name)->value("id");
        return isset($paymentTypeId) ? true : false;
    }

    public static function addPaymentType($data){
        $paymentType = new self();
        $paymentType->name = $data->name;
        $paymentType->save();

        return $paymentType;
    }

    public static function editPaymentType($data){
        $paymentType = self::find($data->id);
        $paymentType->name = $data->name;
        $paymentType->save();

        return $paymentType;
    }

    public static function deletePaymentTypes($paymentTypesId){
        self::destroy($paymentTypesId);
    }
}
