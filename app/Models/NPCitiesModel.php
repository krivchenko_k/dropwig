<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class NPCitiesModel extends Model
{
    protected $table = "NPCities";
    public static $publicTableName = "NPCities";
    public $timestamps = true;

    public static function getCities() {
        return self::all();
    }

    public static function getCityById($cityId) {
        return self::where('id', '=', $cityId)->first();
    }

    public static function getCityByRef($cityRef) {
        return self::where('ref', '=', $cityRef)->first();
    }

    public static function insertCity($data) {
        $cityId = self::insertGetId([
            'ref' => $data->Ref, 'name' => $data->DescriptionRu,
            'isActive' => 1, 'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
        return $cityId;
    }

    public static function updateCity($data) {

        $affectedRows = self::where('ref', '=', $data->Ref)->update([
            'name' => $data->DescriptionRu, 'isActive' => 1
        ]);
        return ($affectedRows >= 1) ? true : false;
    }

    public static function updateActivity() {
        DB::update('UPDATE NPCities SET isActive = 0 WHERE DATE(updated_at) < CURRENT_DATE');
    }

}
