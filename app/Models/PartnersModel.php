<?php

namespace App\Models;

use App\Http\Controllers\OrdersController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class PartnersModel
 * @package App\Models
 */
class PartnersModel extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'Partners';
    /**
     * @var string
     */
    public static $publicTableName = 'Partners';
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne("App\Models\PartnerProfilesModel", "partnerId");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function hashcode()
    {
        return $this->hasOne("App\Models\PartnerHashcodesModel", "partnerId");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo("App\Models\UserStatusesModel", "statusId");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo("App\Models\UserGroupsModel", "groupId");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function order(){
        return $this->hasMany('App\Models\OrdersModel', "partnerId");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders(){
        return $this->hasMany('App\Models\OrdersModel', "partnerId");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function balance(){
        return $this->hasOne('App\Models\PartnerBalancesModel', "partnerId");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payment(){
        return $this->hasMany('App\Models\PaymentHistoryModel', "partnerId");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rejections() {
        return $this->hasMany('App\Models\Partners_RejectionReasonsModel', 'partnerId');
    }

    /**
     * @param $login
     * @param $password
     * @return mixed
     */
    public static function getPartnerByLoginPass($login, $password)
    {
        return self::where("login", "=", $login)->where("password", "=", md5($password))->first();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function get($id = 0)
    {
        if (!$id)
            return self::where('statusId', '>=', 3)->get();
        else
            return self::find($id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public static function getWithPayout(){
        return self::with(["payment" => function($query){
            $query->where("paymentOperationId", 4);
        }])->get();
    }

    /**
     * @return mixed
     */
    public static function getUnregisteredPartners()
    {
        return self::where("statusId", 2)->get();
    }

    /**
     * @param $id
     * @param $statusId
     */
    public static function setStatus($id, $statusId)
    {
        $partner = self::find($id);
        $partner->statusId = $statusId;
        $partner->save();
    }

    /**
     * @param $data
     */
    public static function add($data)
    {
        $partner = new self();
        $partner->login = $data->login;
        $partner->password = md5($data->password);
        if (!empty($data->statusId)) {
            $partner->statusId = $data->statusId;
        }
        else {
            $partner->statusId = 3;//$data->statusId;
        }
        $partner->groupId = 3;

        $partner->save();

        $partnerProfile = new PartnerProfilesModel();

        $partnerProfile->partnerId = $partner->id;
        $partnerProfile->firstName = $data->firstName;
        $partnerProfile->lastName = $data->lastName;
        $partnerProfile->nickname = $data->nickname;
        $partnerProfile->phone = $data->phone;
        $partnerProfile->email = $data->email;
        $partnerProfile->contacts = $data->contacts;
        $partnerProfile->card = $data->card;
        $partnerProfile->cardOwner = $data->cardOwner;
        $partnerProfile->isCardOwnerSelf = $data->isCardOwnerSelf;
        $partnerProfile->save();

        $partnerBalance = new PartnerBalancesModel();
        $partnerBalance->partnerId = $partner->id;
        $partnerBalance->amount = 0;
        $partnerBalance->save();

        return $partner->id;

    }

    /**
     * @param $idList
     */
    public static function remove($idList)
    {
        self::whereIn("id", $idList)->delete();
    }

    /**
     * @param $login
     * @return mixed
     */
    public static function getPartnerByLogin($login)
    {
        return self::where("login", $login)->first();
    }

    /**
     * @param $partnerId
     * @param $statusId
     */
    public static function setPartnerStatus($partnerId, $statusId)
    {
        $partner = self::find($partnerId);
        $partner->statusId = $statusId;
        $partner->save();
    }

    /**
     * @return mixed
     */
    public static function getOnlineCount()
    {
        $queryParams["period"] = date("Y-m-d H:i:s", time() - 5 * 60);
        return DB::select("SELECT COUNT(P.id) as online 
                FROM Partners AS P
                LEFT JOIN PartnerProfiles AS PP ON P.id = PP.partnerId
                WHERE PP.lastActivity >= :period
                AND statusId >= 3
                AND P.deleted_at IS NULL",
            $queryParams)[0]->online;
    }

    /**
     * @return mixed
     */
    public static function getNewPerWeekCount()
    {
        $queryParams["period"] = date('Y-m-d', time() - (date('N') - 1) * 24 * 60 * 60);
        return DB::select("SELECT COUNT(P.id) as newPerWeek 
                FROM Partners AS P
                LEFT JOIN PartnerProfiles AS PP ON P.id = PP.partnerId
                WHERE PP.created_at >= :period
                AND statusId >= 3
                AND P.deleted_at IS NULL",
            $queryParams)[0]->newPerWeek;
    }

    /**
     * @return mixed
     */
    public static function getActivePerWeekCount()
    {
        $queryParams["period"] = date('Y-m-d', time() - (date('N') - 1) * 24 * 60 * 60);
        return DB::select("SELECT COUNT(P.id) as activePerWeek 
                FROM Partners AS P
                LEFT JOIN PartnerProfiles AS PP ON P.id = PP.partnerId
                WHERE PP.lastActivity >= :period
                AND statusId >= 3
                AND P.deleted_at IS NULL",
            $queryParams)[0]->activePerWeek;
    }

    /**
     * @return mixed
     */
    public static function getParthersCreatedOrderPerWeekCount()
    {
        $queryParams["dateFrom"] = date('Y-m-d', time() - (date('N') - 1) * 24 * 60 * 60);
        return DB::select("SELECT COUNT(P.id) as hasOrdersPerWeek
                From Partners AS P
                WHERE P.statusId >= 3 
                AND P.deleted_at IS NULL 
                AND (SELECT Count(O.id) 
                FROM Orders AS O  
                WHERE P.id = O.partnerId 
                AND O.created_at >= :dateFrom
                AND O.deleted_at IS NULL) > 0",
            $queryParams)[0]->hasOrdersPerWeek;
    }

    /**
     * @param $login
     * @return mixed
     */
    public static function isLoginExist($login){
        return self::where("login", $login)->value("id");
    }

    /**
     * @param $statusId
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getPartnersByStatus($statusId) {
        if(isset($statusId) && !empty($statusId)) {
            return self::where('statusId', $statusId)->get();
        }
        else {
            return self::all();
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public static function getPartnersWithOrders(){
        return self::with("orders")->get();
    }
}