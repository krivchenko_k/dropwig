<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders_PaymentRequestsModel extends Model
{
    protected $table = "Orders_PaymentRequests";
    public $timestamps = false;

    public static function get($id) {
        if(isset($id)) {
            return self::where('id', $id)->first();
        }
        else {
            return self::all();
        }
    }

    public static function getOrdersByRequest($requestId) {
        return self::where('paymentRequestId', $requestId)->pluck('orderId')->all();
    }

    public static function add($data) {
        return self::insertGetId([ 'paymentRequestId' => $data->paymentRequestId, 'orderId' => $data->orderId ]);
    }

    public static function deleteIfExists($orderIdArray) {
        $selectedRowsCount = self::whereIn('orderId', $orderIdArray)->count();
        if($selectedRowsCount) {
            return self::whereIn('orderId', $orderIdArray)->delete();
        }
        else {
            return 0;
        }
    }

    public static function erase($idArray) {
        return self::destroy($idArray);
    }

    public static  function eraseByRequest($requestId) {
        return self::where('paymentRequestId', $requestId)->delete();
    }

}
