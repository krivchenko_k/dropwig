<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FAQModel extends Model
{
    protected $table = "FAQ";
    public static $publicTableName = "FAQ";

    public $timestamps = false;

    public static function getFAQList(){
        return self::all();
    }

    public static function getFAQ($id){
        return self::find($id);
    }

    public static function isQuestionExist($question){
        $contactId = self::where("question", "=", $question)->value("id");
        return isset($contactId) ? true : false;
    }

    public static function addFAQ($data){
        $contact = new self();
        $contact->question = $data->question;
        $contact->answer = $data->answer;
        $contact->save();

        return $contact;
    }

    public static function editFAQ($data){
        $contact = self::find($data->id);
        $contact->question = $data->question;
        $contact->answer = $data->answer;
        $contact->save();

        return $contact;
    }

    public static function deleteFAQList($contactsId){
        self::destroy($contactsId);
    }
    
}
