<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientsModel extends Model
{
    protected $table = "Clients";
    public static $publicTableName = 'Clients';

    public $timestamps = true;

    public static function getClients() {
        return self::all();
    }

    public static function getClientById($clientId) {
        return self::where('id', '=', $clientId)->first();
    }

    public static function isExist($clientFirstName, $clientMiddleName, $clientLastName) {
        $selectedRowsCount = self::where('firstName', 'like', $clientFirstName)
            ->where('lastName', 'like', $clientLastName)
            ->count();
        return ($selectedRowsCount > 0) ? true : false;
    }

    public static function getClientIdByName($clientFirstName, $clientMiddleName, $clientLastName) {
        $clientData = self::where('firstName', 'like', $clientFirstName)
                            ->where('lastName', 'like', $clientLastName)
                            ->first();
        if(!isset($clientData) || empty($clientData)) {
            return -1;
        }
        return $clientData->id;
    }

    public static function addClient($clientData) {
        return self::insertGetId([ 'firstName' => $clientData->firstName, 'middleName' => $clientData->middleName, 'lastName' => $clientData->lastName ]);
    }

    public static function editClient($clientId, $clientData){
        return self::where("id", $clientId)->update([ 'firstName' => $clientData->firstName, 'middleName' => $clientData->middleName, 'lastName' => $clientData->lastName ]);
    }

}
