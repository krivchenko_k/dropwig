<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaffProfilesModel extends Model
{
    protected $table = "StaffProfiles";
    public static $publicTableName = "StaffProfiles";

    public static function add($data)
    {
        $profile = new self();
        $profile->employeeId = $data->id;
        $profile->firstName = $data->firstName;
        $profile->lastName = $data->lastName;
        $profile->phone = $data->phone;

        $profile->save();
    }

    public static function edit($data)
    {
        $profile = self::where("employeeId", $data->id)->first();
        $profile->firstName = $data->firstName;
        $profile->lastName = $data->lastName;
        $profile->phone = $data->phone;

        $profile->save();

        return $profile;
    }
}
