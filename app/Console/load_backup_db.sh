#!/bin/sh
cd /home/admin/web/dropshipping.city/public_html/backup/to_load

sudo chown -R 1000:1000 /home/admin/web/dropshipping.city/public_html/backup/to_load/DB_.sql.tar.gz
mv /home/admin/web/dropshipping.city/public_html/backup/to_load/DB_.sql.tar.gz /home/admin/web/dropshipping.city/public_html/backup/to_load/DB_`date +%d-%m-%Y`.sql.tar.gz

ftp -n 185.69.152.146 <<END_SCRIPT
quote USER admin
quote PASS admin
cd /backup_drop/
put /home/admin/web/dropshipping.city/public_html/backup/to_load/DB_`date +%d-%m-%Y`.sql.tar.gz DB_`date +%d-%m-%Y`.sql.tar.gz
quit
END_SCRIPT

mv /home/admin/web/dropshipping.city/public_html/backup/to_load/DB_`date +%d-%m-%Y`.sql.tar.gz /home/admin/web/dropshipping.city/public_html/backup/DB_`date +%d-%m-%Y`.sql.tar.gz