#!/bin/sh
cd /home/admin/web/dropshipping.city/public_html/backup/to_load

sudo chown -R 1000:1000 /home/admin/web/dropshipping.city/public_html/backup/to_load/FILES_.tar.gz
mv /home/admin/web/dropshipping.city/public_html/backup/to_load/FILES_.tar.gz /home/admin/web/dropshipping.city/public_html/backup/to_load/FILES_`date +%d-%m-%Y`.tar.gz

ftp -n 185.69.152.146 <<END_SCRIPT
quote USER admin
quote PASS admin
cd /backup_drop/
put /home/admin/web/dropshipping.city/public_html/backup/to_load/FILES_`date +%d-%m-%Y`.tar.gz FILES_`date +%d-%m-%Y`.tar.gz
quit
END_SCRIPT

mv /home/admin/web/dropshipping.city/public_html/backup/to_load/FILES_`date +%d-%m-%Y`.tar.gz /home/admin/web/dropshipping.city/public_html/backup/FILES_`date +%d-%m-%Y`.tar.gz