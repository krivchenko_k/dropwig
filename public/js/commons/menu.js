$(document).ready(function () {
    $("#contacts-menu").click(function () {
        $.ajax({
            url: "/login/ContactsPopup",
            type: "GET",
            success: function (popupContent) {
                if (!isValidJSON(popupContent)) {
                    var popupSelector = "contacts-popup";
                    showCommonPopup(popupSelector, "Контакты", popupContent);
                }
            }
        });
    });

    $(".profile-btn").click(function () {
        $.ajax({
            url: "/profile",
            type: "GET",
            success: function (popupContent) {
                if (!isValidJSON(popupContent)) {
                    var popupSelector = "profile-popup";
                    showCommonPopup(popupSelector, "Профиль", popupContent);
                }

            }
        });
    });

    $(".contacts-btn").click(function () {
        $.ajax({
            url: "/contacts",
            type: "GET",
            success: function (popupContent) {
                if (!isValidJSON(popupContent)) {
                    var popupSelector = "contacts-popup";
                    showCommonPopup(popupSelector, "Контакты", popupContent);
                }

            }
        });
    });

})

function checkNeedToCollapseMenu() {
    if (getCookie("collapseMenu") == "true"){
        $(".sidebar-menu").addClass("collapsed");
    }
}

checkNeedToCollapseMenu();