$(document).ready(function () {
    setTimeout(function () {
        var es = new EventSource("/rt-balance");
        es.addEventListener("message", function (e) {
            var data = JSON.parse(e.data);
            $('#balance .loader').remove();
            $('#balance').find("div").show().find("span").html(data.balance);
        }, false);
    }, 1000);


    $(".toogle-menu").click(function () {
        if ($(".sidebar-menu").hasClass("collapsed")) {
            var opt = {};
            opt.date = new Date(2099, 11, 31);
            setCookie("collapseMenu", "true", opt);
        }
        else {
            deleteCookie("collapseMenu");
        }
    });
})