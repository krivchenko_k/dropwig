$(document).ready(function () {
    var notifications = {};
    var notificationOptions = {
        "closeButton": true,
        "debug": false,
        "onclick": null,
        "showDuration": "200",
        "hideDuration": "200",
        "timeOut": "10000",
        "extendedTimeOut": "10000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    setTimeout(function(){
        var es = new EventSource("/rt-notifications");
        es.addEventListener("message", function(e) {
            var data = JSON.parse(e.data);
            $('.reg-requests-count').removeClass("loader").find("div").html(data.registrationRequestsCount);
            $('.order-requests-count').removeClass("loader").find("div").html(data.moderationRequestsCount);
            $('.payout-requests-count').removeClass("loader").find("div").html(data.paymentRequestsCount);

            if(isset(notifications.registrationRequestsCount)) {
                if((data.registrationRequestsCount - notifications.registrationRequestsCount) > 0) {
                    toastr.error('Новых заявок на регистрацию: <b>' + (data.registrationRequestsCount - notifications.registrationRequestsCount) + '</b>', null, notificationOptions);
                }
            }
            if(isset(notifications.moderationRequestsCount)) {
                if((data.moderationRequestsCount - notifications.moderationRequestsCount) > 0) {
                    toastr.info('Новых заявок на модерацию: <b>' + (data.moderationRequestsCount - notifications.moderationRequestsCount) + '</b>', null, notificationOptions);
                }
            }
            if(isset(notifications.paymentRequestsCount)) {
                if((data.paymentRequestsCount - notifications.paymentRequestsCount) > 0) {
                    toastr.success('Новых заявок на выплату: <b>' + (data.paymentRequestsCount - notifications.paymentRequestsCount) + '</b>', null, notificationOptions);
                }
            }
            notifications.registrationRequestsCount = data.registrationRequestsCount;
            notifications.moderationRequestsCount = data.moderationRequestsCount;
            notifications.paymentRequestsCount = data.paymentRequestsCount;

        }, false);
    }, 1000);


    $(".toogle-menu").click(function () {
        if ($(".sidebar-menu").hasClass("collapsed")){
            console.log("set")
            var opt = {};
            opt.date = new Date(2099, 11, 31);
            setCookie("collapseMenu", "true", opt);
        }
        else{
            console.log("set")
            deleteCookie("collapseMenu");
        }
    });
})