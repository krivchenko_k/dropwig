var selectedElems = {
    "idArray": []
}

function getAvailabilityTypesPopup(selectedElemId, popupTitle) {
    $.ajax({
        url: "/settings/availability-types/" + selectedElemId,
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "availability-type-popup";
            showCommonPopup(popupSelector, popupTitle, popupContent);
            setPopupListeners();
        }
    })
}

function showAddAvailabilityTypePopup() {
    var popupTitle = "Добавление типа наличия товара";
    getAvailabilityTypesPopup(0, popupTitle);
}

function showEditAvailabilityTypePopup(selectedElemId) {
    var popupTitle = "Редактирование типа наличия товара";
    getAvailabilityTypesPopup(selectedElemId, popupTitle);
}


function setListenersToAvailabilityTypesTable() {
    $("tbody .cbr").change(function () {
        var tr = $(this).closest("tr");
        var selectedElemId = $(tr).data("availability-type-id");
        changeCbxState(tr, selectedElems, selectedElemId);
    });

    // table.find(".dblclickable").dblclick(function () {
    //     var selectedElemId = $(this).closest("tr").data("availability-type-id");
    //     showEditAvailabilityTypePopup(selectedElemId);
    // });

    table.find(".btn-edit").click(function () {
        var selectedElemId = $(this).closest("tr").data("availability-type-id");
        showEditAvailabilityTypePopup(selectedElemId);
    });

    cbr_replace();
}

function updateAvailabilityTypesTable() {
    $.ajax({
        url: "/settings/availability-types/update",
        success: function (response) {
            if (isValidJSON(response)) {
                response = JSON.parse(response);
                if (!response.success) {
                    for (var i = 0; i < response.errors.length; i++) {
                        toastr.error(response.errors[i], null, commonNotificationOptions);
                    }
                }
                return
            }
            clearDatatable(table);
            table.find("tbody").append(response);
            setListenersToAvailabilityTypesTable();
            initializeDatatable(table);
        }
    });
    cancelSelection(table);
    selectedElems.idArray = [];
    deleteBtn.hide();
}

function deleteAvailabilityTypes() {
    if (confirm('Удаляем выделенные элементы 131?')) {
        $.ajax({
            url: "/settings/availability-types",
            type: "DELETE",
            data: {
                "availabilityTypesId": JSON.stringify(selectedElems.idArray),
            },
            success: function (response) {
                response = JSON.parse(response);
                if (response.success)
                    updateAvailabilityTypesTable();
            }
        });
    }
}


function addAvailabilityType(name) {
    $.ajax({
        url: "/settings/availability-types",
        type: "POST",
        data: {
            "name": name
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                updateAvailabilityTypesTable();
            else {
                var message = "";
                for (var i = 0; i < response.errors.length; i++) {
                    message += response.errors[i] + "\n";
                }
                alert(message);
            }
        }
    });
}


function editAvailabilityType(id, name) {
    $.ajax({
        url: "/settings/availability-types",
        type: "PUT",
        data: {
            "id": id,
            "name": name,
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                updateAvailabilityTypesTable();
        }
    });
}


function setPopupListeners() {
    $(".btn-save").click(function () {
        var id = $(this).data("availability-type-id");
        var name = $(".availability-type-name-inp").val();

        if (!id)
            addAvailabilityType(name);
        else
            editAvailabilityType(id, name);
        $(".popup-frame").hide();
    });

    $(".btn-close-popup").click(function () {
        $(".popup-frame").hide();
    });
}

var table, addBtn, deleteBtn;

$(document).ready(function () {
    table = $(".availability-types-table");
    addBtn = $(".add-availability-type-btn");
    deleteBtn = $(".delete-availability-types-btn");

    addBtn.click(function () {
        showAddAvailabilityTypePopup();
    });

    deleteBtn.click(function () {
        deleteAvailabilityTypes();
    });

    setListenersToAvailabilityTypesTable();

    setSelectAllCbxListener(table);

    initializeDatatable(table);
})