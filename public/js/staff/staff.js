var selectedElems = {
    "idArray" : []
}

function getStaffPopup(staffId, popupTitle) {
    $.ajax({
        url: "/staff/" + staffId,
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "employee-popup";
            showCommonPopup(popupSelector, popupTitle, popupContent);
            setPopupListeners();
        }
    })
}

function showAddStaffPopup() {
    var popupTitle = "Добавление сотрудника";
    getStaffPopup(0, popupTitle);
}

function showEditStaffPopup(staffId) {
    var popupTitle = "Редактирование сотрудника";
    getStaffPopup(staffId, popupTitle);
}

function setListenersToStaffTable() {
    $("tbody .cbr").change(function () {
        var tr = $(this).closest("tr");
        var selectedElemId = $(tr).data("employee-id");
        changeCbxState(tr, selectedElems, selectedElemId);
    });

    // table.find(".dblclickable").dblclick(function () {
    //     var staffId = $(this).closest("tr").data("staff-id");
    //     showEditStaffPopup(staffId);
    // });

    table.find(".btn-edit").click(function () {
        var employeeId = $(this).closest("tr").data("employee-id");
        showEditStaffPopup(employeeId);
    });

    cbr_replace();
}


function deleteStaff() {
    if (confirm('Вы действительно хотите совершить данную операцию?')) {
        $.ajax({
            url: "/staff",
            type: "DELETE",
            data: {
                "idArray": JSON.stringify(selectedElems.idArray),
            },
            success: function (response) {
                response = JSON.parse(response);
                if (response.success)
                    updateStaffTable();
            }
        });
    }
}

function updateStaffTable() {
    $.ajax({
        url: "/staff/tableBody",
        success: function (response) {
            if (isValidJSON(response)) {
                response = JSON.parse(response);
                if (!response.success) {
                    for (var i = 0; i < response.errors.length; i++) {
                        toastr.error(response.errors[i], null, commonNotificationOptions);
                    }
                }
                else{
                    cancelSelection(table);
                    selectedElems.idArray = [];
                    deleteBtn.hide();
                }
                return;
            }
            clearDatatable(table);
            deleteBtn.hide();
            table.find("tbody").append(response);
            setListenersToStaffTable();
            initializeDatatable(table);
        }
    });

}

function addEmployee(data) {
    $.ajax({
        url: "/staff",
        type: "POST",
        data: data,
        success: function (response) {
            response = JSON.parse(response);
            if (response.success){
                closeBtn.click();
                updateStaffTable();
            }
            else {
                for (var i = 0; i < response.errors.length; i++) {
                    toastr.error(response.errors[i], null, commonNotificationOptions);
                }
            }
        }
    });
}

function editEmployee(data) {
    $.ajax({
        url: "/staff/edit",
        type: "PUT",
        data: data,
        success: function (response) {
            response = JSON.parse(response);
            if (response.success){
                closeBtn.click();
                updateStaffTable();
            }
            else {
                for (var i = 0; i < response.errors.length; i++) {
                    toastr.error(response.errors[i], null, commonNotificationOptions);
                }
            }
        }
    });
}

function setPopupListeners() {
    $(".btn-save").click(function () {
        var data = {
             id : $(this).data("employee-id"),
             login : $(".employee-login-inp").val(),
             firstName : $(".employee-first-name-inp").val(),
             lastName : $(".employee-last-name-inp").val(),
             phone : $(".employee-phone-inp").val(),
             groupId : $(".employee-group-id").val(),
        }

        if (!isset(data.id)){
            data.password = $(".employee-password-inp").val();
            addEmployee(data);
        }
        else{
            if ($(".employee-password-inp").css("display") != "none")
                data.password = $(".employee-password-inp").val();
            editEmployee(data);
        }
    });

    $(".change-password-btn").click(function () {
        $(".employee-password-inp").toggle();
        if ($(".employee-password-inp").css("display") != "none")
            $(this).text("Отмена");
        else
            $(this).text("Изменить пароль");
    });

    closeBtn = $(".btn-close-popup");
    closeBtn.click(function () {
        closePopup($(this));
    })

}

var table, addBtn, deleteBtn, closeBtn;

$(document).ready(function () {
    table = $(".staff-table");
    addBtn = $(".add-employee-btn");
    deleteBtn = $(".delete-staff-btn");

    addBtn.click(function () {
        showAddStaffPopup();
    });

    deleteBtn.click(function () {
        deleteStaff();
    });

    setListenersToStaffTable();

    setSelectAllCbxListener(table);

    initializeDatatable(table);
})