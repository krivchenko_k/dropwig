var selectedElems = {
    "idArray" : []
}
function getRoutePopup(routeId, popupTitle) {
    $.ajax({
        url: "/settings/routes/" + routeId,
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "route-popup";
            showCommonPopup(popupSelector, popupTitle, popupContent);
        }
    })
}

function showAddRoutePopup() {
    var popupTitle = "Добавление маршрута";
    getRoutePopup(0, popupTitle);
}

function showEditRoutePopup(routeId) {
    var popupTitle = "Редактирование маршрута";
    getRoutePopup(routeId, popupTitle);
}

function setListenersToRoutesTable() {
    $("tbody .cbr").change(function () {
        var tr = $(this).closest("tr");
        var selectedElemId = $(tr).data("route-id");
        changeCbxState(tr, selectedElems, selectedElemId);
    });

    // table.find(".dblclickable").dblclick(function () {
    //     var routeId = $(this).closest("tr").data("route-id");
    //     showEditRoutePopup(routeId);
    // });

    table.find(".btn-edit").click(function () {
        var routeId = $(this).closest("tr").data("route-id");
        showEditRoutePopup(routeId);
    });

    cbr_replace();
}

function updateRoutesTable() {
    $.ajax({
        url: "/settings/routes/update",
        success: function (response) {
            if (isValidJSON(response)) {
                response = JSON.parse(response);
                if (!response.success) {
                    for (var i = 0; i < response.errors.length; i++) {
                        toastr.error(response.errors[i], null, commonNotificationOptions);
                    }
                }
                return;
            }
            clearDatatable(table);
            table.find("tbody").append(response);
            setListenersToRoutesTable();
            initializeDatatable(table);
        }
    });
}

function deleteRoutes() {
    if (confirm('Удаляем выделенные элементы?')) {
        $.ajax({
            url: "/settings/routes",
            type: "DELETE",
            data: {
                "routes": JSON.stringify(selectedElems.idArray),
            },
            success: function (response) {
                response = JSON.parse(response);
                if (response.success){
                    selectedElems.idArray = [];
                    deleteBtn.hide();
                    updateRoutesTable();
                }
            }
        })
    }
}

var table, addBtn, deleteBtn;

$(document).ready(function () {
    table = $(".routes-table");
    addBtn = $(".add-route-btn");
    deleteBtn = $(".delete-routes-btn");

    addBtn.click(function () {
        showAddRoutePopup();
    });

    deleteBtn.click(function () {
        deleteRoutes();
    });

    setListenersToRoutesTable();

    setSelectAllCbxListener(table);

    initializeDatatable(table);
})