function addRoute(name, route) {
    $.ajax({
        url: "/settings/routes",
        type: "POST",
        data: {
            "name": name,
            "route": route
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                updateRoutesTable();
            else {
                var message = "";
                for (var i = 0; i < response.errors.length; i++) {
                    message += response.errors[i] + "\n";
                }
                alert(message);
            }
        }
    });
}

function editRoute(id, name, route) {
    $.ajax({
        url: "/settings/routes",
        type: "PUT",
        data: {
            "id": id,
            "name": name,
            "route": route
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                updateRoutesTable();
        }
    });
}

function setPopupListeners() {
    $(".btn-save-route").click(function () {
        var id = $(this).data("route-id");
        var name = $(".route-name-inp").val();
        var route = $(".route-route-inp").val();

        if (!id)
            addRoute(name, route);
        else
            editRoute(id, name, route);
        $(".popup-frame").hide();
    });
}
$(document).ready(function () {
    setPopupListeners();
})