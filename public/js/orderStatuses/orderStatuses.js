var selectedElems = {
	"idArray" : []
}
function getOrderStatusesPopup(orderStatusId, popupTitle) {
	$.ajax({
		url: "/order-statuses/" + orderStatusId,
		type: "GET",
		success: function (popupContent) {
			var popupSelector = "order-status-popup";
			showCommonPopup(popupSelector, popupTitle, popupContent);
			setPopupListeners();
		}
	})
}

function showAddOrderStatusPopup() {
	var popupTitle = "Добавление статуса";
	getOrderStatusesPopup(0, popupTitle);
}

function showEditOrderStatusPopup(orderStatusId) {
	var popupTitle = "Редактирование статуса";
	getOrderStatusesPopup(orderStatusId, popupTitle);
}

function setListenersToOrderStatusesTable() {
	$("tbody .cbr").change(function () {
		var tr = $(this).closest("tr");
		var selectedElemId = $(tr).data("order-status-id");
		changeCbxState(tr, selectedElems, selectedElemId);
	});
	// table.find(".dblclickable").dblclick(function () {
	// 	var orderStatusId = $(this).closest("tr").data("order-status-id");
	// 	showEditOrderStatusPopup(orderStatusId);
	// });

	table.find(".btn-edit").click(function () {
		var orderStatusId = $(this).closest("tr").data("order-status-id");
		showEditOrderStatusPopup(orderStatusId);
	});

	cbr_replace();
}

function updateOrderStatusesTable() {
	$.ajax({
		url: "/order-statuses/update",
		success: function (response) {
			if (isValidJSON(response)) {
				response = JSON.parse(response);
				if (!response.success) {
					for (var i = 0; i < response.errors.length; i++) {
						toastr.error(response.errors[i], null, commonNotificationOptions);
					}
				}
				return
			}
			clearDatatable(table);
			table.find("tbody").append(response);
			setListenersToOrderStatusesTable();
			initializeDatatable(table);
		}
	});
	cancelSelection(table);
	selectedElems.idArray = [];
	deleteBtn.hide();
}

function deleteOrderStatuses() {

	if (confirm('Удаляем выделенные элементы?')) {
		$.ajax({
			url: "/order-statuses",
			type: "DELETE",
			data: {
				"orderStatusesId": JSON.stringify(selectedElems.idArray),
			},
			success: function (response) {
				response = JSON.parse(response);
				if (response.success)
					updateOrderStatusesTable();
			}
		});
	}
}

function addOrderStatus(name, color) {
	$.ajax({
		url: "/order-statuses",
		type: "POST",
		data: {
			"name": name,
			color: color,
		},
		success: function (response) {
			response = JSON.parse(response);
			if (response.success)
				updateOrderStatusesTable();
			else {
				var message = "";
				for (var i = 0; i < response.errors.length; i++) {
					message += response.errors[i] + "\n";
				}
				alert(message);
			}
		}
	});
}

function editOrderStatus(id, name, color) {
	$.ajax({
		url: "/order-statuses",
		type: "PUT",
		data: {
			"id": id,
			"name": name,
			color: color
		},
		success: function (response) {
			response = JSON.parse(response);
			if (response.success)
				updateOrderStatusesTable();
		}
	});
}


function setPopupListeners() {
	$(".btn-save").click(function () {
		var id = $(this).data("order-status-id");
		var name = $(".order-status-name-inp").val();
		var color = $(".order-status-color-inp").val();

		if (!id)
			addOrderStatus(name, color);
		else
			editOrderStatus(id, name, color);
		$(".popup-frame").hide();
	});

	$(".btn-close-popup").click(function () {
		$(".popup-frame").hide();
	});
}

var table, addBtn, deleteBtn;

$(document).ready(function () {
	table = $(".order-statuses-table");
	addBtn = $(".add-order-status-btn");
	deleteBtn = $(".delete-order-statuses-btn");

	addBtn.click(function () {
		showAddOrderStatusPopup();
	});

	deleteBtn.click(function () {
		deleteOrderStatuses();
	});

	setListenersToOrderStatusesTable();

	setSelectAllCbxListener(table);

	initializeDatatable(table);
})