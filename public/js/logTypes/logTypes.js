var selectedElems = {
    "idArray" : []
}
function getLogTypesPopup(logTypeId, popupTitle) {
    $.ajax({
        url: "/settings/log-types/" + logTypeId,
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "log-type-popup";
            showCommonPopup(popupSelector, popupTitle, popupContent);
            setPopupListeners();
        }
    })
}

function showAddLogTypePopup() {
    var popupTitle = "Добавление типа логов";
    getLogTypesPopup(0, popupTitle);
}

function showEditLogTypePopup(logTypeId) {
    var popupTitle = "Редактирование типа логов";
    getLogTypesPopup(logTypeId, popupTitle);
}


function setListenersToLogTypesTable() {
    $("tbody .cbr").change(function () {
        var tr = $(this).closest("tr");
        var selectedElemId = $(tr).data("log-type-id");
        changeCbxState(tr, selectedElems, selectedElemId);
    });
    // table.find(".dblclickable").dblclick(function () {
    //     var logTypeId = $(this).closest("tr").data("log-type-id");
    //     showEditLogTypePopup(logTypeId);
    // });

    table.find(".btn-edit").click(function () {
        var logTypeId = $(this).closest("tr").data("log-type-id");
        showEditLogTypePopup(logTypeId);
    });

    cbr_replace();
}

function updateLogTypesTable() {
    $.ajax({
        url: "/settings/log-types/tableBody",
        success: function (response) {
            if (isValidJSON(response)) {
                response = JSON.parse(response);
                if (!response.success) {
                    for (var i = 0; i < response.errors.length; i++) {
                        toastr.error(response.errors[i], null, commonNotificationOptions);
                    }
                }
                else{
                    cancelSelection(table);
                    selectedElems.idArray = [];
                    deleteBtn.hide();
                }
                return;
            }
            clearDatatable(table);
            table.find("tbody").append(response);
            setListenersToLogTypesTable();
            initializeDatatable(table);
        }
    });

}

function deleteLogTypes() {
    if (confirm('Удаляем выделенные элементы?')) {
        $.ajax({
            url: "/settings/log-types",
            type: "DELETE",
            data: {
                "idList": JSON.stringify(selectedElems.idArray),
            },
            success: function (response) {
                response = JSON.parse(response);
                if (response.success)
                    updateLogTypesTable();
            }
        });
    }
}


function addLogType(data) {
    $.ajax({
        type: 'POST',
        url: '/settings/log-types',
        data: data,
        success: function(response) {
            response = JSON.parse(response);
            if (response.success){
                $(".popup-frame").hide();
                updateLogTypesTable();
            }
            else {
                for (var i = 0; i < response.errors.length; i++) {
                    toastr.error(response.errors[i], null, commonNotificationOptions);
                }
            }
        }
    });
}


function editLogType(data) {
    $.ajax({
        url: "/settings/log-types",
        type: "PUT",
        data: data,
        success: function (response) {
            response = JSON.parse(response);
            if (response.success){
                $(".popup-frame").hide();
                updateLogTypesTable();
            }
            else {
                for (var i = 0; i < response.errors.length; i++) {
                    toastr.error(response.errors[i], null, commonNotificationOptions);
                }
            }
        }
    });
}


function setPopupListeners() {
    $(".btn-close-popup").click(function () {
        closePopup(this);
    });

    cbr_replace();

    $('.btn-save').on('click', function() {
        var data = {
            id: $(this).data("log-type-id"),
            name: $(".log-type-name-inp").val(),
        };

        if (!isset(data.id))
            addLogType(data);
        else
            editLogType(data);
    });


}

var table, addBtn, deleteBtn;

$(document).ready(function () {
    table = $(".log-types-table");
    addBtn = $(".add-log-type-btn");
    deleteBtn = $(".delete-log-types-btn");

    addBtn.click(function () {
        showAddLogTypePopup();
    });

    deleteBtn.click(function () {
        deleteLogTypes();
    });

    setListenersToLogTypesTable();

    setSelectAllCbxListener(table);

    initializeDatatable(table);
})