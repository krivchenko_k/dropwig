function getTopGoods() {
    $.ajax({
        url: "/top-goods",
        success: function (response) {
            if (!isValidJSON(response)) {
                $(".top-goods-container .content .loader").remove();
                $(".top-goods-container .content .fade-in-effect").empty().append(response).addClass("in");
            }
        }
    })
}

function getTopPartners() {
    $.ajax({
        url: "/top-partners",
        success: function (response) {
            if (!isValidJSON(response)) {
                $(".top-partners-container .content .loader").remove();
                $(".top-partners-container .content .fade-in-effect").empty().append(response).addClass("in");
            }
        }
    })
}

function getPartnersOnline() {
    $.ajax({
        url: "/partners-online",
        success: function (response) {
            if (!isValidJSON(response)) {
                $(".partners-online-container .content .loader").remove();
                $(".partners-online-container .content .fade-in-effect").empty().append(response).addClass("in");
            }
        }
    })
}

function getPartnersStat() {
    $.ajax({
        url: "/partners-stat",
        async: true,
        success: function (response) {
            if (!isValidJSON(response)) {
                $(".statistic-container .partners-stat .loader").remove();
                $(".statistic-container .partners-stat .content").append(response).addClass("in");
            }
        }
    })
}

function getOrdersStat() {
    $.ajax({
        url: "/orders-stat",
        async: true,
        success: function (response) {
            if (!isValidJSON(response)) {
                $(".statistic-container .goods-stat .loader").remove();
                $(".statistic-container .goods-stat .content").append(response).addClass("in");
            }
        }
    })
}

$(document).ready(function () {
    getPartnersStat();
    getOrdersStat();
    getTopPartners();
    getPartnersOnline();
    getTopGoods();

    $(".adm-main-page-container").addClass("in");
});