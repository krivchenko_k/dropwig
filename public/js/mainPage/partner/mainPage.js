function getPersonalStat() {
    $.ajax({
        url: "/personal-stat",
        success: function (response) {
            if (!isValidJSON(response)) {
                $(".goods-stat .loader").remove();
                $(".goods-stat .fade-in-effect").empty().append(response).addClass("in");
            }
        }
    })
}

function getTopGoods() {
    $.ajax({
        url: "/top-goods",
        success: function (response) {
            if (!isValidJSON(response)) {
                $(".top-goods-container .content .loader").remove();
                $(".top-goods-container .content .fade-in-effect").empty().append(response).addClass("in");
            }
        }
    })
}

function getTopPartners() {
    $.ajax({
        url: "/top-partners",
        success: function (response) {
            if (!isValidJSON(response)) {
                $(".top-partners-container .content .loader").remove();
                $(".top-partners-container .content .fade-in-effect").empty().append(response).addClass("in");
            }
        }
    });
}


$(document).ready(function () {
    getPersonalStat();
    getTopPartners();
    getTopGoods();

    $(".partner-main-page-container").addClass("in");
});