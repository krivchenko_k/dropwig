var selectedElems = {
    "idArray" : []
}
function getNewsPopup(newsId, popupTitle) {
    $.ajax({
        url: "/news/" + newsId,
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "news-popup";
            showCommonPopup(popupSelector, popupTitle, popupContent);
            setPopupListeners();
        }
    })
}

function showAddNewsPopup() {
    var popupTitle = "Добавление новости";
    getNewsPopup(0, popupTitle);
}

function showEditNewsPopup(newsId) {
    var popupTitle = "Редактирование новости";
    getNewsPopup(newsId, popupTitle);
}

function setListenersToNewsTable() {
    $("tbody .cbr").change(function () {
        var tr = $(this).closest("tr");
        var selectedElemId = $(tr).data("news-id");
        changeCbxState(tr, selectedElems, selectedElemId);
    });

    // table.find(".dblclickable").dblclick(function () {
    //     var newsId = $(this).closest("tr").data("news-id");
    //     showEditNewsPopup(newsId);
    // });

    table.find(".btn-edit").click(function () {
        var newsId = $(this).closest("tr").data("news-id");
        showEditNewsPopup(newsId);
    });

    cbr_replace();
}


function deleteNews() {
    if (confirm('Вы действительно хотите совершить данную операцию?')) {
        $.ajax({
            url: "/news",
            type: "DELETE",
            data: {
                "newsId": JSON.stringify(selectedElems.idArray),
            },
            success: function (response) {
                response = JSON.parse(response);
                if (response.success)
                    location.reload();
                    cancelSelection(table);
                    selectedElems.idArray = [];
                    deleteBtn.hide();
            }
        });
    }
}

function addNews(title, text) {
    $.ajax({
        url: "/news",
        type: "POST",
        data: {
            "title": title,
            "text": text
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success) {
                location.reload();
                cancelSelection(table);
                selectedElems.idArray = [];
                deleteBtn.hide();
            }
            else {
                var message = "";
                for (var i = 0; i < response.errors.length; i++) {
                    message += response.errors[i] + "\n";
                }
                alert(message);
            }
        }
    });
}

function editNews(id, title, text) {
    $.ajax({
        url: "/news/edit",
        type: "PUT",
        data: {
            "id": id,
            "title": title,
            "text": text
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success) {
                location.reload();
                cancelSelection(table);
                selectedElems.idArray = [];
                deleteBtn.hide();
            }
        }
    });
}

function setPopupListeners() {
    $(".btn-save-news").click(function () {
        var id = $(this).data("news-id");
        var title = $(".news-title-inp").val();
        var text = CKEDITOR.instances['news-text'].getData();
        

        
        if (!id)
            addNews(title, text);
        else
            editNews(id, title, text);
        $(".popup-frame").hide();
    });

    $(".btn-close-popup").click(function () {
        $(".popup-frame").hide();
    });

    var editor = CKEDITOR.replace('news-text', {contentsCss: "body {font-size: 14px;}"} );
}

var table, addBtn, deleteBtn;

$(document).ready(function () {
    table = $(".news-table");
    addBtn = $(".add-news-btn");
    deleteBtn = $(".delete-news-btn");

    addBtn.click(function () {
        showAddNewsPopup();
    });

    deleteBtn.click(function () {
        deleteNews();
    });

    setListenersToNewsTable();

    setSelectAllCbxListener(table);

    initializeDatatable(table);
})