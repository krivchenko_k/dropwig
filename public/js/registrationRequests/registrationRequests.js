$("body").addClass("login-page");

function getRegistrationRequestPopup(partnerId) {
    $.ajax({
        type: 'GET',
        url: "/reg-requests/confirmation/" + partnerId,
        success: function (content) {
            var title = "Подтверждение рагистрации пользователя";
            var popupSelector = "reg-request-popup";
            showCommonPopup(popupSelector, title, content);
            setPopupListeners();
        }
    });
}

function getRegistrationDeclinePopup(partnerId) {
    $.ajax({
        type: 'GET',
        url: "/reg-requests/decline/" + partnerId,
        success: function (content) {
            var title = "Отказ в рагистрации";
            showCommonPopup("reg-decline-popup", title, content);
            setDeclinePopupListeners();
        }
    });
}

function setPopupListeners() {

    cbr_replace();

    var $validator;

    $(".btn-close-popup").click(function () {
        closePopup($(".reg-request-popup"));
    })

    $validator = $('.registration-form').validate({
        rules: {
            login: {
                required: true,
                minlength: 5,
                maxlength: 20,
                pattern: /^[a-zA-Z][a-zA-Z0-9-_\.]+$/
            },
            firstName: {
                required: true,
                minlength: 2,
                maxlength: 20,
                pattern: /^[a-zA-Zа-яА-Я ]+$/
            },

            lastName: {
                required: true,
                minlength: 2,
                maxlength: 20,
                pattern: /^[a-zA-Zа-яА-Я ]+$/
            },

            phone: {
                required: true,
                // minlength: 13
            },

            email: {
                required: true,
                email: true,
                maxlength: 100
            },

//            card: {
//                required: true
//            },
//
//            cardOwner: {
//                required: true,
//                maxlength: 50,
//                pattern: /^[a-zA-Zа-яА-Я ]+$/
//            },

            nickname: {
                required: true,
                minlength: 4,
                maxlength: 20,
                pattern: /^[a-zA-Z][a-zA-Z0-9-_\.]+$/
            },


        },
        messages: {
            login: {
                required: 'Введите логин',
                minlength: "Не менее 5 символов",
                maxlength: "Не более 20 символов",
                pattern: "Неверный формат"
            },
            firstName: {
                required: 'Введите имя',
                minlength: "Не менее 2 символов",
                maxlength: "Не более 20 символов",
                pattern: "Неверный формат"
            },

            lastName: {
                required: 'Введите фамилию',
                minlength: "Не менее 2 символов",
                maxlength: "Не более 20 символов",
                pattern: "Неверный формат"
            },

            phone: {
                required: 'Введите телефон',
                // minlength: "Не менее 10 символов",
            },

            email: {
                required: 'Введите E-mail',
                maxlength: "Не более 100 символов",
                email: "Неверный формат"
            },

//            card: {
//                required: 'Введите номер карты',
//            },
//
//            cardOwner: {
//                required: 'Введите имя владельца карты',
//                maxlength: "Не более 50 символов",
//                pattern: "Неверный формат"
//            },

            nickname: {
                required: 'Введите ник',
                minlength: "Не менее 4 символов",
                maxlength: "Не более 20 символов",
                pattern: "Неверный формат"
            },

        }
    });

    $('.partner-phone').mask('38(000)000-00-00', {placeholder: "38(0__)000-00-00"});
    $('.partner-card').mask('0000 0000 0000 0000', {placeholder: "____ ____ ____ ____"});

    $('.btn-decline').on('click', function () {
        var partnerId = $(this).data('partner-id');
        if (isset(partnerId)) {
            getRegistrationDeclinePopup(partnerId);
        }
    });

    $('.btn-confirm').on('click', function () {
        $('.registration-form').valid();
        if ($validator.errorList.length != 0) {
            return;
        }
        else{
            confirmRegistration();
        }
    });

    $('.partner-card-owner-self').on('change', function () {
        if ($(this).prop('checked')) {
            var fullName = '';
            if (isset($('.partner-name').val()) && isset($('.partner-surname').val())) {
                fullName = $('.partner-surname').val() + ' ' + $('.partner-name').val();
            }
            $('.partner-card-owner').val(fullName);
            $('.partner-card-owner').prop('readonly', 'true');
        }
        else {
            $('.partner-card-owner').removeAttr('readonly');
        }
    });

    $('.partner-name').on('keyup', function () {
        if (isset($(this).val())) {
            if ($('.partner-card-owner-self').prop('checked')) {
                $('.partner-card-owner').val((isset($('.partner-surname').val()) ? $('.partner-surname').val() : '') + ' ' + $(this).val());

            }
        }
        if (isset($(this).val()) && isset($('.partner-surname').val())) {
            $('.partner-card-owner-self').removeAttr('disabled');
            $('.card-owner-label').removeAttr('readonly');
        }
        else {
            $('.partner-card-owner-self').prop('disabled', 'true');
            $('.card-owner-label').prop('disabled', 'true');
        }
    });

    $('.partner-surname').on('keyup', function (event) {
        if (isset($(this).val())) {
            if ($('.partner-card-owner-self').prop('checked')) {
                $('.partner-card-owner').val($(this).val() + ' ' + (isset($('.partner-name').val()) ? $('.partner-name').val() : ''));
            }
        }
        if (isset($(this).val()) && isset($('.partner-name').val())) {
            $('.partner-card-owner-self').removeAttr('disabled');
            $('.card-owner-label').removeAttr('readonly');
        }
        else {
            $('.partner-card-owner-self').prop('disabled', 'true');
            $('.card-owner-label').prop('disabled', 'true');
        }
    });
}

function setDeclinePopupListeners() {
    $('.btn-close').on('click', function() {
        $(".popup-frame").hide();
    });

    $('.sel-decline-reason').on('change', function() {
        if($(this).val() == -1) {
            $('.decline-reason').show();
        }
        else {
            $('.decline-reason').hide();
        }
    });

    $('.btn-decline-reg').on('click', function() {
        var partnerId = $(this).data('partner-id');
        var reasonId = $('.sel-decline-reason').val();
        var reasonText = $('.decline-reason').val();

        if(!isset(partnerId) || !isset(reasonId)) {
            return;
        }
        else{
            declineRegistration(partnerId, reasonId, reasonText);
        }
    });
}

function confirmRegistration() {
    $.ajax({
        type: 'PUT',
        url: '/reg-requests/confirm',
        data: {
            id: $('.btn-confirm').data('partner-id'),
            formData: $(".registration-form").serialize()
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success){
                $(".popup-frame").hide();
                updateRegistrationRequestsTable();
            }
            else {
                for (var i = 0; i < response.errors.length; i++) {
                    toastr.error(response.errors[i], null, commonNotificationOptions);
                }
            }
        }
    });
}

function declineRegistration(partnerId, reasonId, reasonText) {

    console.log(partnerId, reasonId, reasonText);

    $.ajax({
        type: 'PUT',
        url: '/reg-requests/decline',
        data: {
            'partnerId': partnerId,
            'reasonId': reasonId,
            'reasonText': reasonText
        },
        success: function(response) {
            response = JSON.parse(response);
            if (response.success){
                $(".popup-frame").hide();
                updateRegistrationRequestsTable();
            }
            else {
                for (var i = 0; i < response.errors.length; i++) {
                    toastr.error(response.errors[i], null, commonNotificationOptions);
                }
            }
        }
    });
}

function setListenersToRegistrationRequestsTable() {
    table.find(".dblclickable").dblclick(function () {
        var partnerId = $(this).closest("tr").data("partner-id");
        getRegistrationRequestPopup(partnerId);
    });

    table.find(".btn-edit").click(function () {
        var partnerId = $(this).closest("tr").data("partner-id");
        getRegistrationRequestPopup(partnerId);
    });
}

function updateRegistrationRequestsTable() {
    $.ajax({
        url: "/reg-requests/tableBody",
        success: function (response) {
            if (isValidJSON(response)) {
                response = JSON.parse(response);
                if (!response.success) {
                    for (var i = 0; i < response.errors.length; i++) {
                        toastr.error(response.errors[i], null, commonNotificationOptions);
                    }
                }
                return;
            }
            clearDatatable(table);
            table.find("tbody").append(response);
            setListenersToRegistrationRequestsTable();
            initializeDatatable(table);
        }
    });
}

var table;

$(document).ready(function () {
    table = $(".reg-requests-table");
    $('.btn-edit').on('click', function () {
        var partnerId = $(this).data('partner-id');
        if (isset(partnerId)) {
            getRegistrationRequestPopup(partnerId);
        }
    });

    setListenersToRegistrationRequestsTable();

    initializeDatatable(table);
});