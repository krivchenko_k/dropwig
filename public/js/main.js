$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name=_token]').attr('content'),
        'X-XSRF-TOKEN': $('meta[name=_token]').attr('content'),
    }
});

function isValidJSON(src) {
    if (src.length == 0)
        return false;

    var filtered = src;
    filtered = filtered.replace(/\\["\\\/bfnrtu]/g, '@');
    filtered = filtered.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']');
    filtered = filtered.replace(/(?:^|:|,)(?:\s*\[)+/g, '');

    return (/^[\],:{}\s]*$/.test(filtered));
}

function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

// возвращает cookie с именем name, если есть, если нет, то undefined
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function deleteCookie(name) {
    setCookie(name, "", {
        expires: -1
    })
}

function startFadeInEffect() {
    setTimeout(function () {
        $(".fade-in-effect").addClass('in');
    }, 1);
}

function startPopupFadeInEffect() {
    setTimeout(function () {
        $(".popup-fade-in-effect").addClass('in');
    }, 1);
}

function showCommonPopup(selector, title, content) {
    $(".popup-frame .common-popup").addClass(selector);
    $(".panel-title").html(title);
    $(".panel-body").html(content);
    startPopupFadeInEffect();
    $(".popup-frame").css({
        "display": "flex",
        "z-index": 2000
    });
    $("html").css("overflow", "hidden");
    

    $(".btn-close-popup").click(function () {
        $(".close-popup-btn").click();
    });
}

function isset(field) {
    if (field === undefined || field === '' || field === NaN || field === null) {
        return false;
    }
    return true;
}

function empty(elem) {
    if (elem.length == 0)
        return true
    else
        return false;
}

var dataTableOptions = {
    "aLengthMenu": [
        [10, 25, 50, 100, 1000, -1], [10, 25, 50, 100, 1000, "Все"]
    ],
    "iDisplayLength": 10,
    "language": {
        "url": "/lib/js/datatables/locale/russian.lang"
    },
    "columnDefs": [
        {
            orderable: false,
            targets: []
        }
    ]
};

function clearDatatable(sourceTable) {
    sourceTable.find("tbody").empty();
    sourceTable.dataTable().fnClearTable();
    sourceTable.dataTable().fnDraw();
    sourceTable.dataTable().fnDestroy();
}


function initializeDatatable(sourceTable, noSortableCols, bPaginate, sorting, displayLength) {
    if (!isset(bPaginate)) bPaginate = true;
    dataTableOptions["bPaginate"] = bPaginate;

    if (!isset(noSortableCols)) noSortableCols = [0];
        dataTableOptions["columnDefs"][0]["targets"] = noSortableCols;

    if (isset(sorting)) {
        dataTableOptions['order'] = sorting;
    }

    if (isset(displayLength)) {
        dataTableOptions['iDisplayLength'] = displayLength;
    }

    sourceTable.DataTable(dataTableOptions);

    sourceTable.on('draw.dt', function () {
        checkNeedActivateSelectAllCbx(sourceTable);
    });
}


function cancelSelection(sourceTable) {
    // sourceTable.find(".select-all-cbx").closest(".cbr-replaced").removeClass("cbr-checked");
    // sourceTable.find(".select-all-cbx").prop("checked", false);
}

function changeCbxState(tr, selectedItems, selectetItemId) {
    var checkbox = $(tr).find(".cbr-replaced");

    if (isCbrCbxCheched(checkbox)) {
        if ($.inArray(selectetItemId, selectedItems.idArray) === -1) {
            selectedItems.idArray.push(selectetItemId);
        }
    }
    else {
        if ($.inArray(selectetItemId, selectedItems.idArray) !== -1) {
            selectedItems.idArray.splice(selectedItems.idArray.indexOf(selectetItemId), 1);
        }
    }

    if (selectedItems.idArray.length != 0) {
        deleteBtn.find(".deleted-row-count").text(selectedItems.idArray.length);
        deleteBtn.show();
    }
    else {
        deleteBtn.hide();
    }

    console.log(selectedItems.idArray);

    checkNeedActivateSelectAllCbx();
}


function isCbrCbxCheched(checkbox) {
    return $(checkbox).find(".cbr").prop("checked");
}


function showTableLoader(table, colSpan) {
    setTimeout(function () {
        table.find("tbody").empty().append(
            $("<tr>").append(
                $("<td>",{
                    "colspan" : colSpan
                }).append($("<div>", {
                    "class" : "loader"
                }))
            )
        );
    }, 50)
}


function setSelectAllCbxListener() {
    $(".select-all-cbx").change(function () {
        if ($(this).prop("checked")) {
            $(this).closest(".dataTables_scroll").find(".dataTables_scrollBody .cbr").prop("checked", true);
            $(this).closest(".table").find(".cbr").prop("checked", true);
        }
        else {
            $(this).closest(".dataTables_scroll").find(".dataTables_scrollBody .cbr").prop("checked", false);
            $(this).closest(".table").find(".cbr").prop("checked", false);
        }
        $(this).closest(".dataTables_scroll").find(".dataTables_scrollBody .cbr").change();
        $(this).closest("table").find("tbody .cbr").change();
    });
}


function checkNeedActivateSelectAllCbx() {
    var cbxCount = table.find(".cbx").length;
    var cbxCheckedCount = table.find(".cbx:checked").length;

    if (cbxCount == cbxCheckedCount) {
        table.find(".select-all-cbx").prop("checked", true);
        table.find(".select-all-cbx").closest(".cbr-replaced").addClass("cbr-checked");
    }
    else {
        table.find(".select-all-cbx").prop("checked", false);
        table.find(".select-all-cbx").closest(".cbr-replaced").removeClass("cbr-checked");
    }
}

function setCommonPopupListeners(){
    $(".close-popup-btn").click(function () {
        closePopup(this);
    });
}

var commonNotificationOptions = {
    "closeButton": true,
    "debug": false,
    "onclick": null,
    "showDuration": "200",
    "hideDuration": "200",
    "timeOut": "10000",
    "extendedTimeOut": "10000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

function cbr_recheck()
{
    var $inputs = jQuery("input.cbr-done");

    $inputs.each(function(i, el)
    {
        var $el = jQuery(el),
            is_radio = $el.is(':radio'),
            is_checkbox = $el.is(':checkbox'),
            is_disabled = $el.is(':disabled'),
            $wrp = $el.closest('.cbr-replaced');

        if(is_disabled)
            $wrp.addClass('cbr-disabled');

        if(is_radio && ! $el.prop('checked') && $wrp.hasClass('cbr-checked'))
        {
            $wrp.removeClass('cbr-checked');
        }
    });
}

// Radio and Check box replacement by Arlind Nushi
function cbr_replace()
{
    var $inputs = jQuery('input[type="checkbox"].cbr, input[type="radio"].cbr').filter(':not(.cbr-done)'),
        $wrapper = '<div class="cbr-replaced"><div class="cbr-input"></div><div class="cbr-state"><span></span></div></div>';

    $inputs.each(function(i, el)
    {
        var $el = jQuery(el),
            is_radio = $el.is(':radio'),
            is_checkbox = $el.is(':checkbox'),
            is_disabled = $el.is(':disabled'),
            styles = ['primary', 'secondary', 'success', 'danger', 'warning', 'info', 'purple', 'blue', 'red', 'gray', 'pink', 'yellow', 'orange', 'turquoise'];

        if( ! is_radio && ! is_checkbox)
            return;

        $el.after( $wrapper );
        $el.addClass('cbr-done');

        var $wrp = $el.next();
        $wrp.find('.cbr-input').append( $el );

        if(is_radio)
            $wrp.addClass('cbr-radio');

        if(is_disabled)
            $wrp.addClass('cbr-disabled');

        if($el.is(':checked'))
        {
            $wrp.addClass('cbr-checked');
        }


        // Style apply
        jQuery.each(styles, function(key, val)
        {
            var cb_class = 'cbr-' + val;

            if( $el.hasClass(cb_class))
            {
                $wrp.addClass(cb_class);
                $el.removeClass(cb_class);
            }
        });


        // Events
        $wrp.on('click', function(ev)
        {
            if(is_radio && $el.prop('checked') || $wrp.parent().is('label'))
                return;

            if(jQuery(ev.target).is($el) == false)
            {
                $el.prop('checked', ! $el.is(':checked'));
                $el.trigger('change');
            }
        });

        $el.on('change', function(ev)
        {
            $wrp.removeClass('cbr-checked');

            if($el.is(':checked'))
                $wrp.addClass('cbr-checked');

            cbr_recheck();
        });
    });
}


function closePopup(_this) {
    $(_this).closest(".popup-frame.popup-fade-in-effect").removeClass("in").css({
        "z-index": -1,
        "display" : "none",
    }).find(".panel-body").empty();
    $("html").css("overflow", "initial");
}


function pageFadeIn() {
    $(".main-content").addClass("in");
}


$(document).ready(function () {
    $.fn.dataTable.moment('DD.MM.YYYY');
    $.fn.dataTable.moment('dd.mm.YYYY');
    $.fn.dataTable.moment( 'DD.MM.YYYY (HH:mm)');
    $.fn.dataTable.moment( 'dd.mm.YYYY (HH:mm)');

    setCommonPopupListeners();

    pageFadeIn();
});
