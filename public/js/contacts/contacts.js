var selectedElems = {
    "idArray": []
}

function getСontactsPopup(rejectionReasonId, popupTitle) {
    $.ajax({
        url: "/settings/contacts/" + rejectionReasonId,
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "contact-popup";
            showCommonPopup(popupSelector, popupTitle, popupContent);
            setPopupListeners();
        }
    })
}

function showAddСontactPopup() {
    var popupTitle = "Добавление контакта";
    getСontactsPopup(0, popupTitle);
}

function showEditСontactPopup(rejectionReasonId) {
    var popupTitle = "Редактирование контакта";
    getСontactsPopup(rejectionReasonId, popupTitle);
}

function setListenersToСontactsTable() {
    $("tbody .cbr").change(function () {
        var tr = $(this).closest("tr");
        var selectedElemId = $(tr).data("contact-id");
        changeCbxState(tr, selectedElems, selectedElemId);
    });

    // table.find(".dblclickable").dblclick(function () {
    //     var rejectionReasonId = $(this).closest("tr").data("contact-id");
    //     showEditСontactPopup(rejectionReasonId);
    // });

    table.find(".btn-edit").click(function () {
        var rejectionReasonId = $(this).closest("tr").data("contact-id");
        showEditСontactPopup(rejectionReasonId);
    });

    cbr_replace();
}

function updateСontactsTable() {
    $.ajax({
        url: "/settings/contacts/update",
        success: function (response) {
            if (isValidJSON(response)) {
                response = JSON.parse(response);
                if (!response.success) {
                    for (var i = 0; i < response.errors.length; i++) {
                        toastr.error(response.errors[i], null, commonNotificationOptions);
                    }
                }
                return
            }
            clearDatatable(table);
            table.find("tbody").append(response);
            setListenersToСontactsTable();
            initializeDatatable(table);
        }
    });
    cancelSelection(table);
    selectedElems.idArray = [];
    deleteBtn.hide();
}

function deleteСontacts() {
    if (confirm('Удаляем выделенные элементы?')) {
        $.ajax({
            url: "/settings/contacts",
            type: "DELETE",
            data: {
                "contactsId": JSON.stringify(selectedElems.idArray),
            },
            success: function (response) {
                response = JSON.parse(response);
                if (response.success)
                    updateСontactsTable();
            }
        });
    }
}


function addСontact(text) {
    $.ajax({
        url: "/settings/contacts",
        type: "POST",
        data: {
            text: text
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                updateСontactsTable();
            else {
                var message = "";
                for (var i = 0; i < response.errors.length; i++) {
                    message += response.errors[i] + "\n";
                }
                alert(message);
            }
        }
    });
}


function editСontact(id, text) {
    $.ajax({
        url: "/settings/contacts",
        type: "PUT",
        data: {
            id: id,
            text: text
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                updateСontactsTable();
        }
    });
}


function setPopupListeners() {
    $(".btn-save").click(function () {
        var id = $(this).data("contact-id");
        // var text = $(".contact-text-inp").val();
        var text = CKEDITOR.instances['contact-text'].getData();

        if (!id)
            addСontact(text);
        else
            editСontact(id, text);
        $(".popup-frame").hide();
    });

    $(".btn-close-popup").click(function () {
        $(".popup-frame").hide();
    });

    var editor = CKEDITOR.replace('contact-text');
}

var table, addBtn, deleteBtn;

$(document).ready(function () {
    table = $(".contacts-table");
    addBtn = $(".add-contact-btn");
    deleteBtn = $(".delete-contacts-btn");

    addBtn.click(function () {
        showAddСontactPopup();
    });

    deleteBtn.click(function () {
        deleteСontacts();
    });

    setListenersToСontactsTable();

    setSelectAllCbxListener(table);

    initializeDatatable(table);
})