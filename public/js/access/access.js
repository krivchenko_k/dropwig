//Получить вьюху попапа редактирования доступа к маршрутам для группы
function getAccessPopup(groupId) {
    $.ajax({
        url:"/settings/access/"+groupId,
        success: function (content) {
            var title = "Редактирование прав доступа группы";
            showCommonPopup("access-rule-popup", title, content);
            setPopupListeners();
        }
    })
}

function setListenersToAccessTable() {
    table.find(".dblclickable").dblclick(function () {
        var groupId = $(this).closest("tr").data("group-id");
        getAccessPopup(groupId);
    });

    table.find(".btn-edit").click(function () {
        var groupId = $(this).closest("tr").data("group-id");
        getAccessPopup(groupId);
    });

    cbr_replace();
}

//Включить/выключить доступ к маршруту для группы пользователей
function changeAccessState(groupId, routeId) {
    console.log(groupId, routeId)
    $.ajax({
        url: "/settings/access/"+groupId+"/"+routeId,
        type: "PUT"
    });
}

function setPopupListeners() {
    $(".change-access-rule-cbx").change(function () {
        var groupId = $(this).closest("tr").data("group-id");
        var routeId = $(this).closest("tr").data("route-id");
        changeAccessState(groupId, routeId);
    });

    $(".popup-access-table tr").click(function (event) {
        if (!$(event.target).hasClass("change-access-rule-cbx")){
            var groupId = $(this).data("group-id");
            var routeId = $(this).data("route-id");
            changeAccessState(groupId, routeId);
            if ($(this).find(".change-access-rule-cbx").prop("checked"))
                $(this).find(".change-access-rule-cbx").prop("checked", false);
            else
                $(this).find(".change-access-rule-cbx").prop("checked", true);
        }
    });

    initializeDatatable($(".popup-access-table"), false);

    $(".btn-close-popup").cli
}

var table;

$(document).ready(function () {
    table = $(".access-table");

    setListenersToAccessTable();

    initializeDatatable(table);
});