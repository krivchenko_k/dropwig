var selectedElems = {
    "idArray" : []
}

function getRejectionReasonsPopup(rejectionReasonId, popupTitle) {
    $.ajax({
        url: "/settings/rejection-reasons/" + rejectionReasonId,
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "rejection-reason-popup";
            showCommonPopup(popupSelector, popupTitle, popupContent);
            setPopupListeners();
        }
    })
}

function showAddRejectionReasonPopup() {
    var popupTitle = "Добавление причины отказа";
    getRejectionReasonsPopup(0, popupTitle);
}

function showEditRejectionReasonPopup(rejectionReasonId) {
    var popupTitle = "Редактирование  причины отказа";
    getRejectionReasonsPopup(rejectionReasonId, popupTitle);
}

function setListenersToRejectionReasonsTable() {
    $("tbody .cbr").change(function () {
        var tr = $(this).closest("tr");
        var selectedElemId = $(tr).data("rejection-reason-id");
        changeCbxState(tr, selectedElems, selectedElemId);
    });

    // table.find(".dblclickable").dblclick(function () {
    //     var rejectionReasonId = $(this).closest("tr").data("rejection-reason-id");
    //     showEditRejectionReasonPopup(rejectionReasonId);
    // });

    table.find(".btn-edit").click(function () {
        var rejectionReasonId = $(this).closest("tr").data("rejection-reason-id");
        showEditRejectionReasonPopup(rejectionReasonId);
    });

    cbr_replace();
}

function updateRejectionReasonsTable() {
    $.ajax({
        url: "/settings/rejection-reasons/update",
        success: function (response) {
            if (isValidJSON(response)) {
                response = JSON.parse(response);
                if (!response.success) {
                    for (var i = 0; i < response.errors.length; i++) {
                        toastr.error(response.errors[i], null, commonNotificationOptions);
                    }
                }
                return;
            }
            clearDatatable(table);
            table.find("tbody").append(response);
            setListenersToRejectionReasonsTable();
            initializeDatatable(table);
        }
    });
    cancelSelection(table);
    selectedElems.idArray = [];
    deleteBtn.hide();
}

function deleteRejectionReasons() {
    if (confirm('Удаляем выделенные элементы?')) {
        $.ajax({
            url: "/settings/rejection-reasons",
            type: "DELETE",
            data: {
                "rejectionReasonsId": JSON.stringify(selectedElems.idArray),
            },
            success: function (response) {
                response = JSON.parse(response);
                if (response.success)
                    updateRejectionReasonsTable();
            }
        });
    }
}


function addRejectionReason(typeId, text) {
    $.ajax({
        url: "/settings/rejection-reasons",
        type: "POST",
        data: {
            typeId: typeId,
            text: text
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                updateRejectionReasonsTable();
            else {
                var message = "";
                for (var i = 0; i < response.errors.length; i++) {
                    message += response.errors[i] + "\n";
                }
                alert(message);
            }
        }
    });
}


function editRejectionReason(id, typeId, text) {
    $.ajax({
        url: "/settings/rejection-reasons",
        type: "PUT",
        data: {
            id: id,
            typeId: typeId,
            text: text
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                updateRejectionReasonsTable();
        }
    });
}


function setPopupListeners() {
    $(".btn-save").click(function () {
        var id = $(this).data("rejection-reason-id");
        var typeId = $(".rejection-type-id-sel").val()
        var text = $(".rejection-reason-text-inp").val();

        if (!id)
            addRejectionReason(typeId, text);
        else
            editRejectionReason(id, typeId, text);
        $(".popup-frame").hide();
    });

    $(".btn-close-popup").click(function () {
        $(".popup-frame").hide();
    });
}

var table, addBtn, deleteBtn;

$(document).ready(function () {
    table = $(".rejection-reasons-table");
    addBtn = $(".add-rejection-reason-btn");
    deleteBtn = $(".delete-rejection-reasons-btn");

    addBtn.click(function () {
        showAddRejectionReasonPopup();
    });

    deleteBtn.click(function () {
        deleteRejectionReasons();
    });

    setListenersToRejectionReasonsTable();

    setSelectAllCbxListener(table);

    initializeDatatable(table);
})