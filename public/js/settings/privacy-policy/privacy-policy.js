function showEditPrivacyPopup() {
    var popupTitle = "Редактировать пользовательское соглашение";

    $.ajax({
        url: "/settings/privacy-policy/edit",
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "privacy-popup";
            showCommonPopup(popupSelector, popupTitle, popupContent);
            setPopupListeners();
        }
    });
}

function savePrivacy(text) {
    $.ajax({
        url: "/settings/privacy-policy/save",
        type: "POST",
        data: {
            "text": text
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success) {
                location.reload();
            }
            else {
                var message = "";
                for (var i = 0; i < response.errors.length; i++) {
                    message += response.errors[i] + "\n";
                }
                alert(message);
            }
        }
    });
}

function setPopupListeners() {
    $(".btn-save-privacy").click(function () {
        var text = CKEDITOR.instances['privacy-text'].getData();
        savePrivacy(text);
        $(".popup-frame").hide();
    });

    $(".btn-close-popup").click(function () {
        $(".popup-frame").hide();
    });

    var editor = CKEDITOR.replace('privacy-text');
}

$(document).ready(function () {
    var editBtn = $(".edit-privacy-btn");

    editBtn.click(function () {
        showEditPrivacyPopup();
    });
});