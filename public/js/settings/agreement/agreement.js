function showEditAgreementPopup() {
    var popupTitle = "Редактировать пользовательское соглашение";

    $.ajax({
        url: "/settings/agreement/edit",
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "agreement-popup";
            showCommonPopup(popupSelector, popupTitle, popupContent);
            setPopupListeners();
        }
    });
}

function saveAgreement(text) {
        $.ajax({
            url: "/settings/agreement/save",
            type: "POST",
            data: {
                "text": text
            },
            success: function (response) {
                response = JSON.parse(response);
                if (response.success) {
                    location.reload();
                }
                else {
                    var message = "";
                    for (var i = 0; i < response.errors.length; i++) {
                        message += response.errors[i] + "\n";
                    }
                    alert(message);
                }
            }
        });
}

function setPopupListeners() {
    $(".btn-save-agreement").click(function () {
        var text = CKEDITOR.instances['agreement-text'].getData();
            saveAgreement(text);
        $(".popup-frame").hide();
    });

    $(".btn-close-popup").click(function () {
        $(".popup-frame").hide();
    });

    var editor = CKEDITOR.replace('agreement-text');
}

$(document).ready(function () {
    var editBtn = $(".edit-agreement-btn");

    editBtn.click(function () {
        showEditAgreementPopup();
    });
});