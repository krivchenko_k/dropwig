var NP_DELIVERY_SERVICE = 2;
var table, addBtn, deleteBtn;
var printBtn;
var exportBtn;
var statusId;
var selectedElems = {
    "idArray": []
};

var dataTableOptions = {
    "aLengthMenu": [
        [10, 25, 50, 100, 1000, -1], [10, 25, 50, 100, 1000, "Все"]
    ],
    "iDisplayLength": 100,
    "language": {
        "url": "/lib/js/datatables/locale/russian.lang"
    },
    "columnDefs": [
        {
            orderable: false,
            targets: [0]
        },
        {
            width: 15,
            targets: 0
        },
        {
            width: 50,
            targets: [1]
        },
        {
            width: 20,
            targets: [2]
        },
        {
            width: 200,
            targets: [3]
        },
        {
            width: 130,
            targets: [4]
        },
        {
            width: 125,
            targets: [5]
        },
        {
            width: 50,
            targets: [6]
        },
        {
            width: 130,
            targets: [7]
        },
        {
            width: 140,
            targets: [8]
        },
        {
            width: 110,
            targets: [9, 10]
        },
        {
            width: 120,
            targets: [11]
        },
        {
            width: 200,
            targets: [12]
        },
        {
            width: 130,
            targets: [13]
        },
        {
            width: 130,
            targets: [14]
        },
        {
            width: 200,
            targets: [15 ]
        },
        {
            width: 141,
            targets: [16]
        },
        {
            width: 150,
            targets: [17]
        },
        {
            width: 141,
            targets: [18, 19]
        }
    ],
    "dom": 'f<"dt_scroll"t><"row dt_footer"<"col-xs-12"lip>>',
};


function loadOrders() {
    showTableLoader(table, 1);

    $.ajax({
        type: 'GET',
        url: '/invoice-printing/tableBody',
        success: function (tbodyView) {
            selectedElems.idArray = [];
            deleteBtn.hide();
            clearDatatable(table);
            table.find("tbody").append(tbodyView);
            setTimeout(function () {
                setListenersToOrdersTable();
            }, 100);
            initializeDatatable(table);
        }
    });

}

function deleteOrders() {
    if (!isset(selectedElems.idArray)) {
        return;
    }
    if (confirm("Вы действительно желаете удалить выбранные заказы?")) {
        deleteBtn.prop("disabled", true);
        printBtn.prop("disabled", true);
        $.ajax({
            type: 'DELETE',
            url: '/invoice-printing',
            async: false,
            data: {
                removalOrders: JSON.stringify(selectedElems.idArray)
            },
            success: function (response) {
                loadOrders();
                console.log(response);
            },
            error: function (response) {
                console.log(response);
            },
            always: function () {
                deleteBtn.prop("disabled", false);
                printBtn.prop("disabled", false);
            }
        });
    }
}


function setListenersToOrdersTable() {
    cbr_replace();

    $("tbody .cbr").change(function () {
        var tr = $(this).closest("tr");
        var orderId = $(tr).data("order-id");
        console.log(orderId);
        changeCbxState(tr, selectedElems, orderId);
        $('.selected-rows-count').html(selectedElems.idArray.length);
    });

    $('.dt-goods-quantity').each(function () {
        $(this).popover({
            html: true,
            trigger: 'hover',
            content: function () {
                var content = $(this).attr("data-popover-content");
                return $(content).html() || 'Информация о товарах отсутствует';
            }
        });
    });

    $('.dt-comment').each(function () {
        $(this).popover({
            html: true,
            trigger: 'hover',
            content: function () {
                var content = $(this).attr("data-popover-content");
                return $(content).html();
            }
        });
    });

    $('.btn-edit').on('click', function () {
        var orderId = $(this).attr('data-order-id');
        if (!isset(orderId)) {
            orderId = 0;
        }
        $.ajax({
            type: 'GET',
            url: '/invoice-printing/' + orderId,
            async: false,
            success: function (response) {
                showCommonPopup('order-edit-popup', 'Редактирование заказа', response);
            },
            error: function (response) {
                console.log(response);
            }
        });
    });
}

function setTabListeners() {
    $(".tab").click(function () {
        $(".tab.active").removeClass("active");
        $(this).addClass("active");
        statusId = $(this).data('order-status-id');
        loadOrders(statusId);
    })
}

var notificationOptions = {
    "closeButton": true,
    "onclick": null,
    "showDuration": "200",
    "hideDuration": "200",
    "timeOut": "60000",
    "extendedTimeOut": "60000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

function makePrintRequest() {
    if (selectedElems.idArray.length == 0) {
        return;
    }
    deleteBtn.prop("disabled", true);
    printBtn.prop("disabled", true);
    $.ajax({
        type: 'POST',
        url: 'invoice-printing',
        data: {
            orderIdArray: JSON.stringify(selectedElems.idArray)
        },
        success: function (response) {
            // console.log(response);
            response = response.slice(response.indexOf('{"success'));
            if (isValidJSON(response)) {
                response = JSON.parse(response);
                if (response.success) {
                    for (var i = response.data.errors.length-1; i >= 0 ; i--){
                        var errorText = "Заказ №"+ response.data.errors[i].orderId;
                        for (var k = response.data.errors[i].reasons.length - 1; k>=0; k--)
                            errorText += "<br>" + response.data.errors[i].reasons[k];
                        toastr.error(errorText, null, notificationOptions);
                    }

                    var infoText = "Успешно: " + response.data.successedCount + "<br>Не удалось создать: "+ response.data.failedCount;
                    toastr.info(infoText, null, commonNotificationOptions);

                    if (response.data.successedCount > 0) {
                        window.open(response.data.link);
                        printBtn.find(".selected-rows-count").text(0);
                        loadOrders();
                    }
                }
                else {
                    for (var i = 0; i < response.errors.length; i++) {
                        toastr.error(response.errors[i], null, notificationOptions);
                    }
                }
            }
            // else
            //     console.log(response);

            deleteBtn.prop("disabled", false);
            printBtn.prop("disabled", false);
        }
    });

}

function exportToExcel () {
    $.ajax({
        url: "/invoice-printing/export",
        type: "GET",
        success: function (response) {
            location.href = "/invoice-printing/export";
            $('.btn-export-excel>span').replaceWith("Экспорт удался");
        }
    });
}

$(document).ready(function () {

    table = $('.orders-table');
    deleteBtn = $('.btn-delete-orders');
    printBtn = $('.btn-print-invoices');
    exportBtn = $('.btn-export-excel');

    deleteBtn.on('click', function () {
        deleteOrders();
    });

    exportBtn.on('click', function () {
        exportToExcel();
    });

    printBtn.on('click', function () {
        makePrintRequest();
    });

    initializeDatatable(table);

    setTabListeners();

    setSelectAllCbxListener(table);

    loadOrders();

});
