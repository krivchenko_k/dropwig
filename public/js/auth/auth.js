$("body").addClass("login-page");

var validator;

function setLoginFormValidator() {
    validator = $("form#login").validate({
        rules: {
            login: {
                required: true,
                minlength: 4
            },

            password: {
                required: true
            }
        },

        messages: {
            login: {
                required: 'Введите логин',
                minlength: "Минимальная длинна - 4 символа"
            },
            password: {
                required: 'Введите пароль'
            }
        }
    });
}

function setFocus() {
    $("form#login .form-group:has(.form-control):first .form-control").focus();
}

function getFAQPopup() {
    $.ajax({
        url: "/login/faq",
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "faq-popup";
            var popupTitle = "Ответы на вопросы";
            showCommonPopup(popupSelector, popupTitle, popupContent);
        }
    });
}

function getContactsPopup() {
    $.ajax({
        url: "/login/contacts",
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "contacts-popup";
            var popupTitle = "Наши контакты";
            showCommonPopup(popupSelector, popupTitle, popupContent);
        }
    });
}

function getPolicyPopup() {
    $.ajax({
        url: "/login/policy",
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "policy-popup";
            var popupTitle = "Политика конфиденциальности";
            showCommonPopup(popupSelector, popupTitle, popupContent);
        }
    });
}

function getAgreementPopup() {
     $.ajax({
         url: "/login/agreement",
         type: "GET",
         success: function (popupContent) {
             var popupSelector = "agreement-popup";
             var popupTitle = "Пользовательское соглашение";
             showCommonPopup(popupSelector, popupTitle, popupContent);
         }
     });
}

$(document).ready(function () {
    setLoginFormValidator();

    setFocus();

    startFadeInEffect();

    $(".btn-login").click(function () {
        $('form#login').valid();
        if(validator.errorList.length != 0){
            return;
        }
    });

    $(".show-faq-btn").click(function () {
        getFAQPopup();
    });

    $(".show-contacts-btn").click(function () {
        getContactsPopup();
    });

    $(".show-policy").click(function () {
        getPolicyPopup();
    });

    $(".show-agreement").click(function () {
        getAgreementPopup();
    });
});
