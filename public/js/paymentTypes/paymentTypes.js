var selectedElems = {
    "idArray" : []
}

function getPaymentTypesPopup(paymentTypeId, popupTitle) {
    $.ajax({
        url: "/settings/payment-types/" + paymentTypeId,
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "payment-type-popup";
            showCommonPopup(popupSelector, popupTitle, popupContent);
            setPopupListeners();
        }
    })
}

function showAddPaymentTypePopup() {
    var popupTitle = "Добавление типа оплаты";
    getPaymentTypesPopup(0, popupTitle);
}

function showEditPaymentTypePopup(paymentTypeId) {
    var popupTitle = "Редактирование типа оплаты";
    getPaymentTypesPopup(paymentTypeId, popupTitle);
}


function setListenersToPaymentTypesTable() {
    $("tbody .cbr").change(function () {
        var tr = $(this).closest("tr");
        var selectedElemId = $(tr).data("payment-type-id");
        changeCbxState(tr, selectedElems, selectedElemId);
    });

    // table.find(".dblclickable").dblclick(function () {
    //     var paymentTypeId = $(this).closest("tr").data("payment-type-id");
    //     showEditPaymentTypePopup(paymentTypeId);
    // });

    table.find(".btn-edit").click(function () {
        var paymentTypeId = $(this).closest("tr").data("payment-type-id");
        showEditPaymentTypePopup(paymentTypeId);
    });

    cbr_replace();
}

function updatePaymentTypesTable() {
    $.ajax({
        url: "/settings/payment-types/update",
        success: function (response) {
            if (isValidJSON(response)) {
                response = JSON.parse(response);
                if (!response.success) {
                    for (var i = 0; i < response.errors.length; i++) {
                        toastr.error(response.errors[i], null, commonNotificationOptions);
                    }
                }
                return;
            }
            clearDatatable(table);
            table.find("tbody").append(response);
            setListenersToPaymentTypesTable();
            initializeDatatable(table);
        }
    });
    cancelSelection(table);
    selectedElems.idArray = [];    deleteBtn.hide();
}

function deletePaymentTypes() {
    if (confirm('Удаляем выделенные элементы 131?')) {
        $.ajax({
            url: "/settings/payment-types",
            type: "DELETE",
            data: {
                "paymentTypesId": JSON.stringify(selectedElems.idArray),
            },
            success: function (response) {
                response = JSON.parse(response);
                if (response.success)
                    updatePaymentTypesTable();
            }
        });
    }
}


function addPaymentType(name) {
    $.ajax({
        url: "/settings/payment-types",
        type: "POST",
        data: {
            "name": name
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                updatePaymentTypesTable();
            else {
                var message = "";
                for (var i = 0; i < response.errors.length; i++) {
                    message += response.errors[i] + "\n";
                }
                alert(message);
            }
        }
    });
}


function editPaymentType(id, name) {
    $.ajax({
        url: "/settings/payment-types",
        type: "PUT",
        data: {
            "id": id,
            "name": name,
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                updatePaymentTypesTable();
        }
    });
}


function setPopupListeners() {
    $(".btn-save").click(function () {
        var id = $(this).data("payment-type-id");
        var name = $(".payment-type-name-inp").val();

        if (!id)
            addPaymentType(name);
        else
            editPaymentType(id, name);
        $(".popup-frame").hide();
    });

    $(".btn-close-popup").click(function () {
        $(".popup-frame").hide();
    });
}

var table, addBtn, deleteBtn;

$(document).ready(function () {
    table = $(".payment-types-table");
    addBtn = $(".add-payment-type-btn");
    deleteBtn = $(".delete-payment-types-btn");

    addBtn.click(function () {
        showAddPaymentTypePopup();
    });

    deleteBtn.click(function () {
        deletePaymentTypes();
    });

    setListenersToPaymentTypesTable();

    setSelectAllCbxListener(table);

    initializeDatatable(table);
})