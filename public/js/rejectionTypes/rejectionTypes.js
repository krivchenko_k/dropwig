var selectedElems = {
    "idArray" : []
}

function getRejectionTypesPopup(goodsMeasureId, popupTitle) {
    $.ajax({
        url: "/settings/rejection-types/" + goodsMeasureId,
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "rejection-type-popup";
            showCommonPopup(popupSelector, popupTitle, popupContent);
            setPopupListeners();
        }
    })
}

function showAddRejectionTypePopup() {
    var popupTitle = "Добавление типа отказов";
    getRejectionTypesPopup(0, popupTitle);
}

function showEditRejectionTypePopup(goodsMeasureId) {
    var popupTitle = "Редактирование типа отказов";
    getRejectionTypesPopup(goodsMeasureId, popupTitle);
}

function setListenersToRejectionTypesTable() {
    $("tbody .cbr").change(function () {
        var tr = $(this).closest("tr");
        var selectedElemId = $(tr).data("rejection-type-id");
        changeCbxState(tr, selectedElems, selectedElemId);
    });

    // table.find(".dblclickable").dblclick(function () {
    //     var goodsMeasureId = $(this).closest("tr").data("rejection-type-id");
    //     showEditRejectionTypePopup(goodsMeasureId);
    // });

    table.find(".btn-edit").click(function () {
        var goodsMeasureId = $(this).closest("tr").data("rejection-type-id");
        showEditRejectionTypePopup(goodsMeasureId);
    });

    cbr_replace();
}

function updateRejectionTypesTable() {
    // cancelSelection(table);
    selectedElems.idArray = [];
    deleteBtn.hide();

    $.ajax({
        url: "/settings/rejection-types/update",
        success: function (response) {
            if (isValidJSON(response)) {
                response = JSON.parse(response);
                if (!response.success) {
                    for (var i = 0; i < response.errors.length; i++) {
                        toastr.error(response.errors[i], null, commonNotificationOptions);
                    }
                }
                return;
            }
            clearDatatable(table);
            table.find("tbody").append(response);
            setListenersToRejectionTypesTable();
            initializeDatatable(table);
        }
    });
}

function deleteRejectionTypes() {
    if (confirm('Удаляем выделенные элементы?')) {
        $.ajax({
            url: "/settings/rejection-types",
            type: "DELETE",
            data: {
                "rejectionTypesId": JSON.stringify(selectedElems.idArray),
            },
            success: function (response) {
                response = JSON.parse(response);
                if (response.success)
                    updateRejectionTypesTable();
            }
        });
    }
}


function addRejectionType(name) {
    $.ajax({
        url: "/settings/rejection-types",
        type: "POST",
        data: {
            "name": name
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                updateRejectionTypesTable();
            else {
                var message = "";
                for (var i = 0; i < response.errors.length; i++) {
                    message += response.errors[i] + "\n";
                }
                alert(message);
            }
        }
    });
}


function editRejectionType(id, name) {
    $.ajax({
        url: "/settings/rejection-types",
        type: "PUT",
        data: {
            "id": id,
            "name": name,
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                updateRejectionTypesTable();
        }
    });
}


function setPopupListeners() {
    $(".btn-save").click(function () {
        var id = $(this).data("rejection-type-id");
        var name = $(".rejection-type-name-inp").val();

        if (!id)
            addRejectionType(name);
        else
            editRejectionType(id, name);
        $(".popup-frame").hide();
    });

    $(".btn-close-popup").click(function () {
        $(".popup-frame").hide();
    });
}

var table, addBtn, deleteBtn;

$(document).ready(function () {
    table = $(".rejection-types-table");
    addBtn = $(".add-rejection-type-btn");
    deleteBtn = $(".delete-rejection-types-btn");

    addBtn.click(function () {
        showAddRejectionTypePopup();
    });

    deleteBtn.click(function () {
        deleteRejectionTypes();
    });

    setListenersToRejectionTypesTable();

    setSelectAllCbxListener(table);

    initializeDatatable(table);
})