var selectedElems = {
	"idArray": []
}

function getDeliveryServicesPopup(deliveryServiceId, popupTitle) {
	$.ajax({
		url: "/delivery-services/" + deliveryServiceId,
		type: "GET",
		success: function (popupContent) {
			var popupSelector = "delivery-service-popup";
			showCommonPopup(popupSelector, popupTitle, popupContent);
		}
	})
}

function showAddOrderStatusPopup() {
	var popupTitle = "Добавление статуса";
	getDeliveryServicesPopup(0, popupTitle);
}

function showEditOrderStatusPopup(deliveryServiceId) {
	var popupTitle = "Редактирование статуса";
	getDeliveryServicesPopup(deliveryServiceId, popupTitle);
}

function setListenersToDeliveryServicesTable() {
	$("tbody .cbr").change(function () {
		var tr = $(this).closest("tr");
		var selectedElemId = $(tr).data("delivery-service-id");
		changeCbxState(tr, selectedElems, selectedElemId);
	});

	// table.find(".dblclickable").dblclick(function () {
	// 	var deliveryServiceId = $(this).closest("tr").data("delivery-service-id");
	// 	showEditOrderStatusPopup(deliveryServiceId);
	// });

	table.find(".btn-edit").click(function () {
		var deliveryServiceId = $(this).closest("tr").data("delivery-service-id");
		showEditOrderStatusPopup(deliveryServiceId);
	});

	cbr_replace();
}

function updateDeliveryServicesTable() {
	$.ajax({
		url: "/delivery-services/update",
		success: function (response) {
			if (isValidJSON(response)) {
				response = JSON.parse(response);
				if (!response.success) {
					for (var i = 0; i < response.errors.length; i++) {
						toastr.error(response.errors[i], null, commonNotificationOptions);
					}
				}
				return
			}
			clearDatatable(table);
			table.find("tbody").append(response);
			setListenersToDeliveryServicesTable();
			initializeDatatable(table);
		}
	});
	cancelSelection(table);
	selectedElems.idArray = [];
	deleteBtn.hide();
}

function deleteDeliveryServices() {
	if (confirm('Удаляем выделенные элементы?')) {
		$.ajax({
			url: "/delivery-services",
			type: "DELETE",
			data: {
				"deliveryServicesId": JSON.stringify(selectedElems.idArray),
			},
			success: function (response) {
				response = JSON.parse(response);
				if (response.success)
					updateDeliveryServicesTable();
			}
		});
	}
}

var table, addBtn, deleteBtn;

$(document).ready(function () {
	table = $(".delivery-services-table");
	addBtn = $(".add-delivery-service-btn");
	deleteBtn = $(".delete-delivery-services-btn");

	addBtn.click(function () {
		showAddOrderStatusPopup();
	});

	deleteBtn.click(function () {
		deleteDeliveryServices();
	});

	setListenersToDeliveryServicesTable();

	setSelectAllCbxListener(table);

	initializeDatatable(table);
})