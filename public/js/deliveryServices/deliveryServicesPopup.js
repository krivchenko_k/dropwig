function addDeliveryService(name) {
    $.ajax({
        url: "/delivery-services",
        type: "POST",
        data: {
            "name": name
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                updateDeliveryServicesTable();
            else {
                var message = "";
                for (var i = 0; i < response.errors.length; i++) {
                    message += response.errors[i] + "\n";
                }
                alert(message);
            }
        }
    });
}

function editDeliveryService(id, name) {
    $.ajax({
        url: "/delivery-services",
        type: "PUT",
        data: {
            "id": id,
            "name": name,
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                updateDeliveryServicesTable();
        }
    });
}


function setPopupListeners() {
    $(".btn-save").click(function () {
        var id = $(this).data("delivery-service-id");
        var name = $(".delivery-service-name-inp").val();

        if (!id)
            addDeliveryService(name);
        else
            editDeliveryService(id, name);
        $(".popup-frame").hide();
    });

    $(".btn-close-popup").click(function () {
        $(".popup-frame").hide();
    });
}


$(document).ready(function () {
    setPopupListeners();
})