var selectedElems = {
    "idArray": []
}

function getGoodsCategoriesPopup(goodsCategoryId, popupTitle) {
    $.ajax({
        url: "/goods-categories/" + goodsCategoryId,
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "goods-category-popup";
            showCommonPopup(popupSelector, popupTitle, popupContent);
            setPopupListeners();
        }
    })
}

function showAddGoodsCategoryPopup() {
    var popupTitle = "Добавление категории товара";
    getGoodsCategoriesPopup(0, popupTitle);
}

function showEditGoodsCategoryPopup(goodsCategoryId) {
    var popupTitle = "Редактирование категории товара";
    getGoodsCategoriesPopup(goodsCategoryId, popupTitle);
}


function setListenersToGoodsCategoriesTable() {
    $("tbody .cbr").change(function () {
        var tr = $(this).closest("tr");
        var selectedElemId = $(tr).data("goods-category-id");
        changeCbxState(tr, selectedElems, selectedElemId);
    });

    // table.find(".dblclickable").dblclick(function () {
    //     var goodsCategoryId = $(this).closest("tr").data("goods-category-id");
    //     showEditGoodsCategoryPopup(goodsCategoryId);
    // });

    table.find(".btn-edit").click(function () {
        var goodsCategoryId = $(this).closest("tr").data("goods-category-id");
        showEditGoodsCategoryPopup(goodsCategoryId);
    });

    cbr_replace();
}

function updateGoodsCategoriesTable() {
    $.ajax({
        url: "/goods-categories/update",
        success: function (response) {
            if (isValidJSON(response)) {
                response = JSON.parse(response);
                if (!response.success) {
                    for (var i = 0; i < response.errors.length; i++) {
                        toastr.error(response.errors[i], null, commonNotificationOptions);
                    }
                }
                return
            }
            clearDatatable(table);
            table.find("tbody").append(response);
            setListenersToGoodsCategoriesTable();
            initializeDatatable(table);
        }
    });
    cancelSelection(table);
    selectedElems.idArray = [];
    deleteBtn.hide();
}

function deleteGoodsCategories() {
    if (confirm('Удаляем выделенные элементы 131?')) {
        $.ajax({
            url: "/goods-categories",
            type: "DELETE",
            data: {
                "goodsCategoriesId": JSON.stringify(selectedElems.idArray),
            },
            success: function (response) {
                response = JSON.parse(response);
                if (response.success)
                    updateGoodsCategoriesTable();
            }
        });
    }
}


function addGoodsCategory(name) {
    $.ajax({
        url: "/goods-categories",
        type: "POST",
        data: {
            "name": name
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                updateGoodsCategoriesTable();
            else {
                var message = "";
                for (var i = 0; i < response.errors.length; i++) {
                    message += response.errors[i] + "\n";
                }
                alert(message);
            }
        }
    });
}


function editGoodsCategory(id, name) {
    $.ajax({
        url: "/goods-categories",
        type: "PUT",
        data: {
            "id": id,
            "name": name,
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                updateGoodsCategoriesTable();
        }
    });
}


function setPopupListeners() {
    $(".btn-save").click(function () {
        var id = $(this).data("goods-category-id");
        var name = $(".goods-сategory-name-inp").val();

        if (!id)
            addGoodsCategory(name);
        else
            editGoodsCategory(id, name);
        $(".popup-frame").hide();
    });

    $(".btn-close-popup").click(function () {
        $(".popup-frame").hide();
    });
}

var table, addBtn, deleteBtn;

$(document).ready(function () {
    table = $(".goods-categories-table");
    addBtn = $(".add-goods-сategory-btn");
    deleteBtn = $(".delete-goods-сategories-btn");

    addBtn.click(function () {
        showAddGoodsCategoryPopup();
    });

    deleteBtn.click(function () {
        deleteGoodsCategories();
    });

    setListenersToGoodsCategoriesTable();

    setSelectAllCbxListener(table);

    initializeDatatable(table);
})