function addGoodsMeasure(name) {
    $.ajax({
        url: "/goods-measures",
        type: "POST",
        data: {
            "name": name
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                updateGoodsMeasuresTable();
            else {
                var message = "";
                for (var i = 0; i < response.errors.length; i++) {
                    message += response.errors[i] + "\n";
                }
                alert(message);
            }
        }
    });
}

function editGoodsMeasure(id, name) {
    $.ajax({
        url: "/goods-measures",
        type: "PUT",
        data: {
            "id": id,
            "name": name,
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                updateGoodsMeasuresTable();
        }
    });
}


function setPopupListeners() {
    $(".btn-save").click(function () {
        var id = $(this).data("goods-measure-id");
        var name = $(".goods-measure-name-inp").val();

        if (!id)
            addGoodsMeasure(name);
        else
            editGoodsMeasure(id, name);
        $(".popup-frame").hide();
    });

    $(".btn-close-popup").click(function () {
        $(".popup-frame").hide();
    });
}


$(document).ready(function () {
    setPopupListeners();
})