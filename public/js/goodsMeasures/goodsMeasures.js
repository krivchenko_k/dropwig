var selectedElems = {
	"idArray": []
}


function getGoodsMeasuresPopup(goodsMeasureId, popupTitle) {
	$.ajax({
		url: "/goods-measures/" + goodsMeasureId,
		type: "GET",
		success: function (popupContent) {
			var popupSelector = "goods-measure-popup";
			showCommonPopup(popupSelector, popupTitle, popupContent);
		}
	})
}

function showAddOrderStatusPopup() {
	var popupTitle = "Добавление единицы измерения";
	getGoodsMeasuresPopup(0, popupTitle);
}

function showEditOrderStatusPopup(goodsMeasureId) {
	var popupTitle = "Редактирование единицы измерения";
	getGoodsMeasuresPopup(goodsMeasureId, popupTitle);
}


function setListenersToGoodsMeasuresTable() {
	$("tbody .cbr").change(function () {
		var tr = $(this).closest("tr");
		var selectedElemId = $(tr).data("goods-measure-id");
		changeCbxState(tr, selectedElems, selectedElemId);
	});

	// table.find(".dblclickable").dblclick(function () {
	// 	var goodsMeasureId = $(this).closest("tr").data("goods-measure-id");
	// 	showEditOrderStatusPopup(goodsMeasureId);
	// });

	table.find(".btn-edit").click(function () {
		var goodsMeasureId = $(this).closest("tr").data("goods-measure-id");
		showEditOrderStatusPopup(goodsMeasureId);
	});

	cbr_replace();
}

function updateGoodsMeasuresTable() {
	$.ajax({
		url: "/goods-measures/update",
		success: function (response) {
			if (isValidJSON(response)) {
				response = JSON.parse(response);
				if (!response.success) {
					for (var i = 0; i < response.errors.length; i++) {
						toastr.error(response.errors[i], null, commonNotificationOptions);
					}
				}
				return
			}
			clearDatatable(table);
			table.find("tbody").append(response);
			setListenersToGoodsMeasuresTable();
			initializeDatatable(table);
		}
	});
	cancelSelection(table);
	selectedElems.idArray = [];
	deleteBtn.hide();
}

function deleteGoodsMeasures() {
	if (confirm('Удаляем выделенные элементы?')) {
		$.ajax({
			url: "/goods-measures",
			type: "DELETE",
			data: {
				"goodsMeasuresId": JSON.stringify(selectedElems.idArray),
			},
			success: function (response) {
				response = JSON.parse(response);
				if (response.success)
					updateGoodsMeasuresTable();
			}
		});
	}
}

var table, addBtn, deleteBtn;

$(document).ready(function () {
	table = $(".goods-measures-table");
	addBtn = $(".add-goods-measure-btn");
	deleteBtn = $(".delete-goods-measures-btn");

	addBtn.click(function () {
		showAddOrderStatusPopup();
	});

	deleteBtn.click(function () {
		deleteGoodsMeasures();
	});

	setListenersToGoodsMeasuresTable();

	setSelectAllCbxListener(table);

	initializeDatatable(table);
})