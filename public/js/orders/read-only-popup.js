var goodsTable;

function printTotalOnForm() {
    $('#order-total-price').text(calculateTotalPrice() + " грн.");
    $('#order-total-profit').text(calculateTotalProfit() + " грн.");
}

function initializeGoodsPopupListeners() {


    $('.btn-close-popup').off('click').on('click', function () {
        $(".close-popup-btn").click();
    });
}

function initializeGoodsTableListeners() {
    $('.goods-description').popover({
        trigger: 'hover',
        placement: 'right',
        content: function () {
            var content = $(this).attr("data-content");
            return content || 'Информация о товарах отсутствует';
        },
    });
}

function initializeRecommendedGoodsTableListeners() {
    $('.goods-description').popover({
        trigger: 'hover',
        placement: 'right',
        content: function () {
            var content = $(this).attr("data-content");
            return content || 'Информация о товарах отсутствует';
        },
    });
}

function loadRecommendedGoodsTable() {
    var goods = [];
    $('tr.goods-table-row', goodsTable).each(function (index) {
        goods.push($(this).attr('data-goods-id'));
    });
    if (goods.length == 0) {
        return;
    }
    $.ajax({
        type: 'GET',
        url: '/orders/recommended-goods',
        data: {
            goodsIdArray: JSON.stringify(goods)
        },
        success: function (tbodyView) {
            var tableBody = $('tbody', '.order-recommended-goods-table');
            tableBody.empty();
            tableBody.append(tbodyView);
            initializeRecommendedGoodsTableListeners();
        }
    });
}

function calculateTotalPrice() {
    var totalPrice = 0;
    $('.total-goods-price').each(function (index) {
        totalPrice += Number($(this).html());
    });
    return totalPrice;
}

function calculateTotalProfit() {
    var totalProfit = 0;
    $('.total-goods-profit').each(function (index) {
        totalProfit += Number($(this).html());
    });
    return totalProfit;
}

function recountOrderItemsNumbers() {
    $('.goods-number').each(function (index) {
        $(this).html(index + 1);
        if ((index + 1) % 2 == 0)
            $(this).closest("tr").addClass("even");
        else
            $(this).closest("tr").removeClass("even");

    });
}

function refreshGoodsTable() {

    recountOrderItemsNumbers();
    $('tr', goodsTable).each(function (index) {
        var dropPrice = Number($(this).find('.goods-drop-price').html());
        var markup = Number($(this).find('.goods-markup').val());
        var quantity = Number($(this).find('.goods-quantity').val());
        $(this).find('.goods-price').html(dropPrice + markup);
        $(this).find('.total-goods-price').html((dropPrice + markup) * quantity);
        $(this).find('.total-goods-profit').html(markup * quantity);
        printTotalOnForm();
    });
    initializeGoodsTableListeners();
}

$(document).ready(function () {

    goodsTable = $('.order-goods-table');
    initializeGoodsPopupListeners();
    refreshGoodsTable();
    initializeRecommendedGoodsTableListeners();

    printTotalOnForm();
});