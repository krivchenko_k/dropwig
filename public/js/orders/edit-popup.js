var goodsTable;

function printTotalOnForm() {
    $('#order-total-price').text(calculateTotalPrice() + " грн.");
    $('#order-total-profit').text(calculateTotalProfit() + " грн.");
}

function initializeGoodsPopupListeners() {

    $('.add-goods-btn').off('click').on('click', function () {

        var goodsId = $('#goods-list').val();
        var selectedItem = $('#goods-list :selected');
        var goodsName = selectedItem.text();
        var goodsDescription = selectedItem.data('description');
        var dropPrice = selectedItem.data('price');
        if (!isset(goodsId) || !isset(goodsName) || !isset(dropPrice)) {
            return;
        }
        var tbody = $('tbody', goodsTable);
        var tr = $('<tr class="goods-table-row" data-row-number="0" data-goods-id="' + goodsId + '"></tr>');
        tr.append('<td class="goods-number"></td>');
        var infoBtn = $('<button class="btn btn-edit goods-description"><i class="fa fa-question-circle"></i></button>');
        tr.append($('<td class="btn-td"></td>').append(infoBtn));
        tr.append('<td class="goods-name">' + goodsName + '</td>');
        tr.append('<td class="goods-drop-price">' + dropPrice + '</td>');
        tr.append('<td><input type="number" class="table-input goods-markup form-control" maxlength="6" min="0" value="0"></td>');
        tr.append('<td class="goods-price">0</td>');
        tr.append('<td><input type="number" class="table-input goods-quantity form-control" maxlength="6" min="0" value="0"></td>');
        tr.append('<td class="total-goods-price">0</td>');
        tr.append('<td class="total-goods-profit">0</td>');
        tr.append('<td><button class="btn btn-edit btn-order-item-delete"><i class="fa-trash"></i></button></td>');

        tbody.append(tr);

        infoBtn.popover({
            trigger: 'hover',
            content: function () {
                return goodsDescription;
            },
        });
        refreshGoodsTable();
        loadRecommendedGoodsTable();
    });

    $('.btn-save-order').off('click').on('click', function () {
        var orderId = $(this).data('order-id');
        if (isset(orderId))
            makeUpdateRequest(orderId);
        else
            makeInsertRequest();
    });

    $('.btn-moderate-order').off('click').on('click', function () {
        var orderId = $(this).data('order-id');
        var statusId = $('.tab.active').data('order-status-id');
        if (isset(orderId)) {
            var data = getOrderDataInJson(orderId, 2);
            if (!data) {
                return;
            }
            $.ajax({
                type: 'PUT',
                url: '/orders',
                async: false,
                data: {
                    updateData: data
                },
                success: function (response) {
                    response = JSON.parse(response);
                    if (response.success) {
                        var statusId = $('.tab.active').data('order-status-id');
                        loadOrders(statusId);
                        $(".close-popup-btn").click();
                    }
                    else {
                        for (var i = 0; i < response.errors.length; i++) {
                            toastr.error(response.errors[i], null, commonNotificationOptions);
                        }
                    }
                },
                error: function (response) {
                    console.log(response);
                }
            });
        }
        else {
            var data = getOrderDataInJson();
            if (!data) {
                return;
            }
            $.ajax({
                type: 'POST',
                url: '/orders',
                async: false,
                data: {
                    insertData: data
                },
                success: function (response) {
                    response = JSON.parse(response);
                    if (response.success) {
                        var statusId = $('.tab.active').data('order-status-id');
                        loadOrders(statusId);
                        $(".close-popup-btn").click();
                    }
                    else {
                        for (var i = response.errors.length; i>=0; i--) {
                            toastr.error(response.errors[i], null, commonNotificationOptions);
                        }
                    }
                },
                error: function (response) {
                    console.log(response);
                }
            });
            return;
        }
    });

    $('.btn-close-popup').off('click').on('click', function () {
        $(".close-popup-btn").click();
    });

    $('#order-delivery-service').off('change').on('change', function () {
        if ($(this).val() == NP_DELIVERY_SERVICE) {
            $('#order-city-holder').css('display', 'none');
            $('#order-office-holder').css('display', 'none');
            // $('#order-city').val('');
            // $('#order-office').val('');
            $('#order-np-city-holder').css('display', 'block');
            $('#order-np-office-holder').css('display', 'block');
        }
        else {
            $('#order-np-city-holder').css('display', 'none');
            $('#order-np-office-holder').css('display', 'none');
            $('#order-city-holder').css('display', 'block');
            $('#order-office-holder').css('display', 'block');
        }
    });

    $('#order-np-city').select2({
        allowClear: false
    });

    $('#order-np-office').select2({
        allowClear: false
    });

    $('#order-np-city').off('change').on('change', function () {
        var cityId = $(this).val();
        if (isset(cityId)) {
            $.ajax({
                type: 'GET',
                url: '/np/getWarehousesForCity/' + cityId,
                async: false,
                success: function (response) {
                    var data = JSON.parse(response);
                    console.log(data);
                    var NPOffice = $('#order-np-office');
                    NPOffice.empty();
                    NPOffice.append('<option value="">Выберите отделение</option>');
                    for (var key in data) {
                        NPOffice.append('<option value="' + data[key].id + '">' + data[key].name + '</option>');
                    }
                    NPOffice.select2({
                        allowClear: false
                    });
                },
                error: function (response) {
                    console.log(response);
                }
            });
        }
    });

    $(".close-popup-btn").on('click', function () {
        $('.popup-frame').css('display', 'none');
    });

    $('#goods-list').select2({
        placeholder: 'Выберите товар...',
    }).on('select2-open', function () {
        $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
    });

    $('#order-creator').select2({
        placeholder: 'Выберите партнера...',
    }).on('select2-open', function () {
        $(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
    });


    $("#order-payment-type").change(function () {
        if ($(this).val() == 2){
            console.log("true");
            $(".goods-markup").val(0);
            $(".goods-markup").prop("disabled", true);
            refreshGoodsTable();
        }
        else{
            $(".goods-markup").prop("disabled", false);
        }
    })
}

function initializeGoodsTableListeners() {
    $('.goods-markup').off('change').on('change', function () {
        var markup = Number($(this).val());
        var dropPrice = Number($(this).parents('tr').find('.goods-drop-price').html());
        var quantity = Number($(this).parents('tr').find('.goods-quantity').val());
        $(this).parents('tr').find('.goods-price').html(dropPrice + markup);
        $(this).parents('tr').find('.total-goods-price').html((dropPrice + markup) * quantity);
        $(this).parents('tr').find('.total-goods-profit').html(markup * quantity);
        printTotalOnForm();
    });

    $('.goods-markup').on('keyup', function () {
        $(this).trigger('change');
    });

    $('.goods-quantity').off('change').on('change', function () {
        var markup = Number($(this).parents('tr').find('.goods-markup').val());
        var dropPrice = Number($(this).parents('tr').find('.goods-drop-price').html());
        var quantity = Number($(this).val());
        $(this).parents('tr').find('.goods-price').html(dropPrice + markup);
        $(this).parents('tr').find('.total-goods-price').html((dropPrice + markup) * quantity);
        $(this).parents('tr').find('.total-goods-profit').html(markup * quantity);
        printTotalOnForm();
    });

    $('.goods-quantity').on('keyup', function () {
        $(this).trigger('change');
    });

    $('.btn-order-item-delete').off('click').on('click', function () {
        $(this).parent().parent().remove();
        printTotalOnForm();
        recountOrderItemsNumbers();
        loadRecommendedGoodsTable();
    });

    $('.btn-order-item-edit').off('click').on('click', function () {
        $('.btn-insert-goods').css('display', 'none');
        $('.btn-change-goods').css('display', 'block');
        var quantity = $(this).parents('tr').find('.goods-quantity').val();
        var markup = $(this).parents('tr').find('.goods-markup').val();
        var categoryId = $(this).parents('tr').attr('data-category-id');
        var goodsId = $(this).parents('tr').attr('data-goods-id');
        $('#add-goods-quantity').val(quantity);
        $('#add-goods-markup').val(markup);
        $('.add-goods-popup').css('display', 'flex');
        categorySelector.val(categoryId).trigger('change', goodsId);
        editRowNumber = $(this).parents('tr').attr('data-row-number');
        $('.btn-insert-goods').css('display', 'none');
        $('.btn-change-goods').css('display', 'block');
        additionalPopup.css('display', 'flex');
    });

    $('.goods-description').popover({
        trigger: 'hover',
        placement: 'right',
        content: function () {
            var content = $(this).attr("data-content");
            return content || 'Информация о товарах отсутствует';
        },
    });

}

function initializeRecommendedGoodsTableListeners() {

    $('.add-recommended-goods-btn').on('click', function () {

        var goodsId = $(this).data('goodsId');
        var tableRow = $(this).parent().parent();
        var goodsName = $('.goods-name', tableRow).html();
        var goodsDescription = $('.goods-description', tableRow).data('description');
        var dropPrice = $('.goods-price', tableRow).html();
        console.log(goodsId, goodsName, dropPrice);
        if (!isset(goodsId) || !isset(goodsName) || !isset(dropPrice)) {
            return;
        }
        var tbody = $('tbody', goodsTable);
        var tr = $('<tr class="goods-table-row" data-row-number="0" data-goods-id="' + goodsId + '"></tr>');
        tr.append('<td class="goods-number"></td>');
        var infoBtn = $('<button class="btn btn-edit goods-description"><i class="fa fa-question-circle"></i></button>');
        tr.append($('<td></td>').append(infoBtn));
        tr.append('<td class="goods-name">' + goodsName + '</td>');
        tr.append('<td class="goods-drop-price">' + dropPrice + '</td>');
        tr.append('<td><input type="number" class="table-input goods-markup form-control" maxlength="6" min="0" value="0"></td>');
        tr.append('<td class="goods-price">0</td>');
        tr.append('<td><input type="number" class="table-input goods-quantity form-control" maxlength="6" min="0" value="0"></td>');
        tr.append('<td class="total-goods-price">0</td>');
        tr.append('<td class="total-goods-profit">0</td>');
        tr.append('<td><button class="btn btn-edit btn-order-item-delete"><i class="fa-trash"></i></button></td>');

        tbody.append(tr);

        infoBtn.popover({
            trigger: 'hover',
            content: function () {
                return goodsDescription;
            },
        });
        refreshGoodsTable();

    });

    $('.goods-description').popover({
        trigger: 'hover',
        placement: 'right',
        content: function () {
            var content = $(this).attr("data-content");
            return content || 'Информация о товарах отсутствует';
        },
    });
}

function loadRecommendedGoodsTable() {
    var goods = [];
    $('tr.goods-table-row', goodsTable).each(function (index) {
        goods.push($(this).attr('data-goods-id'));
    });
    if (goods.length == 0) {
        return;
    }
    $.ajax({
        type: 'GET',
        url: '/orders/recommended-goods',
        data: {
            goodsIdArray: JSON.stringify(goods)
        },
        success: function (tbodyView) {
            var tableBody = $('tbody', '.order-recommended-goods-table');
            tableBody.empty();
            tableBody.append(tbodyView);
            initializeRecommendedGoodsTableListeners();
        }
    });
}

function calculateTotalPrice() {
    var totalPrice = 0;
    $('.total-goods-price').each(function (index) {
        totalPrice += Number($(this).html());
    });
    return totalPrice;
}

function calculateTotalProfit() {
    var totalProfit = 0;
    $('.total-goods-profit').each(function (index) {
        totalProfit += Number($(this).html());
    });
    return totalProfit;
}

function recountOrderItemsNumbers() {
    $('.goods-number').each(function (index) {
        $(this).html(index + 1);
        if ((index + 1) % 2 == 0)
            $(this).closest("tr").addClass("even");
        else
            $(this).closest("tr").removeClass("even");

    });
}

function refreshGoodsTable() {

    recountOrderItemsNumbers();
    $('tr', goodsTable).each(function (index) {
        var dropPrice = Number($(this).find('.goods-drop-price').html());
        var markup = Number($(this).find('.goods-markup').val());
        var quantity = Number($(this).find('.goods-quantity').val());
        $(this).find('.goods-price').html(dropPrice + markup);
        $(this).find('.total-goods-price').html((dropPrice + markup) * quantity);
        $(this).find('.total-goods-profit').html(markup * quantity);
        printTotalOnForm();
    });
    initializeGoodsTableListeners();
}

function getOrderDataInJson(orderId, statusId) {
    if (!isset(orderId)) {
        orderId = '';
    }
    var data = {};
    data['orderData'] = {};
    data['goods'] = [];
    var goods = {};
    $('tr.goods-table-row', goodsTable).each(function (index) {
        goods.rowNumber = $(this).attr('data-row-number');
        goods.categoryId = $(this).attr('data-category-id');
        goods.goodsId = $(this).attr('data-goods-id');
        goods.markup = $('.goods-markup', $(this)).val();
        goods.quantity = $('.goods-quantity', $(this)).val();
        data['goods'].push(goods);
        goods = {};
    });

    // if (data['goods'].length <= 0) {
    //     return null;
    // }
    data['orderData'].partnerId = $('#order-creator').val();
    data['orderData'].orderId = orderId;
    data['orderData'].orderStatusId = isset($('#order-status').val()) ? $('#order-status').val() : ((isset(statusId)) ? statusId : $('.active').data('order-status-id')); //(isset($('.active').data('order-status-id')) ? $('.active').data('order-status-id') : 2)
    data['orderData'].clientFirstName = $('#client-first-name').val();
    data['orderData'].clientMiddleName = $('#client-middle-name').val();
    data['orderData'].clientLastName = $('#client-last-name').val();
    data['orderData'].orderPhone = $('#order-phone').val();
    data['orderData'].paymentTypeId = $('#order-payment-type').val();
    data['orderData'].deliveryServiceId = $('#order-delivery-service').val();
    data['orderData'].orderCity = $('#order-city').val();
    data['orderData'].orderNPCityId = $('#order-np-city').val();
    data['orderData'].orderAddress = $('#order-office').val();
    data['orderData'].orderNPAddressId = $('#order-np-office').val();
    data['orderData'].orderComment = $('#order-comment').val();
    data['orderData'].invoice = $('#order-invoice').val();

    return JSON.stringify(data);
}

function makeUpdateRequest(orderId) {
    if (!isset(orderId)) {
        return;
    }
    var data = getOrderDataInJson(orderId);                     
    if (!data) {
        return;
    }
    $.ajax({
        type: 'PUT',
        url: '/orders',
        async: false,
        data: {
            updateData: data
        },
        success: function (response) {
            response = JSON.parse(response);            
            if (response.success) {
                var statusId = $('.tab.active').data('order-status-id');
                loadOrders(statusId);
                $(".close-popup-btn").click();
            }
            else {
                for (var i = 0; i < response.errors.length; i++) {
                    toastr.error(response.errors[i], null, commonNotificationOptions);
                }
            }
        },
        error: function (response) {
            console.log(response);
        }
    });
}

function makeInsertRequest() {
    var data = getOrderDataInJson();
    console.log(data);
    if (!data) {
        return;
    }
    $.ajax({
        type: 'POST',
        url: '/orders',
        async: false,
        data: {
            insertData: data
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success) {
                var statusId = $('.tab.active').data('order-status-id');
                loadOrders(statusId);
                $(".close-popup-btn").click();
            }
            else {
                for (var i = response.errors.length; i>=0; i--) {
                    toastr.error(response.errors[i], null, commonNotificationOptions);
                }
            }
        },
        error: function (response) {
            console.log(response);
        }
    });
}

$(document).ready(function () {

    goodsTable = $('.order-goods-table');
    initializeGoodsPopupListeners();
    refreshGoodsTable();
    initializeRecommendedGoodsTableListeners();

    printTotalOnForm();
});