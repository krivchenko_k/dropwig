var NP_DELIVERY_SERVICE = 2;
var table, addBtn, deleteBtn;
var dataTable;
var statusId;
var selectedElems = {
    "idArray": []
};

var dataTableOptions = {
    "aLengthMenu": [
        [10, 25, 50, 100, 1000, -1], [10, 25, 50, 100, 1000, "Все"]
    ],
    "iDisplayLength": 25,
    "language": {
        "url": "/lib/js/datatables/locale/russian.lang"
    },
    "columnDefs": [
        {
            orderable: false,
            targets: [0]
        },
        {
            width: 15,
            targets: 0
        },
        {
            width: 50,
            targets: [1]
        },
        {
            width: 20,
            targets: [2]
        },
        {
            width: 200,
            targets: [3]
        },
        {
            width: 130,
            targets: [4]
        },
        {
            width: 125,
            targets: [5]
        },
        {
            width: 50,
            targets: [6]
        },
        {
            width: 130,
            targets: [7]
        },
        {
            width: 140,
            targets: [8]
        },
        {
            width: 110,
            targets: [9, 10]
        },
        {
            width: 120,
            targets: [11]
        },
        {
            width: 200,
            targets: [12]
        },
        {
            width: 130,
            targets: [13]
        },
        {
            width: 130,
            targets: [14]
        },
        {
            width: 200,
            targets: [15 ]
        },
        {
            width: 141,
            targets: [16]
        },
        {
            width: 150,
            targets: [17]
        },
        {
            width: 141,
            targets: [18, 19]
        }
    ],
    "dom": 'f<"dt_scroll"t><"row dt_footer"<"col-xs-12"lip>>'
};


function loadOrders(statusId) {
    showTableLoader(table, 1);

    if (!isset(statusId)) {
        statusId = $('.active').data('order-status-id');
        if(!isset(statusId)) {
            return;
        }
    }

    var startDate = $('#datepicker-1').val();
    var endDate = $('#datepicker-2').val();
    var paymentTypeId = $('#payment-type-filter').val();
    var deliveryServiceId = $('#delivery-service-filter').val();
    var partnerId = $('#partner-filter').val();

    $.ajax({
        type: 'GET',
        url: '/orders/tableBody',
        data: {
            statusId: statusId,
            startDate: startDate,
            endDate: endDate,
            paymentTypeId: paymentTypeId,
            deliveryServiceId: deliveryServiceId,
            partnerId: partnerId
        },
        success: function (tbodyView) {
            selectedElems.idArray = [];
            deleteBtn.hide();

            clearDatatable(table);
            table.find("tbody").empty().append(tbodyView);
            setTimeout(function () {
            }, 100);
            setListenersToOrdersTable();
            initializeDatatable(table);
        }
    });

}

function addOrder() {
    $.ajax({
        type: 'GET',
        url: '/orders/0',
        async: false,
        success: function (response) {
            showCommonPopup('order-edit-popup', "Новый заказ", response);
        },
        error: function (response) {
            console.log(response);
        }
    });
}

function deleteOrders() {
    if (!isset(selectedElems.idArray)) {
        return;
    }
    if (confirm('Удаляем выделенные элементы?')) {
        $.ajax({
            type: 'DELETE',
            url: '/orders',
            async: false,
            data: {
                removalOrders: JSON.stringify(selectedElems.idArray)
            },
            success: function (response) {
                var statusId = $('.tab.active').data('order-status-id');
                loadOrders(statusId);
            },
            error: function (response) {
                console.log(response);
            }
        });
    }
}

function setListenersToOrdersTable() {
    cbr_replace();

    $("tbody .cbr").change(function () {
        var tr = $(this).closest("tr");
        var orderId = $(tr).data("order-id");
        console.log(orderId);
        changeCbxState(tr, selectedElems, orderId);
    });

    $('.dt-goods-quantity').each(function () {
        $(this).popover({
            html: true,
            trigger: 'hover',
            content: function () {
                var content = $(this).attr("data-popover-content");
                return $(content).html() || 'Информация о товарах отсутствует';
            }
        });
    });

    $('.dt-comment').each(function () {
        $(this).popover({
            html: true,
            trigger: 'hover',
            content: function () {
                var content = $(this).attr("data-popover-content");
                return $(content).html();
            }
        });
    });

    $('.btn-edit').on('click', function () {
        var orderId = $(this).attr('data-order-id');
        if (!isset(orderId)) {
            orderId = 0;
        }
        $.ajax({
            type: 'GET',
            url: '/orders/' + orderId,
            async: false,
            success: function (response) {
                showCommonPopup('order-edit-popup', "Заказ №"+orderId, response);
            },
            error: function (response) {
                console.log(response);
            }
        });
    });
}

function setTabListeners() {
    $(".tab").click(function () {
        $(".tab.active").removeClass("active");
        $(this).addClass("active");
        statusId = $(this).data('order-status-id');
        loadOrders(statusId);
    })
}

$(document).ready(function () {

    table = $('.orders-table');
    addBtn = $('.btn-add-order');
    deleteBtn = $('.btn-delete-orders');

    addBtn.click(function () {
        addOrder();
    });

    deleteBtn.on('click', function () {
        deleteOrders();
    });

    $('#datepicker-1').datepicker({
        format: 'dd.mm.yyyy'
    });
    $('#datepicker-2').datepicker({
        format: 'dd.mm.yyyy'
    });

    $("#partner-filter").select2();
    $('.accept-filters-btn').on('click', function() {
        loadOrders();
    });

    $('.date-range-btn').on('click', function () {
        var startDate = $(this).data('date-start');
        var endDate = $(this).data('date-end');
        $("#datepicker-1").val(startDate);
        $("#datepicker-2").val(endDate);
        loadOrders();
    });

    $('.clear-filters-btn').on('click', function () {
        $('#datepicker-1').val('');
        $('#datepicker-2').val('');
        $('#payment-type-filter').val(0);
        $('#delivery-service-filter').val(0);
        $('#partner-filter').val(0);
        loadOrders();
    });

    initializeDatatable(table);

    setTabListeners();

    setSelectAllCbxListener(table);

    loadOrders(1);

});
