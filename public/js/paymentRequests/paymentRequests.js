function initializePaymentRequestsTableListeners() {

    $('.confirm-payment').on('click', function () {
        var requestId = $(this).data('request-id');
        if (!isset(requestId)) {
            return;
        }
        $.ajax({
            type: 'GET',
            url: '/payment-requests/confirm/' + requestId,
            success: function (response) {
                showCommonPopup('confirm-popup', 'Подтверждение выплаты', response);
                initializeConfirmPopupListeners();
            },
            error: function (response) {
                console.log(response);
            }
        });
    });

    $('.decline-payment').on('click', function () {
        var requestId = $(this).data('request-id');
        if (!isset(requestId)) {
            return;
        }
        $.ajax({
            type: 'GET',
            url: '/payment-requests/decline/' + requestId,
            success: function (response) {
                showCommonPopup('decline-popup', 'Отказ в выплате', response);
                initializeDeclinePopupListeners();
            },
            error: function (response) {
                console.log(response);
            }
        });
    });
}


function updatePaymentRequestsTable() {
    var dateFrom = $("#datepicker-1").val();
    var dateTo = $("#datepicker-2").val();
    console.log(dateFrom, dateTo);
    $.ajax({
        type: 'GET',
        url: '/payment-requests/tableBody',
        async: false,
        data: {
            dateFrom: dateFrom,
            dateTo: dateTo
        },
        success: function (tbodyView) {
            clearDatatable(table);
            var tableBody = $('tbody', table);
            tableBody.append(tbodyView);
            initializePaymentRequestsTableListeners();
            initializeDatatable(table, null, null, [[ 0, "desc" ]]);
        },
        error: function (response) {
            console.log(response);
        }
    });
}

function initializeConfirmPopupListeners() {

    $(".upload-image-btn").change(function (event) {
        var target = event.target || window.event.srcElement,
            files = target.files;
        if (FileReader && files && files.length) {
            var fr = new FileReader();
            fr.onload = function () {
                $(".image-preview img").attr("src", fr.result);
                $(".image-preview-text").hide();
            };
            fr.readAsDataURL(files[0]);
        }
    });

    $('.btn-confirm').on('click', function () {
        var requestId = $(this).data('request-id');
        if (!isset(requestId)) {
            return;
        }
        var formData = new FormData();
        formData.append('data', JSON.stringify({'requestId': requestId}));
        if (isset($('.upload-image-btn').prop('files'))) {
            formData.append('file', $('.upload-image-btn').prop('files')[0]);
        }
        $.ajax({
            type: 'POST',
            url: '/payment-requests/confirm',
            dataType: 'text',
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            success: function (data) {
                var response = JSON.parse(data);
                if (response.success) {
                    updatePaymentRequestsTable();
                    $(".popup-frame").hide();
                }
                else {
                    for (var i = response.errors.length; i>=0; i--) {
                        toastr.error(response.errors[i], null, commonNotificationOptions);
                    }
                }

            }
        });
    });

    $('.btn-close-popup').on('click', function () {
        $(".popup-frame").hide();
    });

    $('.close-popup-btn').on('click', function () {
        $(".popup-frame").hide();
    });
}

function initializeDeclinePopupListeners() {

    $('.btn-close').on('click', function () {
        $(".popup-frame").hide();
    });

    $('.close-popup-btn').on('click', function () {
        $(".popup-frame").hide();
    });

    $('.sel-decline-reason').on('change', function () {
        if ($(this).val() == -1) {
            $('.decline-reason').removeAttr('disabled');
        }
        else {
            $('.decline-reason').prop('disabled', 'true');
        }
    });

    $('.btn-decline-request').on('click', function () {

        var data = {};
        data.requestId = $(this).data('request-id');
        data.reasonId = $('.sel-decline-reason').val();
        data.reasonText = $('.decline-reason').val();
        data.reasonText = isset(data.reasonText) ? data.reasonText : '';

        if (!isset(data.requestId) || !isset(data.reasonId)) {
            return;
        }
        $.ajax({
            type: 'POST',
            url: '/payment-requests/decline',
            data: {
                rejectionData: JSON.stringify(data)
            },
            success: function (response) {
                var data = JSON.parse(response);
                if (data.success) {
                    updatePaymentRequestsTable();
                    $(".popup-frame").hide();
                }
                else {
                    //showing errors
                }
            }
        });
    });
}

var table;
$(document).ready(function () {

    $("#datepicker-1").datepicker().on('changeDate', function (e) {
        updatePaymentRequestsTable();
    });

    $("#datepicker-2").datepicker().on('changeDate', function (e) {
        updatePaymentRequestsTable();
    });

    $('.clear-filters-btn').on('click', function () {
        $('#datepicker-1').val('');
        $('#datepicker-2').val('');
        updatePaymentRequestsTable();
    });

    table = $('.payment-requests-table');

    $('.date-range-btn').on('click', function () {
        var startDate = $(this).data('date-start');
        var endDate = $(this).data('date-end');
        $("#datepicker-1").val(startDate);
        $("#datepicker-2").val(endDate);

        updatePaymentRequestsTable();
    });

    updatePaymentRequestsTable();
});