var selectedElems = {
    "idArray": []
};
var table, confirmBtn;
var orderPrice = 0;


function changeCbxState(tr, selectedItems, selectetItemId) {
    var checkbox = $(tr).find(".cbr-replaced");


    if (isCbrCbxCheched(checkbox)) {
        if ($.inArray(selectetItemId, selectedItems.idArray) === -1) {
            selectedItems.idArray.push(selectetItemId);
            orderPrice += Number($(tr).find('.order-markup').html());
        }
    }
    else {
        if ($.inArray(selectetItemId, selectedItems.idArray) !== -1) {
            selectedItems.idArray.splice(selectedItems.idArray.indexOf(selectetItemId), 1);
            orderPrice -= Number($(tr).find('.order-markup').html());
        }
    }

    if (selectedItems.idArray.length != 0) {
        confirmBtn.find(".selected-rows-count").text(selectedItems.idArray.length);
        confirmBtn.show();
    }
    else {
        confirmBtn.hide();
        orderPrice = 0;
        $('#payment-price').html(orderPrice);
    }

    $('#payment-summary').html(orderPrice);
    checkNeedActivateSelectAllCbx();
}

function initializePaymentRequestsTableListeners() {

    $('.dt-goods-quantity').each(function () {
        $(this).popover({
            html: true,
            trigger: 'hover',
            content: function () {
                var content = $(this).attr("data-popover-content");
                return $(content).html() || 'Информация о товарах отсутствует';
            }
        });
    });

    $("tbody .cbr").change(function () {
        var tr = $(this).closest("tr");
        var orderId = $(tr).data("order-id");
        changeCbxState(tr, selectedElems, orderId);
    });

    cbr_replace();
}


function updatePaymentRequestsTable() {
    $.ajax({
        type: 'GET',
        url: '/payment-requests/tableBody',
        success: function(tbodyView) {
            clearDatatable(table);
            table.find("tbody").append(tbodyView);
            initializePaymentRequestsTableListeners();
            initializeDatatable(table);
        },
        error: function (response) {
            console.log(response);
        }
    });
}


$(document).ready(function () {

    table = $('.payment-requests-table');
    confirmBtn = $('.btn-make-request');

    confirmBtn.on('click', function () {
        if (!selectedElems.idArray.length) {
            alert('Необходимо выбрать заказы из списка');
            return;
        }
        var paymentPrice = Number($('#payment-summary').html());
        var balance = Number($('#amount').html());
        if (paymentPrice > balance) {
            swal({
                    title: "Превышение лимита выплаты",
                    text: "Сумма, заявленная к выплате (" + paymentPrice + " грн), превышает Ваш баланс (" + balance + " грн).\nСумма к выплате будет приравнена к сумме Вашего баланса и составит " + balance +  " грн.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-info",
                    cancelButtonText: "Закрыть",
                    confirmButtonText: "Подтвердить выплату",
                },
                function() {
                    $.ajax({
                        type: 'PUT',
                        url: '/payment-requests',
                        data: {
                            idArray: JSON.stringify(selectedElems.idArray),
                        },
                        success: function (response) {      
                            var data = JSON.parse(response);
                            if (data.success) {
                                updatePaymentRequestsTable();
                                confirmBtn.hide();
                                $('#payment-price').html(0);
                            }
                            else {
                                //showing errors
                            }
                        }
                    });
                }
            );
        }
        else {
            $.ajax({
                type: 'PUT',
                url: '/payment-requests',
                data: {
                    idArray: JSON.stringify(selectedElems.idArray),
                },
                success: function (response) {                  
                    var data = JSON.parse(response);
                    if (data.success) {
                        updatePaymentRequestsTable();
                        confirmBtn.hide();
                        $('#payment-price').html(0);
                    }
                    else {
                        //showing errors
                    }
                }
            });
        }

    });

    initializeDatatable(table);

    updatePaymentRequestsTable();
});