
function makeDeclineRequest(requestId) {

    var data = {};
    data.requestId = requestId;
    data.reasonId = $('.sel-decline-reason').val();
    data.reasonText = $('.decline-reason').val();
    data.reasonText = isset(data.reasonText) ? data.reasonText : null;

    if (!isset(data.requestId) || !isset(data.reasonId)) {
        return;
    }

    $.ajax({
        type: 'POST',
        url: '/payment-requests/decline',
        data: {
            rejectionData: JSON.stringify(data)
        },
        success: function (response) {
            console.log(response); 
            var data = JSON.parse(response);
            if (data.success) {
                updatePaymentRequestsTable();
                $(".popup-frame").hide();
            }
            else {
                for (var i = 0; i < response.errors.length; i++) {
                    toastr.error(response.errors[i], null, errorNotificationOptions);
                }
            }
        },
        error: function(response) {
            console.log(response);
        }
    });
}

function initializeDeclinePopupListeners() {

    $('.btn-decline').off('click').on('click', function() {
        var requestId = $(this).data('request-id');
        makeDeclineRequest(requestId);
    });

    $('.btn-close').off('click').on('click', function() {
        $(".close-popup-btn").click();
    });

    $('.sel-decline-reason').on('change', function () {
        if ($(this).val() == -1) {
            $('.decline-reason').removeAttr('disabled');
        }
        else {
            $('.decline-reason').prop('disabled', 'true');
        }
    });

}



$(document).ready(function() {

    initializeDeclinePopupListeners();
});