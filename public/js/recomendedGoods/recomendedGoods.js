

function getRecomendedGoodsPopup(goodsId, popupTitle) {
    $.ajax({
        url: "/recomended-goods/" + goodsId,
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "recomended-goods-popup";
            showCommonPopup(popupSelector, popupTitle, popupContent);
            setPopupListeners();
            setRecomendedTablePopupListeners();
            initializeDatatable(innerTable, [0]);
        }
    })
}

function setListenersToRecomendedGoodsTable() {
    table.find(".btn-edit").click(function () {
        var goodsId = $(this).closest("tr").data("goods-id");
        getRecomendedGoodsPopup(goodsId, "Список рекомендованых товаров");
    });
}

function deleteRecomendedGoods(recordId) {
    console.log(recordId);
    $.ajax({
        url: "/recomended-goods",
        type: "DELETE",
        data: {
            "recordId": recordId
        },
        success: function (response) {
            updateRecomendedGoodsTable();
        }
    });
}

function addRecomendedGoods(data) {
    console.log(data);
    $.ajax({
        url: "/recomended-goods",
        type: "POST",
        data: data,
        success: function (response) {
            response = JSON.parse(response);
            if (response.success){
                updateRecomendedGoodsTable();
            }
            else {
                for (var i = 0; i < response.errors.length; i++) {
                    toastr.error(response.errors[i], null, commonNotificationOptions);
                }
            }
        }
    });
}


function setPopupListeners() {
    innerTable = $(".recomended-goods-table-popup");

    $(".btn-close-popup").click(function () {
        $(".popup-frame").hide();
    });

    $(".recomended-goods-id-select").select2({
        placeholder: 'Выберите товар',
        allowClear: true
    });

    $(".add-recomended-goods").click(function () {
        var data = {
            goodsId : $(".goods-info").data("goods-id"),
            recomendedGoodsId : $(".recomended-goods-id-select").select2("data").id,
        }
        addRecomendedGoods(data);
    });
}

function setRecomendedTablePopupListeners() {
    $(".delete-recomended-goods-btn").click(function () {
        var recordId = $(this).data("recomended-goods-record-id");
        deleteRecomendedGoods(recordId);
    });
}

function updateRecomendedGoodsTable() {
    $.ajax({
        url: "/recomended-goods/popupTableBody",
        data: {
            goodsId : $(".goods-info").data("goods-id")
        },
        success: function (response) {
            if (isValidJSON(response)) {
                response = JSON.parse(response);
                if (!response.success) {
                    for (var i = 0; i < response.errors.length; i++) {
                        toastr.error(response.errors[i], null, commonNotificationOptions);
                    }
                }
                return;
            }
            clearDatatable(innerTable);
            $(".recomended-goods-table-popup tbody").append(response);
            setRecomendedTablePopupListeners();
            initializeDatatable(innerTable, [0]);
        }
    })
}

var table, innerTable, addBtn, deleteBtn;

$(document).ready(function () {
    table = $(".recomended-goods-table");

    setListenersToRecomendedGoodsTable();

    setSelectAllCbxListener(table);

    initializeDatatable(table, false);
})