var selectedElems = {
    "idArray": []
}


function getFAQPopup(FAQId, popupTitle) {
    $.ajax({
        url: "/settings/faq/" + FAQId,
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "faq-popup";
            showCommonPopup(popupSelector, popupTitle, popupContent);
            setPopupListeners();
        }
    })
}

function showAddFAQPopup() {
    var popupTitle = "Добавление вопроса-ответа";
    getFAQPopup(0, popupTitle);
}

function showEditFAQPopup(FAQId) {
    var popupTitle = "Редактирование вопроса-ответа";
    getFAQPopup(FAQId, popupTitle);
}

function setListenersToFAQTable() {
    $("tbody .cbr").change(function () {
        var tr = $(this).closest("tr");
        var selectedElemId = $(tr).data("faq-id");
        changeCbxState(tr, selectedElems, selectedElemId);
    });

    // table.find(".dblclickable").dblclick(function () {
    //     var FAQId = $(this).closest("tr").data("faq-id");
    //     showEditFAQPopup(FAQId);
    // });

    table.find(".btn-edit").click(function () {
        var FAQId = $(this).closest("tr").data("faq-id");
        showEditFAQPopup(FAQId);
    });

    cbr_replace();
}

function updateFAQTable() {
    $.ajax({
        url: "/settings/faq/update",
        success: function (response) {
            if (isValidJSON(response)) {
                response = JSON.parse(response);
                if (!response.success) {
                    for (var i = 0; i < response.errors.length; i++) {
                        toastr.error(response.errors[i], null, commonNotificationOptions);
                    }
                }
                return
            }
            clearDatatable(table);
            table.find("tbody").append(response);
            setListenersToFAQTable();
            initializeDatatable(table);
        }
    });
    cancelSelection(table);
    selectedElems.idArray = [];
    deleteBtn.hide();
}

function deleteFAQ() {
    if (confirm('Удаляем выделенные элементы?')) {
        $.ajax({
            url: "/settings/faq",
            type: "DELETE",
            data: {
                "FAQId": JSON.stringify(selectedElems.idArray),
            },
            success: function (response) {
                response = JSON.parse(response);
                if (response.success)
                    updateFAQTable();
            }
        });
    }
}


function addFAQ(question, answer) {
    $.ajax({
        url: "/settings/faq",
        type: "POST",
        data: {
            "question": question,
            "answer": answer
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success) {
                location.reload();
            }
            else
            {
                alert(response.errors[0]);
            }
        }
    });
}


function editFAQ(id, question, answer) {
    $.ajax({
        url: "/settings/faq",
        type: "PUT",
        data: {
            "id": id,
            "question": question,
            "answer": answer
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                location.reload();
        }
    });
}


function setPopupListeners() {
    $(".btn-save").click(function () {
        var id = $(this).data("faq-id");
        var question = $(".faq-question-inp").val();
        var answer = CKEDITOR.instances['answer'].getData();

        if (!id)
            addFAQ(question, answer);
        else
            editFAQ(id, question, answer);
        $(".popup-frame").hide();
    });

    $(".btn-close-popup").click(function () {
        $(".popup-frame").hide();
    });

    var editor = CKEDITOR.replace('answer');
}

var table, addBtn, deleteBtn;

$(document).ready(function () {
    table = $(".faq-table");
    addBtn = $(".add-faq-btn");
    deleteBtn = $(".delete-faq-btn");

    addBtn.click(function () {
        showAddFAQPopup();
    });

    deleteBtn.click(function () {
        deleteFAQ();
    });

    setListenersToFAQTable();

    setSelectAllCbxListener(table);

    initializeDatatable(table);
})