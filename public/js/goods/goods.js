var selectedElems = {
    "idArray": []
}


function getGoodsPopup(goodsId, popupTitle) {
    $.ajax({
        url: "/goods/" + goodsId,
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "goods-popup";
            showCommonPopup(popupSelector, popupTitle, popupContent);
            setPopupListeners();
        }
    })
}

function showAddGoodsPopup() {
    var popupTitle = "Добавление товара";
    getGoodsPopup(0, popupTitle);
}

function showEditGoodsPopup(goodsId) {
    var popupTitle = "Редактирование товара";
    getGoodsPopup(goodsId, popupTitle);
}

function changeGoodsActivity(goodsId) {
    $.ajax({
        url: "/goods/"+goodsId+"/changeActivity",
        type: "PUT",
        success: function (response) {
            console.log(response);
        }
    });
}

function setListenersToGoodsTable() {
    $("tbody .cbr").change(function () {
        var tr = $(this).closest("tr");
        var selectedElemId = $(tr).data("goods-id");
        changeCbxState(tr, selectedElems, selectedElemId);
    });

    // table.find(".dblclickable").dblclick(function () {
    //     var goodsId = $(this).closest("tr").data("goods-id");
    //     showEditGoodsPopup(goodsId);
    // });

    table.find(".btn-edit").click(function () {
        var goodsId = $(this).closest("tr").data("goods-id");
        showEditGoodsPopup(goodsId);
    });

    $(".change-goods-activity-cbx").change(function () {
        var goodsId = $(this).closest("tr").data("goods-id");
        changeGoodsActivity(goodsId);
    })

    cbr_replace();
}

function updateGoodsTable() {
    showTableLoader(table, 10);
    $.ajax({
        url: "/goods/update",
        success: function (response) {
            if (isValidJSON(response)) {
                response = JSON.parse(response);
                if (!response.success) {
                    for (var i = 0; i < response.errors.length; i++) {
                        toastr.error(response.errors[i], null, commonNotificationOptions);
                    }
                }
                return
            }
            clearDatatable(table);
            table.find("tbody").append(response);
            setListenersToGoodsTable();
            initializeDatatable(table, null, null, null, 100);
        }
    });
    selectedElems.idArray = [];
    deleteBtn.hide();
}

function deleteGoods() {

    if (confirm('Удаляем выделенные элементы?')) {
        $.ajax({
            url: "/goods",
            type: "DELETE",
            data: {
                "goodsIdList": JSON.stringify(selectedElems.idArray),
            },
            success: function (response) {
                response = JSON.parse(response);
                if (response.success)
                    updateGoodsTable();
            }
        });
    }
}

function addGoods(formData) {
    $.ajax({
        url: "/goods",
        type: "POST",
        dataType: 'text',
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        success: function (response) {
            console.log(response);
            response = JSON.parse(response);
            if (response.success){
                $(".popup-frame").hide();
                updateGoodsTable();
            }
            else {
                for (var i = 0; i < response.errors.length; i++) {
                    toastr.error(response.errors[i], null, commonNotificationOptions);
                }
            }
        }
    });
}

function editGoods(formData) {
    $.ajax({
        url: "/goods/update",
        type: "POST",
        dataType: 'text',
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        success: function (response) {
            response = JSON.parse(response);
            console.log(response);
            if (response.success){
                $(".popup-frame").hide();
                updateGoodsTable();
            }
            else {
                for (var i = 0; i < response.errors.length; i++) {
                    toastr.error(response.errors[i], null, commonNotificationOptions);
                }
            }
        }
    });
}


function setPopupListeners() {
    $(".btn-save").click(function () {
        $("html").css("overflow", "initial");
        var data = {
            id: $(this).data("goods-id"),
            name: $("#goods-name").val(),
            categoryId: $("#goods-category").val(),
            price: $("#goods-price").val(),
            weight: $("#goods-weight").val(),
            length: $("#goods-length").val(),
            width: $("#goods-width").val(),
            height: $("#goods-height").val(),
            volume: $("#goods-volume").val(),
            measureId: $("#measure-id").val(),
            availability: $("#goods-availability").val(),
            availabilityTypeId: $("#goods-availability-type").val(),
            link: $("#goods-link").val(),
            shortDesc: $("#goods-short-desc").val(),
            fullDesc: $("#goods-full-desc").val(),
        };

        var formData = new FormData();
        formData.append('data', JSON.stringify(data));
        if (isset($('.upload-image-btn').prop('files'))) {
            formData.append('file', $('.upload-image-btn').prop('files')[0]);
        }

        if (!isset(data.id))
            addGoods(formData);
        else
            editGoods(formData);
    });

    $(".btn-close-popup").click(function () {
        $(".popup-frame").hide();
    });

    $(".upload-image-btn").change(function (event) {
        var target = event.target || window.event.srcElement,
            files = target.files;
        if (FileReader && files && files.length) {
            var fr = new FileReader();
            fr.onload = function () {
                $(".image-preview img").attr("src", fr.result);
                $(".image-preview-text").hide();
            }
            fr.readAsDataURL(files[0]);
        }
    });
}

var table, addBtn, deleteBtn;

$(document).ready(function () {
    table = $(".goods-table");
    addBtn = $(".add-goods-btn");
    deleteBtn = $(".delete-goods-btn");

    addBtn.click(function () {
        showAddGoodsPopup();
    });

    deleteBtn.click(function () {
        deleteGoods();
    });

    setListenersToGoodsTable();

    setSelectAllCbxListener(table);

    updateGoodsTable();
})