var selectedElems = {
    "idArray": []
}


function showGoodsPopup(goodsId, popupTitle) {
    $.ajax({
        url: "/catalog/" + goodsId,
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "goods-popup";
            showCommonPopup(popupSelector, "Информация о товаре", popupContent);
            setPopupListeners();
        }
    })
}

function setListenersToGoodsTable() {

    table.find(".btn-view").click(function () {
        var goodsId = $(this).closest("tr").data("goods-id");
        showGoodsPopup(goodsId);
    });

    cbr_replace();
}

function updateGoodsTable() {
    showTableLoader(table, 10);
    $.ajax({
        url: "/catalog/update",
        success: function (response) {
            if (isValidJSON(response)) {
                response = JSON.parse(response);
                if (!response.success) {
                    for (var i = 0; i < response.errors.length; i++) {
                        toastr.error(response.errors[i], null, commonNotificationOptions);
                    }
                }
                return
            }
            clearDatatable(table);
            table.find("tbody").append(response);
            setListenersToGoodsTable();
            initializeDatatable(table);
        }
    });
}


function setPopupListeners() {
    $(".btn-close-popup").click(function () {
        $(".popup-frame").hide();
    });
}

var table, addBtn, deleteBtn;

$(document).ready(function () {
    table = $(".goods-table");

    setListenersToGoodsTable();

    setSelectAllCbxListener(table);

    updateGoodsTable();
})