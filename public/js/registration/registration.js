$("body").addClass("login-page");

function comparePasswords() {
    var originalPassword = $('.partner-password').val();
    var confirmationPassword = $('.partner-password-confirm').val();
    if (isset(originalPassword) && isset(confirmationPassword)) {
        if (originalPassword != confirmationPassword) {
            $('.error-block').css('display', 'block');
            $('.error-block').html('The passwords are not equal');
            return false;
        }
        else {
            $('.error-block').css('display', 'none');
            return true;
        }
    }
    else {
        $('.error-block').css('display', 'block');
        $('.error-block').html('The passwords are not equal');
        return false;
    }
}

var $validator;

function goBack() {
    window.history.back();
}

function regPageFadeId() {
    $(".reg-page-container").addClass("in");
}

var opts = {
    "closeButton": true,
    "debug": false,
    "positionClass": "toast-top-full-width",
    "onclick": null,
    "showDuration": "500",
    "hideDuration": "500",
    "timeOut": "10000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};


$(document).ready(function () {

    regPageFadeId();

    $validator = $('.reg-form').validate({
        rules: {
            login: {
                required: true,
                minlength: 5,
                maxlength: 20,
                pattern: /^[a-zA-Z][a-zA-Z0-9-_\.]+$/
            },
            firstName: {
                required: true,
                minlength: 2,
                maxlength: 20,
                pattern: /^[a-zA-Zа-яА-Я ]+$/
            },

            lastName: {
                required: true,
                minlength: 2,
                maxlength: 20,
                pattern: /^[a-zA-Zа-яА-Я ]+$/
            },

            phone: {
                required: true,
                // minlength: 13
            },

            email: {
                required: true,
                email: true,
                maxlength: 100
            },

            // card: {
            //     required: true
            // },
            //
            // cardOwner: {
            //     required: true,
            //     maxlength: 50,
            //     pattern: /^[a-zA-Zа-яА-Я ]+$/
            // },

            nickname: {
                required: true,
                minlength: 4,
                maxlength: 20,
                pattern: /^[a-zA-Z0-9]+$/
            },

            password: {
                required: true,
                minlength: 6,
                maxlength: 50,
                pattern: /^[a-zA-Zа-яА-Я0-9]+$/    ///^[A-zА-яЁё0-9]+$/
            },

            passwordConfirm: {
                required: true,
                minlength: 6,
                maxlength: 50,
            },
        },
        messages: {
            login: {
                required: 'Введите логин',
                minlength: "Не менее 5 символов",
                maxlength: "Не более 20 символов",
                pattern: "Неверный формат"
            },
            firstName: {
                required: 'Введите имя',
                minlength: "Не менее 2 символов",
                maxlength: "Не более 20 символов",
                pattern: "Неверный формат"
            },

            lastName: {
                required: 'Введите фамилию',
                minlength: "Не менее 2 символов",
                maxlength: "Не более 20 символов",
                pattern: "Неверный формат"
            },

            phone: {
                required: 'Введите телефон',
                // minlength: "Не менее 10 символов",
            },

            email: {
                required: 'Введите E-mail',
                maxlength: "Не более 100 символов",
                email: "Неверный формат"
            },

            // card: {
            //     required: 'Введите номер карты',
            // },
            //
            // cardOwner: {
            //     required: 'Введите имя владельца карты',
            //     maxlength: "Не более 50 символов",
            //     pattern: "Неверный формат"
            // },

            nickname: {
                required: 'Введите ник',
                minlength: "Не менее 4 символов",
                maxlength: "Не более 20 символов",
                pattern: "Неверный формат"
            },

            password: {
                required: 'Введите пароль',
                minlength: "Не менее 6 символов",
                maxlength: "Не более 30 символов",
                pattern: "Неверный формат"
            },

            passwordConfirm: {
                required: 'Введите подтверждение пароля',
            },
        }
    });


    $('.partner-phone').mask('(000)000-00-00', {placeholder: "(0__)000-00-00"});
    $('.partner-card').mask('0000 0000 0000 0000', {placeholder: "____ ____ ____ ____"});

    $('.reg-submit-button').on('click', function () {

        if (!$(".agree-with-policy").prop("checked")){
            toastr.error(null, "Необходимо ознакомится и принять политику конфиденциальности", commonNotificationOptions);
            return;
        }

        $('.reg-form').valid();

        if ($validator.errorList.length != 0) {
            return;
        }

        if (!comparePasswords()) {
            return;
        }

        var data = {
            login: $(".partner-login").val(),
            password: $(".partner-password").val(),
            passwordConfirm: $(".partner-password-confirm").val(),
            firstName: $(".partner-name").val(),
            lastName: $(".partner-surname").val(),
            phone: $(".partner-phone").val(),
            email: $(".partner-email").val(),
            contacts: $(".partner-contacts").val(),
            card: $(".partner-card").val(),
            cardOwner: $(".partner-card-owner").val(),
            isCardOwnerSelf: $(".partner-card-owner-self").prop("checked"),
            nickname: $(".partner-nickname").val(),
            contacts: $(".partner-contacts").val(),
        }

        $.ajax({
            url: "/registration",
            type: "POST",
            data: {
                data: JSON.stringify(data),
            },
            success: function (response) {
                if (isValidJSON(response)) {
                    response = JSON.parse(response);
                    if (!response.success) {
                        for (var i = 0; i < response.errors.length; i++) {
                            toastr.error(response.errors[i], null, commonNotificationOptions);
                        }
                    } else {
                        // $(this).prop("disabled", true);
                        // $(".form-control").prop("disabled", true);
                        //
                        // toastr.success("Данные для подтверждения регистрации отправлены Вам на почту. Вы будете автоматически перенаправлены на страницу авторизации", "Благодарим за регистрацию!", opts);
                        //
                        // setTimeout(function () {
                        //     window.location.href = "/login";
                        //
                        // }, 10000);
                    }
                }
                else{
                    history.pushState(3, "Авторизация", "/login");
                    $("body").html(response);
                }
            }
        })

    });


    $('.partner-card-owner-self').on('change', function () {
        if ($(this).prop('checked')) {
            var fullName = '';
            if (isset($('.partner-name').val()) && isset($('.partner-surname').val())) {
                fullName = $('.partner-name').val() + ' ' + $('.partner-surname').val();
            }
            $('.partner-card-owner').val(fullName);
            $('.partner-card-owner').prop('readonly', 'true');
        }
        else {
            $('.partner-card-owner').removeAttr('readonly');
        }
    });

    $('.partner-name').on('keyup', function () {
        if (isset($(this).val())) {
            if ($('.partner-card-owner-self').prop('checked')) {
                $('.partner-card-owner').val((isset($('.partner-surname').val()) ? $('.partner-surname').val() : '') + ' ' + $(this).val());
            }
        }
        if (isset($(this).val()) && isset($('.partner-surname').val())) {
            $('.partner-card-owner-self').removeAttr('disabled');
            $('.card-owner-label').removeAttr('readonly');
        }
        else {
            $('.partner-card-owner-self').prop('disabled', 'true');
            $('.card-owner-label').prop('disabled', 'true');
        }
    });

    $('.partner-name').change(function () {
        $(this).keyup();
    })

    $('.partner-surname').on('keyup', function (event) {
        if (isset($(this).val())) {
            if ($('.partner-card-owner-self').prop('checked')) {
                $('.partner-card-owner').val($(this).val() + ' ' + (isset($('.partner-name').val()) ? $('.partner-name').val() : ''));
            }
        }
        if (isset($(this).val()) && isset($('.partner-name').val())) {
            $('.partner-card-owner-self').removeAttr('disabled');
            $('.card-owner-label').removeAttr('readonly');
        }
        else {
            $('.partner-card-owner-self').prop('disabled', 'true');
            $('.card-owner-label').prop('disabled', 'true');
        }
    });

    $('.partner-surname').change(function () {
        $(this).keyup();
    })

    $('.partner-password-confirm').on('keyup', function (event) {
        var originalPassword = $('.partner-password').val();
        var confirmationPassword = $(this).val();
        if (isset(originalPassword) && isset(confirmationPassword)) {
            if (originalPassword != confirmationPassword) {
                $('.error-block').css('display', 'block');
                $('.error-block').html('Пароли не совпадают');
                return;
            }
            else {
                $('.error-block').css('display', 'none');
            }
        }
        else {
            $('.error-block').css('display', 'block');
            $('.error-block').html('Пароли не совпадают');
            return;
        }
    });

    $(".back-btn").click(function () {
        window.location = "/login";
    });

    $(".show-policy").click(function () {
        getPolicyPopup();
    });

    cbr_replace();
});

function getPolicyPopup() {
    $.ajax({
        url: "/login/policy",
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "policy-popup";
            var popupTitle = "Политика конфиденциальности";
            showCommonPopup(popupSelector, popupTitle, popupContent);
        }
    });
}

