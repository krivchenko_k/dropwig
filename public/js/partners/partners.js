$("body").addClass("login-page");

var selectedElems = {
    "idArray": []
}

var dataTableOptions = {
    "aLengthMenu": [
        [10, 25, 50, 100, 1000, -1], [10, 25, 50, 100, 1000, "Все"]
    ],
    "iDisplayLength": 10,
    "language": {
        "url": "/lib/js/datatables/locale/russian.lang"
    },
    "columnDefs": [
        {
            orderable: false,
            targets: [0,2]
        }
    ]
};

function getPartnersPopup(rejectionReasonId, popupTitle) {
    $.ajax({
        url: "/partners/" + rejectionReasonId,
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "partner-popup";
            showCommonPopup(popupSelector, popupTitle, popupContent);
            setPopupListeners();
        }
    })
}

function showAddPartnerPopup() {
    var popupTitle = "Добавление партнёра";
    getPartnersPopup(0, popupTitle);
}

function showEditPartnerPopup(rejectionReasonId) {
    var popupTitle = "Редактирование данных партнёра";
    getPartnersPopup(rejectionReasonId, popupTitle);
}

function setListenersToPartnersTable() {
    $("tbody .cbr").change(function () {
        var tr = $(this).closest("tr");
        var selectedElemId = $(tr).data("partner-id");
        changeCbxState(tr, selectedElems, selectedElemId);
    });

    // table.find(".dblclickable").dblclick(function () {
    //     var rejectionReasonId = $(this).closest("tr").data("partner-id");
    //     showEditPartnerPopup(rejectionReasonId);
    // });

    table.find(".btn-edit").click(function () {
        var rejectionReasonId = $(this).closest("tr").data("partner-id");
        showEditPartnerPopup(rejectionReasonId);
    });

    cbr_replace();
}

function updatePartnersTable() {
    $.ajax({
        url: "/partners/tableBody",
        success: function (response) {
            if (isValidJSON(response)) {
                response = JSON.parse(response);
                if (!response.success) {
                    for (var i = 0; i < response.errors.length; i++) {
                        toastr.error(response.errors[i], null, commonNotificationOptions);
                    }
                }
                return
            }
            clearDatatable(table);
            table.find("tbody").append(response);
            setListenersToPartnersTable();
            initializeDatatable(table);
        }
    });
    cancelSelection(table);
    selectedElems.idArray = [];
    deleteBtn.hide();
}

function deletePartners() {
    if (confirm('Удаляем выделенные элементы?')) {
        $.ajax({
            url: "/partners",
            type: "DELETE",
            data: {
                "idList": JSON.stringify(selectedElems.idArray),
            },
            success: function (response) {
                response = JSON.parse(response);
                if (response.success)
                    updatePartnersTable();
            }
        });
    }
}


function addPartner(data) {
    $.ajax({
        type: 'POST',
        url: '/partners',
        data: data,
        success: function (response) {
            response = JSON.parse(response);
            if (response.success) {
                $(".popup-frame").hide();
                updatePartnersTable();
            }
            else {
                for (var i = 0; i < response.errors.length; i++) {
                    toastr.error(response.errors[i], null, commonNotificationOptions);
                }
            }
        }
    });
}


function editPartner(data) {
    $.ajax({
        url: "/partners",
        type: "PUT",
        data: data,
        success: function (response) {
            console.l
            response = JSON.parse(response);
            if (response.success) {
                $(".popup-frame").hide();
                updatePartnersTable();
            }
            else {
                for (var i = 0; i < response.errors.length; i++) {
                    toastr.error(response.errors[i], null, commonNotificationOptions);
                }
            }
        }
    });
}


function setPopupListeners() {
    $(".btn-close-popup").click(function () {
        $(".popup-frame").hide();
    });

    $(".change-password-btn").click(function () {
        $(".partner-password").toggle();
        $(".partner-password-label").toggle();
        if ($(".partner-password").css("display") != "none")
            $(this).text("Отмена");
        else
            $(this).text("Изменить пароль");
    });
    
    cbr_replace();

    var $validator;

    $validator = $('.edit-partner-form').validate({
        rules: {
            login: {
                required: true,
                minlength: 4,
                maxlength: 20,
                pattern: /^[a-zA-Z][a-zA-Z0-9-_\.]+$/
            },
            password: {
                required: true,
                minlength: 5,
                maxlength: 50
            },
            firstName: {
                required: true,
                minlength: 2,
                maxlength: 20,
                pattern: /^[a-zA-Zа-яА-Я ]+$/
            },

            lastName: {
                required: true,
                minlength: 2,
                maxlength: 20,
                pattern: /^[a-zA-Zа-яА-Я ]+$/
            },

            phone: {
                required: true,
                // minlength: 13
            },

            email: {
                required: true,
                email: true,
                maxlength: 100
            },

            // card: {
            //     required: true
            // },
            //
            // cardOwner: {
            //     required: true,
            //     maxlength: 50,
            //     pattern: /^[a-zA-Zа-яА-Я ]+$/
            // },

            nickname: {
                required: true,
                minlength: 4,
                maxlength: 20,
                pattern: /^[a-zA-Z][a-zA-Z0-9-_\.]+$/
            },


        },
        messages: {
            login: {
                required: 'Введите логин',
                minlength: "Не менее 5 символов",
                maxlength: "Не более 20 символов",
                pattern: "Неверный формат"
            },
            password: {
                required: 'Введите пароль',
                minlength: "Не менее 5 символов",
                maxlength: "Не более 30 символов"
            },
            firstName: {
                required: 'Введите имя',
                minlength: "Не менее 2 символов",
                maxlength: "Не более 20 символов",
                pattern: "Неверный формат"
            },

            lastName: {
                required: 'Введите фамилию',
                minlength: "Не менее 2 символов",
                maxlength: "Не более 20 символов",
                pattern: "Неверный формат"
            },

            phone: {
                required: 'Введите телефон',
                // minlength: "Не менее 10 символов",
            },

            email: {
                required: 'Введите E-mail',
                maxlength: "Не более 100 символов",
                email: "Неверный формат"
            },

            // card: {
            //     required: 'Введите номер карты',
            // },
            //
            // cardOwner: {
            //     required: 'Введите имя владельца карты',
            //     maxlength: "Не более 50 символов",
            //     pattern: "Неверный формат"
            // },

            nickname: {
                required: 'Введите ник',
                minlength: "Не менее 4 символов",
                maxlength: "Не более 20 символов",
                pattern: "Неверный формат"
            },

        }
    });

    $('.partner-phone').mask('38(000)000-00-00', {placeholder: "38(0__)000-00-00"});
    $('.partner-card').mask('0000 0000 0000 0000', {placeholder: "____ ____ ____ ____"});

    $('.btn-close').on('click', function () {
        $(".popup-frame").hide();
    });

    $('.btn-save').on('click', function () {

        $('.edit-partner-form').valid();

        if ($validator.errorList.length != 0) {
            return;
        }
        
        var partnerId = $('.btn-save').data('partner-id');
        var data = {
            login: $('.partner-login').val(),
            password: $('.partner-password').val(),
            email: $('.partner-email').val(),
            nickname: $('.partner-nickname').val(),
            statusId: $('.partner-status :selected').val(),
            firstName: $('.partner-name').val(),
            lastName: $('.partner-surname').val(),
            phone: $('.partner-phone').val(),
            contacts: $('.partner-contacts').val(),
            card: $('.partner-card').val(),
            cardOwner: $('.partner-card-owner').val(),
            isCardOwnerSelf: $('.partner-card-owner-self').prop('checked')
        };


        if(data["password"] === "") {
            delete data["password"];
        }

        if (!isset(partnerId)) {
            addPartner(data);
        }
        else {
            data.partnerId = partnerId;
            editPartner(data);
        }
    });

    $('.partner-card-owner-self').on('change', function () {
        if ($(this).prop('checked')) {
            var fullName = '';
            if (isset($('.partner-name').val()) && isset($('.partner-surname').val())) {
                fullName = $('.partner-name').val() + ' ' + $('.partner-surname').val();
            }
            $('.partner-card-owner').val(fullName);
            $('.partner-card-owner').prop('readonly', 'true');
        }
        else {
            $('.partner-card-owner').removeAttr('readonly');
        }
    });

    $('.partner-name').on('keyup', function () {
        if (isset($(this).val())) {
            if ($('.partner-card-owner-self').prop('checked')) {
                $('.partner-card-owner').val($(this).val() + ' ' + (isset($('.partner-surname').val()) ? $('.partner-surname').val() : ''));
            }
        }
        if (isset($(this).val()) && isset($('.partner-surname').val())) {
            $('.partner-card-owner-self').removeAttr('disabled');
            $('.card-owner-label').removeAttr('readonly');
        }
        else {
            $('.partner-card-owner-self').prop('disabled', 'true');
            $('.card-owner-label').prop('disabled', 'true');
        }
    });

    $('.partner-surname').on('keyup', function () {
        if (isset($(this).val())) {
            if ($('.partner-card-owner-self').prop('checked')) {
                $('.partner-card-owner').val((isset($('.partner-name').val()) ? $('.partner-name').val() : '') + ' ' + $(this).val());
            }
        }
        if (isset($(this).val()) && isset($('.partner-name').val())) {
            $('.partner-card-owner-self').removeAttr('disabled');
            $('.card-owner-label').removeAttr('readonly');
        }
        else {
            $('.partner-card-owner-self').prop('disabled', 'true');
            $('.card-owner-label').prop('disabled', 'true');
        }
    });
}

var table, addBtn, deleteBtn;

$(document).ready(function () {
    table = $(".partners-table");
    addBtn = $(".add-partner-btn");
    deleteBtn = $(".delete-partners-btn");

    addBtn.click(function () {
        showAddPartnerPopup();
    });

    deleteBtn.click(function () {
        deletePartners();
    });

    setListenersToPartnersTable();

    setSelectAllCbxListener(table);

    initializeDatatable(table);
})