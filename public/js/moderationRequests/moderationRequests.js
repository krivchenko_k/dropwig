var NP_DELIVERY_SERVICE = 2;
var table, addBtn, deleteBtn;
var statusId;
var selectedElems = {
    "idArray": []
};

var dataTableOptions = {
    "aLengthMenu": [
        [10, 25, 50, 100, 1000, -1], [10, 25, 50, 100, 1000, "Все"]
    ],
    "iDisplayLength": 100,
    "language": {
        "url": "/lib/js/datatables/locale/russian.lang"
    },
    "columnDefs": [
        {
            orderable: false,
            targets: [0]
        },
        {
            width: 15,
            targets: 0
        },
        {
            width: 50,
            targets: [1]
        },
        {
            width: 20,
            targets: [2]
        },
        {
            width: 200,
            targets: [3]
        },
        {
            width: 130,
            targets: [4]
        },
        {
            width: 125,
            targets: [5]
        },
        {
            width: 50,
            targets: [6]
        },
        {
            width: 130,
            targets: [7]
        },
        {
            width: 140,
            targets: [8]
        },
        {
            width: 110,
            targets: [9, 10]
        },
        {
            width: 120,
            targets: [11]
        },
        {
            width: 200,
            targets: [12]
        },
        {
            width: 130,
            targets: [13]
        },
        {
            width: 130,
            targets: [14]
        },
        {
            width: 200,
            targets: [15 ]
        },
        {
            width: 141,
            targets: [16]
        },
        {
            width: 150,
            targets: [17]
        },
        {
            width: 141,
            targets: [18, 19]
        }
    ],
    "dom": 'f<"dt_scroll"t><"row dt_footer"<"col-xs-12"lip>>',
};


function loadOrdersForModeration() {
    showTableLoader(table, 1);

    $.ajax({
        type: 'GET',
        url: '/moderation-requests/tableBody',
        success: function (tbodyView) {
            selectedElems.idArray = [];
            deleteBtn.hide();
            clearDatatable(table);
            table.find("tbody").append(tbodyView);
            setTimeout(function () {
                setListenersToOrdersTable();
            }, 100)
            initializeDatatable(table, [0, 2]);
        }
    });

}

function deleteOrders() {
    if (!isset(selectedElems.idArray)) {
        return;
    }
    if (confirm('Удаляем выделенные элементы?')) {
        $.ajax({
            type: 'DELETE',
            url: '/orders',
            async: false,
            data: {
                removalOrders: JSON.stringify(selectedElems.idArray)
            },
            success: function (response) {
                loadOrdersForModeration();
            },
            error: function (response) {
                console.log(response);
            }
        });
    }
}

function setListenersToOrdersTable() {
    cbr_replace();

    $("tbody .cbr").change(function () {
        var tr = $(this).closest("tr");
        var orderId = $(tr).data("order-id");
        console.log(orderId);
        changeCbxState(tr, selectedElems, orderId);
    });

    $('.dt-goods-quantity').each(function () {
        $(this).popover({
            html: true,
            trigger: 'hover',
            content: function () {
                var content = $(this).attr("data-popover-content");
                return $(content).html() || 'Информация о товарах отсутствует';
            }
        });
    });

    $('.dt-comment').each(function () {
        $(this).popover({
            html: true,
            trigger: 'hover',
            content: function () {
                var content = $(this).attr("data-popover-content");
                return $(content).html();
            }
        });
    });

    $('.btn-edit').on('click', function () {
        var orderId = $(this).attr('data-order-id');
        if (!isset(orderId)) {
            orderId = 0;
        }
        $.ajax({
            type: 'GET',
            url: '/moderation-requests/' + orderId,
            async: false,
            success: function (response) {
                showCommonPopup('order-edit-popup', 'Редактирование заказа на модерации', response);
            },
            error: function (response) {
                console.log(response);
            }
        });
    });
}


function getRegistrationDeclinePopup(orderId) {
    if(!isset(orderId)) {
        return;
    }
    $.ajax({
        type: 'GET',
        url: "/moderation-requests/declinePopup",
        data: {
            orderId: orderId
        },
        success: function (content) {
            var title = "Отказ в оформлении заказа";
            showCommonPopup("decline-popup", title, content);
        }
    });
}


$(document).ready(function () {

    table = $('.orders-table');
    deleteBtn = $('.btn-delete-orders');

    deleteBtn.on('click', function () {
        deleteOrders();
    });

    initializeDatatable(table);

    setSelectAllCbxListener(table);

    loadOrdersForModeration();
});
