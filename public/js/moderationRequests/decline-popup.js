
function makeDeclineRequest(orderId) {
    if(!isset(orderId)) {
        return;
    }

    var rejectionReasonId = $('.sel-decline-reason').val();

    if(!isset(rejectionReasonId)) {
        return;
    }

    var rejectionText = $('.decline-reason').val();

    $.ajax({
        type: 'PUT',
        url: '/moderation-requests/decline',
        async: false,
        data: {
            orderId: orderId,
            rejectionReasonId: rejectionReasonId,
            rejectionText: rejectionText
        },
        success: function(response) {
            console.log(response);
            response = JSON.parse(response);
            if (response.success) {
                loadOrdersForModeration();
                $(".close-popup-btn").click();
            }
            else {
                for (var i = 0; i < response.errors.length; i++) {
                    toastr.error(response.errors[i], null, errorNotificationOptions);
                }
            }
        },
        error: function(response) {
            console.log(response);
        }
    });
}

function initializeDeclinePopupListeners() {

    $('.btn-decline').on('click', function() {
        var orderId = $(this).data('order-id');
        makeDeclineRequest(orderId);
    });

    $('.btn-close').on('click', function() {
        $(".close-popup-btn").click();
    });

}



$(document).ready(function() {

    initializeDeclinePopupListeners();
});