function addUserGroup(name) {
    $.ajax({
        url: "/user-groups",
        type: "POST",
        data: {
            "name": name
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                updateUserGroupsTable();
            else {
                var message = "";
                for (var i = 0; i < response.errors.length; i++) {
                    message += response.errors[i] + "\n";
                }
                alert(message);
            }
        }
    });
}

function editUserGroup(id, name) {
    $.ajax({
        url: "/user-groups",
        type: "PUT",
        data: {
            "id": id,
            "name": name,
        },
        success: function (response) {
            response = JSON.parse(response);
            if (response.success)
                updateUserGroupsTable();
        }
    });
}


function setPopupListeners() {
    $(".btn-save-user-group").click(function () {
        var id = $(this).data("user-group-id");
        var name = $(".user-group-name-inp").val();

        if (!id)
            addUserGroup(name);
        else
            editUserGroup(id, name);
        $(".popup-frame").hide();
    });

    $(".btn-close-popup").click(function () {
        $(".popup-frame").hide();
    });
}


$(document).ready(function () {
    setPopupListeners();
})