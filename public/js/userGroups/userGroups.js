var selectedElems = {
    "idArray" : []
}

function getUserGroupPopup(userGroupId, popupTitle) {
    $.ajax({
        url: "/user-groups/" + userGroupId,
        type: "GET",
        success: function (popupContent) {
            var popupSelector = "user-groups-popup";
            showCommonPopup(popupSelector, popupTitle, popupContent);
        }
    })
}

function showAddUserGroupPopup() {
    var popupTitle = "Добавление группы";
    getUserGroupPopup(0, popupTitle);
}

function showEditRoutePopup(userGroupId) {
    var popupTitle = "Редактирование группы";
    getUserGroupPopup(userGroupId, popupTitle);
}

function setListenersToUserGroupsTable() {
    $("tbody .cbr").change(function () {
        var tr = $(this).closest("tr");
        var selectedElemId = $(tr).data("user-group-id");
        changeCbxState(tr, selectedElems, selectedElemId);
    });

    // table.find(".dblclickable").dblclick(function () {
    //     var userGroupId = $(this).closest("tr").data("user-group-id");
    //     showEditRoutePopup(userGroupId);
    // });

    table.find(".btn-edit").click(function () {
        var userGroupId = $(this).closest("tr").data("user-group-id");
        showEditRoutePopup(userGroupId);
    });

    cbr_replace();
}

function updateUserGroupsTable() {
    $.ajax({
        url: "/user-groups/update",
        success: function (response) {
            if (isValidJSON(response)) {
                response = JSON.parse(response);
                if (!response.success) {
                    for (var i = 0; i < response.errors.length; i++) {
                        toastr.error(response.errors[i], null, commonNotificationOptions);
                    }
                }
                return;
            }
            clearDatatable(table);
            table.find("tbody").append(response);
            setListenersToUserGroupsTable();
            initializeDatatable(table);
        }
    });
    cancelSelection(table);
    selectedElems.idArray = [];
    deleteBtn.hide();
}

function deleteUserGroups() {
    if (confirm('Удаляем выделенные элементы?')) {
        $.ajax({
            url: "/user-groups",
            type: "DELETE",
            data: {
                "userGroupsId": JSON.stringify(selectedElems.idArray),
            },
            success: function (response) {
                response = JSON.parse(response);
                if (response.success)
                    updateUserGroupsTable();
            }
        })
    }
}


var table, addBtn, deleteBtn;

$(document).ready(function () {
    table = $(".user-groups-table")
    addBtn =  $(".add-user-group-btn")
    deleteBtn = $(".delete-user-group-btn")

    addBtn.click(function () {
        showAddUserGroupPopup();
    });

    deleteBtn.click(function () {
        deleteUserGroups();
    });

    setListenersToUserGroupsTable();

    setSelectAllCbxListener(table);

    initializeDatatable(table);
})